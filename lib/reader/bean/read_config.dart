import 'package:json_annotation/json_annotation.dart';

part 'read_config.g.dart';

@JsonSerializable()
class ReadConfig {
  int id;
  String fileUrl;
  double viewWidth;
  double viewHeight;
  double statusBarHeight;
  String fontFamily;
  int source;
  int taskType;
  int version;
  int fontSize;
  int textLayOutDirection;
  int flipType;
  bool simplifiedChinese;
  int themeIndex;
  double brightness;
  double fontHeight;
  int currentPageIndex;
  int screenDirection;
  bool readFinished;
  double scrollOffset;
  bool sliderStart;
  int currentTaskRecordId;

  ReadConfig(
      {this.fontFamily = 'songti',
      this.fontSize = 1,
      this.textLayOutDirection = 0,
      this.simplifiedChinese = true,
      this.themeIndex = 0,
      this.fontHeight = 1.5,
      this.screenDirection = 1,
      this.fileUrl = '',
      this.id = 0,
      this.source = 0,
      this.taskType = 0,
      this.version = 0,
      this.viewHeight = 0.0,
      this.viewWidth = 0.0,
      this.statusBarHeight = 0.0,
      this.currentPageIndex = 0,
      this.flipType = 1,
      this.scrollOffset = 0.0,
      this.readFinished = false,
      this.sliderStart = false,
      this.currentTaskRecordId = 0,
      this.brightness = 0.3});

  factory ReadConfig.fromJson(Map<String, dynamic> json) {
    return _$ReadConfigFromJson(json);
  }
  Map<String, dynamic> toJson() => _$ReadConfigToJson(this);
}
