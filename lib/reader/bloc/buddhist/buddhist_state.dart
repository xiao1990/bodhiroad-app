import 'package:bodhiroad/reader/bean/buddhist_catalog.dart';
import 'package:bodhiroad/reader/bean/common_book.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class BuddhistState extends Equatable {
  BuddhistState([Iterable props]) : super(props);
}

class InitBuddhistState extends BuddhistState {
  final List<CommonBook> common;
  final BuddhistCatalogBean qldzj;
  final BuddhistCatalogBean brwh;
  InitBuddhistState({this.common = const [], this.qldzj, this.brwh}) : super([common, qldzj, brwh]);
  @override
  String toString() => 'InitBuddhistEvent';
}

class LoadBuddhistState extends BuddhistState {
  final List<CommonBook> common;
  final BuddhistCatalogBean qldzj;
  final BuddhistCatalogBean brwh;

  LoadBuddhistState({this.common, this.qldzj, this.brwh}) : super([common, qldzj, brwh]);

  @override
  String toString() => 'LoadBuddhistEvent';
}
