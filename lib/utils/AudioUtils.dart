import 'package:audiofileplayer/audiofileplayer.dart';
import 'package:audioplayers/audio_cache.dart';
import 'package:audioplayers/audioplayers.dart';
import 'package:bodhiroad/config/CommonConfig.dart';
import 'package:bodhiroad/task/zen/bloc/music_bloc.dart';
import 'package:bodhiroad/task/zen/bloc/music_event.dart';
import 'package:flutter/services.dart';
import 'package:soundpool/soundpool.dart';

class AudioUtils {
  static AudioUtils _audioUtils;
  factory AudioUtils() => _getInstance();
  static AudioPlayer audioPlayer = AudioPlayer();
  static AudioCache assertAudioPlayer = AudioCache();
  static Audio audio;
  static Soundpool pool;
  //  木鱼声
  static int muyu;
  //  引罄声
  static int qing;
  AudioUtils._internal() {}

  static AudioUtils _getInstance() {
    if (_audioUtils == null) {
      return AudioUtils._internal();
    }

    return _audioUtils;
  }

  initAudio(String url) {
    print('初始化播放器');
    audioPlayer?.pause();
    audioPlayer.play(CommonConfig.DOMAIN_PROD + "/" + url);
    audioPlayer.setPlaybackRate();
    audioPlayer.onDurationChanged.listen((duration) {
      MusicBloc().musicBloc.dispatch(MusicLoadedEvent(duration: duration.inSeconds.toDouble()));
    });
    audioPlayer.onAudioPositionChanged.listen((duration) {
      MusicBloc().musicBloc.dispatch(MusicPlayingEvent(position: duration.inSeconds.toDouble()));
    });
    audioPlayer.onPlayerError.listen((error) {
      MusicBloc().musicBloc.dispatch(MusicErrorEvent(error: error));
    });
    audioPlayer.onPlayerCompletion.listen((v) {
      MusicBloc().musicBloc.dispatch(MusicFinishEvent());
    });
//    audioPlayer = Audio.loadFromRemoteUrl(CommonConfig.DOMAIN_PROD + "/" + url, onDuration: (double duration) {
//      print(duration);
//      MusicBloc().musicBloc.dispatch(MusicLoadedEvent(duration: duration));
//    }, onPosition: (double position) {
////      print('开始播放');
//      MusicBloc().musicBloc.dispatch(MusicPlayingEvent(position: position));
////      print(position);
//    }, onError: (String error) {
//      print('播放错误');
//      MusicBloc().musicBloc.dispatch(MusicErrorEvent(error: error));
//      print(error);
//    });
  }

  /// 播放网络音频
  void play(String url) async {
    print('播放网络音频缓冲');
    initAudio(url);
//    audio?.play();
//    audioPlayer.stop();
//    int result = await audioPlayer.play(CommonConfig.DOMAIN_PROD + "/" + url);
//    print('播放网络音频' + result.toString());
  }

  void pause() async {
    audioPlayer?.pause();
//    int result = await audioPlayer.pause();
//    print(result);
  }

  void stop() async {
//    await audioPlayer?.pause();
    await audioPlayer?.stop();
  }

  void resume() async {
    audioPlayer?.resume();

//    int result = await audioPlayer.resume();
//    print(result);
  }

  /// 播放本地音频文件
  playLocal(String url) async {
    int result = await audioPlayer.play(url, isLocal: true);
    if (result == 1) {
      // success
    }
  }

  /// 调整音量
  changeLongMusicVolume(double volume) {
//    print(volume);
    audioPlayer?.setVolume(volume);
  }

  ///  高于30分钟禅修开始提示音
  void chanxiuStartL() {
    this.playAssets('audio/chanxiuStart.mp3');
  }

  ///  禅修完成提示音
  void chanxiuFinish() {
    this.playAssets('audio/zhongsheng.mp3');
  }

  ///  低于30分钟禅修开始提示音/木鱼声
  void chanxiuStartS() async {
    if (pool == null) {
      pool = Soundpool(streamType: StreamType.music);
      await initSoundPool();
    }
    pool.play(muyu);
  }

  ///  念佛完成提示音/引罄
  void nianfoFinish() async {
    if (pool == null) {
      pool = Soundpool(streamType: StreamType.music);
      await initSoundPool();
    }
    pool.play(qing);
  }

  /// 播放assets音频文件
  ///
  /// └── assets
  ///    └── explosion.mp3
  ///
  playAssets(String url) async {
    assertAudioPlayer.play(url);
  }

  /// 音量调节
  changeVolume(double volume) async {
    pool.setVolume(volume: volume, soundId: muyu);
  }

  /// 初始化音乐池
  initSoundPool() async {
    muyu = await rootBundle.load("assets/audio/muyu.mp3").then((ByteData soundData) {
      return pool.load(soundData);
    });
    print(muyu);
    qing = await rootBundle.load("assets/audio/qing.wav").then((ByteData soundData) {
      return pool.load(soundData);
    });
    print(qing);
  }
}
