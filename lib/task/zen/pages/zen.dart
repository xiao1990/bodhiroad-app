import 'dart:ui';

import 'package:bodhiroad/components/bodhiroad_dialog_confirm.dart';
import 'package:bodhiroad/components/circular_slider/src/single_circular_slider.dart';
import 'package:bodhiroad/config/GlobalColorCOnfig.dart';
import 'package:bodhiroad/task/zen/bean/music.dart';
import 'package:bodhiroad/task/zen/bloc/music_bloc.dart';
import 'package:bodhiroad/task/zen/bloc/music_event.dart';
import 'package:bodhiroad/task/zen/bloc/zen_bloc.dart';
import 'package:bodhiroad/task/zen/bloc/zen_event.dart';
import 'package:bodhiroad/task/zen/bloc/zen_state.dart';
import 'package:bodhiroad/task/zen/pages/zen_music.dart';
import 'package:bodhiroad/theme/bloc.dart';
import 'package:common_utils/common_utils.dart';
import 'package:flip_card/flip_card.dart';
import 'package:flustars/flustars.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:global_configuration/global_configuration.dart';

class ZenPage extends StatefulWidget {
  final int taskId;
  final int duration;
  ZenPage({this.duration = 60, this.taskId});
  @override
  _ZenReaderState createState() => _ZenReaderState();
}

class _ZenReaderState extends State<ZenPage> with WidgetsBindingObserver, SingleTickerProviderStateMixin {
  final baseColor = Colors.pinkAccent;
  ZenBloc _zenBloc = ZenBloc();
  MusicBloc _musicBloc = MusicBloc();
  int initTime = 0;
  int endTime = 300;
  int totalTime = 10000;
  bool preventSlide = false;
  TimerUtil timerUtil;
  int zenState = 0;
  int currentId = 0;
  AppThemeBloc appThemeBloc = AppThemeBloc();
  double currentVolume = 0.3;

  @override
  void initState() {
    if (SpUtil.getInt('currentMusicIndex') == 0) {
      SpUtil.putInt('currentMusicIndex', 0);
    }
    totalTime = widget.duration * 60;
    GlobalConfiguration().loadFromMap({"taskId": widget.taskId});
    GlobalConfiguration().updateValue('id', widget.taskId);
    SystemChrome.setEnabledSystemUIOverlays([]);
    _zenBloc.dispatch(ZenChangeTimeEvent(position: widget.duration * 60));
    currentId = SpUtil.getInt('currentMusicId') ?? 0;
    currentVolume = SpUtil.getDouble('currentVolume') ?? 0.3;
    WidgetsBinding.instance.addObserver(this);
    super.initState();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    switch (state) {
      case AppLifecycleState.inactive: // 处于这种状态的应用程序应该假设它们可能在任何时候暂停。
        break;
      case AppLifecycleState.resumed: // 应用程序可见，前台
        break;
      case AppLifecycleState.paused: // 应用程序不可见，后台
//      切换至后台自动停止禅修
        if (zenState == 1) {
          _zenBloc.dispatch(ZenStopEvent());
        }
        break;
      case AppLifecycleState.detached: // 申请将暂时暂停
        break;
      default:
        {}
    }
  }

  @override
  void dispose() {
    SystemChrome.setEnabledSystemUIOverlays([SystemUiOverlay.top]);
    super.dispose();
  }

  void _updateLabels(int init, int end) {
    _zenBloc.dispatch(ZenChangeTimeEvent(position: end * 60));
    totalTime = end * 60;
    // _changetimeBloc.dispatch(end * 60);
  }

  confirmBack(BuildContext context, String description, String title, bool justClose) {
    showDialog(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context) {
          return StatefulBuilder(
            builder: (BuildContext context, setState) {
              return BodhiRoadConfirmDialog(
                title: title,
                confirmText: '退出功课',
                cancelText: justClose ? '关 闭' : '取消',
                justClose: justClose,
                description: Text(description, textScaleFactor: 1.0, style: TextStyle(fontSize: 14, color: GlobalColorConfig.mainTextColor)),
                onConfirm: () {
                  _zenBloc.dispatch(ZenStopEvent());
//                  _musicBloc.dispatch(MusicStopEvent());
                  Navigator.pop(context);
                },
              );
            },
          );
        });
  }

  GlobalKey<FlipCardState> cardKey = GlobalKey<FlipCardState>();
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  @override
  Widget build(BuildContext context) {
    return BlocBuilder(
      bloc: appThemeBloc,
      builder: (context, ThemeChangeState appThemeState) {
        return WillPopScope(
            onWillPop: () async {
              if (zenState == 1) {
                confirmBack(context, '功课进行中，如果返回，功课将终止！\n是否确认终止？', '退出确认', false);
                return false;
              } else {
                return true;
              }
            },
            child: Stack(
              children: <Widget>[
                Container(
                  width: MediaQuery.of(context).size.width,
                  height: MediaQuery.of(context).size.height,
                  color: Colors.white,
                  child: Hero(
                      tag: '3_' + widget.taskId.toString(),
                      flightShuttleBuilder: (flightContext, animation, direction, fromContext, toContext) {
                        return ScaleTransition(
                          scale: animation.drive(Tween(begin: 1.0, end: 1.0)),
                          child: Image.asset(
                            'assets/pusa/${widget.taskId}.png',
                            fit: BoxFit.contain,
                          ),
                        );
                      },
                      child: Image.asset(
                        'assets/zen_common.jpg',
                        fit: BoxFit.cover,
                      )),
                ),
                FlipCard(
                  key: cardKey,
                  front: Scaffold(
                    extendBody: true,
                    appBar: PreferredSize(
                      preferredSize: Size.fromHeight(56),
                      child: Container(
                          child: Column(
                        children: <Widget>[
                          AppBar(
                            title: Text(
                              '禅修',
                              style: TextStyle(color: appThemeState.themeConfig.mainTextColor),
                            ),
                            centerTitle: true,
                            elevation: 0,
                            backgroundColor: Colors.transparent,
//                        leading: Material(
//                          color: Colors.transparent,
//                          child: Ink(
//                            child: InkWell(
//                              borderRadius: BorderRadius.circular(30),
//                              child: Icon(
//                                Icons.arrow_back,
//                                color: appThemeState.themeConfig.mainTextColor,
//                              ),
//                              onTap: () {
//                                Navigator.pop(context);
//                              },
//                            ),
//                          ),
//                        ),
                          ),
                        ],
                      )),
                    ),
                    key: _scaffoldKey,
                    backgroundColor: Colors.transparent,
                    body: Container(
                      width: MediaQuery.of(context).size.width,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Expanded(
                              child: BlocBuilder(
                            bloc: _zenBloc,
                            condition: (previousState, currentState) {
                              if (currentState is ZenStopState) {
                                totalTime = currentState.position;
                              } else if (currentState is ZenUnderwayState) {
                                totalTime = currentState.position;
                              }
                            },
                            builder: (BuildContext context, currentState) {
                              return Container(
                                alignment: Alignment.center,
                                child: GestureDetector(
                                  onTap: () {},
                                  child: Container(
                                    height: ScreenUtil.getInstance().getWidth(600),
                                    width: ScreenUtil.getInstance().getWidth(600),
                                    alignment: Alignment.center,
                                    child: SingleCircularSlider(
                                      360,
                                      (currentState.position / 60).ceil(),
                                      height: ScreenUtil.getInstance().getWidth(550),
                                      width: ScreenUtil.getInstance().getWidth(550),
                                      baseColor: appThemeState.themeConfig.mainTextColor.withOpacity(0.1),
                                      selectionColor: Colors.black38,
                                      handlerColor: Colors.black87,
                                      handlerOutterRadius: 12.0,
                                      showHandlerOutter: false,
                                      onSelectionChange: _updateLabels,
                                      preventSlide: (zenState == 1 || zenState == 2 || widget.duration > 0) ? true : false,
                                      child: Padding(
                                        padding: const EdgeInsets.all(12.0),
                                        child: Center(
                                            child:
                                                Text(currentState.countDownTextStr, style: TextStyle(fontSize: ScreenUtil.getInstance().getSp(72), color: appThemeState.themeConfig.embellishColorA))),
                                      ),
                                    ),
                                  ),
                                ),
                              );
                            },
                          )),
                          Container(
                            height: ScreenUtil.getInstance().screenHeight * 0.25,
                            alignment: Alignment.center,
                            child: Material(
                              color: Colors.transparent,
                              child: BlocBuilder(
                                bloc: _zenBloc,
                                condition: (prev, curt) {
                                  if (curt is ZenUnderwayState) {
                                    zenState = 1;
                                  }
                                  if (curt is ZenStopState) {
                                    totalTime = curt.position;
                                    zenState = 2;
                                  }
                                  if (curt is ZenFinishState) {
                                    totalTime = widget.duration * 60;
                                    zenState = 3;
                                  }
                                },
                                builder: (BuildContext context, zenStatusState) {
                                  return Ink(
                                    child: InkWell(
                                      borderRadius: BorderRadius.circular(ScreenUtil.getInstance().getWidth(100)),
                                      onTap: () {
                                        if (zenState == 0 || zenState == 3) {
                                          _zenBloc.dispatch(ZenStartEvent(totalTime: totalTime, originalTotalTime: widget.duration));
                                        } else if (zenState == 2) {
                                          _zenBloc.dispatch(ZenContinueEvent(totalTime: totalTime));
                                        } else {
                                          _zenBloc.dispatch(ZenStopEvent());
                                        }
                                      },
                                      child: Container(
                                        height: ScreenUtil.getInstance().getWidth(180),
                                        width: ScreenUtil.getInstance().getWidth(180),
                                        decoration: BoxDecoration(
                                            color: Color(0xFFA70400),
                                            boxShadow: [BoxShadow(color: Colors.black26, blurRadius: 15)],
                                            borderRadius: BorderRadius.circular(ScreenUtil.getInstance().getWidth(100))),
                                        alignment: Alignment.center,
                                        child: Text(
                                          zenState == 1 ? '暂 停' : zenState == 2 ? '继 续' : '开 始',
                                          style: TextStyle(color: Colors.white, fontSize: ScreenUtil.getInstance().getSp(42)),
                                        ),
                                      ),
                                    ),
                                  );
                                },
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                  back: ZenMusic(
                    toggleCard: () {
                      cardKey.currentState.toggleCard();
                    },
                  ),
                )
              ],
            ));
      },
    );
  }

  ListView buildListView(List<Music> mlist, ThemeChangeState appThemeState) {
    return ListView.builder(
      itemCount: mlist.length,
      physics: BouncingScrollPhysics(),
      itemBuilder: (BuildContext context, index) {
        return Container(
          // height: ,
          alignment: Alignment.centerLeft,
          child: Material(
            color: Colors.transparent,
            child: Ink(
              height: ScreenUtil.getInstance().getWidth(136),
              width: MediaQuery.of(context).size.width,
              decoration: BoxDecoration(border: Border(bottom: BorderSide(color: appThemeState.themeConfig.mainBorderColor, width: 0.3))),
              child: InkWell(
                child: Row(
                  children: <Widget>[
                    Container(
                      width: ScreenUtil.getInstance().getWidth(136),
                      alignment: Alignment.center,
                      child: Icon(
                        Icons.queue_music,
                        size: 18,
                        color: mlist[index].id == currentId ? GlobalColorConfig.mainRedColor : GlobalColorConfig.mainTextColor,
                      ),
                    ),
                    Expanded(
                      child: Text(
                        mlist[index].musicTitle,
                        style: TextStyle(color: mlist[index].id == currentId ? GlobalColorConfig.mainRedColor : GlobalColorConfig.mainTextColor, fontSize: ScreenUtil.getInstance().getSp(36)),
                      ),
                    ),
                    Container(
                      width: ScreenUtil.getInstance().getWidth(136),
                      alignment: Alignment.center,
                      child: Icon(
                        Icons.album,
                        size: 18,
                        color: mlist[index].id == currentId ? GlobalColorConfig.mainRedColor : GlobalColorConfig.mainTextColor,
                      ),
                    ),
                  ],
                ),
                onTap: () {
                  currentId = mlist[index].id;
                  _musicBloc.dispatch(MusicChangeEvent(currentMusicId: currentId, list: mlist, state: 1, musicUrl: mlist[index].musicUrl));
                },
              ),
            ),
          ),
        );
      },
    );
  }
}

class MusicList extends StatefulWidget {
  @override
  _MusicListState createState() => _MusicListState();
}

class _MusicListState extends State<MusicList> {
  List<String> list = ['森林', '溪水', '风在沙漠', '池塘', '虫鸣', '大海', '篝火', '雷雨', '纯雨声', '寂静的夜晚', '宇宙', '钵'];

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width * 0.8,
      height: MediaQuery.of(context).size.height,
      // color: Colors.red,
      child: ClipRect(
        child: BackdropFilter(
          filter: ImageFilter.blur(sigmaX: 10, sigmaY: 10),
          child: Container(
//            color: Colors.white.withOpacity(0.6),
            child: ListView.builder(
              itemCount: list.length,
              physics: BouncingScrollPhysics(),
              itemBuilder: (BuildContext context, index) {
                return Container(
                  // height: ,
                  alignment: Alignment.centerLeft,
                  child: Material(
                    color: Colors.transparent,
                    child: Ink(
                      height: ScreenUtil.getInstance().getWidth(136),
                      width: MediaQuery.of(context).size.width * 0.8,
                      decoration: BoxDecoration(border: Border(bottom: BorderSide(color: GlobalColorConfig.mainBorderColor, width: 0.3))),
                      child: InkWell(
                        child: Row(
                          children: <Widget>[
                            Container(
                              width: ScreenUtil.getInstance().getWidth(136),
                              alignment: Alignment.center,
                              child: Icon(
                                Icons.queue_music,
                                size: 18,
                              ),
                            ),
                            Expanded(
                              child: Text(
                                list[index],
                                style: TextStyle(color: GlobalColorConfig.mainTextColor, fontSize: ScreenUtil.getInstance().getSp(36)),
                              ),
                            ),
                            Container(
                              width: ScreenUtil.getInstance().getWidth(136),
                              alignment: Alignment.center,
                              child: Icon(
                                Icons.play_circle_outline,
                                size: 18,
                                color: index == 5 ? GlobalColorConfig.mainRedColor : GlobalColorConfig.mainTextColor,
                              ),
                            ),
                          ],
                        ),
                        onTap: () {},
                      ),
                    ),
                  ),
                );
              },
            ),
          ),
        ),
      ),
    );
  }
}
