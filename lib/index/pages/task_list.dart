import 'dart:math' as math;
import 'dart:ui';

import 'package:bodhiroad/common/FontFamilyName.dart';
import 'package:bodhiroad/common/calendar_day.dart';
import 'package:bodhiroad/common/update_title_bloc.dart';
import 'package:bodhiroad/components/bodhiroad_dialog.dart';
import 'package:bodhiroad/components/bodhiroad_dialog_confirm.dart';
import 'package:bodhiroad/config/CommonConfig.dart';
import 'package:bodhiroad/config/GlobalColorCOnfig.dart';
import 'package:bodhiroad/config/ThemeConfig.dart';
import 'package:bodhiroad/index/bean/taskStatisticSimple.dart';
import 'package:bodhiroad/index/bloc/calendar/calendar_bloc.dart';
import 'package:bodhiroad/index/bloc/hide_bloc.dart';
import 'package:bodhiroad/index/bloc/task_slide_bloc.dart';
import 'package:bodhiroad/task/read/bean/task.dart';
import 'package:bodhiroad/task/read/bean/task_classify.dart';
import 'package:bodhiroad/task/read/bloc/task_bloc.dart';
import 'package:bodhiroad/task/read/bloc/task_event.dart';
import 'package:bodhiroad/task/read/bloc/task_state.dart';
import 'package:bodhiroad/theme/bloc.dart';
import 'package:bodhiroad/theme/theme_bloc.dart';
import 'package:bodhiroad/utils/GlobalConfigUtil.dart';
import 'package:bodhiroad/utils/Route.dart';
import 'package:bodhiroad/utils/ToastUtils.dart';
import 'package:bodhiroad/utils/parameter_convert_cn.dart';
import 'package:dotted_border/dotted_border.dart';
import 'package:fluro/fluro.dart';
import 'package:flustars/flustars.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:lunar_calendar_converter/lunar_solar_converter.dart';
import 'package:page_indicator/page_indicator.dart';

class TaskList extends StatefulWidget {
  final Widget leftAction;
  final Widget rightAction;
  const TaskList({this.leftAction, this.rightAction});
  @override
  _TaskListState createState() => _TaskListState();
}

class _TaskListState extends State<TaskList> with TickerProviderStateMixin {
  UpdateTitleBloc _updateTitleBloc = UpdateTitleBloc();
  List<TaskClassify> taskList = [];
  List<TaskStatisticSimpleBean> today = [];
  List<TaskStatisticSimpleBean> total = [];
  TaskBloc _taskBloc = TaskBloc();
  AnimationController _sizeAnimationController;
  Animation<double> sizeAnimation;
  HideBloc hideBloc = HideBloc();
  double lastPosition = 0.0;
  ScrollController scrollController;

  @override
  void initState() {
    scrollController = ScrollController(initialScrollOffset: 0.0);
    _sizeAnimationController = AnimationController(duration: const Duration(milliseconds: 250), vsync: this);
    sizeAnimation = Tween(begin: 0.0, end: 1.0).animate(_sizeAnimationController);
    super.initState();
    print('task_list 初始化');
    _taskBloc.dispatch(UserTaskEvent(forceUpdate: false));
  }

  @override
  void dispose() {
    _sizeAnimationController.dispose();
    scrollController.dispose();
    super.dispose();
  }

  List<Widget> getVerticalText(String text) {
    text = text.replaceAll("《", '').replaceAll("》", '');
    text = text.substring(0, math.min(18, text.length));

    int cols = (text.length / 9).ceil();
    List<Widget> list = [];
    for (int i = 0; i < cols; i++) {
      String text2 = text.substring(i * 9, math.min(i * 9 + 9 - 1, text.length - 1) + 1).replaceAll('', '\n');
      list.add(Container(
        width: ScreenUtil.getInstance().getWidth(23),
        height: ScreenUtil.getInstance().screenHeight * 0.5,
        alignment: Alignment.topCenter,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
              width: ScreenUtil.getInstance().getWidth(23),
//              decoration: BoxDecoration(border: Border(left: BorderSide(color: Colors.black26, width: 0.5))),
              alignment: i == 0 ? Alignment.topCenter : Alignment.topRight,
              child: Text(
                text2,
                style: TextStyle(
                    decoration: TextDecoration.none,
                    color: Colors.black87,
                    fontSize: GlobalConfigUtil.getCoverTitleFontSize(),
                    shadows: [Shadow(color: Colors.black26, blurRadius: 6)],
                    fontWeight: FontWeight.normal,
                    fontFamily: FontFamilyName.SONGTI),
              ),
            )
          ],
        ),
      ));
    }
    return list.reversed.toList();
  }

  selectChange(date) {
    _updateTitleBloc.dispatch(date);
    if (date.foposa != null && date.foposa != '') {
      if (sizeAnimation.value > 0) {
        _sizeAnimationController.reverse();
      } else {
        _sizeAnimationController.forward();
      }
    } else if (sizeAnimation.value > 0) {
      _sizeAnimationController.reverse();
    }
  }

  GlobalKey _scaffoldKey = GlobalKey();
  @override
  Widget build(BuildContext context) {
    return BlocBuilder(
        bloc: BlocProvider.of<AppThemeBloc>(context),
        builder: (BuildContext context, ThemeChangeState appThemeState) {
          return NotificationListener<ScrollNotification>(
            child: Scaffold(
                backgroundColor: Colors.white,
                key: _scaffoldKey,
                body: BlocBuilder(
                    bloc: _taskBloc,
                    condition: (previousState, currentState) {
                      if (currentState is UserTaskState) {
                        today = currentState.today;
                        total = currentState.total;
                        return true;
                      } else {
                        return false;
                      }
                    },
                    builder: (context, state) {
                      return NestedScrollView(
                        controller: scrollController,
                        headerSliverBuilder: (context, isScroll) {
                          return [
                            SliverPadding(
                                padding: EdgeInsets.only(top: 0),
                                sliver: SliverAppBar(
                                  backgroundColor: Colors.white,
                                  centerTitle: true,
                                  title: Text('我的功课',
                                      textScaleFactor: 1.0, style: TextStyle(fontFamily: 'songti', color: appThemeState.themeConfig.mainTextColor, fontSize: GlobalConfigUtil.getTitleFontSize())),
                                  pinned: true,
                                  floating: true,
                                  elevation: 4,
                                  leading: widget.leftAction,
                                  actions: <Widget>[widget.rightAction],
                                  forceElevated: isScroll,
                                  bottom: PreferredSize(
                                    preferredSize: Size.fromHeight(ScreenUtil.getInstance().getWidth(180)),
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.center,
                                      children: <Widget>[
                                        Container(
                                          width: MediaQuery.of(context).size.width,
                                          height: ScreenUtil.getInstance().getWidth(180),
                                          child: ListView.builder(
                                            itemBuilder: (context, index) => Container(
                                              width: ScreenUtil.getInstance().getWidth(360),
                                              height: ScreenUtil.getInstance().getWidth(200),
                                              child: Row(
                                                mainAxisAlignment: MainAxisAlignment.center,
                                                crossAxisAlignment: CrossAxisAlignment.center,
                                                children: <Widget>[
                                                  Container(
                                                    width: ScreenUtil.getInstance().getWidth(280),
                                                    height: ScreenUtil.getInstance().getWidth(130),
                                                    decoration: BoxDecoration(
                                                        color: Colors.white,
                                                        boxShadow: [BoxShadow(color: Color(0xFFF2F2F2), blurRadius: 12, spreadRadius: 2)],
                                                        borderRadius: BorderRadius.circular(ScreenUtil.getInstance().getWidth(10))),
                                                    child: Row(
                                                      children: <Widget>[
                                                        Container(
                                                          width: ScreenUtil.getInstance().getWidth(80),
                                                          child: Image.asset(
                                                            'assets/icons/${index + 1}.png',
                                                            height: ScreenUtil.getInstance().getWidth(54),
                                                            color: Color(0xFFA70400),
                                                          ),
                                                        ),
                                                        Expanded(
                                                          child: Column(
                                                            mainAxisAlignment: MainAxisAlignment.center,
                                                            crossAxisAlignment: CrossAxisAlignment.start,
                                                            children: <Widget>[
                                                              Text(
                                                                '今日' +
                                                                    (index == 2 ? (today[index].total / 60).ceil().toString() : today[index].total.toString()) +
                                                                    CommonConfig.taskCountUnit[index + 1],
                                                                style: TextStyle(fontSize: ScreenUtil.getInstance().getSp(30)),
                                                              ),
                                                              Text(
                                                                '累计' +
                                                                    (index == 2 ? (total[index].total / 60).ceil().toString() : total[index].total.toString()) +
                                                                    CommonConfig.taskCountUnit[index + 1],
                                                                style: TextStyle(fontSize: ScreenUtil.getInstance().getSp(25), color: Colors.black38),
                                                              ),
                                                            ],
                                                          ),
                                                        )
                                                      ],
                                                    ),
                                                  )
                                                ],
                                              ),
                                            ),
                                            itemCount: today.length,
                                            physics: BouncingScrollPhysics(),
                                            scrollDirection: Axis.horizontal,
                                          ),
                                        )
                                      ],
                                    ),
                                  ),
                                )),
                          ];
                        },
                        body: Container(
                            decoration: BoxDecoration(
                                color: Colors.white,
                                image: DecorationImage(
                                  fit: BoxFit.cover,
                                  image: AssetImage('assets/texture/bgt2.jpg'),
                                )),
                            child: Column(
                              children: <Widget>[
                                SizeTransition(
                                  axis: Axis.vertical,
                                  sizeFactor: sizeAnimation,
                                  child: BlocBuilder(
                                      bloc: _updateTitleBloc,
                                      builder: (context, date) {
                                        return Container(
                                          height: ScreenUtil.getInstance().getWidth(120),
                                          color: GlobalColorConfig.mainBackgroundColor,
                                          alignment: Alignment.center,
                                          child: Text(
                                            date is CalendarDay && date.foposa != null ? date.foposa : '',
                                            style: TextStyle(color: GlobalColorConfig.mainforegroundColor, fontSize: ScreenUtil.getInstance().getSp(42)),
                                          ),
                                        );
                                      }),
                                ),
                                MultiBlocProvider(
                                  providers: [
                                    BlocProvider<UpdateTitleBloc>(
                                      builder: (BuildContext context) => _updateTitleBloc,
                                    ),
                                    BlocProvider<TaskBloc>(
                                      builder: (BuildContext context) => _taskBloc,
                                    )
                                  ],
                                  child: TaskListHead(),
                                ),
                                Expanded(
                                    child: Container(
                                        child: BlocBuilder(
                                            bloc: _taskBloc,
                                            condition: (previousState, currentState) {
                                              if (currentState is UserTaskState) {
                                                taskList = currentState.list;
                                                return true;
                                              } else {
                                                return false;
                                              }
                                            },
                                            builder: (context, state) {
                                              return taskList.length > 0
                                                  ? Container(
                                                      width: ScreenUtil.getInstance().screenWidth,
//                                                  padding: EdgeInsets.only(bottom: ScreenUtil.getInstance().getWidth(140)),
                                                      child: ListView.builder(
                                                          itemCount: taskList.length,
                                                          physics: BouncingScrollPhysics(),
                                                          padding: EdgeInsets.all(0),
                                                          itemBuilder: (context, index) {
                                                            return Container(
                                                              height: ScreenUtil.getInstance().getWidth(560.0 + (index == taskList.length - 1 ? 160 : 0)),
                                                              width: ScreenUtil.getInstance().screenWidth,
                                                              child: Column(
                                                                children: <Widget>[
                                                                  Container(
                                                                    height: ScreenUtil.getInstance().getWidth(120),
                                                                    padding: EdgeInsets.symmetric(horizontal: 15),
                                                                    alignment: Alignment.centerLeft,
                                                                    child: Row(
//                                      crossAxisAlignment: CrossAxisAlignment.end,
                                                                      children: <Widget>[
                                                                        Container(
                                                                          height: ScreenUtil.getInstance().getWidth(60),
                                                                          width: ScreenUtil.getInstance().getWidth(60),
                                                                          alignment: Alignment.center,
                                                                          decoration:
                                                                              BoxDecoration(color: Color(0xFFA70400), borderRadius: BorderRadius.circular(ScreenUtil.getInstance().getWidth(45))),
                                                                          margin: EdgeInsets.only(right: ScreenUtil.getInstance().getWidth(10)),
                                                                          child: Text(
                                                                            taskList[index].taskTypeName.substring(0, 1),
                                                                            style: TextStyle(
                                                                                fontSize: GlobalConfigUtil.getSubTitleFontSize(),
                                                                                fontWeight: FontWeight.normal,
                                                                                color: Colors.white,
                                                                                fontFamily: FontFamilyName.SONGTI),
                                                                          ),
                                                                        ),
                                                                        Text(
                                                                          taskList[index].taskTypeName.substring(1),
                                                                          style: TextStyle(
                                                                              fontSize: GlobalConfigUtil.getSubTitleFontSize(),
                                                                              fontWeight: FontWeight.bold,
                                                                              color: Colors.black87,
                                                                              fontFamily: FontFamilyName.SONGTI),
                                                                        )
                                                                      ],
                                                                    ),
                                                                  ),
                                                                  Expanded(
                                                                      child: ListView.builder(
                                                                          itemCount: taskList[index].list.length,
                                                                          scrollDirection: Axis.horizontal,
                                                                          physics: BouncingScrollPhysics(),
                                                                          itemBuilder: (context, idx) {
                                                                            return Container(
                                                                              width: ScreenUtil.getInstance().getWidth(300),
                                                                              height: ScreenUtil.getInstance().getWidth(450),
                                                                              alignment: Alignment.center,
                                                                              child: Column(
                                                                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                                                children: <Widget>[
                                                                                  GestureDetector(
                                                                                      onTap: () {
                                                                                        Navigator.of(context).push(PageRouteBuilder(
                                                                                          opaque: false,
                                                                                          pageBuilder: (BuildContext context, ani1, ani2) {
                                                                                            return TaskSlideView(
                                                                                              currentIndex: idx,
                                                                                              taskList: taskList[index].list,
                                                                                            );
                                                                                          },
                                                                                        ));
                                                                                      },
                                                                                      child: Container(
                                                                                        width: ScreenUtil.getInstance().getWidth(230),
                                                                                        height: ScreenUtil.getInstance().getWidth(325),
                                                                                        decoration: BoxDecoration(
                                                                                          border: Border.all(color: Color(0xFFdbd8d9), width: 0.5),
                                                                                          boxShadow: [BoxShadow(color: Colors.black12, blurRadius: 8)],
                                                                                          color: Colors.white,
//                                                                                            image: DecorationImage(
//                                                                                                fit: BoxFit.cover,
//                                                                                                image: AssetImage(
//                                                                                                  taskList[index].list[idx].taskTypeId == 2
//                                                                                                      ? 'assets/task_cover/${taskList[index].list[idx].taskId}.jpg'
//                                                                                                      : 'assets/task_cover/${taskList[index].list[idx].taskTypeId}.jpg',
//                                                                                                ))
                                                                                        ),
                                                                                        alignment: Alignment.center,
                                                                                        child: Stack(
                                                                                          alignment: Alignment.center,
                                                                                          children: <Widget>[
                                                                                            FadeInImage(
                                                                                                width: ScreenUtil.getInstance().getWidth(230),
                                                                                                height: ScreenUtil.getInstance().getWidth(325),
                                                                                                placeholder: AssetImage('assets/pusa/common_bg.jpg'),
                                                                                                fit: BoxFit.cover,
                                                                                                image: AssetImage(
                                                                                                  taskList[index].list[idx].taskTypeId == 2
                                                                                                      ? 'assets/task_cover/${taskList[index].list[idx].taskId}.jpg'
                                                                                                      : 'assets/task_cover/${taskList[index].list[idx].taskTypeId}.jpg',
                                                                                                )),
                                                                                            Offstage(
                                                                                              offstage: taskList[index].list[idx].taskTypeId == 2 ||
                                                                                                  taskList[index].list[idx].taskTypeId == 3 ||
                                                                                                  taskList[index].list[idx].taskTypeId == 5,
                                                                                              child: Container(
                                                                                                width: ScreenUtil.getInstance().getWidth(46),
                                                                                                child: Row(
                                                                                                  mainAxisAlignment: MainAxisAlignment.center,
                                                                                                  children: getVerticalText(taskList[index].list[idx].taskName),
                                                                                                ),
                                                                                              ),
                                                                                            )
                                                                                          ],
                                                                                        ),
                                                                                      )),
                                                                                  GestureDetector(
                                                                                    onTap: () {
                                                                                      Task task = taskList[index].list[idx];
                                                                                      String url = task.taskContentUrl?.replaceAll('/', '%2F');
                                                                                      task.taskContentUrl = url;
                                                                                      String bookName = ParameterConvertUtils.convertCN(task.taskName);
                                                                                      if (task.taskTypeId == 1) {
                                                                                        String url = task.taskContentUrl.replaceAll('/', '%2F');
                                                                                        String bookName = ParameterConvertUtils.convertCN(task.taskName);
                                                                                        Routes.router.navigateTo(context,
                                                                                            'bookView?fileUrl=${url}&source=0&id=${task.taskId}&taskType=${task.taskTypeId}&bookName=${bookName}&version=${task.version}',
                                                                                            transitionDuration: Duration(milliseconds: 600));
                                                                                      } else if (task.taskTypeId == 2) {
                                                                                        Routes.router.navigateTo(
                                                                                          context,
                                                                                          'sukhavati?count=${task.count}&taskId=${task.taskId}&taskName=${bookName}',
                                                                                        );
                                                                                      } else if (task.taskTypeId == 3) {
                                                                                        Routes.router.navigateTo(
                                                                                          context,
                                                                                          'zen?duration=${task.count}&taskId=${task.taskId}',
                                                                                        );
                                                                                      } else if (task.taskTypeId > 3) {
                                                                                        String url = task.taskContentUrl.replaceAll('/', '%2F');
                                                                                        String bookName = ParameterConvertUtils.convertCN(task.taskName);
                                                                                        Routes.router.navigateTo(
                                                                                          context,
                                                                                          'bookView?fileUrl=${url}&source=0&id=${task.taskId}&taskType=${task.taskTypeId}&bookName=${bookName}&version=${task.version}',
                                                                                        );
                                                                                      }
                                                                                    },
                                                                                    child: Container(
                                                                                      height: ScreenUtil.getInstance().getWidth(100),
                                                                                      width: ScreenUtil.getInstance().getWidth(230),
                                                                                      padding: EdgeInsets.symmetric(horizontal: ScreenUtil.getInstance().getWidth(25)),
                                                                                      child: Text(
                                                                                        taskList[index].list[idx].taskName,
                                                                                        style: TextStyle(fontSize: GlobalConfigUtil.getTabMainBodyFontSize()),
                                                                                        maxLines: 2,
                                                                                        overflow: TextOverflow.ellipsis,
                                                                                      ),
                                                                                    ),
                                                                                  ),
                                                                                ],
                                                                              ),
                                                                            );
                                                                          })),
                                                                  Offstage(
                                                                    offstage: index < taskList.length - 1,
                                                                    child: Container(
                                                                      height: ScreenUtil.getInstance().getWidth(160),
                                                                    ),
                                                                  )
                                                                ],
                                                              ),
                                                            );
                                                          }),
                                                    )
                                                  : state is UserTaskState
                                                      ? Center(child: Text('您还没有添加任何功课！'))
                                                      : Center(
                                                          child: SpinKitCircle(
                                                            size: 20,
                                                            color: Colors.black54,
                                                          ),
                                                        );
                                            })))
                              ],
                            )),
                      );
                    })),
            onNotification: (notification) {
              if (notification.metrics.pixels <= 0 || notification.metrics.axisDirection != AxisDirection.down || notification.depth > 1) {
                return false;
              }

              int currentPosition = (notification.metrics.pixels / 20).floor() * 10;
              if (currentPosition > lastPosition) {
                hideBloc.dispatch(0.0);
                lastPosition = currentPosition.toDouble();
              } else if (currentPosition < lastPosition) {
                hideBloc.dispatch(1.0);
                lastPosition = currentPosition.toDouble();
              }

              return true;
            },
          );
        });
  }

  @override
  // TODO: implement wantKeepAlive
  bool get wantKeepAlive => true;
}

class TaskItem extends StatelessWidget {
  final int itemIndex;
  final Task task;
  final GlobalKey key;
  const TaskItem({@required this.itemIndex, @required this.task, this.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: ScreenUtil.getInstance().getWidth(500),
      width: ScreenUtil.getInstance().getWidth(300),
      decoration: BoxDecoration(color: GlobalColorConfig.mainBackgroundColor, border: Border.all(color: Colors.black12, width: 0.5)),
      child: Stack(
        children: <Widget>[
          Container(
              height: ScreenUtil.getInstance().getWidth(500),
              width: ScreenUtil.getInstance().getWidth(300),
              child: Container(
                height: ScreenUtil.getInstance().getWidth(800),
                width: ScreenUtil.getInstance().getWidth(300),
                child: Stack(
                  children: <Widget>[
                    GestureDetector(
                      onTap: () {
                        Routes.router.navigateTo(context, 'taskView?currentIndex=${itemIndex}', transitionDuration: Duration(milliseconds: 250));
                      },
                      child: Container(
                          width: ScreenUtil.getInstance().getWidth(300),
                          height: ScreenUtil.getInstance().getWidth(380),
                          child: Stack(
                            children: <Widget>[
                              Opacity(
                                opacity: 1,
                                child: FadeInImage(
                                  width: ScreenUtil.getInstance().getWidth(300),
                                  height: ScreenUtil.getInstance().getWidth(380),
                                  image: AssetImage('assets/cover5.jpg'),
                                  placeholder: AssetImage('assets/cover5.jpg'),
                                ),
                              ),
                              Container(
                                width: ScreenUtil.getInstance().getWidth(300),
                                height: ScreenUtil.getInstance().getWidth(380),
                                padding: EdgeInsets.only(right: ScreenUtil.getInstance().getWidth(28), top: ScreenUtil.getInstance().getWidth(36)),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.end,
                                  children: []..addAll(List.generate((task.taskName.length / 5).ceil(), (index) {
                                      return Container(
                                        width: 18,
                                        alignment: Alignment.topCenter,
                                        child: Text(
                                          task.taskName.substring(5 * index, 5 * index + 5 > task.taskName.length - 1 ? task.taskName.length : 5 * index + 5),
                                          textScaleFactor: 1.0,
                                          style: TextStyle(fontWeight: FontWeight.bold, color: GlobalColorConfig.mainTextColor),
                                        ),
                                      );
                                    }).toList().reversed),
                                ),
                              )
                            ],
                          )),
                    )
                  ],
                ),
              )),
          Positioned(
            bottom: 0,
            child: Container(
                height: ScreenUtil.getInstance().getWidth(120),
                width: ScreenUtil.getInstance().getWidth(300),
                padding: EdgeInsets.symmetric(horizontal: 4),
                decoration: BoxDecoration(color: GlobalColorConfig.mainBackgroundColor),
                child: Stack(
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Expanded(
                          child: ButtonTheme(
                            minWidth: 60,
                            child: FlatButton(
                              padding: EdgeInsets.all(0),
                              onPressed: () {
                                String bookName = ParameterConvertUtils.convertCN(task.taskName);
                                if (task.taskTypeId == 1) {
                                  String url = task.taskContentUrl.replaceAll('/', '%2F');
                                  Routes.router.navigateTo(
                                    context,
                                    'bookView?fileUrl=${url}&source=0&id=${task.taskId}&taskType=${task.taskTypeId}&bookName=${bookName}&version=${task.version}',
                                  );
                                } else if (task.taskTypeId == 2) {
                                  Routes.router.navigateTo(
                                    context,
                                    'sukhavati?count=${task.count}&taskId=${task.taskId}&taskName=${bookName}',
                                  );
                                } else if (task.taskTypeId == 3) {
                                  Routes.router.navigateTo(
                                    context,
                                    'zen?duration=${task.count}&taskId=${task.taskId}',
                                  );
                                } else if (task.taskTypeId > 3) {
                                  String url = task.taskContentUrl.replaceAll('/', '%2F');
                                  Routes.router.navigateTo(
                                    context,
                                    'bookView?fileUrl=${url}&source=0&id=${task.taskId}&taskType=${task.taskTypeId}&bookName=${bookName}&version=${task.version}',
                                  );
                                }
                              },
                              child: Text(
                                CommonConfig.taskCatalogList[task.taskTypeId - 1].replaceAll('', '   '),
                                textScaleFactor: 1.0,
                                style: TextStyle(fontSize: 14, color: GlobalColorConfig.mainforegroundColor),
                              ),
                            ),
                          ),
                        ),
                      ],
                    )
                  ],
                )),
          )
        ],
      ),
//        )
    );
  }
}

/// 空列表
class EmptyList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: EdgeInsets.symmetric(horizontal: 15),
        child: DottedBorder(
          color: GlobalColorConfig.mainBorderColor,
          gap: 6,
          strokeWidth: 2,
          child: Container(
              height: 150,
              decoration: BoxDecoration(
                  color: Colors.white.withOpacity(0.2), borderRadius: BorderRadius.circular(15), border: Border.all(color: GlobalColorConfig.mainBorderColor, width: 0.5, style: BorderStyle.solid)),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text('菩萨，这里一片空白'),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text('点击右下角'),
                      Container(
                        height: ScreenUtil.getInstance().getWidth(60),
                        width: ScreenUtil.getInstance().getWidth(60),
                        decoration: BoxDecoration(color: GlobalColorConfig.buttonColorA, borderRadius: BorderRadius.circular(20)),
                        child: Icon(Icons.add, size: 16, color: GlobalColorConfig.mainforegroundColor),
                      ),
                      Text('可添加功课'),
                    ],
                  )
                ],
              )),
        ));
  }
}

/// 列表头部
class TaskListHead extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final UpdateTitleBloc _updateTitleBloc = BlocProvider.of<UpdateTitleBloc>(context);
    final TaskBloc _taskBloc = BlocProvider.of<TaskBloc>(context);
    final CalendarBloc calendarBloc = CalendarBloc();
    Solar solar = Solar(solarYear: DateTime.now().year, solarMonth: DateTime.now().month, solarDay: DateTime.now().day);
    Lunar lunar = LunarSolarConverter.solarToLunar(solar);

    return BlocBuilder(
      bloc: _updateTitleBloc,
      builder: (context, date) {
        return Container(
            height: ScreenUtil.getInstance().getWidth(90),
            padding: EdgeInsets.symmetric(horizontal: ScreenUtil.getInstance().getWidth(40)),
            alignment: Alignment.center,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Text(
                      '我的功课',
                      textScaleFactor: 1.0,
                      style: TextStyle(fontSize: ScreenUtil.getInstance().getSp(36)),
                    ),
                    BlocBuilder(
                        bloc: _taskBloc,
                        condition: (prevState, curtState) {
                          if (curtState is UserTaskQueryingState || curtState is UserTaskState) {
                            return true;
                          } else {
                            return false;
                          }
                        },
                        builder: (context, taskState) {
                          return Container(
                            width: ScreenUtil.getInstance().getWidth(75),
                            child: taskState is UserTaskQueryingState
                                ? SpinKitThreeBounce(
                                    color: GlobalColorConfig.buttonColorA,
                                    size: ScreenUtil.getInstance().getWidth(25),
                                  )
                                : ButtonTheme(
                                    minWidth: ScreenUtil.getInstance().getWidth(75),
                                    padding: EdgeInsets.symmetric(horizontal: 0),
                                    child: IconButton(
                                      padding: EdgeInsets.symmetric(horizontal: 0),
                                      icon: Icon(
                                        Icons.refresh,
                                        size: ScreenUtil.getInstance().getWidth(40),
                                      ),
                                      onPressed: () {
                                        _taskBloc.dispatch(UserTaskEvent(forceUpdate: true));
                                      },
                                    ),
                                  ),
                          );
                        })
                  ],
                ),
                Text(
                  DateTime.now().month.toString() + '月' + DateTime.now().day.toString() + '日·' + lunar.toString(),
                  textScaleFactor: 1.0,
                  style: TextStyle(fontSize: ScreenUtil.getInstance().getSp(36)),
                )
              ],
            ));
      },
    );
  }
}

class TaskSlideView extends StatefulWidget {
  final int currentIndex;
  final TaskBloc taskBloc;
  final List<Task> taskList;
  TaskSlideView({this.currentIndex, this.taskBloc, this.taskList});
  @override
  _TaskSlideViewState createState() => _TaskSlideViewState();
}

class _TaskSlideViewState extends State<TaskSlideView> {
  PageController _pageController;
  TaskSlideBloc _taskSlideBloc = TaskSlideBloc();
  TaskBloc _taskBloc = TaskBloc();
  int i = 0;
  double _currentPage = 0.0;
  AppThemeBloc appThemeBloc = AppThemeBloc();
  @override
  void initState() {
    _taskSlideBloc.dispatch(_currentPage);
    _pageController = PageController(viewportFraction: 0.87, initialPage: widget.currentIndex);
    super.initState();
//    WidgetsBinding widgetsBinding = WidgetsBinding.instance;
//    widgetsBinding.addPostFrameCallback((callback) {
//      _pageController.jumpToPage(widget.currentIndex);
//    });
  }

  @override
  void dispose() {
    _pageController.dispose();
    super.dispose();
  }

  //  报数弹层
  showCustomDialog(BuildContext context, Task task, int itemIndex) {
    showDialog(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context) {
          return StatefulBuilder(
            builder: (BuildContext context, setState) {
              return BodhiRoadDialog(
                unit: Text(CommonConfig.taskCountUnit[task.taskTypeId]),
                title: Text('报数', textScaleFactor: 1.0, style: TextStyle(fontSize: 20, color: GlobalColorConfig.mainTextColor)),
                description: Text(task.taskName, textScaleFactor: 1.0, style: TextStyle(fontSize: 14, color: GlobalColorConfig.mainTextColor)),
                onConfirm: (text) {
                  int count = int.parse(text);
                  if (task.taskTypeId == 3) {
                    count = count * 60;
                  }
                  _taskBloc.dispatch(ReportTaskEvent(count: count, taskId: task.taskId, taskType: task.taskTypeId, itemIndex: itemIndex));
                  Navigator.pop(context);
                },
              );
            },
          );
        });
  }

//  删除弹层
  showDeleteDialog(BuildContext context, Task task, ThemeConfig themeConfig) {
    showDialog(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context) {
          return StatefulBuilder(
            builder: (BuildContext context, setState) {
              return BlocBuilder(
                bloc: _taskBloc,
                condition: (prev, curt) {
                  if (curt is DeleteTaskSuccessState) {
                    Navigator.pop(context);
                  }
                  return false;
                },
                builder: (context, state) {
                  return BodhiRoadConfirmDialog(
                    confirmText: '删除',
                    cancelText: '取消',
                    description: Text('删除 ${task.taskName} ?', style: TextStyle(fontSize: 14, color: GlobalColorConfig.mainTextColor)),
                    onConfirm: () {
                      _taskBloc.dispatch(DeleteTaskEvent(taskId: task.id));
                    },
                  );
                },
              );
            },
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder(
      bloc: appThemeBloc,
      builder: (context, ThemeChangeState appThemeState) {
        return BlocListener(
            bloc: _taskBloc,
            listener: (context, currentState) {
              if (currentState is ReportTaskSuccessState) {
                ToastUtils.success('报数成功！');
              } else if (currentState is DeleteTaskSuccessState) {
                ToastUtils.success('删除成功！');
              }
            },
            child: Scaffold(
              resizeToAvoidBottomPadding: false,
              backgroundColor: Colors.transparent,
              body: GestureDetector(
                child: Container(
                  height: MediaQuery.of(context).size.height,
                  width: MediaQuery.of(context).size.width,
                  child: BackdropFilter(
                      filter: ImageFilter.blur(sigmaY: 20, sigmaX: 20),
                      child: Container(
                        child: LayoutBuilder(
                          builder: (context, constraints) => PageIndicatorContainer(
                              align: IndicatorAlign.top,
                              length: widget.taskList.length,
                              indicatorColor: GlobalColorConfig.mainTextColor.withOpacity(0.05),
                              indicatorSelectorColor: GlobalColorConfig.mainRedColor,
                              indicatorSpace: 4,
                              padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.1 * 0.25),
                              shape: IndicatorShape.circle(size: 4),
                              pageView: PageView.builder(
                                  itemCount: widget.taskList.length,
                                  physics: const BouncingScrollPhysics(),
                                  controller: _pageController,
                                  itemBuilder: ((context, index) => GestureDetector(
                                        child: Container(
                                            width: MediaQuery.of(context).size.width * 0.87 - ScreenUtil.getInstance().getWidth(40),
                                            height: MediaQuery.of(context).size.height * 0.9,
                                            child: Row(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
                                              Container(
                                                  width: MediaQuery.of(context).size.width * 0.87 - ScreenUtil.getInstance().getWidth(40),
                                                  height: MediaQuery.of(context).size.height * 0.9,
                                                  decoration: BoxDecoration(
                                                      color: appThemeState.themeConfig.sectionBgColor,
                                                      boxShadow: [BoxShadow(color: Colors.black26, blurRadius: 15)],
//                                                          image: DecorationImage(
//                                                              fit: BoxFit.fitWidth, alignment: Alignment.bottomCenter, image: AssetImage('assets/pusa/common_bg.jpg')),
                                                      borderRadius: BorderRadius.circular(15)),
                                                  child: Column(
                                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                    children: <Widget>[
//                                                    删除按钮
                                                      Container(
                                                        height: ScreenUtil.getInstance().getWidth(160),
                                                        alignment: Alignment.topLeft,
                                                        child: IconButton(
                                                            icon: Icon(
                                                              Icons.close,
                                                              color: appThemeState.themeConfig.subTitleTextColor,
                                                            ),
                                                            onPressed: () {
                                                              showDeleteDialog(context, widget.taskList[index], appThemeState.themeConfig);
                                                            }),
                                                      ),
//                                                    经文名称和作者
                                                      Expanded(
                                                        child: BodyCover(task: widget.taskList[index]),
                                                      ),
                                                      Container(
                                                        width: MediaQuery.of(context).size.width * 0.87 - ScreenUtil.getInstance().getWidth(40),
                                                        height: ScreenUtil.getInstance().getWidth(330),
                                                        child: Column(
                                                          children: <Widget>[
                                                            Container(
                                                              height: ScreenUtil.getInstance().getWidth(120),
                                                              child: Row(
                                                                mainAxisAlignment: MainAxisAlignment.center,
                                                                children: <Widget>[
                                                                  Text.rich(
                                                                    TextSpan(children: [
                                                                      TextSpan(text: '累 计 ', style: TextStyle(color: appThemeState.themeConfig.mainTextColor, fontFamily: 'fzys')),
                                                                      TextSpan(
                                                                          text: (widget.taskList[index].taskTypeId == 3
                                                                              ? (widget.taskList[index].totalCount / 60).ceil().toString()
                                                                              : widget.taskList[index].totalCount.toString()),
                                                                          style: TextStyle(
                                                                              color: appThemeState.themeConfig.embellishColorA, fontSize: ScreenUtil.getInstance().getSp(72), fontFamily: 'fzys')),
                                                                      TextSpan(
                                                                          text: ' ' + CommonConfig.taskCountUnit[widget.taskList[index].taskTypeId],
                                                                          style: TextStyle(color: appThemeState.themeConfig.mainTextColor, fontFamily: 'fzys')),
                                                                    ]),
                                                                    textScaleFactor: 1.0,
                                                                  ),
                                                                ],
                                                              ),
                                                            ),
//
                                                            Container(
                                                                height: ScreenUtil.getInstance().getWidth(210),
                                                                child: Row(
                                                                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                                                                  children: <Widget>[
                                                                    ButtonTheme(
                                                                        minWidth: ScreenUtil.getInstance().getWidth(300),
                                                                        height: ScreenUtil.getInstance().getWidth(100),
                                                                        shape: RoundedRectangleBorder(
                                                                          side: BorderSide(width: 0.5, color: Colors.black12),
                                                                          borderRadius: BorderRadius.circular(ScreenUtil.getInstance().getWidth(60)),
                                                                        ),
                                                                        child: FlatButton(
                                                                            color: Colors.white.withOpacity(0.7),
                                                                            child: Text(
                                                                              '报数',
                                                                              textScaleFactor: 1.0,
                                                                              style: TextStyle(color: GlobalColorConfig.mainTextColor, fontSize: 16, fontFamily: 'fzys'),
                                                                            ),
                                                                            onPressed: () {
                                                                              showCustomDialog(context, widget.taskList[index], index);
                                                                            })),
                                                                    ButtonTheme(
                                                                      minWidth: ScreenUtil.getInstance().getWidth(300),
                                                                      height: ScreenUtil.getInstance().getWidth(100),
                                                                      shape: RoundedRectangleBorder(
                                                                        side: BorderSide(width: 0.5, color: Colors.black12),
                                                                        borderRadius: BorderRadius.circular(ScreenUtil.getInstance().getWidth(60)),
                                                                      ),
                                                                      child: FlatButton(
                                                                        color: Colors.white.withOpacity(0.7),
                                                                        child: Text(
                                                                          CommonConfig.taskCatalogList[widget.taskList[index].taskTypeId - 1],
                                                                          textScaleFactor: 1.0,
                                                                          style: TextStyle(color: GlobalColorConfig.mainTextColor, fontSize: 16, fontFamily: 'fzys'),
                                                                        ),
                                                                        onPressed: () {
                                                                          String bookName = ParameterConvertUtils.convertCN(widget.taskList[index].taskName);
                                                                          if (widget.taskList[index].taskTypeId == 1) {
                                                                            String url = widget.taskList[index].taskContentUrl.replaceAll('/', '%2F');

                                                                            Routes.router.navigateTo(context,
                                                                                'bookView?fileUrl=${url}&source=0&id=${widget.taskList[index].taskId}&taskType=${widget.taskList[index].taskTypeId}&bookName=${bookName}&version=${widget.taskList[index].version}');
                                                                          } else if (widget.taskList[index].taskTypeId == 2) {
                                                                            Routes.router.navigateTo(
                                                                                context, 'sukhavati?count=${widget.taskList[index].count}&taskId=${widget.taskList[index].taskId}&taskName=${bookName}',
                                                                                transition: TransitionType.fadeIn);
                                                                          } else if (widget.taskList[index].taskTypeId == 3) {
                                                                            Routes.router.navigateTo(context, 'zen?duration=${widget.taskList[index].count}&taskId=${widget.taskList[index].taskId}',
                                                                                transition: TransitionType.fadeIn);
                                                                          } else if (widget.taskList[index].taskTypeId > 3) {
                                                                            String url = widget.taskList[index].taskContentUrl.replaceAll('/', '%2F');
                                                                            String bookName = ParameterConvertUtils.convertCN(widget.taskList[index].taskName);
                                                                            Routes.router.navigateTo(context,
                                                                                'bookView?fileUrl=${url}&source=0&id=${widget.taskList[index].taskId}&taskType=${widget.taskList[index].taskTypeId}&bookName=${bookName}&version=${widget.taskList[index].version}');
                                                                          }
                                                                        },
                                                                      ),
                                                                    ),
                                                                  ],
                                                                ))
                                                          ],
                                                        ),
                                                      ),

//                                                    统计
                                                    ],
                                                  )),
                                            ])
//                                                    ],
                                            ),
                                        onTap: () {},
                                      )))),
                        ),
                      )),
                ),
                onTap: () {
                  Navigator.pop(context);
                },
              ),
            ));
      },
    );
  }
}

class BodyCover extends StatelessWidget {
  final Task task;
  const BodyCover({this.task});

  /// 获取直排文字
  List<Widget> getVerticalText(String text, int columnNum, double fontSize, double columnWidth) {
//    text = text.replaceAll("《", '').replaceAll("》", '');
    text = text.substring(0, math.min(columnNum * 2, text.length));
    int cols = (text.length / columnNum).ceil();
    List<Widget> list = [];
    for (int i = 0; i < cols; i++) {
      String text2 = text.substring(i * columnNum, math.min(i * columnNum + columnNum - 1, text.length - 1) + 1);
      text2 = text2.replaceAllMapped('', (match) => match.start == 0 || match.end == text2.length ? '' : '\n');
      list.add(Container(
        alignment: Alignment.topCenter,
        child: Text(
          text2,
          style: TextStyle(
              decoration: TextDecoration.none,
              color: Colors.black87,
              fontSize: ScreenUtil.getInstance().getSp(fontSize),
              shadows: [Shadow(color: Colors.black26, blurRadius: 6)],
              fontWeight: FontWeight.normal,
              fontFamily: FontFamilyName.SONGTI),
        ),
      ));
    }
    return list.reversed.toList();
  }

  Widget getChild(BoxConstraints constraints) {
    Widget wgt = Container();
    switch (task.taskTypeId) {
      case 1:
        wgt = Container(
          padding: EdgeInsets.symmetric(horizontal: ScreenUtil.getInstance().getWidth(72)),
          decoration: BoxDecoration(image: DecorationImage(image: AssetImage('assets/task_cover/1.jpg'), alignment: Alignment.bottomCenter, fit: BoxFit.fitWidth)),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              Expanded(
                  child: Container(
                      alignment: Alignment.topCenter,
                      padding: EdgeInsets.only(right: 4, top: 60),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: getVerticalText(task.author, 12, 40, 30),
                      ))),
              Expanded(
                  child: Container(
//                      alignment: Alignment.center,
                      padding: EdgeInsets.only(left: 4),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: getVerticalText(task.taskName, 9, 60, 30),
                      )))
            ],
          ),
        );

        return wgt;
      case 2:
        wgt = Container(
          padding: EdgeInsets.symmetric(horizontal: ScreenUtil.getInstance().getWidth(72)),
          decoration: BoxDecoration(image: DecorationImage(image: AssetImage('assets/pusa/common_bg.jpg'), alignment: Alignment.bottomCenter, fit: BoxFit.fitWidth)),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              Container(
                  height: ScreenUtil.getInstance().getWidth(90),
                  alignment: Alignment.center,
//                  padding: EdgeInsets.only(right: 4, top: 60),
                  child: Text(
                    task.taskName.split('').reversed.join().substring(2) + '无南',
                    style: TextStyle(fontSize: ScreenUtil.getInstance().getSp(48), fontWeight: FontWeight.bold, color: Color(0xFFA70400)),
                  )),
              Expanded(
                  child: Container(
                      padding: EdgeInsets.symmetric(vertical: 15),
                      child: Hero(
                          tag: task.taskTypeId.toString() + '_' + task.taskId.toString(),
                          flightShuttleBuilder: (flightContext, animation, direction, fromContext, toContext) {
                            return ScaleTransition(
                              scale: animation.drive(Tween(begin: 1.0, end: 1.0)),
                              child: Image.asset(
                                'assets/pusa/${task.taskId}.png',
                                fit: BoxFit.contain,
                              ),
                            );
                          },
                          child: Image.asset(
                            'assets/pusa/${task.taskId}.png',
                            fit: BoxFit.contain,
                          )))),
            ],
          ),
        );
        return wgt;
      case 3:
        wgt = Container(
          padding: EdgeInsets.symmetric(horizontal: ScreenUtil.getInstance().getWidth(72)),
          decoration: BoxDecoration(image: DecorationImage(image: AssetImage('assets/zen_common.jpg'), alignment: Alignment.bottomCenter, fit: BoxFit.fitWidth)),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Container(
                  height: ScreenUtil.getInstance().getWidth(90),
                  alignment: Alignment.center,
                  child: Text(
                    task.taskName,
                    style: TextStyle(fontSize: ScreenUtil.getInstance().getSp(48), fontWeight: FontWeight.bold, color: Color(0xFFA70400)),
                  )),
              Expanded(
                  child: Container(
                      padding: EdgeInsets.only(top: 15),
                      decoration: BoxDecoration(borderRadius: BorderRadius.circular(15)),
                      alignment: Alignment.topCenter,
                      child: Opacity(
                          opacity: 0.5,
                          child: Hero(
                            tag: task.taskTypeId.toString() + '_' + task.taskId.toString(),
                            flightShuttleBuilder: (flightContext, animation, direction, fromContext, toContext) {
                              return ScaleTransition(
                                scale: animation.drive(Tween(begin: 1.0, end: 1.0)),
                                child: Image.asset(
                                  'assets/pusa/${task.taskId}.png',
                                  fit: BoxFit.contain,
                                ),
                              );
                            },
                            child: Image.asset('assets/zen_common.png'),
                          ))))
            ],
          ),
        );
        return wgt;
      case 4:
        wgt = Container(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              Container(
                  height: ScreenUtil.getInstance().getWidth(90),
                  alignment: Alignment.center,
                  child: Text(
                    task.taskName,
                    style: TextStyle(fontSize: ScreenUtil.getInstance().getSp(48), fontWeight: FontWeight.bold, color: Color(0xFFA70400)),
                  )),
              Expanded(
                  child: Container(
                      padding: EdgeInsets.symmetric(horizontal: 0),
                      width: constraints.maxWidth,
//                      decoration: BoxDecoration(color: Colors.white, boxShadow: [BoxShadow(color: Colors.black12, blurRadius: 12)]),
                      child: Opacity(
                        opacity: 0.5,
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(0),
                          child: Image.asset(
                            'assets/task_cover/4.jpg',
                            fit: BoxFit.cover,
                          ),
                        ),
                      ))),
            ],
          ),
        );
        return wgt;
      case 5:
        wgt = Container(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              Container(
                  height: ScreenUtil.getInstance().getWidth(90),
                  alignment: Alignment.center,
                  child: Text(
                    task.taskName,
                    style: TextStyle(fontSize: ScreenUtil.getInstance().getSp(48), fontWeight: FontWeight.bold, color: Color(0xFFA70400)),
                  )),
              Expanded(
                  child: Container(
                      padding: EdgeInsets.symmetric(horizontal: 0),
                      width: constraints.maxWidth,
//                      decoration: BoxDecoration(color: Colors.white, boxShadow: [BoxShadow(color: Colors.black12, blurRadius: 12)]),
                      child: Opacity(
                        opacity: 0.5,
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(0),
                          child: Image.asset(
                            'assets/task_cover/5.jpg',
                            fit: BoxFit.cover,
                          ),
                        ),
                      ))),
            ],
          ),
        );
        return wgt;
        return wgt;
    }
  }

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(builder: (context, constraints) {
      return Container(
        width: constraints.maxWidth,
        child: getChild(constraints),
      );
    });
  }
}
