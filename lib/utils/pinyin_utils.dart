import 'package:lpinyin/lpinyin.dart';

class PinYinUtils {
  static String convertZh2Pinyin(String c) {
    PinyinHelper.addMultiPinyinDict(['南无=ná,mó']);
    PinyinHelper.addPinyinDict(['他=tuó', '伽=qié', '啰=là', '跢=duō', '般=bō', '酰=xī', '鞞=pí', '那=nuó', '阇=dū', '隸=li']);
    String list = PinyinHelper.getPinyin(c, format: PinyinFormat.WITH_TONE_MARK);
    return list;
  }
}
