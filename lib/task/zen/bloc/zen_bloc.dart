import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:bodhiroad/api/api.dart';
import 'package:bodhiroad/task/read/bean/task_record.dart';
import 'package:bodhiroad/utils/AudioUtils.dart';
import 'package:common_utils/common_utils.dart';
import 'package:flutter/services.dart';
import 'package:global_configuration/global_configuration.dart';
import 'package:soundpool/soundpool.dart';

import 'zen_event.dart';
import 'zen_state.dart';

Soundpool pool;
int startRingShort;
int startRingLong;
int finishRing;
int reduceTime;

class ZenBloc extends Bloc<ZenEvent, ZenState> {
  static final ZenBloc _zenBlocSingleton = new ZenBloc._internal();
  factory ZenBloc() {
    return _zenBlocSingleton;
  }
  ZenBloc._internal();

  ZenState get initialState => new ZenInitState(position: 0, countDownTextStr: '00:00:00');
  TimerUtil timer;
  // 总时长
  int count = 0;
  // 已过时长
  int passedTime;
  @override
  Stream<ZenState> mapEventToState(
    ZenEvent event,
  ) async* {
    try {
      if (event is ZenInitEvent) {
        String coundDownStr = calculateTime(event.position);
        yield ZenInitFinishState(position: event.position, countDownTextStr: coundDownStr);
      } else if (event is ZenChangeTimeEvent) {
        String coundDownStr = calculateTime(event.position);
        yield ZenInitFinishState(position: event.position, countDownTextStr: coundDownStr);
      } else if (event is ZenStartEvent) {
        count = event.totalTime;
        startTimer(event.totalTime);
        saveRecord(0, 0);
        if (event.totalTime > 1800) {
          AudioUtils().chanxiuStartL();
        } else {
          AudioUtils().chanxiuStartS();
        }
      } else if (event is ZenUnderwayEvent) {
        //  String coundDownStr = calculateTime(event.totalTime);
        yield ZenUnderwayState(position: event.position, countDownTextStr: event.countDownTextStr);
      } else if (event is ZenStopEvent) {
        if (timer != null) {
          timer.cancel();
        }
        saveRecord(1, 0);
        // pool.play(finishRing);
        String countDownStr = calculateTime(reduceTime ~/ 1000);
        yield ZenStopState(position: reduceTime ~/ 1000, countDownTextStr: countDownStr);
      } else if (event is ZenUnderwayEvent) {
        yield ZenUnderwayState(position: event.position, countDownTextStr: event.countDownTextStr);
      } else if (event is ZenContinueEvent) {
        startTimer(event.totalTime);
      } else if (event is ZenFinishEvent) {
        saveRecord(1, 1);
        yield ZenFinishState(position: event.position, countDownTextStr: event.countDownTextStr);
        AudioUtils().chanxiuFinish();
      }

      // yield await event.applyAsync(currentState: currentState, bloc: this);
    } catch (_, stackTrace) {
      print('$_ $stackTrace');
      yield currentState;
    }
  }

  void startTimer(int totalTime) {
    timer = TimerUtil(mInterval: 1000, mTotalTime: totalTime * 1000);
    timer.setOnTimerTickCallback((callBack) {
      reduceTime = callBack;
      String countDownStr = calculateTime(callBack ~/ 1000);
      _zenBlocSingleton.dispatch(ZenUnderwayEvent(position: (callBack / 1000).ceil(), countDownTextStr: countDownStr));
      passedTime = count - (callBack / 1000).ceil();
      if (callBack == 0) {
        _zenBlocSingleton.dispatch(ZenFinishEvent(position: 0, countDownTextStr: countDownStr));
      }
    });
    timer.startCountDown();
  }

  /// 保存记录
  /// operate 0 新增 1修改
  saveRecord(int operate, int state) async {
    TaskRecord taskRecord =
        TaskRecord(id: GlobalConfiguration().getInt("currentTaskRecordId"), taskId: GlobalConfiguration().getInt("id"), taskCount: passedTime, taskType: 3, state: state, source: 0);
    if (operate == 0) {
      var res = await Api.taskRecordAdd(data: taskRecord.toJson());
      if (res['code'] == 200) {
        GlobalConfiguration().updateValue('currentTaskRecordId', res['data']);
      }
    } else {
      var res = Api.taskRecordUpdate(data: taskRecord.toJson());
      if (taskRecord.state == 1) {
        GlobalConfiguration().updateValue("currentTaskRecordId", 0);
      }
    }
  }

  String calculateTime(int second) {
    int hour = (second / 3600).floor();
    int minute = ((second % 3600) / 60).floor();
    int sec = (second % 3600) % 60;
    String lastmin = minute < 10 ? '0${minute % 60}' : '${minute % 60}';
    String lastsecond = sec < 10 ? '0${sec % 60}' : '${sec % 60}';
    String hours = hour < 10 ? '0' + hour.toString() : hour.toString();
    return hours + ':' + lastmin + ':' + lastsecond;
  }

  initSoundPool() async {
    pool = Soundpool(streamType: StreamType.music);
    startRingShort = await rootBundle.load("assets/audio/muyu.wav").then((ByteData soundData) {
      return pool.load(soundData);
    });
    startRingLong = await rootBundle.load("assets/audio/chanxiuStart.mp3").then((ByteData soundData) {
      return pool.load(soundData);
    });
    finishRing = await rootBundle.load("assets/audio/zhongsheng.mp3").then((ByteData soundData) {
      return pool.load(soundData);
    });
  }
}
