import 'package:json_annotation/json_annotation.dart';

import 'task.dart';
part 'task_catalog.g.dart';

@JsonSerializable()
class TaskCatalog {
  String catalogName;
  List<Task> list;

  TaskCatalog({
    this.catalogName,
    this.list,
  });
  factory TaskCatalog.fromJson(Map<String, dynamic> json) {
    return _$TaskCatalogFromJson(json);
  }
  Map<String, dynamic> toJson() => _$TaskCatalogToJson(this);
}
