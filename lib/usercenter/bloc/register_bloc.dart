import 'dart:async';

import 'package:bloc/bloc.dart';

class RegisterBloc extends Bloc<int, int> {
  static final RegisterBloc _registerBloc = new RegisterBloc._internal();
  factory RegisterBloc() {
    return _registerBloc;
  }
  RegisterBloc._internal();

  int get initialState => 0;

  @override
  Stream<int> mapEventToState(
    int event,
  ) async* {
    try {
      print(event);
      yield event;
    } catch (_, stackTrace) {
      print('$_ $stackTrace');
      yield currentState;
    }
  }
}
