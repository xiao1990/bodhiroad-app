import 'dart:core';
import 'dart:io';

import 'package:bodhiroad/api/api.dart';
import 'package:bodhiroad/config/GlobalColorCOnfig.dart';
import 'package:bodhiroad/usercenter/bloc/login/login_bloc.dart';
import 'package:bodhiroad/usercenter/bloc/login/login_event.dart';
import 'package:bodhiroad/usercenter/bloc/login/login_state.dart';
import 'package:bodhiroad/utils/HttpUtils.dart';
import 'package:bodhiroad/utils/Route.dart';
import 'package:bodhiroad/utils/ToastUtils.dart';
import 'package:flustars/flustars.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:global_configuration/global_configuration.dart';
import 'package:path_provider/path_provider.dart';

class RegisterPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: RegisterBody(),
    );
  }
}

class RegisterBody extends StatefulWidget {
  @override
  _RegisterBodyState createState() => _RegisterBodyState();
}

class _RegisterBodyState extends State<RegisterBody> {
  PageController _pageController;
  int status = 0;
  @override
  void initState() {
    _pageController = PageController(initialPage: 0, keepPage: true);
    super.initState();
  }

  @override
  void dispose() {
    _pageController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height,
      decoration: BoxDecoration(image: DecorationImage(fit: BoxFit.cover, image: AssetImage('assets/texture/bgt2.jpg'))),
      child: PageView(
        scrollDirection: Axis.vertical,
        controller: _pageController,
        physics: BouncingScrollPhysics(),
        children: <Widget>[
          Login(pageController: _pageController),
          Register(
            pageController: _pageController,
          ),
        ],
      ),
    );
  }
}

class Register extends StatefulWidget {
  final PageController pageController;

  const Register({Key key, this.pageController}) : super(key: key);
  @override
  _RegisterState createState() => _RegisterState();
}

class _RegisterState extends State<Register> {
  final GlobalKey<FormBuilderState> _fbKey = GlobalKey<FormBuilderState>();
  final loginBloc = LoginBloc();
  @override
  Widget build(BuildContext context) {
    return Center(
        child: BlocBuilder(
            bloc: loginBloc,
            builder: (context, LoginState registerState) {
              return Container(
                width: MediaQuery.of(context).size.width - 20,
                height: MediaQuery.of(context).size.height - 20,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Container(
                        child: FormBuilder(
                      key: _fbKey,
                      child: Column(
                        children: <Widget>[
                          Container(
                            height: 36,
                            alignment: Alignment.center,
                            child: Text(
                              '欢迎注册',
                              textScaleFactor: 1.0,
                              style: TextStyle(fontSize: 24, color: GlobalColorConfig.mainforegroundColor),
                            ),
                          ),
                          Padding(
                              padding: EdgeInsets.all(10),
                              child: FormBuilderTextField(
                                attribute: 'name',
                                cursorColor: Colors.black87,
                                validators: [
                                  FormBuilderValidators.required(errorText: '用户名不能为空！'),
                                ],
                                readOnly: registerState.statusCode == 1,
                                decoration: InputDecoration(
                                  filled: true,
                                  hintText: '请输入您的名字',
                                  alignLabelWithHint: false,
                                  hasFloatingPlaceholder: false,
                                  labelText: '请输入您的名字',
                                  prefixIcon: Icon(
                                    Icons.account_circle,
                                    color: Colors.black87,
                                  ),
                                  isDense: true,
                                  focusColor: Colors.blueGrey.withOpacity(0.0),
                                  hoverColor: Colors.white,
                                  enabledBorder: OutlineInputBorder(borderSide: BorderSide.none, borderRadius: BorderRadius.circular(6)),
                                  focusedBorder: OutlineInputBorder(borderSide: BorderSide.none, borderRadius: BorderRadius.circular(6)),
                                ),
                              )),
                          Padding(
                              padding: EdgeInsets.all(10),
                              child: FormBuilderTextField(
                                cursorColor: Colors.black87,
                                attribute: 'email',
                                readOnly: registerState.statusCode == 1,
                                validators: [FormBuilderValidators.required(errorText: '邮箱不能为空！'), FormBuilderValidators.email(errorText: '邮箱格式不正确！')],
                                onEditingComplete: () {
                                  _fbKey.currentState.save();
                                  if (_fbKey.currentState.validate()) {
                                    print(_fbKey.currentState.value);
                                  }
                                },
                                decoration: InputDecoration(
                                  filled: true,
                                  hintText: '请输入您的邮箱',
                                  alignLabelWithHint: false,
                                  hasFloatingPlaceholder: false,
                                  labelText: '请输入您的邮箱',
                                  prefixIcon: Icon(
                                    Icons.email,
                                    color: Colors.black87,
                                  ),
                                  isDense: true,
                                  focusColor: Colors.blueGrey.withOpacity(0.0),
                                  hoverColor: Colors.white,
                                  enabledBorder: OutlineInputBorder(borderSide: BorderSide.none, borderRadius: BorderRadius.circular(6)),
                                  focusedBorder: OutlineInputBorder(borderSide: BorderSide.none, borderRadius: BorderRadius.circular(6)),
                                ),
                              )),
                          Padding(
                              padding: EdgeInsets.all(10),
                              child: FormBuilderTextField(
                                attribute: 'password',
                                cursorColor: Colors.black87,
                                obscureText: true,
                                maxLines: 1,
                                readOnly: registerState.statusCode == 1,
                                validators: [
                                  FormBuilderValidators.required(errorText: '密码不能为空！'),
                                  (val) {
                                    RegExp pt = RegExp(r"^[a-zA-Z]\w{5,17}$");
                                    if (!pt.hasMatch(val)) return "以字母开头,可以包含大小写字母、数字、下划线,长度6-18位";
                                  },
                                ],
                                onEditingComplete: () {
                                  _fbKey.currentState.save();
                                  if (_fbKey.currentState.validate()) {
                                    print(_fbKey.currentState.value);
                                  }
                                },
                                decoration: InputDecoration(
                                  filled: true,
                                  hintText: '请输入您的密码',
                                  alignLabelWithHint: false,
                                  hasFloatingPlaceholder: false,
                                  suffixIcon: Icon(
                                    Icons.visibility_off,
                                    color: Colors.black87,
                                  ),
                                  labelText: '请输入您的密码',
                                  prefixIcon: Icon(
                                    Icons.lock,
                                    color: Colors.black87,
                                  ),
                                  isDense: true,
                                  focusColor: Colors.blueGrey.withOpacity(0.0),
                                  hoverColor: Colors.white,
                                  enabledBorder: OutlineInputBorder(borderSide: BorderSide.none, borderRadius: BorderRadius.circular(6)),
                                  focusedBorder: OutlineInputBorder(borderSide: BorderSide.none, borderRadius: BorderRadius.circular(6)),
                                ),
                              )),
                          Padding(
                              padding: EdgeInsets.all(10),
                              child: ButtonTheme(
                                  height: 50,
                                  minWidth: MediaQuery.of(context).size.width,
                                  child: BlocBuilder(
                                      condition: (previousState, currentState) {
                                        return true;
                                      },
                                      bloc: loginBloc,
                                      builder: (BuildContext context, state) {
                                        print(state);
                                        return FlatButton(
                                          shape: RoundedRectangleBorder(side: BorderSide(color: Colors.black87, width: 0.5), borderRadius: BorderRadius.circular(6)),
                                          child: Text(
                                            registerState.statusCode == 1 ? '正 在 注 册···' : '注 册',
                                            textScaleFactor: 1.0,
                                            style: TextStyle(color: Colors.black87, fontSize: ScreenUtil.getInstance().getSp(36)),
                                          ),
                                          color: Colors.white.withOpacity(registerState.statusCode == 1 ? 0.6 : 0.0),
                                          onPressed: () async {
                                            _fbKey.currentState.save();
                                            if (_fbKey.currentState.validate()) {
                                              loginBloc.dispatch(RegisteringEvent());
                                              var res = await Api.register(data: _fbKey.currentState.value);
                                              if (res['code'] == 200) {
                                                ToastUtils.success('注册成功，请去注册邮箱激活账号！');
                                                final loginResult = await Api.login(data: {
                                                  'username': _fbKey.currentState.value['email'].toString(),
                                                  'password': _fbKey.currentState.value['password'].toString()
                                                });
                                                var userResult = null;
                                                if (loginResult['code'] == 200) {
//                                                  ToastUtils.success(context, '登录提示', '登录成功！', ThemeConfig.flt());
                                                  userResult = await Api.getUserInfo();
                                                }
                                                var user = userResult['data'];

                                                if (userResult['code'] == 200) {
                                                  Directory tempDir = await getTemporaryDirectory();
                                                  String tempPath = tempDir.path;
                                                  await HttpUtils.download(user['headPortrait'], '${tempPath}/headPortrait.jpg');
                                                  user['headPortraitUrl'] = '${tempPath}/headPortrait.jpg';
                                                  GlobalConfiguration().loadFromMap(user);
                                                  loginBloc.dispatch(LoggedOnEvent());
                                                  Routes.router.navigateTo(context, '/', replace: true);
                                                  SpUtil.putObject('user', user);
                                                } else {
                                                  loginBloc.dispatch(LoginErrorEvent());
                                                  ToastUtils.fail('注册失败' + res['data'].toString());
                                                }
                                              }
                                            }
                                          },
                                        );
                                      }))),
                          Container(
                            padding: EdgeInsets.symmetric(horizontal: 10),
                            child: ButtonTheme(
                              height: 48,
                              minWidth: MediaQuery.of(context).size.width,
                              child: FlatButton(
                                shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(6)),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    Text(
                                      '已有账号，去登录',
                                      textScaleFactor: 1.0,
                                      style: TextStyle(color: Colors.black87, fontSize: ScreenUtil.getInstance().getSp(36)),
                                    ),
                                    Icon(
                                      Icons.arrow_upward,
                                      size: ScreenUtil.getInstance().getSp(44),
                                      color: Colors.black87,
                                    )
                                  ],
                                ),
                                onPressed: () {
                                  widget.pageController.previousPage(duration: Duration(milliseconds: 500), curve: Curves.easeInOut);
                                },
                              ),
                            ),
                          )
                        ],
                      ),
                    )),
                  ],
                ),
              );
            }));
  }
}

class Login extends StatefulWidget {
  final PageController pageController;

  const Login({Key key, this.pageController}) : super(key: key);
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  final GlobalKey<FormBuilderState> _fbKey2 = GlobalKey<FormBuilderState>();
  double loginState = 0; // 0初始状态 1登陆中 2登录成功 3登录失败
  LoginFuture() async {}

  @override
  Widget build(BuildContext context) {
    final LoginBloc loginBloc = LoginBloc();
    return Center(
      child: Container(
        width: MediaQuery.of(context).size.width - 20,
        height: MediaQuery.of(context).size.height - 20,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
                child: BlocBuilder(
              bloc: loginBloc,
              builder: (BuildContext context, LoginState loginState) {
                return FormBuilder(
                  key: _fbKey2,
                  child: Column(
                    children: <Widget>[
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Container(
                            height: 36,
                            alignment: Alignment.center,
                            child: Text(
                              '欢迎登录',
                              textScaleFactor: 1.0,
                              style: TextStyle(fontSize: 24, color: GlobalColorConfig.mainforegroundColor),
                            ),
                          ),
                        ],
                      ),
                      Padding(
                          padding: EdgeInsets.all(10),
                          child: FormBuilderTextField(
                            cursorColor: Colors.black87,
                            attribute: 'username',
                            validators: [FormBuilderValidators.required(errorText: '邮箱不能为空！'), FormBuilderValidators.email(errorText: '邮箱格式不正确！')],
                            onEditingComplete: () {
                              _fbKey2.currentState.save();
                              if (_fbKey2.currentState.validate()) {
                                print(_fbKey2.currentState.value);
                              }
                            },
                            readOnly: loginState.statusCode == 4,
                            decoration: InputDecoration(
                              filled: true,
                              hintText: '请输入您的邮箱',
                              alignLabelWithHint: false,
                              hasFloatingPlaceholder: false,
                              labelText: '请输入您的邮箱',
                              prefixIcon: Icon(
                                Icons.email,
                                color: Colors.black87,
                              ),
                              isDense: true,
                              focusColor: Colors.blueGrey.withOpacity(0.0),
                              hoverColor: Colors.white,
                              enabledBorder: OutlineInputBorder(borderSide: BorderSide.none, borderRadius: BorderRadius.circular(6)),
                              focusedBorder: OutlineInputBorder(borderSide: BorderSide.none, borderRadius: BorderRadius.circular(6)),
                            ),
                          )),
                      Padding(
                          padding: EdgeInsets.all(10),
                          child: FormBuilderTextField(
                            enabled: false,
                            readOnly: loginState.statusCode == 4,
                            attribute: 'password',
                            cursorColor: Colors.black87,
                            obscureText: true,
                            maxLines: 1,
                            validators: [
                              FormBuilderValidators.required(errorText: '密码不能为空！'),
                              (val) {
                                RegExp pt = RegExp(r"^[a-zA-Z]\w{5,17}$");
                                if (!pt.hasMatch(val)) return "以字母开头,可以包含大小写字母、数字、下划线,长度6-18位";
                              },
                            ],
                            onEditingComplete: () {
                              _fbKey2.currentState.save();
                              if (_fbKey2.currentState.validate()) {
                                print(_fbKey2.currentState.value);
                              }
                            },
                            decoration: InputDecoration(
                              filled: true,
                              hintText: '请输入您的密码',
                              alignLabelWithHint: false,
                              hasFloatingPlaceholder: false,
                              suffixIcon: Icon(
                                Icons.visibility_off,
                                color: Colors.black87,
                              ),
                              labelText: '请输入您的密码',
                              prefixIcon: Icon(
                                Icons.lock,
                                color: Colors.black87,
                              ),
                              isDense: true,
                              focusColor: GlobalColorConfig.mainBackgroundColor.withOpacity(0.0),
                              hoverColor: Colors.white,
                              enabledBorder: OutlineInputBorder(borderSide: BorderSide.none, borderRadius: BorderRadius.circular(6)),
                              focusedBorder: OutlineInputBorder(borderSide: BorderSide.none, borderRadius: BorderRadius.circular(6)),
                            ),
                          )),
                      Padding(
                          padding: EdgeInsets.all(10),
                          child: ButtonTheme(
                            height: 50,
                            minWidth: MediaQuery.of(context).size.width,
                            child: FlatButton(
                              disabledColor: GlobalColorConfig.mainBackgroundColor.withOpacity(0.8),
                              shape: RoundedRectangleBorder(side: BorderSide(color: Colors.black87, width: 0.5), borderRadius: BorderRadius.circular(6)),
                              child: Text(
                                loginState.statusCode == 4 ? '正 在 登 录···' : '登 录',
                                textScaleFactor: 1.0,
                                style: TextStyle(color: GlobalColorConfig.mainforegroundColor, fontSize: ScreenUtil.getInstance().getSp(36)),
                              ),
                              color: Colors.white.withOpacity(loginState.statusCode == 4 ? 0.6 : 0.0),
                              onPressed: () async {
                                _fbKey2.currentState.save();
                                // 表单验证
                                if (_fbKey2.currentState.validate()) {
                                  // 隐藏键盘
                                  FocusScope.of(context).requestFocus(FocusNode());
                                  loginBloc.dispatch(LoggingInEvent());
                                  // widget.loginBloc.dispatch(1);
                                  var loginResult = await Api.login(data: _fbKey2.currentState.value);
                                  var userResult = null;
                                  if (loginResult['code'] == 200) {
                                    userResult = await Api.getUserInfo();
                                    var user = userResult['data'];
                                    if (userResult['code'] == 200) {
                                      Directory tempDir = await getApplicationDocumentsDirectory();
                                      String tempPath = tempDir.path;
                                      await HttpUtils.download(user['headPortrait'], '${tempPath}/headPortrait/headPortrait.jpg');
                                      user['headPortraitUrl'] = '${tempPath}/headPortrait/headPortrait.jpg';
                                      GlobalConfiguration().loadFromMap(user);
                                      loginBloc.dispatch(LoggedOnEvent());
                                      Navigator.of(context).popUntil(ModalRoute.withName("/"));
//                                      Routes.router.navigateTo(context, '/');
                                      ToastUtils.success('登录成功！');
                                      SpUtil.putObject('user', user);
                                    } else {
                                      loginBloc.dispatch(LoginErrorEvent());
                                      ToastUtils.fail(loginResult['message']);
                                    }
                                  } else if (loginResult['code'] == 401) {
                                    ToastUtils.fail(loginResult['message']);
                                    loginBloc.dispatch(LoginErrorEvent());
                                  } else {
                                    ToastUtils.fail('登录失败');
                                    loginBloc.dispatch(LoginErrorEvent());
                                  }
                                }
                              },
                            ),
                          ))
                    ],
                  ),
                );
              },
            )),
            Container(
              height: 48,
              padding: EdgeInsets.symmetric(horizontal: 10),
              child: ButtonTheme(
                height: 48,
                minWidth: MediaQuery.of(context).size.width,
                child: FlatButton(
                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(6)),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text(
                        '还没有账号，去注册',
                        textScaleFactor: 1.0,
                        style: TextStyle(color: Colors.black87, fontSize: ScreenUtil.getInstance().getSp(36)),
                      ),
                      Icon(
                        Icons.arrow_downward,
                        size: ScreenUtil.getInstance().getSp(44),
                        color: Colors.black87,
                      )
                    ],
                  ),
                  onPressed: () {
                    widget.pageController.nextPage(duration: Duration(milliseconds: 500), curve: Curves.easeInOut);
                  },
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
