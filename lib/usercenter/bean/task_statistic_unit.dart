import 'package:json_annotation/json_annotation.dart';
part 'task_statistic_unit.g.dart';

@JsonSerializable()
class TaskStatisticUnit {
  int taskId;
  String taskName;
  int taskCount;
  int taskDuration;
  int taskType;
  TaskStatisticUnit({
    this.taskId,
    this.taskName,
    this.taskCount,
    this.taskDuration,
    this.taskType,
  });
  factory TaskStatisticUnit.fromJson(Map<String, dynamic> json) {
    return _$TaskStatisticUnitFromJson(json);
  }
  Map<String, dynamic> toJson() => _$TaskStatisticUnitToJson(this);
}
