import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:bodhiroad/api/api.dart';
import 'package:bodhiroad/config/CommonConfig.dart';
import 'package:bodhiroad/reader/bean/common_book.dart';
import 'package:flustars/flustars.dart';

import 'buddhist_event.dart';
import 'buddhist_state.dart';

class BuddhistBloc extends Bloc<BuddhistEvent, BuddhistState> {
  static final BuddhistBloc _buddhistBlocSingleton = new BuddhistBloc._internal();
  factory BuddhistBloc() {
    return _buddhistBlocSingleton;
  }
  BuddhistBloc._internal();

  BuddhistState get initialState => new InitBuddhistState();

  @override
  Stream<BuddhistState> mapEventToState(
    BuddhistEvent event,
  ) async* {
    try {
      if (event is LoadBuddhistEvent) {
        List<CommonBook> common = [];
        List commonBuddhist = [];
        if (!event.forceUpdate) {
          commonBuddhist = SpUtil.getObjectList(CommonConfig.BUDDHIST_CATALOG + 'common');
        }
        if (commonBuddhist != null && commonBuddhist.length > 0) {
          common = commonBuddhist.map((item) => CommonBook.fromJson(item)).toList();
        } else {
          var rest = await Api.getBuddhist();
          if (rest['code'] == 200) {
            List<dynamic> list0 = rest['data'];
            common = list0.map((item) => CommonBook.fromJson(item)).toList();
          }
          SpUtil.putObjectList(CommonConfig.BUDDHIST_CATALOG + 'common', common);
        }
        yield LoadBuddhistState(common: common);
      } else {
        yield InitBuddhistState();
      }
    } catch (_, stackTrace) {
      print('$_ $stackTrace');
      yield currentState;
    }
  }
}
