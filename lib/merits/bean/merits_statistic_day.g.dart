// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'merits_statistic_day.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

MeritsStatisticDay _$MeritsStatisticDayFromJson(Map<String, dynamic> json) {
  return MeritsStatisticDay(
      day: json['day'] as String,
      list: (json['list'] as List)
          ?.map((e) =>
              e == null ? null : Merits.fromJson(e as Map<String, dynamic>))
          ?.toList());
}

Map<String, dynamic> _$MeritsStatisticDayToJson(MeritsStatisticDay instance) =>
    <String, dynamic>{'day': instance.day, 'list': instance.list};
