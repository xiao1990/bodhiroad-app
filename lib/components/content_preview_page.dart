import 'package:bodhiroad/theme/bloc.dart';
import 'package:flustars/flustars.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class ContentPreviewPage extends StatelessWidget {
  final String content;
  final String title;
  const ContentPreviewPage({this.content, this.title});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder(
      bloc: AppThemeBloc(),
      builder: (context, ThemeChangeState appThemeState) {
        return Scaffold(
          backgroundColor: Colors.transparent,
          body: Center(
            child: Container(
              width: ScreenUtil.getInstance().getWidth(840),
              height: 400,
              decoration: BoxDecoration(
                  color: appThemeState.themeConfig.sectionBgColor,
                  borderRadius: BorderRadius.circular(15)),
              child: Column(
                children: <Widget>[
                  Container(
                    height: ScreenUtil.getInstance().getWidth(120),
                    alignment: Alignment.center,
                    child: Text(title,
                        textScaleFactor: 1.0,
                        style: TextStyle(
                            fontSize: ScreenUtil.getInstance().getSp(42),
                            color: appThemeState.themeConfig.mainTextColor,
                            fontWeight: FontWeight.bold)),
                    decoration: BoxDecoration(
                        border: Border(
                            bottom: BorderSide(
                                color:
                                    appThemeState.themeConfig.lightBorderColor,
                                width: 0.5))),
                  ),
                  Expanded(
                    child: Container(
                      padding:
                          EdgeInsets.symmetric(vertical: 15, horizontal: 30),
                      alignment: Alignment.center,
                      child: Text(
                        content.replaceAll('-', '\n'),
                        overflow: TextOverflow.ellipsis,
                        maxLines: 12,
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontSize: ScreenUtil.getInstance().getSp(42)),
                      ),
                    ),
                  ),
                  Container(
                      height: ScreenUtil.getInstance().getWidth(150),
                      decoration: BoxDecoration(
                          border: Border(
                              top: BorderSide(
                                  color: appThemeState
                                      .themeConfig.lightBorderColor,
                                  width: 0.5))),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Expanded(
                            child: ButtonTheme(
                              minWidth: ScreenUtil.getInstance().getWidth(420),
                              height: ScreenUtil.getInstance().getWidth(150),
                              child: FlatButton(
                                child: Text(
                                  '关闭',
                                  textScaleFactor: 1.0,
                                  style: TextStyle(
                                      fontSize:
                                          ScreenUtil.getInstance().getSp(42),
                                      color: appThemeState
                                          .themeConfig.mainTextColor,
                                      fontWeight: FontWeight.bold),
                                ),
                                onPressed: () {
                                  Navigator.pop(context);
                                },
                              ),
                            ),
                          ),
                        ],
                      ))
                ],
              ),
            ),
          ),
        );
      },
    );
  }
}
