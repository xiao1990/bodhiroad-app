import 'package:bodhiroad/config/ThemeConfig.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class ThemeState extends Equatable {
  ThemeState([List props = const []]) : super([props]);
}

class InitialThemeState extends ThemeState {
  final ThemeConfig themeConfig;
  final bool pureColor;
  InitialThemeState({this.themeConfig, this.pureColor}) : super([themeConfig]);
}

class ThemeChangeState extends ThemeState {
  final ThemeConfig themeConfig;
  final bool pureColor;
  ThemeChangeState({this.themeConfig, this.pureColor})
      : super([themeConfig, pureColor]);
}
