import 'dart:async';
import 'dart:io';
import 'dart:math' as math;
import 'package:bloc/bloc.dart';
import 'package:bodhiroad/api/api.dart';
import 'package:bodhiroad/common/text_config.dart';
import 'package:bodhiroad/config/CommonConfig.dart';
import 'package:bodhiroad/config/ReaderThemeConfig.dart';
import 'package:bodhiroad/reader/bean/book_paragraph_unit.dart';
import 'package:bodhiroad/reader/bean/bookmark.dart';
import 'package:bodhiroad/reader/bean/favorite.dart';
import 'package:bodhiroad/utils/GlobalConfigUtil.dart';
import 'package:bodhiroad/utils/HttpUtils.dart';
import 'package:bodhiroad/utils/ToastUtils.dart';
import 'package:bodhiroad/utils/page_util.dart';
import 'package:bodhiroad/utils/pagingUtils.dart';
import 'package:bodhiroad/utils/pinyin_utils.dart';
import 'package:flustars/flustars.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:global_configuration/global_configuration.dart';
import 'package:html/dom.dart';
import 'package:html/parser.dart';
import 'package:path_provider/path_provider.dart';
import './bloc.dart';
import 'bean/book_char.dart';

class BuddhistReaderBlocBloc extends Bloc<BuddhistReaderBlocEvent, BuddhistReaderBlocState> {
  static final BuddhistReaderBlocBloc _readerBlocSingleton = new BuddhistReaderBlocBloc._internal();
  factory BuddhistReaderBlocBloc() {
    return _readerBlocSingleton;
  }
  BuddhistReaderBlocBloc._internal();
  @override
  BuddhistReaderBlocState get initialState => InitialBuddhistReaderBlocState();
  static Map<String, List<BookParagraphUnitBean>> map = Map();

  @override
  Stream<BuddhistReaderBlocState> mapEventToState(
    BuddhistReaderBlocEvent event,
  ) async* {
    ReloadReaderState reloadReaderState = ReloadReaderState(
      catalogList: [],
      pageList: [],
      bookMarkList: [],
      unitList: [],
    );

    /// 重载阅读器
    if (event is ReloadReaderEvent) {
      yield BuddhistReaderLoadingState(description: '加载中···');
      bool latest = true;
      // 检查经文是否有更新
      Map checkUpdate = await Api.buddhistCheckUpdate(
          data: {'articleId': GlobalConfiguration().getInt('id'), 'typeId': GlobalConfiguration().getInt('source') + 1, 'version': GlobalConfiguration().getInt('version')});
      if (checkUpdate['data'] > 0) {
        print('需要更新');
        latest = false;
      }
      print('开始获取标记****************************************');
      print(DateTime.now().millisecondsSinceEpoch);

      /// 加载标记
      List<BookMark> underLineList = GlobalConfigUtil.getUnderLineList();
      bool synced = SpUtil.getBool(CommonConfig.BUDDHIST_SYNC + GlobalConfiguration().getInt('source').toString() + '_' + GlobalConfiguration().getInt('id').toString());
      if (underLineList.length == 0 && !synced && SpUtil.getString('token').length > 0) {
        var underlineMap = await Api.getBuddhistMark(data: {'articleId': GlobalConfigUtil.getInt('id'), 'articleType': GlobalConfigUtil.getInt('source')});
        List underlineListDynamic = underlineMap['data'];
        underLineList = underlineListDynamic.map((item) => BookMark.fromJson(item)).toList();
        GlobalConfigUtil.saveUnderLineOrBookmarkList(underLineList);
        SpUtil.putBool(CommonConfig.BUDDHIST_SYNC + GlobalConfiguration().getInt('source').toString() + '_' + GlobalConfiguration().getInt('id').toString(), true);
      }
      print('获取标记完成****************************************');
      print(DateTime.now().millisecondsSinceEpoch);
      reloadReaderState.bookMarkList2 = underLineList;
      String htmlId = CommonConfig.BUDDHIST_HTML + (GlobalConfiguration().getInt('source') + 1).toString() + '_' + GlobalConfiguration().getInt('id').toString();
      // 读取本地存储HTML
      String bookHtml = SpUtil.getString(htmlId);
      reloadReaderState.bookHtml = bookHtml;
      reloadReaderState.configuration = GlobalConfiguration();
      reloadReaderState.map = ReaderThemeConfig.textSizeMap;
      print('开始解析HTML****************************************');
      if (reloadReaderState.bookHtml == null || reloadReaderState.bookHtml == '' || !latest) {
        yield DownloadingBuddhistReaderBlocState(description: '正在下载经文···');
        print(DateTime.now().millisecondsSinceEpoch);
        Directory tempDir = await getTemporaryDirectory();
        String tempPath = tempDir.path;
        File file = File(tempPath + '/' + GlobalConfiguration().getString('fileUrl'));
        if (!file.parent.existsSync()) {
          file.create(recursive: true);
        }
        print('${CommonConfig.DOMAIN_PROD}/${GlobalConfiguration().getString('fileUrl').trim()}');
        await HttpUtils.download('${CommonConfig.DOMAIN_PROD}/${GlobalConfiguration().getString('fileUrl').trim()}', tempPath + '/' + GlobalConfiguration().getString('fileUrl'));
        reloadReaderState.bookHtml = await file.readAsString();
        print(DateTime.now().millisecondsSinceEpoch);
        yield ParsingBuddhistReaderBlocState(description: '正在解析···');

        reloadReaderState = reloadReaderState.configuration.getInt('taskType') == 4 ? readHtml(reloadReaderState) : await compute(readHtml, reloadReaderState);
        save2Local(htmlId, reloadReaderState.bookHtml);
      } else {
        readHtml(reloadReaderState);
      }
      yield BuddhistReaderLoadingState(description: '解析完成,正在加载···');
      print(DateTime.now().millisecondsSinceEpoch);
      print('获取书籍信息完成****************************************');
      print(DateTime.now().millisecondsSinceEpoch);
      yield reloadReaderState;

      /// 简单重载
    } else if (event is SimpleReloadReaderEvent) {
      yield SimpleReloadReaderState();
    } else if (event is SimpleReloadFinishReaderEvent) {
      yield SimpleReloadFinishReaderState();
    } else if (event is ChapterJumpEvent) {
      yield ChapterJumpState(index: event.index, position: event.position);
    } else if (event is ReaderReachBottomEvent) {
      yield ReaderReachBottomState();
    } else if (event is ReaderUnReachBottomEvent) {
      yield ReaderUnReachBottomState();
    } else if (event is ReaderRestartEvent) {
      yield ReaderRestartState();
    } else if (event is ReadProgressEvent) {
      yield ReadProgressState(progress: event.progress);
    } else if (event is OpenBottomMenuEvent) {
      yield OpenBottomMenuState();
    } else if (event is OpenBottomMenuSuccessEvent) {
      yield OpenBottomMenuSuccessState();
    } else if (event is BottomMenuSlidingEvent) {
      yield BottomMenuSlidingState(event.progress);
    } else if (event is AddFavoriteEvent) {
      if (SpUtil.getString('token') == null || SpUtil.getString('token').length == 0) {
        ToastUtils.fail('请先登录！');
        return;
      }

      Map addFavorite;
      if (event.status == 1) {
        addFavorite = await Api.addFavorite(data: {'buddhistId': GlobalConfiguration().getInt('id'), 'type': GlobalConfiguration().getInt('source')});
      } else {
        addFavorite = await Api.deleteFavorite(data: {'buddhistId': GlobalConfiguration().getInt('id'), 'type': GlobalConfiguration().getInt('source')});
      }
      if (addFavorite['code'] == 200 && event.status == 1) {
        ToastUtils.success('收藏成功！');
        SpUtil.putInt('favorite_' + GlobalConfiguration().getInt('id').toString() + GlobalConfiguration().getInt('source').toString(), 1);
        yield AddFavoriteSuccessState();
        SpUtil.remove('favorite');
      } else {
        ToastUtils.success('取消收藏成功！');
        SpUtil.putInt('favorite_' + GlobalConfiguration().getInt('id').toString() + GlobalConfiguration().getInt('source').toString(), 0);
        yield DeleteFavoriteSuccessState();
        _readerBlocSingleton.dispatch(GetFavoriteEvent());
      }
    } else if (event is GetFavoriteEvent) {
      Map map = await Api.getFavorite();
      List<dynamic> list = map['data'];
      List<Favorite> list2 = list.map((item) => Favorite.fromJson(item)).toList();
      yield GetFavoriteSuccessState(list2);
      SpUtil.putObjectList('favorite', list2);
    } else if (event is AddBookMarkEvent) {
      List<BookMark> markList = GlobalConfigUtil.getUnderLineList();
      if (markList == null || markList.length == 0) {
        markList = [];
      }
      for (int i = 0; i < markList.length; i++) {
        if (markList[i].actualStartIndex <= event.bookMark.actualStartIndex && markList[i].actualEndIndex >= event.bookMark.actualEndIndex) {
          return;
        }
      }
      markList
        ..add(event.bookMark)
        ..sort((a, b) => a.actualStartIndex >= b.actualStartIndex ? 1 : -1);
      GlobalConfigUtil.saveUnderLineOrBookmarkList(markList);
      SpUtil.putBool(CommonConfig.BUDDHIST_SYNC + GlobalConfiguration().getInt('source').toString() + '_' + GlobalConfiguration().getInt('id').toString(), false);
      _readerBlocSingleton.dispatch(ReloadReaderEvent());
      compute(composeBookmark, markList);
    } else if (event is RemoveUnderLineEvent) {
      List<BookMark> markList = GlobalConfigUtil.getUnderLineList();
      for (int i = 0; i < markList.length; i++) {
        if (event.id == markList[i].actualStartIndex) {
          markList.removeAt(i);
          GlobalConfigUtil.saveUnderLineOrBookmarkList(markList);
          break;
        }
      }
//      Api.addBuddhistMark()
      _readerBlocSingleton.dispatch(ReloadReaderEvent());
      if (SpUtil.getString('token') != null && SpUtil.getString('token').length > 0) {
        Api.deleteBuddhistMark(data: {
          'articleId': GlobalConfigUtil.getInt('id'),
          'startIndex': event.id,
        });
        SpUtil.putBool(CommonConfig.BUDDHIST_SYNC + GlobalConfiguration().getInt('source').toString() + '_' + GlobalConfiguration().getInt('id').toString(), false);
      }
    }
  }

  /// 缓存到本地
  static save2Local(String key, String value) async {
    SpUtil.putString(key, value);
  }

  /// 读取HTML文件内容
  static ReloadReaderState readHtml(ReloadReaderState state) {
    print('开始读取HTML文本内容并转化****************************************');
    print(DateTime.now().millisecondsSinceEpoch);
    String key = CommonConfig.BUDDHIST_PARAGRAPH_PREFIX + (state.configuration.getInt('source') + 1).toString() + '_' + state.configuration.getInt('id').toString();
//    if (true) {
    if (map[key] == null) {
      map.clear();
      List paragraphsListStr = SpUtil.getObjectList(key);
      List<BookParagraphUnitBean> unitList = [];
      if (paragraphsListStr != null && paragraphsListStr.length > 0) {
        unitList = paragraphsListStr.map((f) => BookParagraphUnitBean.fromJson(f)).toList();
      }
      if (unitList.length == 0) {
        Document document = parse(state.bookHtml);
        print(DateTime.now().millisecondsSinceEpoch);

        if (state.configuration.getInt('taskType') == 4) {
          String keyPrefix = ReaderThemeConfig.fontSizeList[state.configuration.getInt('fontSize')].toString() + '-';
          unitList = document.getElementsByTagName('body')[0].children.map((e) {
            print(e);
            return BookParagraphUnitBean(tag: e.localName, text: e.text.trim(), markIndexs: [], chars: e.localName == 'p' ? getChar(e.text.trim(), e.localName, state.map, keyPrefix) : []);
          }).toList();
        } else {
          int index = 0;
          print(DateUtil.getNowDateMs());
          unitList = document.getElementsByTagName('body')[0].children.map((e) {
            String tag = e.localName;
            String text = e.text.trim();
            print(e.attributes['bold']);
            BookParagraphUnitBean p =
                BookParagraphUnitBean(tag: tag, text: text, firstIndex: index, endIndex: index + text.length - 1, bold: e.attributes['bold'] == '1', colored: e.attributes['colored'] == '1');
            index += text.length;
            return p;
          }).toList();
          print(DateUtil.getNowDateMs());
          print('--------------------');
        }

        SpUtil.putObjectList(CommonConfig.BUDDHIST_PARAGRAPH_PREFIX + (state.configuration.getInt('source') + 1).toString() + '_' + state.configuration.getInt('id').toString(), unitList);
      }
      map[key] = unitList;
    }

    print('开始计算段落分页****************************************');
    print(DateTime.now().millisecondsSinceEpoch);
    if (state.configuration.getInt('taskType') == 4) {
      state = PageUtils.zhou(
        map[key],
        state,
      );
    } else if (state.configuration.getInt('textLayOutDirection') == 1 && state.configuration.getInt('flipType') < 2) {
      state = PagingUtils.textVerticalLayoutWithFlip2Left(map[key], state);
    } else if (state.configuration.getInt('textLayOutDirection') == 0 && state.configuration.getInt('flipType') < 2) {
      PagingUtils.textHorizontalLayoutWithFlip2Left(map[key], state);
    } else {
      state = PagingUtils.textHorizontalLayoutWithVerticalScroll2(map[key], state);
    }
    print('计算段落分页完成****************************************开始缓存');
    return state;
  }

  static List<CharBean> getChar(String text, String tag, Map textLayoutSize, String keyPrefix) {
    text = text.replaceAll('\n', '');
    text = text.replaceAll(' ', '');
    List t = TextConfig.instance.rareTextIndex.keys.toList();
    t.forEach((key) {
      if (text.contains(key.toString())) {
        text = text.replaceAll(key.toString(), TextConfig.instance.rareTextIndex[key.toString()]);
      }
    });

    String pinyin = PinYinUtils.convertZh2Pinyin(text);
    List pinList = pinyin.split(' ');
    List<CharBean> charList = [];

    for (var i = 0; i < pinList.length; i++) {
      CharBean c = CharBean();
      c.char = text.substring(i, i + 1);
      c.tag = tag;
      c.pinyin = TextConfig.instance.rareTextPinyinRela[c.char] ?? pinList[i];
      TextPainter textPainter1 =
          TextPainter(text: TextSpan(text: c.pinyin, style: TextStyle(fontFamily: 'pingfang', fontSize: textLayoutSize[keyPrefix + tag].fontSize * 0.7)), textDirection: TextDirection.ltr);
      textPainter1.layout();
      c.pinyinWidth = textPainter1.size.width;
      charList.add(c);
    }
    return charList;
  }

  /// 去除重复书签和划线
  static List<BookMark> composeBookmark(List<BookMark> list) {
    return list;
  }
}
