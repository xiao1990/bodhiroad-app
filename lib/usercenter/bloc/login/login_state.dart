import 'package:bodhiroad/usercenter/bean/user.dart';

class LoginState {
  final User user;
  final int statusCode;
  LoginState({this.user, this.statusCode});
}
