import 'package:bodhiroad/reader/bean/book_catalog.dart';
import 'package:bodhiroad/reader/bean/book_mark.dart';
import 'package:bodhiroad/reader/bean/book_page.dart';
import 'package:bodhiroad/reader/bean/book_paragraph_unit.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class ReaderState extends Equatable {
  ReaderState([props = const []]) : super(props);
}

class ReaderInitState extends ReaderState {
  @override
  String toString() => 'ReaderInitState';
}

class ReaderFinishState extends ReaderState {
  @override
  String toString() => 'ReaderFinishState';
}

class ReaderReachBottomState extends ReaderState {
  @override
  String toString() => 'ReaderReachBottomState';
}

class ReaderUnReachBottomState extends ReaderState {
  @override
  String toString() => 'ReaderReachBottomState';
}

class ReaderRestartState extends ReaderState {
  @override
  String toString() => 'ReaderRestartState';
}

class ReaderAddFavoriteSuccessState extends ReaderState {
  final int isFavorite;
  ReaderAddFavoriteSuccessState(this.isFavorite) : super([isFavorite]);
  @override
  String toString() => 'ReaderAddFavoriteSuccessState';
}

class ReaderDeleteFavoriteSuccessState extends ReaderState {
  @override
  String toString() => 'ReaderDeleteFavoriteSuccessState';
}

class ReaderDownloadingState extends ReaderState {
  @override
  String toString() => 'ReaderDownloadingState';
  String info;
  ReaderDownloadingState({this.info}) : super([info]);
  @override
  ReaderState getStateCopy() {
    return ReaderDownloadingState();
  }
}

class ReaderParsingState extends ReaderState {
  @override
  String toString() => 'ReaderDownloadingState';
  String info;
  ReaderParsingState({this.info}) : super([info]);
  @override
  ReaderState getStateCopy() {
    return ReaderDownloadingState();
  }
}

class ReaderParsingFinishState extends ReaderState {
  @override
  String toString() => 'ReaderDownloadingState';
  String info;
  ReaderParsingFinishState({this.info}) : super([info]);
  @override
  ReaderState getStateCopy() {
    return ReaderDownloadingState();
  }
}

/// Initialized
class ReaderLoadState extends ReaderState {
  @override
  String toString() => 'ReaderLoadState';

  final List<PageBean> pageList;
  final List<CatalogBean> catalogList;
  final List<BookmarkBean> bookMarkList;
  final List<BookParagraphUnitBean> unitList;

  ReaderLoadState({this.pageList, this.catalogList, this.bookMarkList, this.unitList}) : super([pageList, catalogList, bookMarkList, unitList]);

  @override
  ReaderState getStateCopy() {
    return ReaderLoadState();
  }
}

class TransLangReaderState extends ReaderState {
  @override
  String toString() => 'TransLangReaderState';

  final bool simplifiedChinese;

  TransLangReaderState({this.simplifiedChinese}) : super([simplifiedChinese]);

  @override
  ReaderState getStateCopy() {
    return TransLangReaderState();
  }
}

class ErrorReaderState extends ReaderState {
  final String errorMessage;

  ErrorReaderState(this.errorMessage);

  @override
  String toString() => 'ErrorReaderState';

  @override
  ReaderState getStateCopy() {
    return ErrorReaderState(this.errorMessage);
  }
}

class BookMarkReaderState extends ReaderState {
  List<PageBean> pageList;
  int currentIndex;

  BookMarkReaderState({this.pageList, this.currentIndex});

  @override
  String toString() => 'ErrorReaderState';

  @override
  ReaderState getStateCopy() {
    return BookMarkReaderState();
  }
}
