import 'package:flutter/cupertino.dart';
import 'package:meta/meta.dart';

@immutable
abstract class MusicState {
  MusicState([Iterable props]) : super();
}

/// 初始化状态
class MusicInitState extends MusicState {
  MusicInitState() : super();
}

/// 音频文件正在加载
class MusicLoadingState extends MusicState {
  MusicLoadingState() : super();
}

/// 音频文件加载完成
class MusicLoadedState extends MusicState {
  final int currentMusicId;
  final double duration;
  MusicLoadedState({this.currentMusicId, this.duration}) : super();
}

/// 正在播放
class MusicPlayingState extends MusicState {
  final double position;
  MusicPlayingState({this.position = 0}) : super([position]);
}

/// 暂停
class MusicPauseState extends MusicState {
  MusicPauseState() : super();
}

/// 恢复播放
class MusicResumeState extends MusicState {
  MusicResumeState() : super();
}

/// 恢复播放
class MusicStopState extends MusicState {
  MusicStopState() : super();
}

/// 播放完成
class MusicFinishState extends MusicState {
  MusicFinishState() : super();
}

/// 音量调整
class MusicVolumeState extends MusicState {
  final double volume;
  MusicVolumeState({this.volume}) : super([volume]);
}

/// 播放错误
class MusicErrorState extends MusicState {
  final String error;
  MusicErrorState({this.error}) : super();
}

/// 显示通知
class ShowNotificationState extends MusicState {
  final String error;
  ShowNotificationState({this.error}) : super();
}

/// 显示通知
class MusicStopConfirmState extends MusicState {
  final String error;
  MusicStopConfirmState({this.error}) : super();
}
