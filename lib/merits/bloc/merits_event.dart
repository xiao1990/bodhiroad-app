import 'package:bodhiroad/merits/bean/merits.dart';
import 'package:bodhiroad/merits/bean/merits_statistic.dart';
import 'package:bodhiroad/merits/bean/merits_statistic_count.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class MeritsEvent extends Equatable {
  MeritsEvent([Iterable props]) : super(props);
}

/// 初始化
class MeritsInitEvent extends MeritsEvent {
  @override
  String toString() => 'MeritsInitEvent';
}

/// 翻页
class MeritsSlideEvent extends MeritsEvent {
  final int page;
  MeritsSlideEvent({this.page}) : super([page]);
  @override
  String toString() => 'MeritsSlideEvent';
}

/// 更新
class MeritsUpdateEvent extends MeritsEvent {
  final bool forceUpdate;
  MeritsUpdateEvent({this.forceUpdate});
  @override
  String toString() => 'MeritsSlideEvent';
}

class MarkMeritsEvent extends MeritsEvent {
  final Merits merits;
  MarkMeritsEvent({this.merits});
  @override
  String toString() => 'MeritsSlideEvent';
}

/// 更新
class MeritsExpandEvent extends MeritsEvent {
  final int index;
  MeritsExpandEvent({this.index}) : super([index]);
  @override
  String toString() => 'MeritsExpandEvent';
}

class SwitchTypeEvent extends MeritsEvent {
  final int belong;
  SwitchTypeEvent({this.belong}) : super([belong]);
  @override
  String toString() => 'MeritsExpandEvent';
}

class MeritsStatisticEvent extends MeritsEvent {
  final int catalogId;
  final int interval;
  MeritsStatisticEvent({this.catalogId, this.interval});
  @override
  String toString() => 'MeritsStatisticEvent';
}

class MeritsStatisticLoadEvent extends MeritsEvent {
  final List<MeritsStatistic> list;
  final List<MeritsStatisticCount> list2;
  final int maxY;
  MeritsStatisticLoadEvent({this.list, this.list2, this.maxY});
  @override
  String toString() => 'MeritsStatisticLoadEvent';
}

class AddUserMeritsEvent extends MeritsEvent {
  final int meritId;
  final int isGood;
  final String date;
  final int belong;
  final String operation;
  final Merits merits;
  // final List<List<Merits>> list;
  final int catalogId;
  final int rowId;
  final int columnId;
  AddUserMeritsEvent(
      {this.meritId,
      this.isGood,
      this.date,
      this.belong,
      this.operation,
      this.merits,
      this.catalogId,
      this.rowId,
      this.columnId})
      : super([meritId, isGood, date, belong, operation, merits]);
  @override
  String toString() => 'AddUserMeritsEvent';
}
