import 'package:bodhiroad/usercenter/bean/task_statistic.dart';
import 'package:bodhiroad/usercenter/bean/task_statistic_day.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class StatisticState extends Equatable {
  StatisticState([Iterable props]) : super(props);
}

/// UnInitialized
class UnStatisticState extends StatisticState {
  final List<TaskStatisticDays> taskStatisticByDays;
  final List<TaskStatistic> taskStatistics;
  final int maxDays;
  UnStatisticState(this.taskStatistics, this.taskStatisticByDays, this.maxDays)
      : super([taskStatistics, taskStatisticByDays, maxDays]);
  @override
  String toString() => 'UnStatisticState';
}

/// Initialized
class InStatisticState extends StatisticState {
  @override
  String toString() => 'InStatisticState';
}

class StatisticLoadDataState extends StatisticState {
  final List<TaskStatistic> taskStatistics;

  StatisticLoadDataState(this.taskStatistics) : super([taskStatistics]);

  @override
  String toString() => 'StatisticLoadDataState';
}

class StatisticChangeCataState extends StatisticState {
  final int taskType;
  StatisticChangeCataState({this.taskType}) : super([taskType]);
  @override
  String toString() => 'StatisticChangeCataState';
}

class StatisticLoadDataByCataState extends StatisticState {
  final List<TaskStatisticDays> taskStatistics;
  final int maxDays;
  StatisticLoadDataByCataState(this.taskStatistics, this.maxDays)
      : super([taskStatistics, maxDays]);
  @override
  String toString() => 'StatisticLoadDataState';
}
