import 'package:flustars/flustars.dart';
import 'package:flutter/material.dart';

class DialogFoot extends StatelessWidget {
  final String confirmText;
  final String cancelText;
  final String closeText;
  final bool justClose;
  final VoidCallback onConfirm;
  final VoidCallback onCancel;
  final VoidCallback onClose;
  const DialogFoot({Key key, this.confirmText, this.cancelText, this.justClose, this.closeText, this.onConfirm, this.onCancel, this.onClose});
  @override
  Widget build(BuildContext context) {
    return Container(
      height: ScreenUtil.getInstance().getWidth(120),
      alignment: Alignment.center,
      decoration: BoxDecoration(border: Border(top: BorderSide(color: Colors.black12, width: 0.5))),
      child: justClose
          ? Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: <Widget>[
              Expanded(
                  child: ButtonTheme(
                height: ScreenUtil.getInstance().getWidth(150),
                child: FlatButton(
                  child: Text(
                    closeText ?? '',
                    textScaleFactor: 1.0,
                    style: TextStyle(fontSize: ScreenUtil.getInstance().getSp(42), color: Colors.black87, fontWeight: FontWeight.bold),
                  ),
                  onPressed: () {
                    onClose();
                  },
                ),
              ))
            ])
          : Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: <Widget>[
              Expanded(
                child: ButtonTheme(
                  height: ScreenUtil.getInstance().getWidth(120),
                  child: FlatButton(
                    child: Text(
                      cancelText ?? '',
                      textScaleFactor: 1.0,
                      style: TextStyle(fontSize: ScreenUtil.getInstance().getSp(36), color: Colors.black87, fontWeight: FontWeight.bold),
                    ),
                    onPressed: () {
                      onCancel();
                    },
                  ),
                ),
              ),
              Expanded(
                  child: ButtonTheme(
                height: ScreenUtil.getInstance().getWidth(150),
                child: FlatButton(
                  child: Text(
                    confirmText ?? '',
                    textScaleFactor: 1.0,
                    style: TextStyle(fontSize: ScreenUtil.getInstance().getSp(36), color: Color(0xFFA70400), fontWeight: FontWeight.bold),
                  ),
                  onPressed: () {
                    onConfirm();
                  },
                ),
              )),
            ]),
    );
  }
}
