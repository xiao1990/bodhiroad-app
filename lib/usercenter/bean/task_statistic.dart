import 'package:json_annotation/json_annotation.dart';

import 'task_statistic_unit.dart';

part 'task_statistic.g.dart';

@JsonSerializable()
class TaskStatistic {
  String dateString;
  List<TaskStatisticUnit> list;
  TaskStatistic({
    this.dateString,
    this.list,
  });
  factory TaskStatistic.fromJson(Map<String, dynamic> json) {
    return _$TaskStatisticFromJson(json);
  }
  Map<String, dynamic> toJson() => _$TaskStatisticToJson(this);
}
