import 'dart:convert';
import 'dart:math' as math;

import 'package:bodhiroad/common/FontFamilyName.dart';
import 'package:bodhiroad/config/CommonConfig.dart';
import 'package:bodhiroad/reader/bean/brwh_catalog.dart';
import 'package:bodhiroad/utils/GlobalConfigUtil.dart';
import 'package:bodhiroad/utils/parameter_convert_cn.dart';
import 'package:flustars/flustars.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

import 'brwh_cover_index.dart';

class BrwhCatalogList extends StatefulWidget {
  final String from;
  final Icon icon;
  final String catalogName;
  final String source;
  final String taskType;
  final String id;
  const BrwhCatalogList({this.id, this.icon, this.catalogName, this.taskType, this.source, this.from});
  @override
  _BrwhCatalogListState createState() => _BrwhCatalogListState();
}

class _BrwhCatalogListState extends State<BrwhCatalogList> {
  Future f;

  @override
  void initState() {
    f = getBuddhist();
    super.initState();
  }

  Future<List<BrwhCatalogBean>> getBuddhist() async {
    List<BrwhCatalogBean> list2;
    List<dynamic> oriList = SpUtil.getObjectList("BRWH_CAT_" + widget.id);
    oriList = [];
    if (oriList != null && oriList.length > 0) {
      list2 = oriList.map((item) => BrwhCatalogBean.fromJson(item)).toList();
    } else {
      String content = await rootBundle.loadString((widget.from == 'dict' ? 'assets/dict/' : 'assets/brwh/') + widget.id + ".json");
      List contentList = json.decode(content);
      list2 = contentList.map((item) => BrwhCatalogBean.fromJson(item)).toList();
      SpUtil.putObjectList(CommonConfig.BRWH_CATALOG + widget.id, list2);
    }
    return list2;
  }

  List<Widget> getVerticalText(String text) {
    text = text.replaceAll("《", '').replaceAll("》", '');
    text = text.substring(0, math.min(14, text.length));

    int cols = (text.length / 7).ceil();
    List<Widget> list = [];
    for (int i = 0; i < cols; i++) {
      String text2 = text.substring(i * 7, math.min(i * 7 + 7 - 1, text.length - 1) + 1).replaceAll('', '\n');
      list.add(Container(
        width: ScreenUtil.getInstance().getWidth(48),
        height: ScreenUtil.getInstance().screenHeight * 0.5,
        alignment: Alignment.topCenter,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Container(
              width: ScreenUtil.getInstance().getWidth(48),
//              decoration: BoxDecoration(border: Border(left: BorderSide(color: Colors.black26, width: 0.5))),
              alignment: i == 0 ? Alignment.topCenter : Alignment.topRight,
              child: Text(
                text2,
                style: TextStyle(
                    decoration: TextDecoration.none,
                    color: Colors.black87,
                    fontSize: ScreenUtil.getInstance().getSp(30),
                    shadows: [Shadow(color: Colors.black26, blurRadius: 6)],
                    fontWeight: FontWeight.normal,
                    fontFamily: FontFamilyName.SONGTI),
              ),
            )
          ],
        ),
      ));
    }
    return list.reversed.toList();
  }

  @override
  Widget build(BuildContext context) {
//    final AppThemeBloc appThemeBloc = BlocProvider.of<AppThemeBloc>(context);
    return Scaffold(
        backgroundColor: Colors.transparent,
        appBar: AppBar(
          title: Text(ParameterConvertUtils.parseParameterCN(widget.catalogName)),
          backgroundColor: Colors.white,
          elevation: 0,
        ),
        body: Container(
            decoration: BoxDecoration(image: DecorationImage(image: AssetImage('assets/texture/bgt2.jpg'), fit: BoxFit.cover)),
            child: FutureBuilder<List<BrwhCatalogBean>>(
                future: f,
                builder: (context, AsyncSnapshot<List<BrwhCatalogBean>> snapShot) {
                  return snapShot.data != null && snapShot.data.length > 0
                      ? Container(
//                      padding: EdgeInsets.symmetric(horizontal: 15),
                          child: ListView.separated(
                              itemCount: snapShot.data == null ? 0 : snapShot.data.length,
                              physics: BouncingScrollPhysics(),
                              separatorBuilder: (context, index) {
                                return Container(
                                  height: 0.5,
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: List.generate(32, (index) {
                                      return Container(
                                        width: 4,
                                        height: 0.5,
                                        color: Colors.black12,
                                      );
                                    }).toList(),
                                  ),
                                );
                              },
                              itemBuilder: (BuildContext context, index) {
                                return Material(
                                  color: Colors.transparent,
                                  child: Ink(
                                    child: InkWell(
                                        onTap: () {
                                          Navigator.of(context).push(MaterialPageRoute(builder: (BuildContext context) {
                                            return BrwhCoverIndex(
                                              from: widget.from,
                                              source: widget.source,
                                              taskType: widget.taskType,
                                              article: snapShot.data[index],
                                            );
                                          }));
                                        },
                                        child: Hero(
                                            tag: snapShot.data[index].catalogId + snapShot.data[index].id.toString(),
                                            child: Container(
                                              padding: EdgeInsets.symmetric(horizontal: ScreenUtil.getInstance().getWidth(40)),
                                              height: ScreenUtil.getInstance().getWidth(310),
                                              margin: EdgeInsets.symmetric(vertical: ScreenUtil.getInstance().getWidth(15)),
                                              child: Row(
                                                children: <Widget>[
                                                  Container(
                                                    width: ScreenUtil.getInstance().getWidth(213),
                                                    height: ScreenUtil.getInstance().getWidth(300),
                                                    decoration: BoxDecoration(
                                                        border: Border.all(color: Color(0xFFdbd8d9), width: 1),
                                                        boxShadow: [BoxShadow(color: Colors.black12, blurRadius: 6)],
                                                        image: DecorationImage(
                                                            fit: BoxFit.cover,
                                                            image: AssetImage(
                                                              'assets/texture/cover2.jpg',
                                                            ))),
//                                    color: Colors.deepOrangeAccent,
                                                    padding: EdgeInsets.only(right: ScreenUtil.getInstance().getWidth(25)),
                                                    child: Row(
                                                      mainAxisAlignment: MainAxisAlignment.end,
                                                      children: getVerticalText(snapShot.data[index].title),
                                                    ),
                                                  ),
                                                  Expanded(
                                                      child: Container(
                                                    padding: EdgeInsets.only(
                                                        left: ScreenUtil.getInstance().getWidth(30),
                                                        top: ScreenUtil.getInstance().getWidth(30),
                                                        bottom: ScreenUtil.getInstance().getWidth(30)),
                                                    child: Column(
                                                      mainAxisAlignment: MainAxisAlignment.start,
                                                      crossAxisAlignment: CrossAxisAlignment.start,
                                                      children: <Widget>[
                                                        Container(
                                                          height: ScreenUtil.getInstance().getWidth(75),
                                                          child: Text(
                                                            snapShot.data[index].title,
                                                            maxLines: 1,
                                                            overflow: TextOverflow.ellipsis,
                                                            style: TextStyle(
                                                                fontSize: GlobalConfigUtil.getSubTitleFontSize(),
                                                                fontWeight: FontWeight.bold,
                                                                decoration: TextDecoration.none,
                                                                color: Colors.black87,
                                                                fontFamily: FontFamilyName.SONGTI),
                                                          ),
                                                        ),
                                                        Container(
                                                          height: ScreenUtil.getInstance().getWidth(50),
                                                          child: Text(
                                                            snapShot.data[index].author.length == 0 ? '失' : snapShot.data[index].author,
                                                            style: TextStyle(
                                                                fontSize: GlobalConfigUtil.getTabMainBodyFontSize(),
                                                                decoration: TextDecoration.none,
                                                                fontWeight: FontWeight.normal,
                                                                color: Colors.black54,
                                                                fontFamily: FontFamilyName.SONGTI),
                                                          ),
                                                        ),
                                                        Expanded(
                                                          child: Container(
                                                            child: Text(
                                                              snapShot.data[index].desc ?? '',
                                                              softWrap: true,
                                                              overflow: TextOverflow.ellipsis,
                                                              maxLines: 3,
                                                              style: TextStyle(
                                                                  fontSize: GlobalConfigUtil.getTabMainBodyFontSize(),
                                                                  color: Colors.black54,
                                                                  decoration: TextDecoration.none,
                                                                  fontWeight: FontWeight.normal,
                                                                  fontFamily: FontFamilyName.SONGTI),
                                                            ),
                                                          ),
                                                        )
                                                      ],
                                                    ),
                                                  ))
                                                ],
                                              ),
                                            ))),
                                  ),
                                );
                              }))
                      : Center(
                          child: SpinKitCircle(
                            size: ScreenUtil.getInstance().getWidth(50),
                            color: Colors.black54,
                          ),
                        );
                })));
  }
}
