import 'package:bodhiroad/reader/bean/book_catalog.dart';
import 'package:bodhiroad/reader/bean/book_mark.dart';
import 'package:bodhiroad/reader/bean/book_page.dart';
import 'package:bodhiroad/reader/bean/book_paragraph_unit.dart';
import 'package:bodhiroad/reader/bean/read_config.dart';
import 'package:flutter/material.dart';
import 'package:meta/meta.dart';

@immutable
abstract class ReaderEvent {
  ReaderEvent([props = const []]);
}
// 初始化

class ReaderInitEvent extends ReaderEvent {
  final int source;
  final int taskType;
  ReaderInitEvent({this.source, this.taskType}) : super([source, taskType]);
  @override
  String toString() => 'ReaderInitEvent';
}

// 阅读完成
class ReaderFinishEvent extends ReaderEvent {
  @override
  String toString() => 'ReaderFinishEvent';
}

// 到达最后一页
class ReaderReachBottomEvent extends ReaderEvent {
  @override
  String toString() => 'ReaderReachBottomEvent';
}

// 未到达最后一页
class ReaderUnReachBottomEvent extends ReaderEvent {
  @override
  String toString() => 'ReaderUnReachBottomEvent';
}

// 重新开始阅读
class ReaderRestartEvent extends ReaderEvent {
  @override
  String toString() => 'ReaderRestartEvent';
}

// 检测是否已收藏
class ReaderFavoriteCheckEvent extends ReaderEvent {
  final String buddhistId;

  ReaderFavoriteCheckEvent(this.buddhistId);
  @override
  String toString() => 'ReaderFavoriteCheckEvent';
}

// 添加收藏
class ReaderAddFavoriteEvent extends ReaderEvent {
  final String buddhistId;
  final String fileUrl;
  final String bookName;
  final int isFavorite;
  final int source;
  final int taskType;
  ReaderAddFavoriteEvent(this.buddhistId, this.fileUrl, this.bookName, this.isFavorite, this.source, this.taskType);
  @override
  String toString() => 'ReaderAddFavoriteEvent';
}

// 加载经文
class LoadReaderEvent extends ReaderEvent {
  ReadConfig readConfig;
  String bookHtml;
  String paragraphsListStr;
  List<CatalogBean> catelogList;
  List<PageBean> pageList;
  List<BookmarkBean> bookMarkList;
  List<BookParagraphUnitBean> unitList;
  LoadReaderEvent({this.bookHtml, this.paragraphsListStr, this.bookMarkList, this.catelogList, this.pageList, this.readConfig, this.unitList});
  @override
  String toString() => 'LoadReaderEvent';
}

class InitReaderEvent extends ReaderEvent {
  int count;
  InitReaderEvent({this.count});
  @override
  String toString() => 'InitReaderEvent';
}

class TransLangEvent extends ReaderEvent {
  bool simplifiedChinese;
  TransLangEvent({this.simplifiedChinese});
  @override
  String toString() => 'simplifiedChinese';
}

class BookMarkReaderEvent extends ReaderEvent {
  @override
  String toString() => 'BookMarkEvent';
  List<PageBean> pageList;
  int currentIndex;
  BookMarkReaderEvent({this.pageList, this.currentIndex});
}

class BookFinishEvent extends ReaderEvent {
  @override
  String toString() => 'BookFinishEvent';
}
