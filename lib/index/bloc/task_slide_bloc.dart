import 'dart:async';
import 'package:bloc/bloc.dart';

class TaskSlideBloc extends Bloc<double, double> {
  static final TaskSlideBloc _taskSlideBlocSingleton =
      new TaskSlideBloc._internal();
  factory TaskSlideBloc() {
    return _taskSlideBlocSingleton;
  }
  TaskSlideBloc._internal();

  double get initialState => 0.1;

  @override
  Stream<double> mapEventToState(
    double event,
  ) async* {
    try {
      yield event;
    } catch (_, stackTrace) {
      print('$_ $stackTrace');
      yield currentState;
    }
  }
}
