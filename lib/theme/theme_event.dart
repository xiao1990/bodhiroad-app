import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class ThemeEvent extends Equatable {
  ThemeEvent([List props = const []]) : super(props);
}

class ThemeChangeEvent extends ThemeEvent {
  final String themeName;
  ThemeChangeEvent({this.themeName}) : super([themeName]);
}
