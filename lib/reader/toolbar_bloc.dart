import 'package:bloc/bloc.dart';

class ToolbarBloc extends Bloc<bool, bool> {
  static final ToolbarBloc _readerBlocSingleton = new ToolbarBloc._internal();
  factory ToolbarBloc() {
    return _readerBlocSingleton;
  }
  ToolbarBloc._internal();
  @override
  bool get initialState => false;

  @override
  Stream<bool> mapEventToState(
    bool event,
  ) async* {
    yield event;
  }
}
