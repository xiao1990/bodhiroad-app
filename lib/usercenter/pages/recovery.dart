import 'package:bodhiroad/usercenter/bloc/download_bloc.dart';
import 'package:bodhiroad/usercenter/bloc/download_state.dart';
import 'package:bodhiroad/utils/Route.dart';
import 'package:bodhiroad/utils/ToastUtils.dart';
import 'package:bodhiroad/utils/parameter_convert_cn.dart';
import 'package:flustars/flustars.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class Recovery extends StatefulWidget {
  final String value;
  Recovery({this.value});

  @override
  _RecoveryState createState() => _RecoveryState();
}

class _RecoveryState extends State<Recovery> {
  TextEditingController _controller;
  TextEditingController _recoverController;
  DownloadBloc downloadBloc = DownloadBloc();
  @override
  void initState() {
    print(ParameterConvertUtils.parseParameterCN(widget.value));
    _controller = TextEditingController(text: ParameterConvertUtils.parseParameterCN(widget.value));
    _recoverController = TextEditingController(text: ParameterConvertUtils.parseParameterCN(widget.value));
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  feedback() async {
    String originalText = _controller.text.trim().replaceAll(' ', '');
    String recoveryText = _recoverController.text.trim().replaceAll(' ', '');
    if (recoveryText.length == 0) {
      ToastUtils.fail('内容不能为空');
      return;
    }
//    Api.feedback(data: {'originalText': originalText, 'correctText': recoveryText, 'articleId': GlobalConfigUtil.getInt('id'), 'articleType': GlobalConfigUtil.getInt('source')});
    ToastUtils.success('反馈成功！感谢您的反馈！');
    Routes.router.pop(context);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('纠错反馈'),
        backgroundColor: Colors.white,
        elevation: 0,
      ),
      body: Container(
          decoration: BoxDecoration(image: DecorationImage(image: AssetImage('assets/texture/bgt2.png'), fit: BoxFit.cover)),
          child: BlocBuilder(
            bloc: downloadBloc,
            condition: (prevSate, currentState) {
              if (currentState is DictSearchState) {
                return true;
              } else if (currentState is SelectCatalogState || currentState is SimpleSelectCatalogState) {
                return true;
              } else {
                return false;
              }
            },
            builder: (context, state) {
              return ListView(
                physics: BouncingScrollPhysics(),
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 15),
                    child: Column(
                      children: <Widget>[
                        Container(
                          child: Text('【原文】'),
                          height: 50,
                          alignment: Alignment.centerLeft,
//                          color: Colors.teal,
                        ),
                        TextField(
                          controller: _controller,
                          maxLines: 6,
                          minLines: 5,
                          readOnly: true,
                          enabled: false,
                          cursorColor: Color(0xFFB96652),
                          style: TextStyle(fontSize: 16, color: Colors.black54),
                          textAlignVertical: TextAlignVertical.top,
                          onSubmitted: (value) {
                            print(value);
                          },
                          showCursor: false,
                          decoration: new InputDecoration(
                            hintStyle: TextStyle(color: Colors.black12),
                            fillColor: Color(0xFFF2F2F2),
                            filled: true,
                            focusColor: Colors.red,

                            disabledBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.black12, width: 0.5), borderRadius: BorderRadius.circular(8)),
//                            border: OutlineInputBorder(borderRadius: BorderRadius.circular(6)),
                          ),
                        ),
                        Container(
                          child: Text('【纠正后】'),
                          height: 50,
                          alignment: Alignment.centerLeft,
//                          color: Colors.teal,
                        ),
                        TextField(
                          controller: _recoverController,
                          maxLines: 6,
                          minLines: 5,
                          cursorColor: Color(0xFFB96652),
                          style: TextStyle(fontSize: 16, color: Colors.black87),
                          textAlignVertical: TextAlignVertical.top,
                          onSubmitted: (value) {
                            print(value);
                          },
                          showCursor: true,
                          decoration: new InputDecoration(
                            fillColor: Color(0xFFF2F2F2).withOpacity(0.6),
                            filled: true,
                            enabledBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.black12, width: 0.5), borderRadius: BorderRadius.circular(8)),
                          ),
                        ),
                        Container(
                          height: 20,
                        ),
                        Row(mainAxisSize: MainAxisSize.max, mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: [
                          Expanded(
                              child: ButtonTheme(
                            padding: EdgeInsets.all(0),
                            minWidth: ScreenUtil.getInstance().getWidth(30),
                            buttonColor: Color(0xFFB96652),
                            height: ScreenUtil.getInstance().getWidth(118),
                            child: FlatButton(
                                color: Color(0xFFB96652),
                                padding: EdgeInsets.symmetric(horizontal: 12),
                                onPressed: feedback,
                                shape: RoundedRectangleBorder(side: BorderSide(color: Color(0xFFF2F2F2), width: 0.5), borderRadius: BorderRadius.circular(4)),
                                child: Text(
                                  '提 交',
                                  style: TextStyle(fontSize: ScreenUtil.getInstance().getSp(36), color: Colors.white),
                                )),
                          ))
                        ]),
                      ],
                    ),
                  ),
//    )
                ],
              );
            },
          )),
    );
  }
}
