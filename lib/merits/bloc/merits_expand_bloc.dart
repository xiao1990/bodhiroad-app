import 'dart:async';
import 'package:bloc/bloc.dart';

class MeritsExpandBloc extends Bloc<int, int> {
  static final MeritsExpandBloc _meritsExpandBlocSingleton =
      new MeritsExpandBloc._internal();
  factory MeritsExpandBloc() {
    return _meritsExpandBlocSingleton;
  }
  MeritsExpandBloc._internal();

  int get initialState => -1;

  @override
  Stream<int> mapEventToState(
    int event,
  ) async* {
    try {
      yield event;
    } catch (_, stackTrace) {
      print('$_ $stackTrace');
      yield currentState;
    }
  }
}
