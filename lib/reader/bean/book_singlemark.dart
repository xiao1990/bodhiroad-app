import 'package:json_annotation/json_annotation.dart';
part 'book_singlemark.g.dart';

@JsonSerializable()
class BookSingleMarkBean {
  int nodeIndex;
  int charIndex;
  int position;

  BookSingleMarkBean({this.nodeIndex, this.charIndex, position});

  factory BookSingleMarkBean.fromJson(Map<String, dynamic> json) {
    return _$BookSingleMarkBeanFromJson(json);
  }
  Map<String, dynamic> toJson() => _$BookSingleMarkBeanToJson(this);
}
