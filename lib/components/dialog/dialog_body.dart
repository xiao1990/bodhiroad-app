import 'package:flutter/material.dart';

class DialogBody extends StatelessWidget {
  final Widget child;
  const DialogBody({Key key, this.child});
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 15),
      alignment: Alignment.center,
      child: child,
    );
  }
}
