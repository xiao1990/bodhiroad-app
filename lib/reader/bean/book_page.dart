import 'package:bodhiroad/reader/bean/book_catalog.dart';
import 'package:bodhiroad/reader/bean/line_char.dart';
import 'package:json_annotation/json_annotation.dart';

import 'book_char.dart';
import 'book_mark.dart';
import 'book_singlemark.dart';

part 'book_page.g.dart';

@JsonSerializable()
class PageBean {
  String uuid;
  int pageIndex;
  int nodeIndex;
  int chapterIndex;
  List<CharBean> list;
  List<LineChar> charList;
  CatalogBean firstCatalog;
  CatalogBean lastCatalog;
  final status;
  String pageTitle;
  double padLeft;
  List<BookmarkBean> bookMarkList;
  List<BookSingleMarkBean> bookMarkPageList;
  int marked;
  double pageHeight;

  /// 每一页起始位置在全文中的索引
  int fullTextIndex = 0;
  PageBean(
      {this.uuid,
      this.pageIndex,
      this.chapterIndex,
      this.list,
      this.charList,
      this.firstCatalog,
      this.lastCatalog,
      this.padLeft = 0,
      this.bookMarkList,
      this.bookMarkPageList,
      this.status,
      this.pageTitle,
      this.fullTextIndex,
      this.nodeIndex,
      this.marked,
      this.pageHeight});
  factory PageBean.fromJson(Map<String, dynamic> json) {
    return _$PageBeanFromJson(json);
  }
  Map<String, dynamic> toJson() => _$PageBeanToJson(this);

  @override
  String toString() {
    return 'PageBean{uuid: $uuid, pageIndex: $pageIndex, nodeIndex: $nodeIndex, chapterIndex: $chapterIndex, list: $list, status: $status, pageTitle: $pageTitle, bookMarkList: $bookMarkList, bookMarkPageList: $bookMarkPageList, marked: $marked}';
  }
}
