import 'package:json_annotation/json_annotation.dart';
part 'book_paragraph.g.dart';

@JsonSerializable()
class BookParagraphBean {
  String content;
  int type;
  List<BookParagraphBean> list;
  BookParagraphBean({this.content, this.type, this.list});
  factory BookParagraphBean.fromJson(Map<String, dynamic> json) {
    return _$BookParagraphBeanFromJson(json);
  }
  Map<String, dynamic> toJson() => _$BookParagraphBeanToJson(this);
}
