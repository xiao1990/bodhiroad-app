import 'dart:ui';

import 'package:bodhiroad/common/text_config.dart';
import 'package:bodhiroad/config/ReaderThemeConfig.dart';
import 'package:bodhiroad/reader/bean/book_char.dart';
import 'package:bodhiroad/reader/bean/reader_theme.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:global_configuration/global_configuration.dart';
import 'package:zh_char_converter/zh_char_converter.dart';

// 文字画布
class CanvasText extends StatefulWidget {
  final List<CharBean> text;
  final double viewHeight;

  const CanvasText({
    this.text,
    this.viewHeight,
  });

  @override
  _CanvasTextState createState() => _CanvasTextState();
}

class _CanvasTextState extends State<CanvasText> {
  @override
  Widget build(BuildContext context) {
    ReaderTheme theme = ReaderThemeConfig.instance.list[GlobalConfiguration().getInt('themeIndex')];
    return CustomPaint(
        size: Size(GlobalConfiguration().getDouble('viewWidth'), widget.viewHeight),
        foregroundPainter: ChapterPainter(
          text: widget.text,
          maxWidth: GlobalConfiguration().getDouble('viewWidth'),
          fontColor: theme.fontColor,
        ));
  }
}

// 文字画笔
class ChapterPainter extends CustomPainter {
  // 绘制文字内容
  final List<CharBean> text;
  // 绘制区域宽度
  final double maxWidth;
  final Color fontColor;
  const ChapterPainter({this.text, this.maxWidth, this.fontColor});

  @override
  void paint(Canvas canvas, Size size) {
    TextPainter tp;
    for (var i = 0; i < text.length; i++) {
      tp = TextPainter(
          maxLines: 1,
          text: TextSpan(
            text: GlobalConfiguration().getBool('simplifiedChinese') ? TextConfig.instance.rareText[text[i].char] ?? text[i].char : zhTW(text[i].char),
            style: TextStyle(
                height: text[i].isPinyin ? 1.0 : 1.5,
                fontSize: text[i].fontSize,
                fontFamily: TextConfig.instance.rareText[text[i].char] == null ? 'songti' : 'kaiu',
                color: text[i].isPinyin ? Colors.black38 : fontColor,
                fontWeight: text[i].tag != 'h1' && text[i].tag != 'h2' ? FontWeight.normal : FontWeight.w600),
          ),
          textDirection: TextDirection.ltr);
      tp.layout(minWidth: 0, maxWidth: maxWidth);
      tp.paint(canvas, Offset(text[i].xOffset, text[i].yOffset));
    }
  }

  @override
  bool shouldRepaint(ChapterPainter oldDelegate) => true;

  @override
  bool shouldRebuildSemantics(ChapterPainter oldDelegate) => true;
}
