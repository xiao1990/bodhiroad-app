import 'package:json_annotation/json_annotation.dart';
part 'book_char.g.dart';

@JsonSerializable()
class CharBean {
  String char;
  double xOffset;
  double yOffset;
  double yOffset2;
  double fontSize;
  double height;
  double width;
  int type;
  int maxLine;
  String tag;
  double lastPageCharsHeight;
  int nodeIndex;
  int charIndex;
  int marked;
  int paragraphHead;
  bool paragraphEnd;
  double prefix;
  double surfix;
  int maxCount;
  double pinyinWidth;
  String pinyin;
  bool rareText;
  bool isPinyin;

  CharBean(
      {this.xOffset,
      this.yOffset,
      this.yOffset2,
      this.char,
      this.fontSize,
      this.type,
      this.width,
      this.height,
      this.maxLine,
      this.tag,
      this.nodeIndex,
      this.charIndex,
      this.lastPageCharsHeight,
      this.marked,
      this.prefix,
      this.surfix,
      this.maxCount,
      this.paragraphHead,
      this.paragraphEnd,
      this.isPinyin = false,
      this.rareText});

  factory CharBean.fromJson(Map<String, dynamic> json) {
    return _$CharBeanFromJson(json);
  }
  Map<String, dynamic> toJson() => _$CharBeanToJson(this);
}
