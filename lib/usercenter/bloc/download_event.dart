abstract class DownloadEvent {
  DownloadEvent() : super();
}

/// 重载阅读器事件
class DictDownloadEvent extends DownloadEvent {
  final String path;
  final int source;
  DictDownloadEvent({this.path, this.source});
}

/// 重载阅读器事件
class DictDownloadInitEvent extends DownloadEvent {
  final String path;
  DictDownloadInitEvent({this.path});
}

/// 重载阅读器事件
class DictDownloadingEvent extends DownloadEvent {
  final String taskId;
  final double progress;

  DictDownloadingEvent({this.taskId, this.progress});
}

/// 重载阅读器事件
class DictDownloadCompleteEvent extends DownloadEvent {
  final String taskId;
  DictDownloadCompleteEvent({this.taskId});
}

/// 重载阅读器事件
class DisposeEvent extends DownloadEvent {
  DisposeEvent();
}

/// 重载阅读器事件
class DictSearchEvent extends DownloadEvent {
  final String keyword;
  final int catalogIndex;
  DictSearchEvent({this.keyword, this.catalogIndex});
}

/// 重载阅读器事件
class SelectCatalogEvent extends DownloadEvent {
  final int index;
  SelectCatalogEvent({this.index});
}

/// 重载阅读器事件
class SimpleSelectCatalogEvent extends DownloadEvent {
  final int index;
  SimpleSelectCatalogEvent({this.index});
}
