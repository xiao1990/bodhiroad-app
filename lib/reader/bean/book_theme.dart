import 'package:json_annotation/json_annotation.dart';
part 'book_theme.g.dart';

@JsonSerializable()
class BookTheme {
  int frontColor;
  int backColor;

  BookTheme({
    this.frontColor,
    this.backColor,
  });
  factory BookTheme.fromJson(Map<String, dynamic> json) {
    return _$BookThemeFromJson(json);
  }
  Map<String, dynamic> toJson() => _$BookThemeToJson(this);
}
