// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'calendar_day.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CalendarDay _$CalendarDayFromJson(Map<String, dynamic> json) {
  return CalendarDay(
      gregorianDateTime: json['gregorianDateTime'] as String,
      lDay: json['lDay'] as String,
      lMonth: json['lMonth'] as String,
      yt: json['yt'] as String,
      d: json['d'] as String,
      m: json['m'] as String,
      isToday: json['isToday'] as bool,
      foposa: json['foposa'] as String,
      active: json['active'] as bool);
}

Map<String, dynamic> _$CalendarDayToJson(CalendarDay instance) =>
    <String, dynamic>{
      'gregorianDateTime': instance.gregorianDateTime,
      'lDay': instance.lDay,
      'lMonth': instance.lMonth,
      'yt': instance.yt,
      'm': instance.m,
      'd': instance.d,
      'isToday': instance.isToday,
      'active': instance.active,
      'foposa': instance.foposa
    };
