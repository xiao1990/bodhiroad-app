import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class SelectDateState extends Equatable {
  SelectDateState([Iterable props]) : super(props);

  /// Copy object for use in action
  SelectDateState getStateCopy();
}

/// UnInitialized
class UnSelectDateState extends SelectDateState {
  @override
  String toString() => 'UnSelectDateState';

  @override
  SelectDateState getStateCopy() {
    return UnSelectDateState();
  }
}

/// Initialized
class InSelectDateState extends SelectDateState {
  @override
  String toString() => 'InSelectDateState';

  @override
  SelectDateState getStateCopy() {
    return InSelectDateState();
  }
}

class ErrorSelectDateState extends SelectDateState {
  final String errorMessage;

  ErrorSelectDateState(this.errorMessage);
  
  @override
  String toString() => 'ErrorSelectDateState';

  @override
  SelectDateState getStateCopy() {
    return ErrorSelectDateState(this.errorMessage);
  }
}
