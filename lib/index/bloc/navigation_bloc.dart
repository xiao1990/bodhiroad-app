import 'dart:async';
import 'package:bloc/bloc.dart';

class NaviBloc extends Bloc<int, int> {
  static final NaviBloc _NaviBlocSingleton = new NaviBloc._internal();
  factory NaviBloc() {
    return _NaviBlocSingleton;
  }
  NaviBloc._internal();

  int get initialState => 1;

  @override
  Stream<int> mapEventToState(
    int event,
  ) async* {
    try {
      yield event;
    } catch (_, stackTrace) {
      print('$_ $stackTrace');
      yield currentState;
    }
  }
}
