import 'dart:convert';
import 'dart:math' as math;

import 'package:bodhiroad/common/FontFamilyName.dart';
import 'package:bodhiroad/config/CommonConfig.dart';
import 'package:bodhiroad/reader/bean/brwh_catalog.dart';
import 'package:bodhiroad/utils/GlobalConfigUtil.dart';
import 'package:bodhiroad/utils/parameter_convert_cn.dart';
import 'package:flustars/flustars.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

import 'brwh_catalog_list.dart';

class BrwhTopCatalogList extends StatefulWidget {
  final String from;
  final String source;
  final String taskType;
  const BrwhTopCatalogList({this.taskType, this.source, this.from});
  @override
  _BrwhTopCatalogListState createState() => _BrwhTopCatalogListState();
}

class _BrwhTopCatalogListState extends State<BrwhTopCatalogList> {
  Future f;

  @override
  void initState() {
    f = getBuddhist();
    super.initState();
  }

  Future<List<BrwhCatalogBean>> getBuddhist() async {
    List<BrwhCatalogBean> list2;
    List<dynamic> oriList = SpUtil.getObjectList(CommonConfig.BRWH_CATALOG);
    oriList = [];
    if (oriList != null && oriList.length > 0) {
      list2 = oriList.map((item) => BrwhCatalogBean.fromJson(item)).toList();
    } else {
      List qldzjDynamic = jsonDecode(await rootBundle.loadString('assets/brwh/catalog.json'));
      list2 = qldzjDynamic.map((item) => BrwhCatalogBean.fromJson(item)).toList();
      SpUtil.putObjectList(CommonConfig.BRWH_CATALOG, list2);
    }
    return list2;
  }

  List<Widget> getVerticalText(String text) {
    text = text.replaceAll("《", '').replaceAll("》", '');
    text = text.substring(0, math.min(14, text.length));

    int cols = (text.length / 7).ceil();
    List<Widget> list = [];
    for (int i = 0; i < cols; i++) {
      String text2 = text.substring(i * 7, math.min(i * 7 + 7 - 1, text.length - 1) + 1).replaceAll('', '\n');
      list.add(Container(
        width: ScreenUtil.getInstance().getWidth(48),
        height: ScreenUtil.getInstance().screenHeight * 0.5,
        alignment: Alignment.topCenter,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Container(
              width: ScreenUtil.getInstance().getWidth(48),
//              decoration: BoxDecoration(border: Border(left: BorderSide(color: Colors.black26, width: 0.5))),
              alignment: i == 0 ? Alignment.topCenter : Alignment.topRight,
              child: Text(
                text2,
                style: TextStyle(
                    decoration: TextDecoration.none,
                    color: Colors.black87,
                    fontSize: ScreenUtil.getInstance().getSp(30),
                    shadows: [Shadow(color: Colors.black26, blurRadius: 6)],
                    fontWeight: FontWeight.normal,
                    fontFamily: FontFamilyName.SONGTI),
              ),
            )
          ],
        ),
      ));
    }
    return list.reversed.toList();
  }

  @override
  Widget build(BuildContext context) {
//    final AppThemeBloc appThemeBloc = BlocProvider.of<AppThemeBloc>(context);
    return Container(
        child: FutureBuilder<List<BrwhCatalogBean>>(
            future: f,
            builder: (context, AsyncSnapshot<List<BrwhCatalogBean>> snapShot) {
              return snapShot.data != null && snapShot.data.length > 0
                  ? Container(
//                      padding: EdgeInsets.symmetric(horizontal: 15),
                      child: ListView.separated(
                          itemCount: snapShot.data == null ? 0 : snapShot.data.length,
                          physics: BouncingScrollPhysics(),
                          padding: EdgeInsets.all(0),
                          separatorBuilder: (context, index) {
                            return Container(
                              height: 0.5,
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: List.generate(32, (index) {
                                  return Container(
                                    width: 4,
                                    height: 0.5,
                                    color: Colors.black12,
                                  );
                                }).toList(),
                              ),
                            );
                          },
                          itemBuilder: (BuildContext context, index) {
                            return Material(
                              color: Colors.transparent,
                              child: Ink(
                                child: InkWell(
                                    onTap: () {
                                      Navigator.of(context).push(MaterialPageRoute(builder: (BuildContext context) {
                                        return BrwhCatalogList(
                                          id: snapShot.data[index].catalogId,
                                          from: widget.from,
                                          source: widget.source,
                                          taskType: widget.taskType,
                                          catalogName: ParameterConvertUtils.convertCN(snapShot.data[index].title),
                                        );
                                      }));
                                    },
                                    child: Hero(
                                        tag: snapShot.data[index].catalogId,
                                        child: Container(
                                          padding: EdgeInsets.symmetric(horizontal: 15),
                                          height: ScreenUtil.getInstance().getWidth(310.0 + (index == snapShot.data.length - 1 ? 160 : 0)),
                                          margin: EdgeInsets.symmetric(vertical: 6),
                                          child: Row(
                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            children: <Widget>[
                                              Container(
                                                width: ScreenUtil.getInstance().getWidth(213),
                                                height: ScreenUtil.getInstance().getWidth(300),
                                                decoration: BoxDecoration(
                                                    border: Border.all(color: Color(0xFFdbd8d9), width: 1),
                                                    color: Colors.white,
                                                    boxShadow: [BoxShadow(color: Colors.black12, blurRadius: 6)],
                                                    image: DecorationImage(
                                                        fit: BoxFit.cover,
                                                        image: AssetImage(
                                                          'assets/texture/cover2.jpg',
                                                        ))),
//                                    color: Colors.deepOrangeAccent,
                                                padding: EdgeInsets.only(right: ScreenUtil.getInstance().getWidth(25)),
                                                child: Row(
                                                  mainAxisAlignment: MainAxisAlignment.end,
                                                  children: getVerticalText(snapShot.data[index].title),
                                                ),
                                              ),
                                              Expanded(
                                                  child: Container(
                                                height: ScreenUtil.getInstance().getWidth(300),
                                                padding: EdgeInsets.only(
                                                    left: ScreenUtil.getInstance().getWidth(30),
                                                    top: ScreenUtil.getInstance().getWidth(30),
                                                    bottom: ScreenUtil.getInstance().getWidth(30)),
                                                child: Column(
                                                  mainAxisAlignment: MainAxisAlignment.start,
                                                  crossAxisAlignment: CrossAxisAlignment.start,
                                                  children: <Widget>[
                                                    Container(
                                                      height: ScreenUtil.getInstance().getWidth(75),
                                                      child: Text(
                                                        snapShot.data[index].title,
                                                        style: TextStyle(
                                                            fontSize: GlobalConfigUtil.getSubTitleFontSize(),
                                                            fontWeight: FontWeight.bold,
                                                            decoration: TextDecoration.none,
                                                            color: Colors.black87,
                                                            fontFamily: FontFamilyName.SONGTI),
                                                      ),
                                                    ),
                                                  ]..addAll(List.generate(snapShot.data[index].subTitle.length, (idx) {
                                                      return Expanded(
                                                          child: Container(
                                                        alignment: Alignment.centerLeft,
                                                        child: Text(
                                                          '·' + snapShot.data[index].subTitle[idx].title,
                                                          maxLines: 1,
                                                          overflow: TextOverflow.ellipsis,
                                                          style: TextStyle(
                                                              fontSize: GlobalConfigUtil.getTabMainBodyFontSize(),
                                                              decoration: TextDecoration.none,
                                                              fontWeight: FontWeight.normal,
                                                              color: Colors.black54,
                                                              fontFamily: FontFamilyName.SONGTI),
                                                        ),
                                                      ));
                                                    })),
                                                ),
                                              ))
                                            ],
                                          ),
                                        ))),
                              ),
                            );
                          }))
                  : Center(
                      child: SpinKitCircle(
                        size: 20,
                        color: Colors.black54,
                      ),
                    );
            }));
//      },
//    );
  }
}
