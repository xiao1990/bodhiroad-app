import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:bodhiroad/api/api.dart';
import 'package:bodhiroad/task/read/bean/task_catalog.dart';
import 'package:bodhiroad/task/read/bloc/task_event.dart';
import 'package:bodhiroad/task/read/bloc/task_state.dart';

class TaskSetBloc extends Bloc<TaskEvent, TaskState> {
  static final TaskSetBloc _TaskSetBlocSingleton = new TaskSetBloc._internal();
  factory TaskSetBloc() {
    return _TaskSetBlocSingleton;
  }
  TaskSetBloc._internal();

  TaskState get initialState => new TaskInitState();

  int count = 10;
  List<int> list = [1, 2, 3, 4, 5, 6];
  @override
  Stream<TaskState> mapEventToState(
    TaskEvent event,
  ) async* {
    try {
      if (event is AddTaskEvent) {
        event.list[event.taskCatalogIndex].list[event.itemIndex].state = 1;
        count++;
        if (count % 2 == 0) {
          yield GetTaskGatalogState(list: event.list);
        } else {
          yield TaskGatalogShowState(list: event.list, count: count);
        }
      } else if (event is GetTaskGatalogEvent) {
        var rest = await Api.getAllTask();
        if (rest['code'] == 200) {
          List<dynamic> dlist = rest['data'];
          List<TaskCatalog> taskList = dlist.map((tk) => TaskCatalog.fromJson(tk)).toList();
          yield GetTaskGatalogState(list: taskList);
        } else {
          yield GetTaskGatalogState(list: []);
        }
      }
    } catch (_, stackTrace) {
      print('$_ $stackTrace');
      yield currentState;
    }
  }
}
