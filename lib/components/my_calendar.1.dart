import 'dart:convert';

import 'package:bodhiroad/common/calendar_day.dart';
import 'package:bodhiroad/common/select_date_bloc.dart';
import 'package:bodhiroad/common/update_title_bloc.dart';
import 'package:bodhiroad/config/GlobalColorCOnfig.dart';
import 'package:bodhiroad/merits/bloc/merits_bloc.dart';
import 'package:bodhiroad/merits/bloc/merits_event.dart';
import 'package:flustars/flustars.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class MeritsCalendar extends StatefulWidget {
  final double calendarHeight;
  final BoxDecoration decoration;
  final Color activeBackground;
  final Color activeMainTextColor;
  final Color activeSubTextColor;
  final Color borderColor;
  final double fontSize;
  final onTap;

  static const Color defaultActiveBackground = Color(0xFFC3B59A);
  static const Color defaultActiveMainTextColor = Color(0xFF2A1C0E);
  static const Color defaultActiveSubTextColor = Color(0xFF2F2412);
  static const double defaultCalendarHeight = 90;
  static const BoxDecoration defaultDecoration = BoxDecoration();
//  static const weekDayList = ['一','二','三','四','五','六','日'];

  MeritsCalendar(
      {Key key,
      this.activeBackground,
      this.activeMainTextColor,
      this.activeSubTextColor,
      this.borderColor,
      this.calendarHeight,
      this.decoration,
      this.fontSize,
      this.onTap})
      : super(key: key);

  @override
  _MeritsCalendarState createState() => _MeritsCalendarState();
}

class _MeritsCalendarState extends State<MeritsCalendar> {
  int weeks = 0;
  int startIndex = 0;
  DateTime firstDay = DateTime(2019, 6, 3);
  SelectDateBloc selectDateBloc = SelectDateBloc();
  UpdateTitleBloc updateTitleBloc = UpdateTitleBloc();
  List<CalendarDay> list2 = [];
  List<String> weekDayList = ['一', '二', '三', '四', '五', '六', '日'];
  MeritsBloc meritsBloc = MeritsBloc();
  PageController pageController = PageController();
  void _incrementCounter() async {
    DateTime today = DateTime.now();
    print(today.toString().split(" ")[0]);

    String day = await rootBundle.loadString('assets/2019.json');
    List<dynamic> list = jsonDecode(day);

    int currentDayIndex = 0;

    list2 = list.map((json) {
      if (today.toString().split(" ")[0] == json['gregorianDateTime']) {
        json['isToday'] = true;
        json['active'] = true;
        currentDayIndex = list.indexOf(json);
        updateTitleBloc.dispatch(CalendarDay.fromJson(json));
      } else {
        json['isToday'] = false;
        json['active'] = false;
      }
      return CalendarDay.fromJson(json);
    }).toList();
    firstDay = DateTime(
        int.parse(list2[0].yt), int.parse(list2[0].m), int.parse(list2[0].d));
    weeks = ((list2.length + firstDay.weekday - 1) / 7).ceil();
    WidgetsBinding widgetsBinding = WidgetsBinding.instance;
    widgetsBinding.addPostFrameCallback((callback) {
      pageController
          .jumpToPage(((currentDayIndex + firstDay.weekday - 1) / 7).floor());
    });

    setState(() {});
  }

  @override
  void initState() {
    _incrementCounter();
    super.initState();
  }

  @override
  void dispose() {
    pageController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        height: ScreenUtil.getInstance().getWidth(180),
        alignment: Alignment.center,
        child: Container(
          width: MediaQuery.of(context).size.width - 92,
          child: PageView.builder(
            itemCount: weeks.toInt(),
            controller: pageController,
            physics: NeverScrollableScrollPhysics(),
            onPageChanged: (index) {
              meritsBloc.dispatch(MeritsSlideEvent(page: index));
            },
            itemBuilder: (BuildContext context, index) {
              List<CalendarDay> list = [];
              List<CalendarDay> prev = [];
              if (index == 0) {
                list = list2.sublist(
                    0, DateTime.daysPerWeek - firstDay.weekday + 1);
                prev = List.generate(firstDay.weekday - 1, (ix) {
                  return CalendarDay();
                }).toList();
                list.insertAll(0, prev);
                print(prev.length);
              } else {
                int startIndex = DateTime.daysPerWeek -
                    firstDay.weekday +
                    1 +
                    (index - 1) * DateTime.daysPerWeek;
                list = list2.sublist(startIndex, startIndex + 7);
              }

              return Container(
                  height: ScreenUtil.getInstance().getWidth(180),
                  child: Column(
                    children: <Widget>[
                      Row(
                        mainAxisAlignment: index == 0
                            ? MainAxisAlignment.end
                            : MainAxisAlignment.start,
                        children: list
                            .map((item) => InkWell(
                                  child: Stack(
                                    children: <Widget>[
                                      Container(
                                        height: ScreenUtil.getInstance()
                                            .getWidth(180),
                                        padding: EdgeInsets.all(3),
                                        decoration: BoxDecoration(
                                          border: Border.all(
                                              color: item.isToday != null &&
                                                      item.isToday
                                                  ? widget.borderColor
                                                  : Colors.transparent,
                                              width: 0.5),
                                          color: item.isToday != null &&
                                                  item.isToday
                                              ? widget.activeBackground
                                              : Colors.transparent,
                                        ),
                                        width:
                                            (MediaQuery.of(context).size.width -
                                                    92) /
                                                7,
                                        child: Container(
                                          // decoration: BoxDecoration(
                                          //   color: item.isToday != null &&
                                          //           item.isToday
                                          //       ? GlobalColorConfig.buttonColorA
                                          //           .withOpacity(0.4)
                                          //       : Colors.transparent,
                                          // ),
                                          child: Column(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceAround,
                                            children: <Widget>[
                                              Text(
                                                  weekDayList[
                                                      list.indexOf(item)],
                                                  textScaleFactor: 1.0,
                                                  style: TextStyle(
                                                      fontSize: 12,
                                                      color: item.isToday !=
                                                                  null &&
                                                              item.isToday
                                                          ? GlobalColorConfig
                                                              .mainRedColor
                                                          : GlobalColorConfig
                                                              .mainTextColor)),
                                              Text(item.d,
                                                  textScaleFactor: 1.0,
                                                  style: TextStyle(
                                                      fontSize: widget.fontSize,
                                                      color: item.isToday !=
                                                                  null &&
                                                              item.isToday
                                                          ? GlobalColorConfig
                                                              .mainRedColor
                                                          : GlobalColorConfig
                                                              .mainTextColor)),
                                              Text(
                                                item.lDay,
                                                textScaleFactor: 1.0,
                                                style: TextStyle(
                                                    fontSize: 10,
                                                    color:
                                                        item.isToday != null &&
                                                                item.isToday
                                                            ? GlobalColorConfig
                                                                .mainRedColor
                                                            : GlobalColorConfig
                                                                .mainTextColor),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                      Positioned(
                                        right: 6,
                                        top: 3,
                                        child: ClipRRect(
                                          borderRadius:
                                              BorderRadius.circular(10),
                                          child: Container(
                                            height: 6,
                                            width: 6,
                                            color: item.foposa != null
                                                ? Colors.red
                                                : Colors.transparent,
                                          ),
                                        ),
                                      )
                                    ],
                                  ),
                                  onTap: () {
                                    list[list.indexOf(item)].active = true;
                                    widget.onTap(item);
                                    Map map = Map();
                                    map['weekIndex'] = index;
                                    map['dayIndex'] = list.indexOf(item);
                                    selectDateBloc.dispatch(map);
                                    // setState(() {});
                                  },
                                ))
                            .toList(),
                      ),
                    ],
                  ));
            },
          ),
        ));
  }
}
