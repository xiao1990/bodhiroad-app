// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'html_node.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

HtmlNodeBean _$HtmlNodeBeanFromJson(Map<String, dynamic> json) {
  return HtmlNodeBean(
      type: json['type'] as String, text: json['text'] as String);
}

Map<String, dynamic> _$HtmlNodeBeanToJson(HtmlNodeBean instance) =>
    <String, dynamic>{'type': instance.type, 'text': instance.text};
