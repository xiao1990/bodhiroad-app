import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';

class SukhavatiAnimationBloc extends Bloc<Animation, Animation> {
  Animation animation;
  SukhavatiAnimationBloc(this.animation);
  // static final SukhavatiAnimationBloc _sukhavatiAnimationBlocSingleton =
  //     new SukhavatiAnimationBloc._internal();
  // factory SukhavatiAnimationBloc() {
  //   return _sukhavatiAnimationBlocSingleton;
  // }
  // SukhavatiAnimationBloc._internal();

  Animation get initialState => animation;

  @override
  Stream<Animation> mapEventToState(
    Animation event,
  ) async* {
    try {
      yield event;
    } catch (_, stackTrace) {
      print('$_ $stackTrace');
      yield currentState;
    }
  }
}
