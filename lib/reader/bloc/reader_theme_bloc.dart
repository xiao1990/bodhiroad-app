import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:bodhiroad/reader/bean/read_config.dart';
import 'package:bodhiroad/reader/bloc/reader_bloc.dart';
import 'package:bodhiroad/reader/bloc/reader_event.dart';
import 'package:flustars/flustars.dart';

class ReaderThemeBloc extends Bloc<ReaderThemeEvent, ReaderThemeState> {
  static final ReaderThemeBloc _readerThemeBlocSingleton =
      new ReaderThemeBloc._internal();
  factory ReaderThemeBloc() {
    return _readerThemeBlocSingleton;
  }
  ReaderThemeBloc._internal();

  ReaderThemeState get initialState => new ReaderThemeState(ReadConfig());
  ReaderBloc readerBloc = ReaderBloc();
  @override
  Stream<ReaderThemeState> mapEventToState(
    ReaderThemeEvent event,
  ) async* {
    try {
      yield ReaderThemeState(event.readConfig);
      if (event.refresh) {
        readerBloc.dispatch(LoadReaderEvent(readConfig: event.readConfig));
      }
      SpUtil.putObject('readConfig', event.readConfig);
    } catch (_, stackTrace) {
      print('$_ $stackTrace');
      yield currentState;
    }
  }
}

class ReaderThemeEvent {
  final ReadConfig readConfig;
  final bool refresh;
  ReaderThemeEvent(this.readConfig, this.refresh);
}

class ReaderThemeState {
  ReadConfig readConfig;
  ReaderThemeState(this.readConfig);
}
