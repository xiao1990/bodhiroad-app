// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'book_paragraph_unit.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

BookParagraphUnitBean _$BookParagraphUnitBeanFromJson(
    Map<String, dynamic> json) {
  return BookParagraphUnitBean(
      text: json['text'] as String,
      type: json['type'] as int,
      tag: json['tag'] as String,
      textTraditional: json['textTraditional'] as String,
      marked: json['marked'] as int,
      chars: (json['chars'] as List)
          ?.map((e) =>
              e == null ? null : CharBean.fromJson(e as Map<String, dynamic>))
          ?.toList(),
      firstIndex: json['firstIndex'] as int,
      endIndex: json['endIndex'] as int,
      markIndexs: (json['markIndexs'] as List)?.map((e) => e as int)?.toList(),
      bold: json['bold'] as bool,
      colored: json['colored'] as bool,
      image: json['image'] as String,
      isImage: json['isImage'] as bool);
}

Map<String, dynamic> _$BookParagraphUnitBeanToJson(
        BookParagraphUnitBean instance) =>
    <String, dynamic>{
      'text': instance.text,
      'textTraditional': instance.textTraditional,
      'type': instance.type,
      'tag': instance.tag,
      'marked': instance.marked,
      'firstIndex': instance.firstIndex,
      'endIndex': instance.endIndex,
      'bold': instance.bold,
      'image': instance.image,
      'isImage': instance.isImage,
      'colored': instance.colored,
      'markIndexs': instance.markIndexs,
      'chars': instance.chars
    };
