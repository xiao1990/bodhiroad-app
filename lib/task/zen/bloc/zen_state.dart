import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class ZenState extends Equatable {
  ZenState([Iterable props]) : super(props);
}

/// 初始化
class ZenInitState extends ZenState {
  final int position;
  final String countDownTextStr;
  ZenInitState({this.position = 20, this.countDownTextStr})
      : super([position, countDownTextStr]);
  @override
  String toString() => 'ZenInitState';
}

/// 初始化完成
class ZenInitFinishState extends ZenState {
  final int position;
  final String countDownTextStr;
  ZenInitFinishState({this.position = 20, this.countDownTextStr})
      : super([position, countDownTextStr]);
  @override
  String toString() => 'ZenInitFinishState';
}

/// 改变禅修时间
class ZenChangeTimeState extends ZenState {
  final int position;
  final String countDownTextStr;
  ZenChangeTimeState({this.position = 0, this.countDownTextStr = '00:00:00'})
      : super([position, countDownTextStr]);
  @override
  String toString() => 'ZenChangeTimeState';
}

/// 进行中状态
class ZenUnderwayState extends ZenState {
  final int totalTime;
  final int position;
  final String countDownTextStr;
  ZenUnderwayState({this.totalTime, this.position, this.countDownTextStr})
      : super([totalTime, position, countDownTextStr]);
  @override
  String toString() => 'ZenUnderwayState';
}

/// 禅修开始
class ZenStartState extends ZenState {
  final int totalTime;
  final int position;
  final String countDownTextStr;
  ZenStartState({this.totalTime, this.position, this.countDownTextStr})
      : super([totalTime, position, countDownTextStr]);
  @override
  String toString() => 'ZenStartState';
}

/// 禅修暂停状态
class ZenStopState extends ZenState {
  final int position;
  final String countDownTextStr;
  ZenStopState({this.position, this.countDownTextStr})
      : super([position, countDownTextStr]);
  @override
  String toString() => 'ZenStopState';
}

/// 完成状态
class ZenFinishState extends ZenState {
  final int position;
  final String countDownTextStr;
  ZenFinishState({this.position, this.countDownTextStr})
      : super([position, countDownTextStr]);
  @override
  String toString() => 'ZenFinishState';
}

/// 调节音量状态
class ZenVolumeState extends ZenState {
  final double volume;
  ZenVolumeState({this.volume}) : super([volume]);
  @override
  String toString() => 'ZenVolumeState';
}
