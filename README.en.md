#### 项目简介：

本项目作为个人开源项目，主要用于佛弟子个人修行辅助工具，主要功能模块包括：功课，藏经阁，功过格

 **功课：** 包括诵经，念佛，禅修，持咒，拜忏五大类功课，帮助用户自动完成功课记录并生成实时统计报表。

 **藏经阁：** 包括常诵经典，乾隆大藏经，般若文海三个板块，包揽5000多部古代，近代大德文章著述。用户可搜索、阅读、收藏、  标记所有文章

 **功过格：** 帮助用户记录自己日常生活中的过失与善行，并生成实时统计报表

 **大辞典：** 集成4部佛教经典大辞典，累计词条6万余条

项目包含安卓及iOS两个平台，基于谷歌最新的跨平台开发框架Flutter，使用Dart语言开发，服务端使用Java开发。

#### 技术架构

Spring Boot，MyBatis，MySQL，Redis，Apache，Spring Security，JWT，Jsoup，ElasticSearch

#### 截图
 ### 我的功课
<hr/>

![我的功课](https://images.gitee.com/uploads/images/2020/0308/144110_ba47be74_2302199.png "Screenshot_2020-03-08-14-15-08-481_菩提路.png")

 ### 添加功课
<hr/>

![添加功课](https://images.gitee.com/uploads/images/2020/0308/144125_2b875adf_2302199.png "Screenshot_2020-03-08-14-15-20-105_菩提路.png")


 ### 藏经阁--常诵佛经
<hr/>

![输入图片说明](https://images.gitee.com/uploads/images/2020/0308/144605_b61d70cd_2302199.png "Screenshot_2020-03-08-14-15-36-925_菩提路.png")

 ### 藏经阁--乾隆大藏经
<hr/>

![输入图片说明](https://images.gitee.com/uploads/images/2020/0308/144656_99aa595e_2302199.png "Screenshot_2020-03-08-14-15-40-976_菩提路.png")

 ### 藏经阁--般若文海
<hr/>


![输入图片说明](https://images.gitee.com/uploads/images/2020/0308/144805_ef585215_2302199.png "Screenshot_2020-03-08-14-31-31-567_菩提路.png")

 ### 藏经阁--乾隆大藏经
<hr/>

![输入图片说明](https://images.gitee.com/uploads/images/2020/0308/144934_a343522f_2302199.png "Screenshot_2020-03-08-14-32-17-604_菩提路.png")

 ### 藏经阁--般若文海
<hr/>

![输入图片说明](https://images.gitee.com/uploads/images/2020/0308/145016_6948dac4_2302199.png "Screenshot_2020-03-08-14-32-01-459_菩提路.png")

 ### 阅读器
<hr/>

![输入图片说明](https://images.gitee.com/uploads/images/2020/0308/145102_dd9b68ca_2302199.png "Screenshot_2020-03-08-14-15-59-900_菩提路.png")


 ### 阅读器
<hr/>

![输入图片说明](https://images.gitee.com/uploads/images/2020/0308/145147_892ba32b_2302199.png "Screenshot_2020-03-08-14-16-03-987_菩提路.png")

 ### 阅读器
<hr/>

![输入图片说明](https://images.gitee.com/uploads/images/2020/0308/145212_9bcf4466_2302199.png "Screenshot_2020-03-08-14-16-17-401_菩提路.png")

 ### 阅读器
<hr/>

![输入图片说明](https://images.gitee.com/uploads/images/2020/0308/145238_6d686ab1_2302199.png "Screenshot_2020-03-08-14-17-09-719_菩提路.png")

 ### 阅读器
<hr/>

![输入图片说明](https://images.gitee.com/uploads/images/2020/0308/145301_a494a0cb_2302199.png "Screenshot_2020-03-08-14-16-34-271_菩提路.png")

 ### 阅读器
<hr/>

![输入图片说明](https://images.gitee.com/uploads/images/2020/0308/145336_7af1d4ad_2302199.png "Screenshot_2020-03-08-14-25-51-324_菩提路.png")

 ### 阅读器--标记
<hr/>

![输入图片说明](https://images.gitee.com/uploads/images/2020/0308/145355_7c65bfbd_2302199.png "Screenshot_2020-03-08-14-26-36-925_菩提路.png")

 ### 阅读器--查询大辞典
<hr/>

![输入图片说明](https://images.gitee.com/uploads/images/2020/0308/145439_c7fb4eca_2302199.png "Screenshot_2020-03-08-14-20-55-368_菩提路.png")

 ### 功课--诵经
<hr/>

![输入图片说明](https://images.gitee.com/uploads/images/2020/0308/150455_f03a09de_2302199.png "Screenshot_2020-03-08-14-26-58-183_菩提路.png")


 ### 阅读器--分享卡片
<hr/>


![输入图片说明](https://images.gitee.com/uploads/images/2020/0308/145514_3bac5d7a_2302199.png "Screenshot_2020-03-08-14-26-24-603_菩提路.png")

 ### 咒
<hr/>

![输入图片说明](https://images.gitee.com/uploads/images/2020/0308/145541_531f85a6_2302199.png "Screenshot_2020-03-08-14-29-11-741_菩提路.png")

 ### 咒
<hr/>

![输入图片说明](https://images.gitee.com/uploads/images/2020/0308/150549_e226f2dc_2302199.png "Screenshot_2020-03-08-14-29-16-821_菩提路.png")

 ### 功课--念佛
<hr/>

![输入图片说明](https://images.gitee.com/uploads/images/2020/0308/145640_b487dc07_2302199.png "Screenshot_2020-03-08-14-27-15-690_菩提路.png")

 ### 功课--念佛
<hr/>

![输入图片说明](https://images.gitee.com/uploads/images/2020/0308/150035_aa09cdfa_2302199.png "微信图片_20200308150010.png")


 ### 功课--念佛
<hr/>

![输入图片说明](https://images.gitee.com/uploads/images/2020/0308/150053_a8bdeb30_2302199.png "微信图片_20200308150019.png")

 ### 功课--禅修
<hr/>

![输入图片说明](https://images.gitee.com/uploads/images/2020/0308/150126_6a1e5070_2302199.png "Screenshot_2020-03-08-14-27-34-577_菩提路.png")


 ### 功课--禅修
<hr/>

![输入图片说明](https://images.gitee.com/uploads/images/2020/0308/150155_3927970e_2302199.png "Screenshot_2020-03-08-14-27-46-145_菩提路.png")

 ### 功课--禅修背景音乐
<hr/>

![输入图片说明](https://images.gitee.com/uploads/images/2020/0308/150239_bba9b262_2302199.png "Screenshot_2020-03-08-14-28-28-933_菩提路.png")


 ### 功过格
<hr/>

![输入图片说明](https://images.gitee.com/uploads/images/2020/0308/150607_cdac74fe_2302199.png "Screenshot_2020-03-08-14-29-34-069_菩提路.png")

![输入图片说明](https://images.gitee.com/uploads/images/2020/0308/150630_b10fcb57_2302199.png "Screenshot_2020-03-08-14-29-52-527_菩提路.png")

![输入图片说明](https://images.gitee.com/uploads/images/2020/0308/150655_84a2bc9a_2302199.png "Screenshot_2020-03-08-14-30-09-784_菩提路.png")
 ### 功课记录
<hr/>

![输入图片说明](https://images.gitee.com/uploads/images/2020/0308/150729_61b39773_2302199.png "Screenshot_2020-03-08-14-30-42-179_菩提路.png")

 ### 功过格统计
<hr/>

![输入图片说明](https://images.gitee.com/uploads/images/2020/0308/150821_38d9823f_2302199.png "Screenshot_2020-03-08-14-30-51-555_菩提路.png")

 ### 功课记录
<hr/>


 ### 功课记录
<hr/>
 ### 功课记录
<hr/>
