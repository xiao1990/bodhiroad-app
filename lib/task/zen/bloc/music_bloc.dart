import 'package:bloc/bloc.dart';
import 'package:bodhiroad/utils/AudioUtils.dart';

import 'music_event.dart';
import 'music_state.dart';

class MusicBloc extends Bloc<MusicEvent, MusicState> {
  static final MusicBloc _musicBlocSingleton = MusicBloc._internal();
  factory MusicBloc() {
    return _musicBlocSingleton;
  }
  MusicBloc._internal();

  MusicState get initialState => new MusicInitState();

  MusicBloc get musicBloc => _musicBlocSingleton;

  @override
  Stream<MusicState> mapEventToState(MusicEvent event) async* {
    // 开始播放
    if (event is MusicPlayEvent) {
      AudioUtils().play(event.musicUrl);
      yield MusicLoadingState();
      // 加载完成
    } else if (event is MusicLoadedEvent) {
      yield MusicLoadedState(duration: event.duration);
      // 正在播放
    } else if (event is MusicPlayingEvent) {
      yield MusicPlayingState(position: event.position);
      // 暂停播放
    } else if (event is MusicPauseEvent) {
      AudioUtils().pause();
      yield MusicPauseState();
      // 继续播放
    } else if (event is MusicResumeEvent) {
      AudioUtils().resume();
      yield MusicPlayingState();
      // 播放完成
    } else if (event is MusicFinishEvent) {
      AudioUtils().resume();
      yield MusicFinishState();
      // 停止播放
    } else if (event is MusicStopEvent) {
      AudioUtils().stop();
      yield MusicPauseState();
      // 音量调整
    } else if (event is MusicErrorEvent) {
      yield MusicErrorState(error: event.error);
      // 音量调整
    } else if (event is MusicVolumeEvent) {
      AudioUtils().changeLongMusicVolume(event.volume);
    } else if (event is MusicStopConfirmEvent) {
      if (currentState is MusicPlayingState) {
        yield MusicStopConfirmState(error: event.error);
      }
    }
  }
}
