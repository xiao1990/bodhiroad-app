import 'package:json_annotation/json_annotation.dart';
part 'calendar_day.g.dart';

@JsonSerializable()
class CalendarDay {
  String gregorianDateTime;
  String lDay;
  String lMonth;
  String yt;
  String m;
  String d;
  bool isToday;
  bool active;
  String foposa;

  CalendarDay(
      {this.gregorianDateTime = '',
      this.lDay = '',
      this.lMonth = '',
      this.yt = '',
      this.d = '',
      this.m = '',
      this.isToday = false,
      this.foposa,
      this.active = false});
  factory CalendarDay.fromJson(Map<String, dynamic> json) {
    return _$CalendarDayFromJson(json);
  }
  Map<String, dynamic> toJson() => _$CalendarDayToJson(this);
}
