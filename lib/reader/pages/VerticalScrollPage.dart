import 'package:bodhiroad/components/extended_text/lib/extended_text.dart';
import 'package:bodhiroad/components/extended_text/lib/src/selection/extended_text_selection.dart';
import 'package:bodhiroad/components/extended_text_library/lib/extended_text_library.dart';
import 'package:bodhiroad/components/popup_menu/popup_menu.dart';
import 'package:bodhiroad/config/ReaderThemeConfig.dart';
import 'package:bodhiroad/reader/bean/book_page.dart';
import 'package:bodhiroad/reader/bean/bookmark.dart';
import 'package:bodhiroad/reader/bean/line_char.dart';
import 'package:bodhiroad/reader/bean/reader_theme.dart';
import 'package:bodhiroad/reader/bloc.dart';
import 'package:bodhiroad/reader/bloc/progress_bloc.dart';
import 'package:bodhiroad/reader/pages/select_controller.dart';
import 'package:bodhiroad/reader/toolbar_bloc.dart';
import 'package:bodhiroad/utils/GlobalConfigUtil.dart';
import 'package:flustars/flustars.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:global_configuration/global_configuration.dart';
import 'package:zh_char_converter/zh_char_converter.dart';

/// 垂直滚动翻页
class VerticalScrollPage extends StatefulWidget {
  final List<PageBean> pageList;
  final BuddhistReaderBlocBloc bloc;
  final ProgressBloc progressBloc;
  final openBottomMenu;
  final openMarkedMenu;
  final handleMyCut;
  final handleMyCopy;
  final handleMyPaste;
  final handleMySelectAll;
  final handleLike;
  final handleSearch;
  final handleDrawUnderline;
  final handleShare;
  final handleMakeBookMark;
  final handleRecovery;
  VerticalScrollPage({
    this.pageList,
    this.bloc,
    this.progressBloc,
    this.openBottomMenu,
    this.openMarkedMenu,
    this.handleMyCopy,
    this.handleMySelectAll,
    this.handleMyCut,
    this.handleMyPaste,
    this.handleLike,
    this.handleSearch,
    this.handleDrawUnderline,
    this.handleShare,
    this.handleMakeBookMark,
    this.handleRecovery,
  });

  @override
  _VerticalScrollPageState createState() => _VerticalScrollPageState();
}

class _VerticalScrollPageState extends State<VerticalScrollPage> {
  ScrollController _scrollController;
  bool reachBottom = false;
  @override
  void initState() {
    double initPosition = SpUtil.getDouble(GlobalConfigUtil.PREFIX_POSITION + GlobalConfiguration().getInt('source').toString() + GlobalConfiguration().getInt('id').toString());
    _scrollController = ScrollController(initialScrollOffset: initPosition);
    widget.progressBloc.dispatch(initPosition);
    if (initPosition > widget.pageList.last.charList.last.yOffset - ScreenUtil.getInstance().screenHeight) {
      widget.bloc.dispatch(ReaderReachBottomEvent());
    } else {
      widget.bloc.dispatch(ReaderUnReachBottomEvent());
    }
    _scrollController.addListener(() {
      if (_scrollController.offset >= 0) {
        if (_scrollController.offset + ScreenUtil.getInstance().screenHeight >= widget.pageList.last.charList.last.yOffset) {
          widget.bloc.dispatch(ReaderReachBottomEvent());
          reachBottom = true;
        } else {
          if (reachBottom) {
            widget.bloc.dispatch(ReaderUnReachBottomEvent());
            reachBottom = false;
          }
        }
        GlobalConfiguration().updateValue('scrollOffset', _scrollController.offset);
        GlobalConfigUtil.saveReaderProgress(_scrollController.offset);
        widget.progressBloc.dispatch(_scrollController.offset);
      }
    });
    super.initState();
  }

  ToolbarBloc toolbarBloc = ToolbarBloc();
  @override
  Widget build(BuildContext context) {
    PopupMenu.context = context;
    ReaderTheme _readerTheme = ReaderThemeConfig.instance.list[GlobalConfiguration().getInt('themeIndex')];
    return BlocListener(
      bloc: widget.bloc,
      listener: (context, state) {
        if (GlobalConfiguration().getInt('flipType') == 2) {
          if (state is ReaderRestartState) {
            // 回到第一页
            _scrollController.animateTo(0, duration: Duration(milliseconds: 250), curve: Curves.easeInOut);
            GlobalConfigUtil.saveReaderProgress(0);
          } else if (state is ChapterJumpState) {
            _scrollController.jumpTo(state.position);
            widget.progressBloc.dispatch(state.position);
          }
        }
      },
      child: Container(
          decoration: BoxDecoration(color: _readerTheme.backgroundColor),
          width: MediaQuery.of(context).size.width,
//              height: widget.pageList[0].pageHeight,
          padding: EdgeInsets.symmetric(horizontal: ScreenUtil.getInstance().getWidth(18)),
          alignment: Alignment.center,
          child: ListView.builder(
              itemCount: widget.pageList.length,
              controller: _scrollController,
              cacheExtent: 3,
              itemBuilder: (context, index) {
                return ReaderBody(
                  toolBarBloc: toolbarBloc,
                  lineCharList: widget.pageList[index].charList,
                  openMarkedMenu: (Rect rect, int id, String value) {
                    widget.openMarkedMenu(rect, id, value);
                  },
                  openBottomMenu: () {
                    widget.openBottomMenu();
                  },
                  handleDrawUnderline: (value) {
                    value.yOffset += _scrollController.offset;
                    widget.handleDrawUnderline(value);
                  },
                  handleMakeBookmark: (BookMark value) {
                    widget.handleMakeBookMark(value.title);
                  },
                  handleSearch: (String value) {
                    widget.handleSearch(value);
                  },
                  handleShare: (String content) {
                    widget.handleShare(content);
                  },
                  handleRecovery: (String value) {
                    widget.handleRecovery(value);
                  },
                );
              })),
    );
  }
}

class ReaderBody extends StatelessWidget {
  final List<LineChar> lineCharList;
  final handleMakeBookmark;
  final handleMySelectAll;
  final handleLike;
  final handleSearch;
  final handleShare;
  final handleRecovery;
  final handleDrawUnderline;
  final openBottomMenu;
  final openMarkedMenu;
  final ToolbarBloc toolBarBloc;
  const ReaderBody(
      {this.lineCharList,
      this.toolBarBloc,
      this.handleMakeBookmark,
      this.handleDrawUnderline,
      this.handleMySelectAll,
      this.handleLike,
      this.handleSearch,
      this.handleShare,
      this.handleRecovery,
      this.openBottomMenu,
      this.openMarkedMenu});

  String processChars(String chars) {
    return chars
        .replaceAll(';', '\u200B' + '。')
        .replaceAll('：', '\u200B' + '：')
        .replaceAll('!', '\u200B' + '！')
        .replaceAll('、', '\u200B' + '、')
        .replaceAll('。', '\u200B' + '。')
        .replaceAll('，', '\u200B' + '。');
  }

  @override
  Widget build(BuildContext context) {
    ReaderTheme _readerTheme = ReaderThemeConfig.instance.list[GlobalConfiguration().getInt('themeIndex')];
    double lastPointY = 0;
    double lastPointX = 0;
    List<ExtendedText> extList = [];
    List<TextSpan> children = [];
    bool tapped = false;
    lineCharList.forEach((e) {
      children
        ..add(
          TextSpan(
              text: '\u200B',
              style: TextStyle(
                letterSpacing: e.xOffset,
                height: ReaderThemeConfig.fontHeightList[e.tag],
              ),
              children: e.lineGroup
                  .map((char) => char.isMarked && char.maskType == 2
                      ? SpecialTextSpan(
                          text: processChars(GlobalConfiguration().getBool('simplifiedChinese') ? char.chars : zhTW(char.chars)) + (char == e.lineGroup.last ? '\n' : ''),
                          actualText: char.chars,
                          recognizer: TapGestureRecognizer()
                            ..onTapUp = (d) {
                              lastPointY = d.globalPosition.dy;
                              lastPointX = d.globalPosition.dx;
                            }
                            ..onTap = () {
                              if (GlobalConfiguration().getBool('specialTapped') != null) {
                                GlobalConfiguration().updateValue('specialTapped', true);
                              } else {
                                GlobalConfiguration().addValue('specialTapped', true);
                              }
                              print(DateUtil.getNowDateMs());
                              double left;
                              double top;
                              left = MediaQuery.of(context).size.width * 0.05 + (lastPointX / MediaQuery.of(context).size.width) * MediaQuery.of(context).size.width * 0.1;
                              if (lastPointY < MediaQuery.of(context).size.width * 0.4) {
                                top = lastPointY + MediaQuery.of(context).size.width * 0.2;
                              } else {
                                top = lastPointY - 30;
                              }
                              Rect rect = Rect.fromLTWH(left, top, MediaQuery.of(context).size.width * 0.8, MediaQuery.of(context).size.width * 0.2);
                              openMarkedMenu(rect, char.id, char.actualChars);
                              print('special text tapped');
                            },
                          style: TextStyle(
                              fontSize: e.fontSize,
                              backgroundColor: char.isMarked && char.maskType == 2 ? _readerTheme.markColor : Colors.transparent,
//                              decoration: char.isMarked && char.maskType == 2 ? TextDecoration.underline : TextDecoration.none,
//                              decorationColor: Color(0xFFFCA61B),
//                              decorationThickness: 6,
//                              decorationStyle: TextDecorationStyle.dashed,
                              height: ReaderThemeConfig.fontHeightList[e.tag],
                              color: char.isTitle ? _readerTheme.titleColor : _readerTheme.fontColor,
                              letterSpacing: 0,
                              fontWeight: e.tag == 'h1' || e.tag == 'h2' || char.bold ? FontWeight.bold : FontWeight.normal))
                      : TextSpan(
                          text: processChars(GlobalConfiguration().getBool('simplifiedChinese') ? char.chars : zhTW(char.chars)) + (e.lineGroup.last == char ? '\n' : ''),
                          style: TextStyle(
                              fontSize: e.fontSize,
                              backgroundColor: char.isMarked && char.maskType == 1 ? Colors.green : Colors.transparent,
                              height: ReaderThemeConfig.fontHeightList[e.tag],
                              letterSpacing: 0,
                              color: char.isTitle ? _readerTheme.titleColor : _readerTheme.fontColor,
                              fontWeight: e.tag == 'h1' || e.tag == 'h2' || char.bold ? FontWeight.bold : FontWeight.normal)))
                  .toList()),
        );
    });
    MyExtendedMaterialTextSelectionControls ctls = MyExtendedMaterialTextSelectionControls(
        fullTextIndex: lineCharList.first.fullTextIndex,
        delegate0: ExtendedTextSelectionState(),
        toolbarBloc: toolBarBloc,
        handleMakeBookMark: (BookMark value) {
          handleMakeBookmark(value);
        },
        handleDrawUnderline: (BookMark value) {
          handleDrawUnderline(value);
        },
        handleMyCopy: (value) {
//          widget.handleMyCopy(value);
        },
        handleMySelectAll: (value) {
//          widget.handleMySelectAll(value);
        },
        handleMyCut: (value) {
//          widget.handleMyCut(value);
        },
        handleMyPaste: (value) {
//          widget.handleMyPaste(value);
        },
        handleSearch: (value) {
          handleSearch(value);
        },
        handleShare: (value) {
          handleShare(value);
        },
        handleRecovery: (value) {
          handleRecovery(value);
        });
    ExtendedText extendedText;
    extendedText = ExtendedText.rich(
      TextSpan(children: children),
      onTap: () {
        if (GlobalConfiguration().getBool('specialTapped') != null && GlobalConfiguration().getBool('specialTapped')) {
          GlobalConfiguration().updateValue('specialTapped', false);
        } else {
          if (GlobalConfiguration().getBool('tapped') != null && GlobalConfiguration().getBool('tapped')) {
            ctls.delegate0.hideToolbar();
            GlobalConfiguration().updateValue('tapped', false);
          } else {
            openBottomMenu();
          }
        }
      },
      onSpecialTextTap: (v) {
        print('onSpecialTextTap');
      },
      selectionEnabled: true,
      selectionColor: Color(0xFFC8EBFA),
      textSelectionControls: ctls,
    );
    return extendedText;
  }
}
