import 'package:bodhiroad/api/api.dart';
import 'package:bodhiroad/config/GlobalColorCOnfig.dart';
import 'package:bodhiroad/reader/bean/favorite.dart';
import 'package:bodhiroad/reader/bloc.dart';
import 'package:bodhiroad/theme/bloc.dart';
import 'package:bodhiroad/usercenter/bloc/favorite/favorite_bloc.dart';
import 'package:bodhiroad/usercenter/bloc/favorite/favorite_state.dart';
import 'package:bodhiroad/utils/GlobalConfigUtil.dart';
import 'package:bodhiroad/utils/Route.dart';
import 'package:bodhiroad/utils/parameter_convert_cn.dart';
import 'package:flustars/flustars.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:global_configuration/global_configuration.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

class UserFavorite extends StatefulWidget {
  @override
  _UserFavoriteState createState() => _UserFavoriteState();
}

class _UserFavoriteState extends State<UserFavorite> {
//  FavoriteBloc favoriteBloc = FavoriteBloc();
  List<Favorite> list = [];
  AppThemeBloc appThemeBloc = AppThemeBloc();
  Future future;
  BuddhistReaderBlocBloc readerBlocBloc = BuddhistReaderBlocBloc();
  @override
  void initState() {
    getFavorite();
//    readerBlocBloc.dispatch(GetFavoriteEvent());
    super.initState();
  }

  void getFavorite() async {
    if (SpUtil.getString('token') != null && SpUtil.getString('token').length > 0) {
      List<dynamic> oriList = SpUtil.getObjectList('favorite');
      if (oriList != null && oriList.length > 0) {
        list = oriList.map((item) => Favorite.fromJson(item)).toList();
      } else {
        dynamic map = await Api.getFavorite();
        List<dynamic> list2 = map['data'];
        list = list2.map((item) => Favorite.fromJson(item)).toList();
        SpUtil.putObjectList('favorite', list);
      }
      setState(() {});
    }
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder(
      bloc: appThemeBloc,
      builder: (context, ThemeChangeState appThemeState) {
        return BlocBuilder(
            bloc: readerBlocBloc,
            condition: (prev, curt) {
              if (curt is DeleteFavoriteSuccessState) {
                return true;
              } else if (curt is GetFavoriteSuccessState) {
                list = curt.list;
                return true;
              } else {
                return false;
              }
            },
            builder: (context, state) {
              return Scaffold(
                  appBar: AppBar(
                    backgroundColor: Colors.transparent,
                    elevation: 0,
                    title: Text('我的收藏', textScaleFactor: 1.0, style: TextStyle(color: appThemeState.themeConfig.mainTextColor, fontSize: GlobalConfigUtil.getTitleFontSize())),
                    bottom: PreferredSize(
                      preferredSize: Size.fromHeight(ScreenUtil.getInstance().getWidth(280)),
                      child: FavoriteOverview(
                        count: list.length,
                      ),
                    ),
                  ),
                  body: Container(
                      decoration: BoxDecoration(image: DecorationImage(image: AssetImage('assets/texture/bgt2.jpg'), fit: BoxFit.cover)),
                      alignment: Alignment.topCenter,
                      child: Container(
                          width: ScreenUtil.getInstance().screenWidth - ScreenUtil.getInstance().getWidth(80),
                          child: ListView.separated(
                            physics: BouncingScrollPhysics(),
                            itemCount: list.length,
                            separatorBuilder: (context, index) {
                              return Container(
                                height: ScreenUtil.getInstance().getWidth(40),
                              );
                            },
                            itemBuilder: (context, index) {
                              return Slidable(
                                actionPane: SlidableStrechActionPane(),
                                actionExtentRatio: 0.25,
                                child: Container(
//                                  decoration: BoxDecoration(border: Border(bottom: BorderSide(width: 0.5, color: appThemeState.themeConfig.lightBorderColor))),
                                  child: Material(
                                    color: Colors.transparent,
                                    child: Ink(
                                      child: InkWell(
                                          onTap: () {
                                            String url = list[index].url.replaceAll('/', '%2F');
                                            String bookName = ParameterConvertUtils.convertCN(list[index].title);
                                            Routes.router.navigateTo(context,
                                                'bookView?fileUrl=${url}&source=${list[index].type}&id=${list[index].buddhistId}&taskType=0&bookName=${bookName}&version=0');
                                          },
                                          child: Container(
                                              padding: EdgeInsets.symmetric(horizontal: ScreenUtil.getInstance().getWidth(40)),
                                              decoration: BoxDecoration(
                                                  image: DecorationImage(image: AssetImage('assets/texture/item_bg.jpg'), fit: BoxFit.cover),
                                                  borderRadius: BorderRadius.circular(8),
                                                  boxShadow: [BoxShadow(color: Colors.black12, blurRadius: 12)]),
                                              height: ScreenUtil.getInstance().getWidth(180),
                                              child: Row(
                                                children: <Widget>[
                                                  Leadding(type: list[index].type),
                                                  Expanded(
                                                      child: Column(
                                                    crossAxisAlignment: CrossAxisAlignment.start,
                                                    children: <Widget>[
                                                      Container(
                                                        height: ScreenUtil.getInstance().getWidth(20),
                                                        alignment: Alignment.centerLeft,
                                                      ),
                                                      Container(
                                                        height: ScreenUtil.getInstance().getWidth(40),
//                                                        alignment: Alignment.centerLeft,
                                                        padding: EdgeInsets.only(left: ScreenUtil.getInstance().getWidth(40)),
//                                                    decoration: BoxDecoration(border: Border(left: BorderSide(color: Color(0xFFA70400).withOpacity(0.4), width: 6))),
                                                        child: Text(
                                                          list[index].author ?? '',
                                                          style: TextStyle(fontSize: GlobalConfigUtil.getTabMainBodyFontSize(), color: Colors.black38),
                                                        ),
                                                      ),
                                                      Container(
                                                        height: ScreenUtil.getInstance().getWidth(120),
                                                        alignment: Alignment.centerLeft,
                                                        padding: EdgeInsets.only(left: ScreenUtil.getInstance().getWidth(40)),
                                                        child: Text(
                                                          list[index].title ?? '',
                                                          style: TextStyle(fontSize: GlobalConfigUtil.getSubTitleFontSize()),
                                                        ),
                                                      )
                                                    ],
                                                  )),
                                                ],
                                              ))
//                                    ListTile(
//                                      leading: Leadding(type: list[index].type),
//                                      title: Text(
//                                        list[index].title,
//                                        style: TextStyle(fontSize: ScreenUtil.getInstance().getSp(42)),
//                                      ),
//                                      subtitle: Text(
//                                        list[index].author,
//                                        style: TextStyle(fontSize: ScreenUtil.getInstance().getSp(36), color: Colors.black38),
//                                      ),
//                                    ),
                                          ),
                                    ),
                                  ),
                                ),
                                secondaryActions: <Widget>[
                                  IconSlideAction(
                                    caption: '删 除',
                                    color: Colors.red,
                                    icon: Icons.delete,
                                    onTap: () {
                                      GlobalConfiguration().updateValue('id', int.parse(list[index].buddhistId));
                                      GlobalConfiguration().updateValue('source', list[index].type);
                                      readerBlocBloc.dispatch(AddFavoriteEvent(status: 0));
                                      SpUtil.remove('favorite');
//                                        favoriteBloc.dispatch(FavoriteLoadEvent());
                                    },
                                  ),
                                ],
                              );
                            },
                          ))));
            });
      },
    );
  }
}

class FavoriteOverview extends StatefulWidget {
  final int count;
  FavoriteOverview({this.count});
  @override
  _FavoriteOverviewState createState() => _FavoriteOverviewState();
}

class _FavoriteOverviewState extends State<FavoriteOverview> {
  List<Favorite> list = [];
  FavoriteBloc favoriteBloc = FavoriteBloc();
  @override
  void initState() {
//    favoriteBloc.dispatch(FavoriteLoadEvent());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 112,
      padding: EdgeInsets.symmetric(horizontal: 15),
//      color: GlobalColorConfig.mainBackgroundColor,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          Row(
            children: <Widget>[
              Text(
                '大藏经·般若文海',
                textScaleFactor: 1.0,
                style: TextStyle(color: GlobalColorConfig.mainforegroundColor, fontSize: ScreenUtil.getInstance().getSp(60)),
              )
            ],
          ),
          Row(
            children: <Widget>[
              Padding(
                padding: EdgeInsets.only(right: 15),
                child: Icon(
                  MdiIcons.libraryBooks,
                  color: Colors.black38,
                  size: ScreenUtil.getInstance().getSp(40),
                ),
              ),
              BlocBuilder(
                bloc: favoriteBloc,
                condition: (prev, curt) {
                  if (curt is FavoriteLoadState) {
                    return true;
                  } else {
                    return false;
                  }
                },
                builder: (context, state) {
                  return Text('共${widget.count}部经典',
                      textScaleFactor: 1.0, style: TextStyle(color: GlobalColorConfig.mainforegroundColor, fontSize: GlobalConfigUtil.getTabMainBodyFontSize()));
                },
              )
            ],
          )
        ],
      ),
    );
  }
}

class Leadding extends StatelessWidget {
  final int type;
  const Leadding({this.type});
  TextStyle getStyle() {
    return TextStyle(fontSize: ScreenUtil.getInstance().getSp(64), color: Color(0xFFA70400), fontFamily: 'fzys', shadows: [Shadow(color: Colors.black38, blurRadius: 0)]);
  }

  TextStyle getStyle2() {
    return TextStyle(fontSize: ScreenUtil.getInstance().getSp(96), color: Colors.black12.withOpacity(0.1), fontFamily: 'fzys');
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: ScreenUtil.getInstance().getWidth(120),
      height: ScreenUtil.getInstance().getWidth(120),
      decoration: BoxDecoration(color: Color(0xFFF2F2F2), borderRadius: BorderRadius.circular(ScreenUtil.getInstance().getWidth(60))),
      child: Center(
        child: Stack(
          alignment: Alignment.center,
          children: <Widget>[
            Text(
              type == 1 ? '藏' : '海',
              textAlign: TextAlign.center,
              style: getStyle2(),
            ),
            Text(type == 1 ? '龙' : '文', textAlign: TextAlign.center, style: getStyle())
          ],
        ),
      ),
    );
  }
}
