import 'dart:async';

import 'package:bodhiroad/reader/bean/book_mark.dart';
import 'package:bodhiroad/reader/bean/book_page.dart';

import 'book_mark_bloc.dart';
import 'book_mark_state.dart';

abstract class BookMarkEvent {
  Future<BookMarkState> applyAsync(
      {BookMarkState currentState, BookMarkBloc bloc});
}

class LoadBookMarkEvent extends BookMarkEvent {
  @override
  String toString() => 'LoadBookMarkEvent';

  BookmarkBean bookMark;
  String action;
  List<BookmarkBean> bList;
  List<PageBean> pageList;
  int currentPageIndex;
  LoadBookMarkEvent(
      {this.bookMark,
      this.action,
      this.bList,
      this.pageList,
      this.currentPageIndex});

  @override
  Future<BookMarkState> applyAsync(
      {BookMarkState currentState, BookMarkBloc bloc}) async {
    try {
      await Future.delayed(new Duration(seconds: 2));
      return new InBookMarkState();
    } catch (_, stackTrace) {
      print('$_ $stackTrace');
      return new ErrorBookMarkState(_?.toString());
    }
  }
}
