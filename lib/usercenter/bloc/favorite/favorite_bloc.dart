import 'package:bloc/bloc.dart';
import 'package:bodhiroad/reader/bean/favorite.dart';
import 'package:flustars/flustars.dart';

import 'favorite_event.dart';
import 'favorite_state.dart';

class FavoriteBloc extends Bloc<FavoriteEvent, FavoriteState> {
  static final FavoriteBloc favoriteBloc = FavoriteBloc._internal();

  FavoriteBloc._internal();
  factory FavoriteBloc() {
    return favoriteBloc;
  }

  @override
  FavoriteState get initialState => FavoriteInitState();

  @override
  Stream<FavoriteState> mapEventToState(FavoriteEvent event) async* {
    if (event is FavoriteLoadEvent) {
      List<Favorite> list = SpUtil.getObjList('favorite', (item) => Favorite.fromJson(item));
      if (list.length == 0) {
        list = <Favorite>[];
      }
      yield FavoriteLoadState(list, DateTime.now().millisecondsSinceEpoch);
    }
    if (event is FavoriteInitEvent) {
      yield FavoriteInitState();
    }
  }
}
