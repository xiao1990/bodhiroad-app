import 'package:bodhiroad/reader/bean/book_catalog.dart';
import 'package:bodhiroad/reader/bean/book_mark.dart';
import 'package:bodhiroad/reader/bean/book_page.dart';
import 'package:bodhiroad/reader/bean/book_paragraph_unit.dart';
import 'package:bodhiroad/reader/bean/bookmark.dart';
import 'package:bodhiroad/reader/bean/favorite.dart';
import 'package:bodhiroad/reader/bean/text_layout_size.dart';
import 'package:global_configuration/global_configuration.dart';

abstract class BuddhistReaderBlocState {
  bool get reload => false;
  BuddhistReaderBlocState() : super();
}

/// 初始化状态
class InitialBuddhistReaderBlocState extends BuddhistReaderBlocState {
  InitialBuddhistReaderBlocState() : super();
  @override
  bool get reload => false;
}

/// 经文下载状态
class DownloadingBuddhistReaderBlocState extends BuddhistReaderBlocState {
  final String description;
  DownloadingBuddhistReaderBlocState({this.description});
  @override
  bool get reload => true;
}

/// 经文解析状态
class ParsingBuddhistReaderBlocState extends BuddhistReaderBlocState {
  final String description;
  ParsingBuddhistReaderBlocState({this.description});
  @override
  bool get reload => true;
}

/// 正在加载状态
class BuddhistReaderLoadingState extends BuddhistReaderBlocState {
  final String description;
  BuddhistReaderLoadingState({this.description});
  @override
  bool get reload => true;
}

/// 正在加载状态
class SimpleReloadReaderState extends BuddhistReaderBlocState {
  SimpleReloadReaderState();
  @override
  bool get reload => true;
}

/// 正在加载状态
class SimpleReloadFinishReaderState extends BuddhistReaderBlocState {
  SimpleReloadFinishReaderState();
  @override
  bool get reload => true;
}

/// 重载阅读器
class ReloadReaderState extends BuddhistReaderBlocState {
  List<PageBean> pageList;
  List<CatalogBean> catalogList;
  List<BookMark> bookMarkList2;
  List<BookmarkBean> bookMarkList;
  List<BookParagraphUnitBean> unitList;
  Map<String, TextLayoutSize> map;
  String bookHtml;
  GlobalConfiguration configuration;
  ReloadReaderState({this.pageList, this.bookMarkList, this.bookMarkList2, this.catalogList, this.unitList, this.map, this.bookHtml, this.configuration});

  @override
  bool get reload => true;
}

/// 切换主题
class ChangeThemeState extends BuddhistReaderBlocState {
  ChangeThemeState();
  @override
  bool get reload => true;
}

/// 章节跳转
class ChapterJumpState extends BuddhistReaderBlocState {
  final int index;
  final double position;
  ChapterJumpState({this.index, this.position});
  @override
  bool get reload => true;
}

/// 阅读进度
class ReadProgressState extends BuddhistReaderBlocState {
  final double progress;
  ReadProgressState({this.progress});
  @override
  bool get reload => true;
}

/// 阅读完成
class ReaderFinishState extends BuddhistReaderBlocState {
  @override
  String toString() => 'ReaderFinishState';
  @override
  bool get reload => true;
}

/// 到达最后一页
class ReaderReachBottomState extends BuddhistReaderBlocState {
  @override
  String toString() => 'ReaderReachBottomState';
  @override
  bool get reload => true;
}

/// 未到达最后一页
class ReaderUnReachBottomState extends BuddhistReaderBlocState {
  @override
  String toString() => 'ReaderUnReachBottomState';
  @override
  bool get reload => true;
}

/// 重新开始阅读
class ReaderRestartState extends BuddhistReaderBlocState {
  @override
  String toString() => 'ReaderRestartState';
  @override
  bool get reload => true;
}

/// 收藏
class DeleteFavoriteSuccessState extends BuddhistReaderBlocState {
  DeleteFavoriteSuccessState();
  @override
  String toString() => 'DeleteFavoriteSuccessState';
  @override
  bool get reload => true;
}

/// 收藏
class AddFavoriteSuccessState extends BuddhistReaderBlocState {
  AddFavoriteSuccessState();
  @override
  String toString() => 'AddFavoriteSuccessState';
  @override
  bool get reload => true;
}

/// 收藏
class GetFavoriteSuccessState extends BuddhistReaderBlocState {
  final List<Favorite> list;
  GetFavoriteSuccessState(this.list);
  @override
  String toString() => 'GetFavoriteSuccessState';
  @override
  bool get reload => true;
}

/// 打开底部菜单事件
class OpenBottomMenuState extends BuddhistReaderBlocState {
  OpenBottomMenuState();
  @override
  String toString() => 'OpenBottomMenuState';
}

/// 打开底部菜单事件
class OpenBottomMenuSuccessState extends BuddhistReaderBlocState {
  OpenBottomMenuSuccessState();
  @override
  String toString() => 'OpenBottomMenuSuccessState';
}

/// 打开底部菜单事件
class BottomMenuSlidingState extends BuddhistReaderBlocState {
  final double progress;
  BottomMenuSlidingState(this.progress);

  @override
  bool get reload => true;
}

/// 打开底部菜单事件
class RemoveUnderLineState extends BuddhistReaderBlocState {
  RemoveUnderLineState();

  @override
  bool get reload => true;
}
