import 'dart:async';
import 'package:bloc/bloc.dart';

class HeadSlideBloc extends Bloc<double, double> {
  static final HeadSlideBloc _HeadSlideBlocSingleton = new HeadSlideBloc._internal();
  factory HeadSlideBloc() {
    return _HeadSlideBlocSingleton;
  }
  HeadSlideBloc._internal();

  double get initialState => 0.0;

  @override
  Stream<double> mapEventToState(
    double event,
  ) async* {
    try {
      yield event;
    } catch (_, stackTrace) {
      print('$_ $stackTrace');
      yield currentState;
    }
  }
}
