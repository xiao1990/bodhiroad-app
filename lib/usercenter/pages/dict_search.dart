import 'dart:math' as math;

import 'package:bodhiroad/common/FontFamilyName.dart';
import 'package:bodhiroad/components/dialog/MyDialog.dart';
import 'package:bodhiroad/components/expandable/expandable.dart';
import 'package:bodhiroad/components/extended_text/lib/extended_text.dart';
import 'package:bodhiroad/config/CommonConfig.dart';
import 'package:bodhiroad/usercenter/bloc/download_bloc.dart';
import 'package:bodhiroad/usercenter/bloc/download_event.dart';
import 'package:bodhiroad/usercenter/bloc/download_state.dart';
import 'package:bodhiroad/utils/GlobalConfigUtil.dart';
import 'package:bodhiroad/utils/Route.dart';
import 'package:bodhiroad/utils/ToastUtils.dart';
import 'package:flustars/flustars.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

class DictSearch extends StatefulWidget {
  @override
  _DictSearchState createState() => _DictSearchState();
}

class _DictSearchState extends State<DictSearch> {
  TextEditingController _controller = TextEditingController();
  DownloadBloc downloadBloc = DownloadBloc();
  int catalogIndex = 0;
  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  search() async {
    String value = _controller.text.trim().replaceAll(' ', '');
    if (value.length == 0) {
      ToastUtils.fail('还没输入关键字吧?');
      return;
    }
    downloadBloc.dispatch(DictSearchEvent(keyword: value, catalogIndex: catalogIndex));
  }

  List<Widget> getVerticalText(String text, double fontSize, Color color) {
    int cols = (text.length / 10).ceil();
    List<Widget> list = [];
    for (int i = 0; i < cols; i++) {
      String text2 = text.substring(i * 10, math.min(i * 10 + 10 - 1, text.length - 1) + 1).replaceAll('', '\n');
      text2.substring(1, text2.length - 1);
      list.add(Container(
        width: ScreenUtil.getInstance().getWidth(72),
        alignment: Alignment.topLeft,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Container(
              width: ScreenUtil.getInstance().getWidth(72),
              child: Text(
                text2,
                style: TextStyle(decoration: TextDecoration.none, color: color, fontSize: fontSize, fontWeight: FontWeight.normal, fontFamily: FontFamilyName.CHINESESTYLE),
              ),
            )
          ],
        ),
      ));
    }
    return list.reversed.toList();
  }

  seeMore(BuildContext context, String title, String content) {
    showDialog(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context) {
          return StatefulBuilder(
            builder: (BuildContext context, setState) {
              return MyDialog(
                title: title,
                closeText: '我明了了',
                justClose: true,
                height: ScreenUtil.getInstance().getWidth(900),
                content: SingleChildScrollView(
                  physics: BouncingScrollPhysics(),
                  child: ExtendedText.rich(
                    TextSpan(children: [
                      TextSpan(text: content + '\n', style: TextStyle(height: 1.8, fontSize: ScreenUtil.getInstance().getSp(36))),
                    ]),
                    softWrap: true,
                    maxLines: 100,
                  ),
                ),
                onClose: () {
                  Navigator.pop(context);
                },
              );
            },
          );
        });
  }

  List list = [];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          '查询大辞典',
          style: TextStyle(fontSize: GlobalConfigUtil.getTitleFontSize()),
        ),
        backgroundColor: Colors.white,
        elevation: 0,
        actions: <Widget>[
          FlatButton.icon(
              onPressed: () {
                Routes.router.pop(context);
                Routes.router.navigateTo(context, 'dictDownload');
              },
              icon: Icon(
                MdiIcons.setCenter,
                size: ScreenUtil.getInstance().getSp(45),
              ),
              label: Text(
                '词库管理',
                style: TextStyle(fontSize: GlobalConfigUtil.getTabMainBodyFontSize()),
              ))
        ],
      ),
      body: Container(
          decoration: BoxDecoration(image: DecorationImage(image: AssetImage('assets/texture/bgt2.png'), fit: BoxFit.cover)),
          child: BlocBuilder(
            bloc: downloadBloc,
            condition: (prevSate, currentState) {
              if (currentState is DictSearchState) {
                list = currentState.resultList;
                return true;
              } else if (currentState is SelectCatalogState || currentState is SimpleSelectCatalogState) {
                catalogIndex = currentState.index;
                return true;
              } else {
                return false;
              }
            },
            builder: (context, state) {
              return Column(
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.all(ScreenUtil.getInstance().getWidth(40)),
//                    decoration: BoxDecoration(border: Border(bottom: BorderSide(color: Colors.black12, width: 0.5))),
                    child: Column(
                      children: <Widget>[
                        TextField(
                          controller: _controller,
                          maxLines: 6,
                          minLines: 2,
                          cursorColor: Color(0xFFB96652),
                          style: TextStyle(fontSize: ScreenUtil.getInstance().getSp(75), color: Colors.black38),
                          textAlignVertical: TextAlignVertical.top,
                          onSubmitted: (value) {
                            print(value);
                          },
                          showCursor: true,
                          decoration: new InputDecoration(
                            hintText: '输入关键字',
                            hintStyle: TextStyle(color: Colors.black38),
                            border: InputBorder.none,
                          ),
                        ),
                        Row(
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            ButtonTheme(
                                minWidth: ScreenUtil.getInstance().screenWidth * 0.8 * 0.2,
                                height: ScreenUtil.getInstance().getWidth(48),
                                child: FlatButton(
                                    padding: EdgeInsets.all(6),
                                    onPressed: () {
                                      catalogIndex = 0;
                                      search();
                                    },
                                    color: catalogIndex == 0 ? Color(0xFFF2F2F2) : Colors.transparent,
                                    shape: RoundedRectangleBorder(side: BorderSide(color: Colors.black12, width: 0.5), borderRadius: BorderRadius.circular(4)),
                                    child: Text(
                                      '全部',
                                      style: TextStyle(fontSize: ScreenUtil.getInstance().getSp(30)),
                                    )))
                          ]..addAll(List.generate(CommonConfig.dictList.length, (index) {
                              return ButtonTheme(
                                  minWidth: ScreenUtil.getInstance().screenWidth * 0.8 * 0.2,
                                  height: ScreenUtil.getInstance().getWidth(48),
                                  child: FlatButton(
                                      padding: EdgeInsets.all(ScreenUtil.getInstance().getWidth(15)),
                                      color: catalogIndex == index + 1 ? Color(0xFFF2F2F2) : Colors.transparent,
                                      onPressed: () {
                                        catalogIndex = index + 1;
                                        search();
                                      },
                                      shape: RoundedRectangleBorder(side: BorderSide(color: Colors.black12, width: 0.5), borderRadius: BorderRadius.circular(4)),
                                      child: Text(
                                        CommonConfig.dictList[index]['name'],
                                        style: TextStyle(fontSize: GlobalConfigUtil.getTabMainBodyFontSize()),
                                      )));
                            })),
                        ),
                        Row(mainAxisSize: MainAxisSize.max, mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: [
                          Expanded(
                              child: ButtonTheme(
                            padding: EdgeInsets.all(0),
                            minWidth: ScreenUtil.getInstance().getWidth(30),
                            buttonColor: Color(0xFFB96652),
                            height: ScreenUtil.getInstance().getWidth(118),
                            child: RaisedButton(
                                color: Color(0xFFA70400),
                                padding: EdgeInsets.symmetric(horizontal: 12),
                                onPressed: search,
                                elevation: 8,
                                shape: RoundedRectangleBorder(side: BorderSide(color: Color(0xFFF2F2F2), width: 0.5), borderRadius: BorderRadius.circular(4)),
                                child: Text(
                                  '查询',
                                  style: TextStyle(fontSize: GlobalConfigUtil.getSubTitleFontSize(), color: Colors.white),
                                )),
                          ))
                        ]),
                      ],
                    ),
                  ),

                  Expanded(
                      child: Container(
//                    padding: EdgeInsets.only(left: 15, right: 15),
                    child: ListView(physics: BouncingScrollPhysics(), children: <Widget>[
                      Container(
                        width: ScreenUtil.getInstance().screenWidth,
                        child: MyPanel(
                          title: '佛学大辞典',
                          list: list.where((item) => item['source'] == 1).toList(),
                          seeMore: (String title, String content) {
                            seeMore(context, title, content);
                          },
                        ),
                      ),
                      Container(
                        child: MyPanel(
                          title: '佛光大辞典',
                          list: list.where((item) => item['source'] == 2).toList(),
                          seeMore: (String title, String content) {
                            seeMore(context, title, content);
                          },
                        ),
                      ),
                      Container(
                        alignment: Alignment.center,
//                        decoration: BoxDecoration(color: Colors.white, borderRadius: BorderRadius.circular(12), boxShadow: [BoxShadow(color: Colors.black12, blurRadius: 15)]),
                        child: MyPanel(
                          title: '佛学常见词汇',
                          list: list.where((item) => item['source'] == 3).toList(),
                          seeMore: (String title, String content) {
                            seeMore(context, title, content);
                          },
                        ),
                      ),
                      Container(
                        child: MyPanel(
                          title: '中国佛教',
                          list: list.where((item) => item['source'] == 4).toList(),
                          seeMore: (String title, String content) {
                            seeMore(context, title, content);
                          },
                        ),
                      ),
                    ]),
                  ))
//    )
                ],
              );
            },
          )),
    );
  }
}

class MyPanel extends StatefulWidget {
  final String title;
  final List list;
  final seeMore;
  final bool showMore;
  MyPanel({this.title, this.list, this.seeMore, this.showMore = true});
  @override
  _MyPanelState createState() => _MyPanelState();
}

class _MyPanelState extends State<MyPanel> {
  @override
  Widget build(BuildContext context) {
    return widget.list.length > 0
        ? Container(
            margin: EdgeInsets.only(top: 20),
            width: ScreenUtil.getInstance().screenWidth - ScreenUtil.getInstance().getWidth(80),
            decoration: BoxDecoration(color: Colors.white, borderRadius: BorderRadius.circular(12), boxShadow: [BoxShadow(color: Colors.black12, blurRadius: 15)]),
            child: ExpandablePanel(
              header: Container(
                height: ScreenUtil.getInstance().getWidth(120),
                alignment: Alignment.centerLeft,
                padding: EdgeInsets.symmetric(horizontal: 15),
                width: ScreenUtil.getInstance().screenWidth - -ScreenUtil.getInstance().getWidth(80),
                decoration: BoxDecoration(borderRadius: BorderRadius.vertical(top: Radius.circular(12))),
                child: Text(
                  widget.title + '【' + widget.list.length.toString() + '条结果】',
                  style: TextStyle(fontSize: ScreenUtil.getInstance().getSp(48), color: Color(0xFFB96652)),
                ),
              ),
              collapsed: Container(
                alignment: Alignment.centerLeft,
                padding: EdgeInsets.only(bottom: ScreenUtil.getInstance().getWidth(40), left: ScreenUtil.getInstance().getWidth(40), right: ScreenUtil.getInstance().getWidth(40)),
                width: ScreenUtil.getInstance().screenWidth - ScreenUtil.getInstance().getWidth(80),
                child: Text(
                  '【' + widget.list.first['entry'].toString() + '】\n' + widget.list.first['paraphrase'].toString(),
                  maxLines: 2,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(fontSize: GlobalConfigUtil.getSubTitleFontSize(), color: Colors.black54),
                ),
              ),
              expanded: Column(
                children: List.generate(widget.list.length, (index) {
                  return Container(
                    alignment: Alignment.centerLeft,
                    padding: EdgeInsets.only(bottom: 15, left: 15, right: 15),
                    width: ScreenUtil.getInstance().screenWidth - 30,
                    child: ExtendedText.rich(
                      TextSpan(children: [
                        TextSpan(
                            text: '【' + widget.list[index]['entry'].toString() + '】\n',
                            style: TextStyle(fontWeight: FontWeight.bold, height: 1.8, fontSize: GlobalConfigUtil.getSubTitleFontSize())),
                        TextSpan(text: widget.list[index]['paraphrase'].toString(), style: TextStyle(height: 1.5, fontSize: GlobalConfigUtil.getTabMainBodyFontSize())),
                      ]),
                      softWrap: true,
                      overFlowTextSpan: OverFlowTextSpan(
                          text: '...... 查看更多 > ',
                          recognizer: TapGestureRecognizer()
                            ..onTap = () {
                              widget.seeMore(widget.list[index]['entry'], widget.list[index]['paraphrase'].toString());
                            },
                          style: TextStyle(color: Color(0xFFB96652), fontFamily: FontFamilyName.SONGTI, height: 1.8, fontSize: GlobalConfigUtil.getTabMainBodyFontSize())),
                      maxLines: widget.showMore ? 10 : 200,
                    ),
                  );
                }).toList(),
              ),
              tapHeaderToExpand: true,
              hasIcon: true,
              iconColor: Color(0xFFB96652),
            ),
          )
        : Container();
  }
}
