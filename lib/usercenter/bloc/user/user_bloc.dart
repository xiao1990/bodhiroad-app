import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:bodhiroad/api/api.dart';
import 'package:bodhiroad/usercenter/bloc/user/bloc.dart';
import 'package:bodhiroad/utils/ToastUtils.dart';
import 'package:flustars/flustars.dart';
import 'package:global_configuration/global_configuration.dart';

import 'user_event.dart';
import 'user_state.dart';

class UserBloc extends Bloc<UserEvent, UserState> {
  static final UserBloc _UserBlocSingleton = new UserBloc._internal();
  factory UserBloc() {
    return _UserBlocSingleton;
  }
  UserBloc._internal();

  UserState get initialState => new InitialUserState();
  @override
  Stream<UserState> mapEventToState(
    UserEvent event,
  ) async* {
    if (event is UserUpdateEvent) {
      yield InitialUserState();
      GlobalConfiguration().loadFromMap(event.map);
      yield UserUpdateState();
      var res = await Api.updateUserProfile(data: event.map);
      if (res['code'] == 200) {
        ToastUtils.success('修改成功');
        Map map = SpUtil.getObject('user');
        event.map.forEach((k, v) {
          map[k] = v;
        });
        SpUtil.putObject('user', map);
      }
    }
    try {} catch (_, stackTrace) {
      print('$_ $stackTrace');
      yield currentState;
    }
  }
}
