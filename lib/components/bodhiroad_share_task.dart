import 'dart:io';
import 'dart:ui';

import 'package:bodhiroad/config/CommonConfig.dart';
import 'package:bodhiroad/config/GlobalColorCOnfig.dart';
import 'package:bodhiroad/theme/bloc.dart';
import 'package:bodhiroad/utils/ToastUtils.dart';
import 'package:esys_flutter_share/esys_flutter_share.dart';
import 'package:flustars/flustars.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:global_configuration/global_configuration.dart';
import 'package:image_picker_saver/image_picker_saver.dart';
import 'package:screenshot/screenshot.dart';
import 'package:bodhiroad/usercenter/bean/task_statistic.dart';
import 'package:lunar_calendar_converter/lunar_solar_converter.dart';

class BodhiRoadShareTaskDialog extends StatefulWidget {
  final TaskStatistic model;

  BodhiRoadShareTaskDialog({this.model});
  @override
  _BodhiRoadShareTaskDialogState createState() => _BodhiRoadShareTaskDialogState();
}

class _BodhiRoadShareTaskDialogState extends State<BodhiRoadShareTaskDialog> {
  final AppThemeBloc appThemeBloc = AppThemeBloc();
  ScreenshotController screenshotController = ScreenshotController();
  @override
  Widget build(BuildContext context) {
    return BlocBuilder(
      bloc: appThemeBloc,
      builder: (context, ThemeChangeState appThemeState) {
        return Scaffold(
            backgroundColor: Colors.transparent,
            body: Container(
              child: Center(
                child: BackdropFilter(
                    filter: ImageFilter.blur(sigmaX: 10, sigmaY: 10),
                    child: Container(
                      height: ScreenUtil.getInstance().getWidth(1600),
                      width: ScreenUtil.getInstance().getWidth(1000),
                      decoration: BoxDecoration(),
                      child: Stack(
                        children: <Widget>[
                          Screenshot(
                              controller: screenshotController,
                              child: Center(
                                child: Container(
                                  width: ScreenUtil.getInstance().getWidth(1000),
                                  height: ScreenUtil.getInstance().getWidth(1600),
//                                  color: appThemeState.themeConfig.sectionBgColor,
                                  child: Stack(children: [
                                    Center(
                                      child: Container(
                                        width: ScreenUtil.getInstance().getWidth(800),
                                        height: ScreenUtil.getInstance().getWidth(1300),
                                        padding: EdgeInsets.symmetric(horizontal: 15),
                                        decoration: BoxDecoration(
                                            color: Colors.white,
                                            borderRadius: BorderRadius.circular(15),
                                            image: DecorationImage(alignment: Alignment.bottomCenter, image: AssetImage('assets/texture/ss12.png')),
                                            boxShadow: [BoxShadow(color: Colors.black12, blurRadius: 12)]),
                                        child: Column(
                                          crossAxisAlignment: CrossAxisAlignment.center,
                                          children: <Widget>[
                                            Row(mainAxisAlignment: MainAxisAlignment.center, children: [
                                              Container(
                                                  height: ScreenUtil.getInstance().getWidth(120),
                                                  width: ScreenUtil.getInstance().getWidth(600),
                                                  margin: EdgeInsets.only(top: ScreenUtil.getInstance().getWidth(75)),
                                                  alignment: Alignment.center,
                                                  decoration: BoxDecoration(border: Border(bottom: BorderSide(color: Colors.black12, width: 0.5))),
                                                  child: Text(
                                                    GlobalConfiguration().getString('name'),
                                                    textScaleFactor: 1.0,
                                                    style: TextStyle(fontSize: ScreenUtil.getInstance().getSp(60), fontWeight: FontWeight.bold, color: appThemeState.themeConfig.mainTextColor),
                                                  ))
                                            ]),
                                            Container(
                                              height: ScreenUtil.getInstance().getWidth(75),
                                              alignment: Alignment.center,
                                              child: Text(
                                                '今日已完成',
                                                textScaleFactor: 1.0,
                                                style: TextStyle(fontSize: ScreenUtil.getInstance().getSp(30), color: appThemeState.themeConfig.mainTextColor),
                                              ),
                                            ),
                                            Container(
                                              height: ScreenUtil.getInstance().getWidth(880),
//                                              width: ScreenUtil.getInstance()
//                                                  .getWidth(840),
                                              alignment: Alignment.center,
                                              child: Column(
                                                mainAxisAlignment: MainAxisAlignment.center,
                                                children: List.generate(widget.model.list.length, (index) {
                                                  return Container(
                                                    width: ScreenUtil.getInstance().getWidth(780),
                                                    height:
                                                        widget.model.list.length * 90 > 880 ? ScreenUtil.getInstance().getWidth(880 / widget.model.list.length) : ScreenUtil.getInstance().getWidth(90),
//                                                    padding:
//                                                        EdgeInsets.symmetric(
//                                                            vertical: 3),
                                                    alignment: Alignment.centerLeft,
                                                    child: Row(
                                                      mainAxisAlignment: MainAxisAlignment.center,
                                                      children: <Widget>[
                                                        Container(
                                                            alignment: Alignment.center,
                                                            child: Text(
                                                              widget.model.list[index].taskName,
                                                              textScaleFactor: 1.0,
                                                            )),
                                                        Container(
                                                            padding: EdgeInsets.symmetric(horizontal: 8, vertical: 6),
                                                            decoration: BoxDecoration(color: appThemeState.themeConfig.mainBgColor, borderRadius: BorderRadius.circular(20)),
                                                            alignment: Alignment.center,
                                                            child: Text(
                                                                (widget.model.list[index].taskType == 3
                                                                    ? (widget.model.list[index].taskCount / 60).round().toString()
                                                                    : widget.model.list[index].taskCount.toString()),
                                                                textScaleFactor: 1.0,
                                                                style: TextStyle(fontSize: ScreenUtil.getInstance().getSp(36), fontWeight: FontWeight.bold))),
                                                        Text(CommonConfig.taskCountUnit[widget.model.list[index].taskType] ?? 'null'),
                                                      ],
                                                    ),
                                                  );
                                                }),
                                              ),
                                            ),
                                            Container(
                                              height: ScreenUtil.getInstance().getWidth(150),
                                              alignment: Alignment.center,
                                              child: Row(
                                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                children: <Widget>[
                                                  Row(
                                                    children: <Widget>[
                                                      Image.asset(
                                                        'assets/default.png',
                                                        width: ScreenUtil.getInstance().getWidth(60),
                                                        height: ScreenUtil.getInstance().getWidth(60),
                                                      ),
                                                      Text(
                                                        '菩提路',
                                                        textScaleFactor: 1.0,
                                                        style: TextStyle(fontSize: ScreenUtil.getInstance().getSp(30), color: appThemeState.themeConfig.mainTextColor),
                                                      )
                                                    ],
                                                  ),
                                                  Text(
                                                    widget.model.dateString.replaceAll('-', '/') + '·' + getLunar(widget.model.dateString),
                                                    textScaleFactor: 1.0,
                                                    style: TextStyle(fontSize: ScreenUtil.getInstance().getSp(30), color: appThemeState.themeConfig.mainTextColor),
                                                  )
                                                ],
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                    Positioned(
                                      top: ScreenUtil.getInstance().getWidth(75),
                                      child: Container(
                                        width: ScreenUtil.getInstance().getWidth(1000),
                                        height: ScreenUtil.getInstance().getWidth(150),
                                        alignment: Alignment.topCenter,
                                        child: Container(
                                          width: ScreenUtil.getInstance().getWidth(150),
                                          height: ScreenUtil.getInstance().getWidth(150),
                                          decoration: BoxDecoration(
                                              border: Border.all(color: Colors.black12, width: 1),
                                              borderRadius: BorderRadius.circular(ScreenUtil.getInstance().getWidth(75)),
                                              boxShadow: [BoxShadow(color: Colors.black12, blurRadius: 12)]),
                                          child: ClipOval(
                                            child: Image.file(
                                              File(GlobalConfiguration().getString('headPortraitUrl')),
                                              fit: BoxFit.fitHeight,
                                              height: ScreenUtil.getInstance().getWidth(75),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ]),
                                ),
                              )),
//                          关闭按钮
                          //                          底部按钮
                          Positioned(
                              bottom: 0,
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Container(
                                      padding: EdgeInsets.only(top: ScreenUtil.getInstance().getWidth(30)),
                                      width: ScreenUtil.getInstance().getWidth(800),
                                      height: ScreenUtil.getInstance().getWidth(180),
                                      margin: EdgeInsets.symmetric(horizontal: ScreenUtil.getInstance().getWidth(100)),
                                      decoration: BoxDecoration(
                                        color: Color(0xFFF2F2F2),
                                        borderRadius: BorderRadius.vertical(bottom: Radius.circular(ScreenUtil.getInstance().getWidth(30))),
                                      ),
                                      child: Row(
                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                        children: <Widget>[
                                          Expanded(
                                            child: ButtonTheme(
                                              minWidth: ScreenUtil.getInstance().getWidth(400),
                                              height: ScreenUtil.getInstance().getWidth(200),
                                              buttonColor: GlobalColorConfig.mainRedColor,
                                              child: FlatButton.icon(
                                                icon: Icon(
                                                  Icons.file_download,
                                                  color: appThemeState.themeConfig.mainTextColor,
                                                  size: 16,
                                                ),
                                                label: Text(
                                                  '保存到相册',
                                                  textScaleFactor: 1.0,
                                                  style: TextStyle(color: appThemeState.themeConfig.mainTextColor),
                                                ),
                                                onPressed: () {
                                                  screenshotController.capture(pixelRatio: 2).then((File image) async {
                                                    String filePath = await ImagePickerSaver.saveFile(fileData: image.readAsBytesSync());
                                                    if (filePath != null && filePath.length > 0) {
                                                      ToastUtils.success('保存成功');
                                                    }
                                                  }).catchError((onError) {
                                                    print(onError);
                                                  });
                                                },
                                              ),
                                            ),
                                          ),
                                          Expanded(
                                            child: ButtonTheme(
                                              minWidth: ScreenUtil.getInstance().getWidth(400),
                                              height: ScreenUtil.getInstance().getWidth(200),
                                              buttonColor: GlobalColorConfig.mainRedColor,
                                              child: FlatButton.icon(
                                                icon: Icon(
                                                  Icons.send,
                                                  color: appThemeState.themeConfig.mainTextColor,
                                                  size: 16,
                                                ),
                                                label: Text(
                                                  '分 享',
                                                  textScaleFactor: 1.0,
                                                  style: TextStyle(color: appThemeState.themeConfig.mainTextColor),
                                                ),
                                                onPressed: () {
                                                  screenshotController.capture(pixelRatio: 2).then((File image) async {
                                                    await Share.file('分享到', 'esys.png', image.readAsBytesSync(), 'image/png', text: '');
                                                  }).catchError((onError) {
                                                    print(onError);
                                                  });
                                                },
                                              ),
                                            ),
                                          )
                                        ],
                                      )),
                                ],
                              )),
                          Positioned(
                            top: ScreenUtil.getInstance().getWidth(160),
                            right: ScreenUtil.getInstance().getWidth(120),
                            child: Container(
                              width: ScreenUtil.getInstance().getWidth(60),
                              height: ScreenUtil.getInstance().getWidth(60),
                              child: ButtonTheme(
                                padding: EdgeInsets.all(0),
                                child: IconButton(
                                  padding: EdgeInsets.all(0),
                                  icon: Icon(
                                    Icons.close,
                                    color: appThemeState.themeConfig.mainTextColor.withOpacity(0.3),
                                    size: 24,
                                  ),
                                  onPressed: () {
                                    Navigator.pop(context);
                                  },
                                ),
                              ),
                            ),
                          )
                        ],
                      ),
                    )),
              ),
            ));
      },
    );
  }

  String getLunar(String dateString) {
    widget.model.dateString.split('-');
    String lunar = LunarSolarConverter.solarToLunar(Solar(
            solarYear: int.parse(widget.model.dateString.split('-')[0]), solarMonth: int.parse(widget.model.dateString.split('-')[1]), solarDay: int.parse(widget.model.dateString.split('-')[2])))
        .toString();
    return lunar.substring(lunar.length - 4, lunar.length);
  }
}
