import 'package:json_annotation/json_annotation.dart';
part 'music.g.dart';

@JsonSerializable()
class Music {
  int id;
  String musicTitle;
  String musicCover;
  String musicUrl;
  int musicDuration;

  Music({
    this.id,
    this.musicTitle,
    this.musicCover,
    this.musicUrl,
    this.musicDuration,
  });

  factory Music.fromJson(Map<String, dynamic> json) {
    return _$MusicFromJson(json);
  }
  Map<String, dynamic> toJson() => _$MusicToJson(this);
}
