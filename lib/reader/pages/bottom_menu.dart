import 'package:bodhiroad/components/flutter_xlider.dart';
import 'package:bodhiroad/config/ReaderThemeConfig.dart';
import 'package:bodhiroad/reader/bean/read_config.dart';
import 'package:bodhiroad/reader/bean/reader_theme.dart';
import 'package:bodhiroad/reader/bloc.dart';
import 'package:bodhiroad/reader/bloc/light_ajust_bloc.dart';
import 'package:bodhiroad/reader/bloc/progress_bloc.dart';
import 'package:bodhiroad/reader/bloc/reader_theme_bloc.dart';
import 'package:bodhiroad/theme/theme_bloc.dart';
import 'package:bodhiroad/utils/GlobalConfigUtil.dart';
import 'package:bodhiroad/utils/screenUtils.dart';
import 'package:flustars/flustars.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:global_configuration/global_configuration.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

import '../buddhist_reader_bloc_bloc.dart';

class BottomMenu extends StatefulWidget {
  BottomMenu(
      {this.readerConfig,
      this.goLastChapter,
      this.goNextChapter,
      this.onChapterChangeEnd,
      this.onChapterChange,
      this.onFontSizeChange,
      this.onTextDirectionChanged,
      this.onFlipTypeChange,
      this.onLanguageTypeChange,
      this.onReaderThemeChanged,
      this.switchPagingType,
      this.backToList,
      this.addFavorite,
      this.chapterSliderLabel,
      this.chapterSliderMax,
      this.chapterSliderMin,
      this.divisions,
      this.isFavorite,
      this.currentChapterValue = 0.0,
      this.readerThemeBloc,
      this.progressBloc,
      this.progress,
      this.appThemeBloc,
      this.lightAdjustBloc,
      this.openDrawer,
      this.readerBlocBloc});

  final String chapterSliderLabel;
  final double chapterSliderMax;
  final double chapterSliderMin;
  final int divisions;
  final int isFavorite;
  final goLastChapter;
  final goNextChapter;
  final onChapterChange;
  final onChapterChangeEnd;
  final onFontSizeChange;
  final onFlipTypeChange;
  final onLanguageTypeChange;
  final onTextDirectionChanged;
  final onReaderThemeChanged;
  final switchPagingType;
  final backToList;
  final addFavorite;
  final double progress;
  final BuddhistReaderBlocBloc readerBlocBloc;
  final ReadConfig readerConfig;
  final double currentChapterValue;
  final openDrawer;
  final ReaderThemeBloc readerThemeBloc;
  final ProgressBloc progressBloc;
  final AppThemeBloc appThemeBloc;
  final LightAjustBloc lightAdjustBloc;

  @override
  _BottomMenuState createState() => _BottomMenuState();
}

class _BottomMenuState extends State<BottomMenu> with SingleTickerProviderStateMixin {
  TabController tabController;
  @override
  void initState() {
    tabController = TabController(vsync: this, length: 2);
    super.initState();
  }

  ReaderTheme readerThemeA;
  double progress = 0;
  double brightness = GlobalConfigUtil.getDouble('brightness');
  int isFavorite = SpUtil.getInt('favorite_' + GlobalConfiguration().getInt('id').toString() + GlobalConfiguration().getInt('source').toString());
  @override
  Widget build(BuildContext context) {
    return BlocBuilder(
      bloc: widget.readerBlocBloc,
      condition: (prevState, curtState) {
        if (curtState is ReadProgressState) {
          progress = curtState.progress;
        } else if (curtState is AddFavoriteSuccessState) {
          isFavorite = 1;
        } else if (curtState is DeleteFavoriteSuccessState) {
          isFavorite = 0;
        }
        return curtState.reload;
      },
      builder: (context, BuddhistReaderBlocState state) {
        readerThemeA = ReaderThemeConfig.instance.list[GlobalConfiguration().getInt('themeIndex')];

        return Container(
          decoration: BoxDecoration(color: readerThemeA.backgroundColor, borderRadius: BorderRadius.vertical(top: Radius.circular(20))),
          child: Column(
            children: <Widget>[
              Container(
                  height: 48,
                  padding: EdgeInsets.symmetric(horizontal: 20),
//                  decoration: BoxDecoration(border: Border(bottom: BorderSide(color: GlobalColorConfig.mainBorderColor, width: 0.5))),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Row(
                        children: <Widget>[
                          InkWell(
                            borderRadius: BorderRadius.circular(24),
                            onTap: () {
//                              Navigator.pop(context);
                              widget.backToList();
                            },
                            child: Container(
                              height: 48,
                              width: 48,
                              alignment: Alignment.centerLeft,
                              child: Icon(
                                MdiIcons.backburger,
                                color: readerThemeA.menuFontColor,
                                size: ScreenUtil.getInstance().getWidth(60),
                              ),
                            ),
                          ),
                          Container(
                            width: MediaQuery.of(context).size.width * 0.35,
                            child: TabBar(
                              labelStyle: TextStyle(color: readerThemeA.fontColor, fontWeight: FontWeight.bold),
                              labelColor: readerThemeA.tabActiveColor,
                              controller: tabController,
                              indicatorColor: Colors.transparent,
                              indicatorSize: TabBarIndicatorSize.label,
                              labelPadding: EdgeInsets.symmetric(horizontal: 6, vertical: 8),
                              unselectedLabelStyle: TextStyle(color: readerThemeA.fontColor, fontWeight: FontWeight.normal),
                              tabs: <Widget>[
                                Text(
                                  '常规',
                                  textScaleFactor: 1.0,
                                  style: TextStyle(fontFamily: 'songti', color: readerThemeA.fontColor, fontSize: ScreenUtil.getInstance().getAdapterSize(48)),
                                ),
                                Text('显示', textScaleFactor: 1.0, style: TextStyle(fontFamily: 'songti', color: readerThemeA.fontColor, fontSize: ScreenUtil.getInstance().getSp(48)))
                              ],
                            ),
                          )
                        ],
                      ),
                    ],
                  )),
              Expanded(
                child: Container(
                  child: TabBarView(
                    controller: tabController,
                    children: <Widget>[
                      Container(
                        padding: EdgeInsets.symmetric(horizontal: 15),
                        child: ListView(
                          children: <Widget>[
                            Container(
                              height: ScreenUtil.getInstance().getWidth(450),
                              margin: EdgeInsets.only(top: 20),
                              decoration: BoxDecoration(color: readerThemeA.innerBackgroundColor, borderRadius: BorderRadius.circular(ScreenUtil.getInstance().getWidth(24))),
                              child: Column(
                                children: <Widget>[
                                  BlocBuilder(
                                      bloc: widget.progressBloc,
                                      builder: (context, progressState) {
                                        return Container(
                                            height: ScreenUtil.getInstance().getWidth(210),
                                            child: Column(
                                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                              children: <Widget>[
                                                Container(
                                                  height: ScreenUtil.getInstance().getWidth(60),
                                                  alignment: Alignment.bottomCenter,
                                                  child: Text(
                                                    '已读完 ' + ((progressState) * 100 ~/ widget.chapterSliderMax ?? progressState).toString() + '%',
                                                    style: TextStyle(color: readerThemeA.menuFontColor),
                                                  ),
                                                ),
                                                Container(
                                                  height: ScreenUtil.getInstance().getWidth(150),
                                                  width: MediaQuery.of(context).size.width,
                                                  child: Row(
                                                    crossAxisAlignment: CrossAxisAlignment.center,
                                                    children: <Widget>[
                                                      Material(
                                                        color: Colors.transparent,
                                                        child: Ink(
                                                          width: ScreenUtil.getInstance().getWidth(180),
                                                          height: ScreenUtil.getInstance().getWidth(136),
//              color: Color(widget.backColor),
                                                          child: InkWell(
                                                            borderRadius: BorderRadius.all(Radius.circular(30)),
                                                            child: Row(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: <Widget>[
                                                              Icon(
                                                                Icons.navigate_before,
                                                                color: readerThemeA.menuFontColor,
                                                              ),
                                                              Text('上一章', style: TextStyle(color: readerThemeA.menuFontColor, fontSize: ScreenUtil.getInstance().getWidth(36))),
                                                            ]),
                                                            onTap: widget.goLastChapter,
                                                          ),
                                                        ),
                                                      ),
                                                      Expanded(
                                                        // flex: 1,
                                                        child: Container(
                                                            height: ScreenUtil.getInstance().getWidth(136),
                                                            alignment: Alignment.center,
                                                            child: FlutterSlider(
                                                              values: [progressState],
                                                              max: widget.chapterSliderMax ?? progressState,
                                                              min: widget.chapterSliderMin ?? progressState,
                                                              handlerWidth: 20,
                                                              handlerHeight: 20,
                                                              trackBar: FlutterSliderTrackBar(
                                                                  activeTrackBar: BoxDecoration(color: readerThemeA.sliderActiveColor),
                                                                  inactiveTrackBar: BoxDecoration(color: readerThemeA.backgroundColor),
                                                                  activeTrackBarHeight: 2.5,
                                                                  inactiveTrackBarHeight: 2),
                                                              tooltip: FlutterSliderTooltip(disabled: true),
                                                              onDragging: (handlerIndex, lowerValue, upperValue) {
                                                                widget.progressBloc.dispatch(lowerValue);
                                                              },
                                                              handler: FlutterSliderHandler(
                                                                  decoration: BoxDecoration(
                                                                      color: readerThemeA.sliderActiveColor,
                                                                      borderRadius: BorderRadius.circular(10),
                                                                      boxShadow: [BoxShadow(color: Colors.black12, blurRadius: 4)]),
                                                                  child: Container(
                                                                    height: 20,
                                                                    width: 20,
                                                                    alignment: Alignment.center,
                                                                    child: Icon(
                                                                      Icons.keyboard_arrow_right,
                                                                      size: 16,
                                                                      color: readerThemeA.iconBackgroundColor,
                                                                    ),
                                                                  )),
                                                              onDragCompleted: (handlerIndex, lowerValue, upperValue) {
                                                                print('onDragCompleted');
                                                                widget.onChapterChangeEnd(lowerValue);
                                                              },
                                                            )),
                                                      ),
                                                      Material(
                                                        color: Colors.transparent,
                                                        child: Ink(
                                                          width: ScreenUtil.getInstance().getWidth(180),
                                                          height: ScreenUtil.getInstance().getWidth(136),
                                                          child: InkWell(
                                                            borderRadius: BorderRadius.all(Radius.circular(30)),
                                                            child: Row(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: <Widget>[
                                                              Text('下一章', style: TextStyle(color: readerThemeA.menuFontColor, fontSize: ScreenUtil.getInstance().getWidth(36))),
                                                              Icon(
                                                                Icons.navigate_next,
                                                                color: readerThemeA.menuFontColor,
                                                              ),
                                                            ]),
                                                            onTap: widget.goNextChapter,
                                                          ),
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                )
                                              ],
                                            ));
                                      }),
                                  Container(
                                    height: ScreenUtil.getInstance().getWidth(240),
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                                      crossAxisAlignment: CrossAxisAlignment.center,
                                      children: <Widget>[
                                        Container(
                                          child: Column(
                                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                                            children: <Widget>[
                                              Container(
                                                height: 42,
                                                width: 42,
                                                decoration: BoxDecoration(color: readerThemeA.iconBackgroundColor, borderRadius: BorderRadius.circular(24)),
                                                child: IconButton(
                                                  icon: Icon(
                                                    isFavorite == 0 ? Icons.star_border : Icons.star,
                                                    color: readerThemeA.menuFontColor,
                                                    size: ScreenUtil.getInstance().getSp(48),
                                                  ),
                                                  onPressed: () {
                                                    widget.addFavorite(1 - isFavorite);
                                                  },
                                                ),
                                              ),
                                              Text(
                                                '收藏',
                                                textScaleFactor: 1.0,
                                                style: TextStyle(
                                                  fontSize: ScreenUtil.getInstance().getSp(28),
                                                  color: readerThemeA.menuFontColor,
                                                ),
                                              )
                                            ],
                                          ),
                                        ),
                                        Container(
                                          child: Column(
                                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                                            children: <Widget>[
                                              Container(
                                                height: 42,
                                                width: 42,
                                                decoration: BoxDecoration(color: readerThemeA.iconBackgroundColor, borderRadius: BorderRadius.circular(24)),
                                                child: Icon(Icons.headset, color: readerThemeA.menuFontColor, size: ScreenUtil.getInstance().getSp(48)),
                                              ),
                                              Text(
                                                '听经',
                                                textScaleFactor: 1.0,
                                                style: TextStyle(
                                                  fontSize: ScreenUtil.getInstance().getSp(28),
                                                  color: readerThemeA.menuFontColor,
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                        Container(
                                          child: Column(
                                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                                            children: <Widget>[
                                              Container(
                                                height: 42,
                                                width: 42,
                                                decoration: BoxDecoration(color: readerThemeA.iconBackgroundColor, borderRadius: BorderRadius.circular(24)),
                                                child: IconButton(
                                                  icon: Icon(
                                                    Icons.bookmark_border,
                                                    color: readerThemeA.menuFontColor,
                                                    size: ScreenUtil.getInstance().getSp(48),
                                                  ),
                                                  onPressed: () {
                                                    widget.openDrawer(1);
                                                  },
                                                ),
                                              ),
                                              Text(
                                                '标记',
                                                textScaleFactor: 1.0,
                                                style: TextStyle(
                                                  fontSize: ScreenUtil.getInstance().getSp(28),
                                                  color: readerThemeA.menuFontColor,
                                                ),
                                              )
                                            ],
                                          ),
                                        ),
                                        Container(
                                          child: Column(
                                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                                            children: <Widget>[
                                              Container(
                                                height: 42,
                                                width: 42,
                                                decoration: BoxDecoration(color: readerThemeA.iconBackgroundColor, borderRadius: BorderRadius.circular(24)),
                                                child: IconButton(
                                                  icon: Icon(
                                                    Icons.menu,
                                                    color: readerThemeA.menuFontColor,
                                                    size: ScreenUtil.getInstance().getSp(48),
                                                  ),
                                                  onPressed: () {
                                                    widget.openDrawer(0);
                                                  },
                                                ),
                                              ),
                                              Text(
                                                '目录',
                                                textScaleFactor: 1.0,
                                                style: TextStyle(
                                                  fontSize: ScreenUtil.getInstance().getSp(28),
                                                  color: readerThemeA.menuFontColor,
                                                ),
                                              )
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Container(
                              height: ScreenUtil.getInstance().getWidth(90),
                              alignment: Alignment.centerLeft,
                              child: Text('翻页方式', style: TextStyle(color: readerThemeA.menuFontColor, fontSize: ScreenUtil.getInstance().getSp(30))),
                            ),
                            Container(
                              width: MediaQuery.of(context).size.width,
                              height: ScreenUtil.getInstance().getWidth(120),
                              padding: EdgeInsets.symmetric(horizontal: 15),
                              decoration: BoxDecoration(color: readerThemeA.innerBackgroundColor, borderRadius: BorderRadius.circular(ScreenUtil.getInstance().getWidth(24))),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: List.generate(ReaderThemeConfig.flipType.length, (index) {
                                  return Expanded(
                                      flex: 1,
                                      child: Container(
                                        height: ScreenUtil.getInstance().getWidth(90),
                                        child: ButtonTheme(
                                          minWidth: ScreenUtil.getInstance().getWidth(120),
                                          height: ScreenUtil.getInstance().getWidth(90),
                                          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12), side: BorderSide(color: Colors.transparent, width: 0.5)),
                                          child: FlatButton(
                                            padding: EdgeInsets.all(0),
                                            onPressed: () {
                                              widget.onFlipTypeChange(index);
                                            },
                                            child: Text(
                                              ReaderThemeConfig.flipType[index],
                                              style: TextStyle(color: readerThemeA.menuFontColor),
                                            ),
                                            color: GlobalConfiguration().getInt('flipType') == index ? readerThemeA.iconBackgroundColor : readerThemeA.innerBackgroundColor,
                                          ),
                                        ),
                                      ));
                                }),
                              ),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.symmetric(horizontal: 15),
                        child: ListView(
                          physics: BouncingScrollPhysics(),
                          children: <Widget>[
                            // 主题和亮度
                            Container(
                              height: ScreenUtil.getInstance().getWidth(90),
                              alignment: Alignment.centerLeft,
                              child: Text('主题、亮度', style: TextStyle(color: readerThemeA.menuFontColor, fontSize: ScreenUtil.getInstance().getSp(30))),
                            ),

                            Container(
                              height: ScreenUtil.getInstance().getWidth(300),
                              decoration: BoxDecoration(color: readerThemeA.innerBackgroundColor, borderRadius: BorderRadius.circular(ScreenUtil.getInstance().getWidth(24))),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.spaceAround,
                                children: <Widget>[
                                  BlocBuilder(
                                    bloc: widget.lightAdjustBloc,
                                    builder: (context, lightState) {
                                      return Container(
                                        width: MediaQuery.of(context).size.width,
                                        height: ScreenUtil.getInstance().getWidth(120),
                                        padding: EdgeInsets.symmetric(horizontal: 15),
                                        decoration: BoxDecoration(color: readerThemeA.innerBackgroundColor, borderRadius: BorderRadius.circular(ScreenUtil.getInstance().getWidth(24))),
                                        child: Row(
                                          crossAxisAlignment: CrossAxisAlignment.center,
                                          children: <Widget>[
                                            Material(
                                                child: Icon(
                                                  Icons.brightness_low,
                                                  color: readerThemeA.fontColor,
                                                  size: ScreenUtil.getInstance().getWidth(60),
                                                ),
                                                color: Colors.transparent),
                                            Expanded(
                                              child: Container(
                                                  height: ScreenUtil.getInstance().getWidth(136),
                                                  alignment: Alignment.center,
                                                  child: FlutterSlider(
                                                    values: [brightness],
                                                    max: 100,
                                                    min: 0,
                                                    handlerWidth: 20,
                                                    handlerHeight: 20,
                                                    trackBar: FlutterSliderTrackBar(
                                                        activeTrackBar: BoxDecoration(color: readerThemeA.sliderActiveColor),
                                                        inactiveTrackBar: BoxDecoration(color: readerThemeA.backgroundColor),
                                                        activeTrackBarHeight: 2.5,
                                                        inactiveTrackBarHeight: 2),
                                                    tooltip: FlutterSliderTooltip(disabled: true),
                                                    handler: FlutterSliderHandler(
                                                        decoration: BoxDecoration(
                                                            color: readerThemeA.sliderActiveColor,
                                                            borderRadius: BorderRadius.circular(10),
                                                            boxShadow: [BoxShadow(color: Colors.black12, blurRadius: 4)]),
                                                        child: Container(
                                                          height: 20,
                                                          width: 20,
                                                          alignment: Alignment.center,
                                                          child: Icon(
                                                            Icons.keyboard_arrow_right,
                                                            size: 16,
                                                            color: readerThemeA.iconBackgroundColor,
                                                          ),
                                                        )),
                                                    onDragging: (handlerIndex, lowerValue, upperValue) {
                                                      widget.lightAdjustBloc.dispatch(lowerValue);
                                                      ScreenUtils.setBrightness(lowerValue / 100.0);
                                                      GlobalConfigUtil.updateValue('brightness', lowerValue);
//                                                      }
                                                    },
                                                  )),
                                            ),
                                            Material(
                                              color: Colors.transparent,
                                              child: Icon(
                                                Icons.brightness_high,
                                                color: readerThemeA.fontColor,
                                                size: ScreenUtil.getInstance().getWidth(60),
//                    color: Color(widget.frontColor),
                                              ),
                                            ),
                                          ],
                                        ),
                                      );
                                    },
                                  ),
                                  Container(
                                      width: ScreenUtil.getInstance().screenWidth - 30,
                                      height: ScreenUtil.getInstance().getWidth(90),
                                      padding: EdgeInsets.symmetric(horizontal: 15),
                                      child: ListView(
                                        scrollDirection: Axis.horizontal,
                                        physics: BouncingScrollPhysics(),
//                                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                                        children: List.generate(ReaderThemeConfig.instance.list.length, (index) {
                                          return Container(
                                            height: ScreenUtil.getInstance().getWidth(90),
                                            width: ScreenUtil.getInstance().getWidth(90),
                                            margin: EdgeInsets.only(left: 30),
                                            decoration: BoxDecoration(borderRadius: BorderRadius.circular(45), boxShadow: [BoxShadow(color: Colors.black12, blurRadius: 4)]),
                                            child: ButtonTheme(
                                              minWidth: ScreenUtil.getInstance().getWidth(90),
                                              height: ScreenUtil.getInstance().getWidth(90),
                                              shape: RoundedRectangleBorder(
                                                borderRadius: BorderRadius.circular(24),
                                              ),
                                              child: FlatButton(
                                                padding: EdgeInsets.all(0),
                                                onPressed: () {
                                                  widget.onReaderThemeChanged(index);
                                                },
                                                child: Text(
                                                  '经',
                                                  style: TextStyle(color: ReaderThemeConfig.instance.list[index].fontColor),
                                                ),
                                                color: ReaderThemeConfig.instance.list[index].backgroundColor,
                                              ),
                                            ),
                                          );
                                        }),
                                      ))
                                ],
                              ),
                            ),
                            Container(
                              height: ScreenUtil.getInstance().getWidth(90),
                              alignment: Alignment.centerLeft,
                              child: Text(
                                '字体大小',
                                style: TextStyle(
                                  color: readerThemeA.menuFontColor,
                                  fontSize: ScreenUtil.getInstance().getSp(30),
                                ),
                              ),
                            ),
                            // 字体大小滑块
                            FontSizeSlider(
                              readerTheme: readerThemeA,
                              currentValue: GlobalConfiguration().getInt('fontSize').toDouble(),
                              onFontSizeChange: (double value) {
                                widget.onFontSizeChange(value);
                              },
                            ),
                            Container(
                              height: ScreenUtil.getInstance().getWidth(90),
                              alignment: Alignment.centerLeft,
                              child: Text('文字方向', style: TextStyle(color: readerThemeA.menuFontColor, fontSize: ScreenUtil.getInstance().getSp(30))),
                            ),
                            Container(
                              width: MediaQuery.of(context).size.width,
                              height: ScreenUtil.getInstance().getWidth(120),
                              padding: EdgeInsets.symmetric(horizontal: 15),
                              decoration: BoxDecoration(color: readerThemeA.innerBackgroundColor, borderRadius: BorderRadius.circular(ScreenUtil.getInstance().getWidth(24))),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Text('水平文字', style: TextStyle(color: readerThemeA.menuFontColor, fontSize: ScreenUtil.getInstance().getSp(36))),
                                  CupertinoSwitch(
                                    value: GlobalConfiguration().getInt('textLayOutDirection') == 0,
                                    activeColor: readerThemeA.buttonActiveColor,
                                    onChanged: (value) {
                                      widget.onTextDirectionChanged(value);
                                    },
                                  )
                                ],
                              ),
                            ),

                            Container(
                              height: ScreenUtil.getInstance().getWidth(90),
                              alignment: Alignment.centerLeft,
                              child: Text('简繁转换', style: TextStyle(color: readerThemeA.menuFontColor, fontSize: ScreenUtil.getInstance().getSp(30))),
                            ),
                            Container(
                              width: MediaQuery.of(context).size.width,
                              height: ScreenUtil.getInstance().getWidth(120),
                              padding: EdgeInsets.symmetric(horizontal: 15),
                              decoration: BoxDecoration(color: readerThemeA.innerBackgroundColor, borderRadius: BorderRadius.circular(ScreenUtil.getInstance().getWidth(24))),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Text('简体', style: TextStyle(color: readerThemeA.menuFontColor, fontSize: ScreenUtil.getInstance().getSp(36))),
//                                  XlivSwitch(
//                                    activeColor: Colors.red,
//                                    unActiveColor: Colors.red,
//                                    thumbColor: readerThemeA.buttonFrontColor,
//                                    value: GlobalConfiguration().getBool('simplifiedChinese'),
//                                    onChanged: (value) {
//                                      widget.onLanguageTypeChange(value);
//                                    },
//                                  ),
                                  CupertinoSwitch(
                                    value: GlobalConfiguration().getBool('simplifiedChinese'),
                                    activeColor: readerThemeA.buttonActiveColor,
                                    onChanged: (value) {
                                      widget.onLanguageTypeChange(value);
                                    },
                                  )
                                ],
                              ),
                            ),
                            Container(
                              height: ScreenUtil.getInstance().getWidth(90),
                              alignment: Alignment.centerLeft,
                              // child: Text(''),
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              )
            ],
          ),
        );
      },
    );
  }
}

class FontSizeSlider extends StatefulWidget {
  final ReaderTheme readerTheme;
  final double currentValue;
  final onFontSizeChange;
  FontSizeSlider({this.readerTheme, this.currentValue, this.onFontSizeChange});

  @override
  _FontSizeSliderState createState() => _FontSizeSliderState();
}

class _FontSizeSliderState extends State<FontSizeSlider> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: ScreenUtil.getInstance().getWidth(120),
      padding: EdgeInsets.symmetric(horizontal: 15),
      decoration: BoxDecoration(color: widget.readerTheme.innerBackgroundColor, borderRadius: BorderRadius.circular(ScreenUtil.getInstance().getWidth(24))),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Material(
            color: Colors.transparent,
            child: Icon(
              MdiIcons.formatAnnotationMinus,
              color: widget.readerTheme.fontColor,
              size: ScreenUtil.getInstance().getWidth(60),
//                    color: Color(widget.frontColor),
            ),
          ),
          Expanded(
              // flex: 1,
              child: Container(
                  height: ScreenUtil.getInstance().getWidth(136),
                  alignment: Alignment.center,
                  child: FlutterSlider(
                    values: [widget.currentValue],
                    max: ReaderThemeConfig.fontSizeList.length.toDouble() - 1,
                    min: 0,
                    handlerWidth: 20,
                    handlerHeight: 20,
                    step: 1.0,
                    trackBar: FlutterSliderTrackBar(
                        activeTrackBar: BoxDecoration(color: widget.readerTheme.sliderActiveColor),
                        inactiveTrackBar: BoxDecoration(color: widget.readerTheme.backgroundColor),
                        activeTrackBarHeight: 2.5,
                        inactiveTrackBarHeight: 2),
                    tooltip: FlutterSliderTooltip(disabled: true),
                    handler: FlutterSliderHandler(
                        decoration: BoxDecoration(color: widget.readerTheme.sliderActiveColor, borderRadius: BorderRadius.circular(10), boxShadow: [BoxShadow(color: Colors.black12, blurRadius: 4)]),
                        child: Container(
                          height: 20,
                          width: 20,
                          alignment: Alignment.center,
                          child: Icon(
                            Icons.keyboard_arrow_right,
                            size: 16,
                            color: widget.readerTheme.iconBackgroundColor,
                          ),
                        )),
                    onDragging: (handlerIndex, lowerValue, upperValue) {
                      widget.onFontSizeChange(lowerValue);
                      GlobalConfiguration().updateValue('fontSize', double.parse(lowerValue.toString()).toInt());
                    },
                    jump: true,
                  ))),
          Material(
              color: Colors.transparent,
              child: Icon(
                MdiIcons.formatAnnotationPlus,
                color: widget.readerTheme.fontColor,
                size: ScreenUtil.getInstance().getWidth(60),
//                    color: Color(widget.frontColor),
              )),
        ],
      ),
    );
  }
}
