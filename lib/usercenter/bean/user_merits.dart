import 'package:json_annotation/json_annotation.dart';
part 'user_merits.g.dart';

@JsonSerializable()
class UserMerits {
  int id;
  String userId;
  int meritsId;
  String lastUpdate;
  int isGood;

  UserMerits({
    this.id,
    this.userId,
    this.meritsId,
    this.lastUpdate,
    this.isGood,
  });
  factory UserMerits.fromJson(Map<String, dynamic> json) {
    return _$UserMeritsFromJson(json);
  }
  Map<String, dynamic> toJson() => _$UserMeritsToJson(this);
}
