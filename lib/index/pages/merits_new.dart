import 'package:bodhiroad/components/my_calendar.1.dart';
import 'package:bodhiroad/components/popup_menu/popup_menu.dart';
import 'package:bodhiroad/config/GlobalColorCOnfig.dart';
import 'package:bodhiroad/merits/bean/merits.dart';
import 'package:bodhiroad/merits/bloc/merits_bloc.dart';
import 'package:bodhiroad/merits/bloc/merits_event.dart';
import 'package:bodhiroad/merits/bloc/merits_state.dart';
import 'package:bodhiroad/theme/bloc.dart';
import 'package:bodhiroad/utils/GlobalConfigUtil.dart';
import 'package:bodhiroad/utils/ToastUtils.dart';
import 'package:flustars/flustars.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class MeritsNew extends StatefulWidget {
  Merits merits;
  final int belong;
  MeritsNew({this.merits, this.belong});
  @override
  _MeritsNewState createState() => _MeritsNewState();
}

class _MeritsNewState extends State<MeritsNew> {
  AppThemeBloc appThemeBloc = new AppThemeBloc();
  ScrollController scrollController;
  MeritsBloc meritsBloc = MeritsBloc();
  @override
  void initState() {
    scrollController = ScrollController(initialScrollOffset: 0);
    super.initState();
  }

  void customBackground(bool isLast, int meritId, String date, int belong, int isGood, GlobalKey btnKey2, int catalogId, int rowId, int columnId,
      {Color bgColor, Color fontColor}) {
    if (DateTime.now().weekday < columnId + 1) {
      ToastUtils.fail('是日未到！');
      return;
    }
    PopupMenu menu = PopupMenu(
        backgroundColor: Color(0xFF2B7497),
        lineColor: Colors.transparent,
        isLast: isLast,
        maxColumn: 2,
        itWidth: ScreenUtil.getInstance().getWidth(180),
        itHeight: ScreenUtil.getInstance().getWidth(140),
        // maxColumn: 2,
        items: [
          MenuItem(
              textStyle: TextStyle(color: fontColor),
              title: isGood != 1 ? '功' : '过',
              image: Icon(Icons.done, color: fontColor),
              userInfo: {"meritId": meritId, "isGood": isGood == 1 ? 2 : 1}),
          MenuItem(
              textStyle: TextStyle(color: fontColor),
              title: isGood == 0 ? '过' : '清除',
              userInfo: {"meritId": meritId, "isGood": isGood == 0 ? 2 : 0},
              image: Icon(
                Icons.close,
                color: Colors.white,
              )),
        ],
        onClickMenu: (MenuItemProvider item) {
          if (item is MenuItem) {
            meritsBloc.dispatch(AddUserMeritsEvent(
                catalogId: catalogId,
                rowId: rowId,
                columnId: columnId,
                merits: widget.merits,
                date: date,
                belong: belong,
                operation: isGood > 0 ? 'update' : 'add',
                meritId: int.parse(item.userInfo['meritId'].toString()),
                isGood: int.parse(item.userInfo['isGood'].toString())));
          }
        },
        onDismiss: () {});
    menu.show(widgetKey: btnKey2);
  }

  Widget getTr(Merits merits, int catalogIndex, int rowId, ThemeChangeState appThemeState) {
    return Container(
      height: ScreenUtil.getInstance().getWidth(120),
      margin: EdgeInsets.symmetric(horizontal: ScreenUtil.getInstance().getWidth(25)),
      decoration: BoxDecoration(
//          image: DecorationImage(
//              image: AssetImage('assets/item-back2.jpg'), fit: BoxFit.cover),
          border: Border(
              top: rowId == 0 ? BorderSide(color: appThemeState.themeConfig.mainBorderColor, width: 0.2) : BorderSide.none,
              bottom: BorderSide(color: appThemeState.themeConfig.mainBorderColor, width: 0.2))),
      child: Row(
        children: <Widget>[
          Container(
            width: ScreenUtil.getInstance().getWidth(180),
            decoration: BoxDecoration(
                border: Border(
                    left: BorderSide(color: appThemeState.themeConfig.mainBorderColor, width: 0.2),
                    right: BorderSide(color: appThemeState.themeConfig.mainBorderColor, width: 0.2))),
            child: Center(
              child: Text(
                merits.title.replaceAll(' ', '\n'),
                textScaleFactor: 1.0,
                style: TextStyle(
                  fontSize: GlobalConfigUtil.getTabMainBodyFontSize(),
                  color: appThemeState.themeConfig.mainTextColor,
                ),
              ),
            ),
            alignment: Alignment.center,
          ),
          Expanded(
              child: Container(
            child: BlocBuilder(
                bloc: meritsBloc,
                builder: (context, state) {
                  return Container(
                      child: Row(
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: List.generate(merits.userMerits.length, (columnId) {
                            return getTd(
                              appThemeState,
                              isGood: merits.userMerits[columnId].isGood,
                              date: merits.userMerits[columnId].lastUpdate,
                              meritId: merits.id,
                              isLast: columnId == 6,
                              catalogId: catalogIndex,
                              rowId: rowId,
                              columnId: columnId,
                            );
                          }).toList()));
                }),
          ))
        ],
      ),
    );
  }

  Widget getTd(ThemeChangeState appThemeState, {int catalogId, int rowId, int columnId, int isGood, String date, int meritId, bool isLast}) {
    GlobalKey btnKey2 = GlobalKey();
    return Expanded(
      flex: 1,
      child: Container(
        alignment: Alignment.center,
        decoration: BoxDecoration(
            color: columnId + 1 == DateTime.now().weekday ? appThemeState.themeConfig.selectedBgColor : Colors.transparent,
            border: Border(right: BorderSide(color: appThemeState.themeConfig.mainBorderColor, width: 0.2))),
        child: IconButton(
          key: btnKey2,
          onPressed: () {
            print(DateTime.now().weekday);
            customBackground(isLast, meritId, date, widget.belong, isGood, btnKey2, catalogId, rowId, columnId,
                bgColor: appThemeState.themeConfig.popMenuColor, fontColor: appThemeState.themeConfig.popMenuTextColor);
          },
          icon: Icon(
            isGood == 1 ? Icons.done : isGood == 2 ? Icons.close : Icons.trip_origin,
            size: ScreenUtil.getInstance().getSp(48),
            color: isGood == 1 ? Colors.green : isGood == 2 ? GlobalColorConfig.mainRedColor : Colors.black12,
          ),
        ),
      ),
    );
  }

  Widget getColumnFirst(String title) {
    return Container(
      width: 72,
      decoration: BoxDecoration(border: Border(right: BorderSide(color: GlobalColorConfig.mainBorderColor, width: 0.5))),
      child: Center(
        child: Text(
          title,
          style: TextStyle(fontSize: 14, color: GlobalColorConfig.mainTextColor, fontWeight: FontWeight.bold),
        ),
      ),
      alignment: Alignment.center,
    );
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder(
        bloc: appThemeBloc,
        builder: (context, ThemeChangeState appThemeState) {
          return Scaffold(
              body: NestedScrollView(
            controller: scrollController,
            headerSliverBuilder: (context, isScroll) {
              return [
                SliverAppBar(
                  backgroundColor: appThemeState.themeConfig.sectionBgColor,
                  centerTitle: false,
                  title: Text(widget.merits.catalogName,
                      textScaleFactor: 1.0, style: TextStyle(color: appThemeState.themeConfig.mainTextColor, fontSize: GlobalConfigUtil.getTitleFontSize())),
                  pinned: true,
                  floating: true,
                  elevation: 4,
                  leading: IconButton(
                    icon: Icon(
                      Icons.close,
                      color: appThemeState.themeConfig.mainTextColor,
                    ),
                    onPressed: () {
                      Navigator.pop(context);
                    },
                  ),
                  // expandedHeight: 150,
                  bottom: PreferredSize(
                    preferredSize: Size.fromHeight(ScreenUtil.getInstance().getWidth(180)),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Container(
                          width: MediaQuery.of(context).size.width,
                          height: ScreenUtil.getInstance().getWidth(180),
                          padding: EdgeInsets.symmetric(horizontal: 10),
                          // decoration: BoxDecoration(
                          //     border: Border.all(
                          //         color: appThemeState
                          //             .themeConfig.lightBorderColor,
                          //         width: 0.5)),
                          child: Row(
                            children: <Widget>[
                              Container(
                                width: 72,
                                alignment: Alignment.center,
                                child: IconButton(
                                  icon: Icon(Icons.ac_unit),
                                  onPressed: () {},
                                ),
                                // decoration: BoxDecoration(
                                //     border: Border.all(
                                //         color: appThemeState
                                //             .themeConfig.lightBorderColor,
                                //         width: 0.5)),
                              ),
                              Expanded(
                                child: MeritsCalendar(
                                  activeMainTextColor: appThemeState.themeConfig.mainTextColor,
                                  borderColor: appThemeState.themeConfig.lightBorderColor,
                                  activeBackground: appThemeState.themeConfig.mainBgColor,
                                  decoration: BoxDecoration(
                                    gradient: LinearGradient(colors: [Colors.deepOrangeAccent, Colors.deepOrange]),
                                  ),
                                ),
                              )
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                )
              ];
            },
            body: BlocBuilder(
              bloc: meritsBloc,
              condition: (previousState, currentState) {
                if (currentState is MarkMeritsState) {
                  widget.merits = currentState.merits;
                  return true;
                } else {
                  return false;
                }
              },
              builder: (context, meritsState) {
                return Stack(
                  children: <Widget>[
                    Container(
                      decoration: BoxDecoration(
                          gradient: LinearGradient(begin: Alignment.topCenter, end: Alignment.bottomCenter, colors: [Colors.white, Colors.white.withOpacity(0)]),
                          image: DecorationImage(fit: BoxFit.cover, image: AssetImage('assets/texture/bgt2.jpg'))),
                    ),
                    Container(
                      decoration: BoxDecoration(
                        gradient: LinearGradient(begin: Alignment.topCenter, end: Alignment.bottomCenter, colors: [Colors.white, Colors.white.withOpacity(0)]),
                      ),
                    ),
                    ListView.builder(
                      itemCount: widget.merits.list.length,
                      physics: BouncingScrollPhysics(),
                      padding: EdgeInsets.all(0),
                      itemBuilder: (context, index) {
                        return getTr(widget.merits.list[index], 0, index, appThemeState);
                      },
                    )
                  ],
                );
              },
            ),
          ));
        });
  }
}
