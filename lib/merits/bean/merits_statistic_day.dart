import 'package:json_annotation/json_annotation.dart';

import 'merits.dart';
part 'merits_statistic_day.g.dart';

@JsonSerializable()
class MeritsStatisticDay {
  String day;
  List<Merits> list;

  MeritsStatisticDay({
    this.day,
    this.list,
  });
  factory MeritsStatisticDay.fromJson(Map<String, dynamic> json) {
    return _$MeritsStatisticDayFromJson(json);
  }
  Map<String, dynamic> toJson() => _$MeritsStatisticDayToJson(this);
}
