import 'package:json_annotation/json_annotation.dart';
part 'book_mark.g.dart';

@JsonSerializable()
class BookmarkBean {
  String bookId;
  String title;
  int pageIndex;
  double position;

  BookmarkBean({this.bookId, this.title, this.pageIndex, this.position});

  factory BookmarkBean.fromJson(Map<String, dynamic> json) {
    return _$BookmarkBeanFromJson(json);
  }
  Map<String, dynamic> toJson() => _$BookmarkBeanToJson(this);
}
