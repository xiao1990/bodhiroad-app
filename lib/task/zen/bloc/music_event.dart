import 'package:bodhiroad/task/zen/bean/music.dart';
import 'package:meta/meta.dart';

@immutable
abstract class MusicEvent {
  final int musicId;
  final String musicUrl;
  MusicEvent({this.musicId, this.musicUrl}) : super();
}

/// 选择音乐事件
class MusicChangeEvent extends MusicEvent {
  final int currentMusicId;
  final String musicUrl;
  final List<Music> list;
  final int state;
  MusicChangeEvent({this.currentMusicId, this.musicUrl, this.list, this.state = 1}) : super();
}

/// 选择音乐事件
class MusicPlayEvent extends MusicEvent {
  final int currentMusicId;
  final String musicUrl;
  final int state;
  MusicPlayEvent({this.currentMusicId, this.musicUrl, this.state}) : super();
}

/// 音频文件加载完成
class MusicLoadedEvent extends MusicEvent {
  final int currentMusicId;
  final double duration;
  MusicLoadedEvent({this.currentMusicId, this.duration}) : super();
}

/// 正在播放
class MusicPlayingEvent extends MusicEvent {
  final int currentMusicId;
  final double position;
  MusicPlayingEvent({this.currentMusicId, this.position}) : super();
}

/// 调节音量状态
class MusicVolumeEvent extends MusicEvent {
  final double volume;
  MusicVolumeEvent({this.volume}) : super();
  @override
  String toString() => 'MusicVolumeState';
}

/// 暂停
class MusicPauseEvent extends MusicEvent {
  MusicPauseEvent() : super();
  @override
  String toString() => 'MusicVolumeState';
}

/// 恢复播放
class MusicResumeEvent extends MusicEvent {
  MusicResumeEvent() : super();
  @override
  String toString() => 'MusicResumeEvent';
}

/// 恢复播放
class MusicFinishEvent extends MusicEvent {
  MusicFinishEvent() : super();
  @override
  String toString() => 'MusicFinishEvent';
}

/// 停止
class MusicStopEvent extends MusicEvent {
  MusicStopEvent() : super();
  @override
  String toString() => 'MusicStopEvent';
}

/// 播放错误
class MusicErrorEvent extends MusicEvent {
  String error;
  MusicErrorEvent({this.error}) : super();
  @override
  String toString() => 'MusicErrorEvent';
}

/// 显示通知
class ShowNotificationEvent extends MusicEvent {
  String error;
  ShowNotificationEvent({this.error}) : super();
  @override
  String toString() => 'ShowNotificationEvent';
}

/// 显示通知
class MusicStopConfirmEvent extends MusicEvent {
  String error;
  MusicStopConfirmEvent({this.error}) : super();
  @override
  String toString() => 'MusicStopConfirmEvent';
}
