import 'package:json_annotation/json_annotation.dart';

import 'book_page.dart';
import 'book_paragraph_unit.dart';
part 'book_newcatelog.g.dart';

@JsonSerializable()
class NewCatalogBean {
  BookParagraphUnitBean unit;
  PageBean indexedPage;

  NewCatalogBean({
    this.unit,
    this.indexedPage,
  });

  factory NewCatalogBean.fromJson(Map<String, dynamic> json) {
    return _$NewCatalogBeanFromJson(json);
  }
  Map<String, dynamic> toJson() => _$NewCatalogBeanToJson(this);
}
