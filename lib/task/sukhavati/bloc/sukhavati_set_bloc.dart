import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:bodhiroad/api/api.dart';
import 'package:bodhiroad/task/read/bean/task_record.dart';
import 'package:bodhiroad/utils/AudioUtils.dart';
import 'package:bodhiroad/utils/GlobalConfigUtil.dart';
import 'package:common_utils/common_utils.dart';
import 'package:flustars/flustars.dart';
import 'package:global_configuration/global_configuration.dart';

import 'sukhavati_set_event.dart';
import 'sukhavati_set_state.dart';

TimerUtil timer;
// 已念总数
int passedCount = 0;
// 剩余总数
int totalReduceCount = 0;
// 总数
int count = 0;

class SukhavatiSetBloc extends Bloc<SukhavatiSetEvent, SukhavatiSetState> {
  static final SukhavatiSetBloc _sukhavatiSetBlocSingleton = new SukhavatiSetBloc._internal();
  factory SukhavatiSetBloc() {
    return _sukhavatiSetBlocSingleton;
  }
  SukhavatiSetBloc._internal();

  SukhavatiSetState get initialState => new SukhavatiInitState();
  @override
  Stream<SukhavatiSetState> mapEventToState(
    SukhavatiSetEvent event,
  ) async* {
    try {
      // 初始化
      if (event is SukhavatiInitEvent) {
        // _sukhavatiSetBlocSingleton.dispatch(SukhavatiCountEvent(count: count));
//        yield SukhavatiCountState(count: event.count ?? 0);
//        double frequency = SpUtil.getDouble('nianfo_frequency');
//        if (frequency == null || frequency == 0.0) {
//          frequency = 3;
//        }
//        yield SukhavatiFrequencyState(frequency: frequency);
//        double volume = SpUtil.getDouble('volume');
//        if (volume == null) {
//          volume = 0.5;
//        }
//        yield SukhavatiVolumnState(volume: volume);
//        yield SukhavatiInitFinishState();
        // 设定念佛数
      } else if (event is SukhavatiCountEvent) {
        SpUtil.putInt('count_' + GlobalConfigUtil.getInt('id').toString(), event.count);

        yield SukhavatiCountState(count: event.count);
        // 设定念佛频率
      } else if (event is SukhavatiFrequencyEvent) {
        SpUtil.putDouble('frequency_' + GlobalConfigUtil.getInt('id').toString(), event.frequency);
        yield SukhavatiFrequencyState(frequency: event.frequency);
        // 频率预览
      } else if (event is SukhavatiFrequencyPreviewEvent) {
        previewAudio(event.frequency.toInt(), 10);
        yield SukhavatiFrequencyPreviewState();
        // 停止预览
      } else if (event is SukhavatiFrequencyPreviewStopEvent) {
        pauseAudio();
        yield SukhavatiFrequencyPreviewStopState();
        // 设定音量
      } else if (event is SukhavatiFrequencyPreviewFinishEvent) {
        yield SukhavatiFrequencyPreviewFinishState();
        // 设定音量
      } else if (event is SukhavatiVolumnEvent) {
        AudioUtils().changeVolume(event.volume);
        SpUtil.putDouble('volume', event.volume);
        yield SukhavatiVolumnState(volume: event.volume);
        // 开始计数
      } else if (event is SukhavatiStartEvent) {
        count = event.count;
        yield SukhavatiUnderwayState(count: event.count);
        playAudio(event.interval, event.count, event.taskId);
        saveRecord(0, event.taskId, 0);
        // 暂停后继续
      } else if (event is SukhavatiContinueEvent) {
        playAudio(event.interval, event.count, event.taskId);
        yield SukhavatiUnderwayState(count: event.count);
        // 进行中计数
      } else if (event is SukhavatiUnderwayEvent) {
        yield SukhavatiUnderwayState(count: event.count);
        // 暂停计数
      } else if (event is SukhavatiPauseEvent) {
        yield SukhavatiPauseState(reduceCount: totalReduceCount);
        pauseAudio();
        saveRecord(1, event.taskId, 0);
        // 完成功课
      } else if (event is SukhavatiStopEvent) {
        yield SukhavatiStopState();
        pauseAudio();
        saveRecord(1, event.taskId, 0);
        // 完成功课
      } else if (event is SukhavatiFinishEvent) {
        saveRecord(1, event.taskId, 1);
        yield SukhavatiFinishState();
      }
    } catch (_, stackTrace) {
      print('$_ $stackTrace');
      yield currentState;
    }
  }

  /// 木鱼自动播放
  playAudio(int interval, int reduceCount, int taskId) async {
    timer = TimerUtil(mInterval: interval, mTotalTime: reduceCount * interval);
    timer.setOnTimerTickCallback((index) {
      AudioUtils().chanxiuStartS();
      if (reduceCount == 0) {
        timer.cancel();
        // 目标完成，停止
        _sukhavatiSetBlocSingleton.dispatch(SukhavatiFinishEvent(taskId: taskId));
        AudioUtils().nianfoFinish();
      } else {
        reduceCount--;
        totalReduceCount = reduceCount;
        passedCount = count - reduceCount;
        _sukhavatiSetBlocSingleton.dispatch(SukhavatiUnderwayEvent(count: reduceCount));
      }
    });
    Future.delayed(Duration(milliseconds: interval), () {
      timer.startTimer();
    });
  }

  previewAudio(int interval, int reduceCount) async {
    timer = TimerUtil(mInterval: interval, mTotalTime: reduceCount * interval);
    timer.setOnTimerTickCallback((index) {
      AudioUtils().chanxiuStartS();
      if (reduceCount == 0) {
        timer.cancel();
        _sukhavatiSetBlocSingleton.dispatch(SukhavatiFrequencyPreviewFinishEvent());
      }
      reduceCount--;
    });
    timer.startTimer();
  }

  /// 木鱼暂停
  pauseAudio() async {
    if (timer != null) {
      timer.cancel();
    }
  }

  /// 保存记录
  /// operate 0 新增 1修改
  saveRecord(int operate, int taskId, int state) async {
    TaskRecord taskRecord =
        TaskRecord(id: GlobalConfiguration().getInt("currentTaskRecordId"), taskId: GlobalConfiguration().getInt("id"), taskCount: passedCount, taskType: 2, state: state, source: 0);
    if (operate == 0) {
      var res = await Api.taskRecordAdd(data: taskRecord.toJson());
      if (res['code'] == 200) {
        GlobalConfiguration().updateValue('currentTaskRecordId', res['data']);
      }
    } else {
      var res = await Api.taskRecordUpdate(data: taskRecord.toJson());
      if (res['code'] == 200) {
        GlobalConfiguration().updateValue("currentTaskRecordId", 0);
      }
    }
  }
}
