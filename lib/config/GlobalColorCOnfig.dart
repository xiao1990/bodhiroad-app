import 'package:flutter/material.dart';

class GlobalColorConfig {
  static const mainTextColor = Color(0xFF38344E);
  static const mainBackgroundColor = Colors.white;
//  static const mainBackgroundColor = Color(0xFF38344E);
  static const mainforegroundColor = Color(0xFF38344E);
//  static const mainforegroundColor = Color(0xFFC3B59A);
  static const subTitleTextColor = Colors.black54;
  static const bookCoverTextColor = Colors.white;
  static const mainBorderColor = Color(0xFFF2F2F2);
  static const mainBoxShadowColor = Color(0x5538344E);
  static const buttonColorA = Color(0xFF2B7497);
//  static const buttonColorA = Color(0xFF038664);
  static const mainRedColor = Color(0xFFA70400);
  static const mainWhiteColor = Colors.white;
  static const grayBackGroundColor = Color(0xFFF2F2F2);
  static const lightForeGroundColor = Color(0xFFB5A79E);
//  static const lightForeGroundColor = Color(0xFFD6A679);
}
