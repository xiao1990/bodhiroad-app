// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'book_paragraph.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

BookParagraphBean _$BookParagraphBeanFromJson(Map<String, dynamic> json) {
  return BookParagraphBean(
      content: json['content'] as String,
      type: json['type'] as int,
      list: (json['list'] as List)
          ?.map((e) => e == null
              ? null
              : BookParagraphBean.fromJson(e as Map<String, dynamic>))
          ?.toList());
}

Map<String, dynamic> _$BookParagraphBeanToJson(BookParagraphBean instance) =>
    <String, dynamic>{
      'content': instance.content,
      'type': instance.type,
      'list': instance.list
    };
