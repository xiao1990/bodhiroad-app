import 'dart:ui';

import 'package:bodhiroad/components/popup_menu/popup_menu.dart';
import 'package:bodhiroad/index/bloc/hide_bloc.dart';
import 'package:bodhiroad/index/pages/merits_new.dart';
import 'package:bodhiroad/merits/bean/merits.dart';
import 'package:bodhiroad/merits/bloc/merits_bloc.dart';
import 'package:bodhiroad/merits/bloc/merits_event.dart';
import 'package:bodhiroad/merits/bloc/merits_expand_bloc.dart';
import 'package:bodhiroad/merits/bloc/merits_state.dart';
import 'package:bodhiroad/theme/bloc.dart';
import 'package:bodhiroad/theme/theme_bloc.dart';
import 'package:bodhiroad/utils/GlobalConfigUtil.dart';
import 'package:bubble_tab_indicator/bubble_tab_indicator.dart';
import 'package:flustars/flustars.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:morpheus/page_routes/morpheus_page_route.dart';

int belong = 0;
List<Merits> belong1 = [];
List<Merits> belong2 = [];

class MeritsCatalog extends StatefulWidget {
  final Widget leftAction;
  final Widget rightAction;
  const MeritsCatalog({this.leftAction, this.rightAction});
  @override
  _MeritsReaderState createState() => _MeritsReaderState();
}

class _MeritsReaderState extends State<MeritsCatalog> with TickerProviderStateMixin {
  MeritsBloc meritsBloc = MeritsBloc();
  PageController _pageController = PageController();
  PageController _catapageController = PageController();
  AnimationController _animationController;
  AnimationController _sizeAnimationController;
  Animation sizeAnimation;
  HideBloc hideBloc = HideBloc();
  double lastPosition = 0.0;
  ScrollController scrollController;
  TabController tabController;

  @override
  void initState() {
    // print(double height = await )
    // FlutterStatusbarManager.getHeight.then((value) {});
    scrollController = ScrollController();
    meritsBloc.dispatch(MeritsUpdateEvent(forceUpdate: true));
    _animationController = AnimationController(vsync: this, duration: Duration(milliseconds: 500));
    _sizeAnimationController = AnimationController(duration: const Duration(milliseconds: 150), vsync: this);
    sizeAnimation = Tween(begin: 0.0, end: 1.0).animate(_sizeAnimationController);
    tabController = TabController(vsync: this, length: 2);
    super.initState();
  }

  @override
  void dispose() {
    _pageController.dispose();
    _catapageController.dispose();
    _animationController.dispose();
    _sizeAnimationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    PopupMenu.context = context;
    final AppThemeBloc appThemeBloc = BlocProvider.of<AppThemeBloc>(context);
    return BlocBuilder(
      bloc: appThemeBloc,
      builder: (context, ThemeChangeState appThemeState) {
        return NotificationListener<ScrollNotification>(
          child: Scaffold(
            backgroundColor: Colors.transparent,
            body: NestedScrollView(
              controller: scrollController,
              headerSliverBuilder: (context, isScroll) {
                return [
                  SliverPadding(
                      padding: EdgeInsets.only(top: 0),
                      sliver: SliverAppBar(
                        backgroundColor: appThemeState.themeConfig.sectionBgColor,
                        centerTitle: true,
                        title: Text('功过格', textScaleFactor: 1.0, style: TextStyle(color: appThemeState.themeConfig.mainTextColor, fontSize: GlobalConfigUtil.getTitleFontSize())),
                        pinned: true,
                        floating: true,
                        elevation: 4,
                        forceElevated: isScroll,
                        leading: widget.leftAction,
                        actions: <Widget>[widget.rightAction],
                        bottom: PreferredSize(
                          preferredSize: Size.fromHeight(ScreenUtil.getInstance().getWidth(120)),
                          child: Container(
                              child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Container(
                                width: MediaQuery.of(context).size.width * 0.5,
                                height: ScreenUtil.getInstance().getWidth(120),
                                color: Colors.white,
                                child: TabBar(
                                  controller: tabController,
                                  labelColor: Color(0xFFA70400),
                                  unselectedLabelColor: Colors.black54,
                                  indicatorColor: Colors.transparent,
                                  indicator: BubbleTabIndicator(
                                    indicatorHeight: ScreenUtil.getInstance().getWidth(90),
                                    indicatorColor: appThemeState.themeConfig.mainBgColor,
                                    tabBarIndicatorSize: TabBarIndicatorSize.tab,
                                  ),
                                  tabs: <Widget>[
                                    Text(
                                      '弟子规',
                                      textScaleFactor: 1.0,
                                      style: TextStyle(fontSize: GlobalConfigUtil.getTabFontSize()),
                                    ),
                                    Text(
                                      '感应篇',
                                      textScaleFactor: 1.0,
                                      style: TextStyle(fontSize: GlobalConfigUtil.getTabFontSize()),
                                    ),
                                  ],
                                ),
                              )
                            ],
                          )),
                        ),
                      ))
                ];
              },
              body: BlocBuilder(
                  bloc: meritsBloc,
                  condition: (preState, curnState) {
                    if (curnState is MeritsUpdateState) {
                      belong1 = curnState.list[0];
                      belong2 = curnState.list[1];
                      return true;
                    } else {
                      return false;
                    }
                  },
                  builder: (context, state) {
                    if (state is MeritsUpdateState) {
                      return Container(
                          decoration: BoxDecoration(
                              color: Colors.white,
                              image: DecorationImage(
                                fit: BoxFit.cover,
                                image: AssetImage('assets/texture/bgt2.jpg'),
                              )),
                          child: Column(
                            children: <Widget>[
                              Expanded(
                                  child: Padding(
                                      padding: EdgeInsets.only(bottom: ScreenUtil.getInstance().getWidth(140)),
                                      child: TabBarView(
                                        controller: tabController,
                                        children: <Widget>[
                                          MeritsBody(
                                            sizeAnimation: sizeAnimation,
                                            animationController: _animationController,
                                            list: state.list[0],
                                            belong: 0,
                                          ),
                                          MeritsBody(sizeAnimation: sizeAnimation, animationController: _animationController, list: state.list[1], belong: 1)
                                        ],
                                      )))
                            ],
                          ));
                    } else {
                      return Container();
                    }
                  }),
            ),
          ),
          onNotification: (notification) {
            if (notification.metrics.pixels < 0 || notification.metrics.axisDirection != AxisDirection.down) {
              return false;
            }
            int currentPosition = (notification.metrics.pixels / 10).floor() * 10;
            if (currentPosition > lastPosition) {
              hideBloc.dispatch(0.0);
              lastPosition = currentPosition.toDouble();
            } else if (currentPosition < lastPosition) {
              hideBloc.dispatch(1.0);
            }
            lastPosition = currentPosition.toDouble();
            return true;
          },
        );
      },
    );
  }
}

class MeritsBody extends StatefulWidget {
  final AnimationController animationController;
  final Animation sizeAnimation;
  final List<Merits> list;
  final ScrollController scroll;
  final int belong;

  MeritsBody({this.animationController, this.sizeAnimation, this.list, this.scroll, this.belong});

  @override
  _MeritsBodyState createState() => _MeritsBodyState();
}

class _MeritsBodyState extends State<MeritsBody> with TickerProviderStateMixin {
  MeritsBloc meritsBloc = MeritsBloc();
  MeritsExpandBloc meritsExBloc = MeritsExpandBloc();

  List<Merits> meritsList = [];
  var currentPanelIndex = -1; //设置-1默认全部闭合

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final AppThemeBloc appThemeBloc = BlocProvider.of<AppThemeBloc>(context);
    return BlocBuilder(
      bloc: appThemeBloc,
      builder: (context, ThemeChangeState appThemeState) {
        return Column(
          children: <Widget>[
            Expanded(
              child: ListView.builder(
                  physics: BouncingScrollPhysics(),
                  controller: widget.scroll,
                  itemCount: widget.list.length,
                  padding: EdgeInsets.symmetric(vertical: 0),
                  itemBuilder: (context, index) {
                    final key = GlobalKey();
                    return Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Container(
                            key: key,
                            width: MediaQuery.of(context).size.width - 30,
                            margin: EdgeInsets.only(bottom: ScreenUtil.getInstance().getWidth(50), top: index == 0 ? 20 : 0),
                            decoration: BoxDecoration(
                                color: appThemeState.themeConfig.sectionBgColor, boxShadow: [BoxShadow(blurRadius: 12, color: Colors.black12)], borderRadius: BorderRadius.all(Radius.circular(20))),
                            child: GestureDetector(
                              onTap: () {
                                Navigator.of(context).push(MorpheusPageRoute(
                                  builder: (context) => MeritsNew(
                                    merits: widget.list[index],
                                    belong: widget.belong,
                                  ),
                                  parentKey: key,
                                ));
                              },
                              child: ClipRRect(
                                borderRadius: BorderRadius.all(Radius.circular(12)),
                                child: Column(children: [
                                  Container(
                                    height: ScreenUtil.getInstance().getWidth(480),
                                    padding: EdgeInsets.symmetric(horizontal: ScreenUtil.getInstance().getWidth(90), vertical: ScreenUtil.getInstance().getWidth(50)),
                                    decoration: BoxDecoration(
                                        // color: appThemeState
                                        //     .themeConfig.sectionBgColor,
                                        image: DecorationImage(alignment: Alignment.centerLeft, fit: BoxFit.fitHeight, image: AssetImage('assets/texture/dizigui/' + (index % 7).toString() + '.png'))),
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.end,
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: <Widget>[
                                        Expanded(
                                          child: Container(
                                            padding: EdgeInsets.only(right: ScreenUtil.getInstance().getWidth(50)),
                                            height: ScreenUtil.getInstance().getWidth(380),
                                            child: ListView(
                                              scrollDirection: Axis.horizontal,
                                              reverse: true,
                                              physics: BouncingScrollPhysics(),
                                              padding: EdgeInsets.all(0),
                                              children: widget.list[index].list
                                                  .map((item) => Container(
                                                        width: ScreenUtil.getInstance().getWidth(48),
                                                        height: ScreenUtil.getInstance().getWidth(380),
                                                        alignment: Alignment.center,
                                                        child: Text(
                                                          item.title.replaceAll(' ', '·'),
                                                          textScaleFactor: 1.0,
                                                          style: TextStyle(fontSize: GlobalConfigUtil.getTabMainBodyFontSize()),
                                                        ),
                                                      ))
                                                  .toList(),
                                            ),
                                          ),
                                        ),
                                        Container(
                                          width: ScreenUtil.getInstance().getWidth(100),
                                          decoration: BoxDecoration(border: Border(left: BorderSide(color: appThemeState.themeConfig.embellishColorA, width: 1))),
                                          padding: EdgeInsets.only(left: ScreenUtil.getInstance().getWidth(40)),
                                          child: Text(
                                            widget.list[index].catalogName.replaceAll('', '\n'),
                                            textScaleFactor: 1.0,
                                            style: TextStyle(fontSize: GlobalConfigUtil.getSubTitleFontSize(), fontWeight: FontWeight.bold, color: appThemeState.themeConfig.mainTextColor),
                                          ),
                                        ),
                                      ],
                                    ),
                                  )
                                ]),
                              ),
                            ))
                      ],
                    );
                  }),
            )
          ],
        );
      },
    );
  }
}
