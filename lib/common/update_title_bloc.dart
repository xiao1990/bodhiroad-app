import 'dart:async';

import 'package:bloc/bloc.dart';

import 'calendar_day.dart';

class UpdateTitleBloc extends Bloc<CalendarDay, CalendarDay> {
  static final UpdateTitleBloc _updateTitleBlocSingleton =
      new UpdateTitleBloc._internal();
  factory UpdateTitleBloc() {
    return _updateTitleBlocSingleton;
  }
  UpdateTitleBloc._internal();

  CalendarDay get initialState => new CalendarDay(
      gregorianDateTime: DateTime.now().toString().split(" ")[0]);

  @override
  Stream<CalendarDay> mapEventToState(
    CalendarDay event,
  ) async* {
    try {
      yield event;
    } catch (_, stackTrace) {
      print('$_ $stackTrace');
      yield currentState;
    }
  }
}
