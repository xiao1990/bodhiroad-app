import 'dart:math' as math;

import 'package:bodhiroad/api/api.dart';
import 'package:bodhiroad/common/FontFamilyName.dart';
import 'package:bodhiroad/config/CommonConfig.dart';
import 'package:bodhiroad/reader/bean/buddhist.dart';
import 'package:bodhiroad/reader/bean/common_book.dart';
import 'package:bodhiroad/theme/bloc.dart';
import 'package:bodhiroad/utils/GlobalConfigUtil.dart';
import 'package:bodhiroad/utils/Route.dart';
import 'package:bodhiroad/utils/parameter_convert_cn.dart';
import 'package:flustars/flustars.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

class CommonBuddhist extends StatefulWidget {
  final String from;
  const CommonBuddhist({Key key, @required this.from}) : super(key: key);
  @override
  _CommonBuddhistState createState() => _CommonBuddhistState();
}

class _CommonBuddhistState extends State<CommonBuddhist> {
  Future f;

  List<CommonBook> common = [];
  @override
  void initState() {
    f = getBuddhist();
    super.initState();
  }

  Future<List<CommonBook>> getBuddhist() async {
    List commonBuddhist = SpUtil.getObjectList(CommonConfig.BUDDHIST_CATALOG + 'common');
    if (commonBuddhist != null && commonBuddhist.length > 0) {
      common = commonBuddhist.map((item) => CommonBook.fromJson(item)).toList();
    } else {
      var rest = await Api.getBuddhist();
      if (rest['code'] == 200) {
        List<dynamic> list0 = rest['data'];
        common = list0.map((item) => CommonBook.fromJson(item)).toList();
        SpUtil.putObjectList(CommonConfig.BUDDHIST_CATALOG + 'common', common);
      }
    }
    return common;
  }

  List<Widget> getVerticalText(String text) {
    text = text.replaceAll("《", '').replaceAll("》", '');
    text = text.substring(0, math.min(18, text.length));

    int cols = (text.length / 9).ceil();
    List<Widget> list = [];
    for (int i = 0; i < cols; i++) {
      String text2 = text.substring(i * 9, math.min(i * 9 + 9 - 1, text.length - 1) + 1).replaceAll('', '\n');
      list.add(Container(
        width: ScreenUtil.getInstance().getWidth(23),
        height: ScreenUtil.getInstance().screenHeight * 0.5,
        alignment: Alignment.topCenter,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
              width: ScreenUtil.getInstance().getWidth(23),
//              decoration: BoxDecoration(border: Border(left: BorderSide(color: Colors.black26, width: 0.5))),
              alignment: i == 0 ? Alignment.topCenter : Alignment.topRight,
              child: Text(
                text2,
                style: TextStyle(
                    decoration: TextDecoration.none,
                    color: Colors.black87,
                    fontSize: GlobalConfigUtil.getCoverTitleFontSize(),
                    shadows: [Shadow(color: Colors.black26, blurRadius: 6)],
                    fontWeight: FontWeight.normal,
                    fontFamily: FontFamilyName.SONGTI),
              ),
            )
          ],
        ),
      ));
    }
    return list.reversed.toList();
  }

  @override
  Widget build(BuildContext context) {
    final AppThemeBloc appThemeBloc = BlocProvider.of<AppThemeBloc>(context);
    return BlocBuilder(
        bloc: appThemeBloc,
        builder: (context, ThemeChangeState appThemeState) {
          return FutureBuilder(
            future: f,
            builder: (context, snapShot) {
              return snapShot.data != null && snapShot.data.length > 0
                  ? Container(
                      child: ListView.builder(
                          itemCount: common.length,
                          physics: BouncingScrollPhysics(),
                          padding: EdgeInsets.all(0),
                          itemBuilder: (context, index) {
                            return Container(
                              height: ScreenUtil.getInstance().getWidth(560.0 + (index == common.length - 1 ? 160 : 0)),
//                              color: Colors.green,
                              child: Column(
                                children: <Widget>[
                                  Container(
                                    height: ScreenUtil.getInstance().getWidth(120),
                                    padding: EdgeInsets.symmetric(horizontal: 15),
                                    alignment: Alignment.centerLeft,
                                    child: Row(
//                                      crossAxisAlignment: CrossAxisAlignment.end,
                                      children: <Widget>[
                                        Container(
                                          height: ScreenUtil.getInstance().getWidth(60),
                                          width: ScreenUtil.getInstance().getWidth(60),
                                          alignment: Alignment.center,
                                          decoration: BoxDecoration(color: Color(0xFFA70400), borderRadius: BorderRadius.circular(18)),
                                          margin: EdgeInsets.only(right: ScreenUtil.getInstance().getWidth(10)),
                                          child: Text(
                                            snapShot.data[index].title.substring(0, 1),
                                            style: TextStyle(
                                                fontSize: GlobalConfigUtil.getSubTitleFontSize(),
                                                fontWeight: FontWeight.normal,
                                                color: Colors.white,
                                                fontFamily: FontFamilyName.SONGTI),
                                          ),
                                        ),
                                        Text(
                                          snapShot.data[index].title.substring(1),
                                          style: TextStyle(
                                              fontSize: GlobalConfigUtil.getSubTitleFontSize(),
                                              fontWeight: FontWeight.bold,
                                              color: Colors.black87,
                                              fontFamily: FontFamilyName.SONGTI),
                                        )
                                      ],
                                    ),
                                  ),
                                  Expanded(
                                      child: ListView.builder(
                                          itemCount: snapShot.data[index].list.length,
                                          scrollDirection: Axis.horizontal,
                                          physics: BouncingScrollPhysics(),
                                          itemBuilder: (context, idx) {
                                            return GestureDetector(
                                                onTap: () {
                                                  Buddhist item = snapShot.data[index].list[idx];
                                                  String url = item.url.replaceAll('/', '%2F');
                                                  item.url = url;
                                                  String bookName = ParameterConvertUtils.convertCN(item.title);
                                                  Routes.router.navigateTo(
                                                    context,
                                                    'bookView?fileUrl=${item.url}&source=3&id=${item.id}&taskType=0&bookName=${bookName}&version=${item.version}',
                                                  );
                                                },
                                                child: Container(
                                                  width: ScreenUtil.getInstance().getWidth(300),
                                                  height: ScreenUtil.getInstance().getWidth(450),
                                                  alignment: Alignment.center,
                                                  child: Column(
                                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                    children: <Widget>[
                                                      Container(
                                                        width: ScreenUtil.getInstance().getWidth(230),
                                                        height: ScreenUtil.getInstance().getWidth(325),
                                                        decoration: BoxDecoration(
                                                            border: Border.all(color: Color(0xFFdbd8d9), width: 0.5),
                                                            boxShadow: [BoxShadow(color: Colors.black12, blurRadius: 8)],
                                                            image: DecorationImage(
                                                                fit: BoxFit.cover,
                                                                image: AssetImage(
                                                                  'assets/texture/cover7.jpg',
                                                                ))),
//                                                        padding: EdgeInsets.only(right: 10),
                                                        alignment: Alignment.center,
                                                        child: Container(
                                                          width: ScreenUtil.getInstance().getWidth(46),
                                                          child: Row(
                                                            mainAxisAlignment: MainAxisAlignment.center,
                                                            children: getVerticalText(snapShot.data[index].list[idx].title),
                                                          ),
                                                        ),
                                                      ),
                                                      Container(
                                                        height: ScreenUtil.getInstance().getWidth(100),
                                                        padding: EdgeInsets.symmetric(horizontal: ScreenUtil.getInstance().getWidth(25)),
                                                        alignment: Alignment.centerLeft,
                                                        child: Text(
                                                          snapShot.data[index].list[idx].title,
                                                          style: TextStyle(fontSize: GlobalConfigUtil.getTabMainBodyFontSize()),
                                                          maxLines: 2,
                                                          overflow: TextOverflow.ellipsis,
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                ));
                                          })),
                                  Offstage(
                                    offstage: index < common.length - 1,
                                    child: Container(
                                      height: ScreenUtil.getInstance().getWidth(160),
                                    ),
                                  )
                                ],
                              ),
                            );
                          }),
                    )
                  : Center(
                      child: SpinKitCircle(
                        size: 20,
                        color: Colors.black54,
                      ),
                    );
            },
          );
        });
  }
}
