import 'package:bodhiroad/reader/bean/buddhist.dart';
import 'package:bodhiroad/utils/Route.dart';
import 'package:flustars/flustars.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';

class CommonTask extends StatelessWidget {
  const CommonTask({Key key, @required this.list}) : super(key: key);

  final List<Buddhist> list;

  @override
  Widget build(BuildContext context) {
    return Container(
        // color: Colors.blueGrey[50],
        child: Padding(
            padding: EdgeInsets.only(
              top: ScreenUtil.getInstance().getWidth(0),
            ),
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 15, vertical: 0),
              child: ListView.builder(
                  itemCount: list.length,
                  physics: BouncingScrollPhysics(),
                  padding: EdgeInsets.only(
                      top: ScreenUtil.getInstance().getWidth(40)),
                  itemBuilder: (BuildContext context, idx) {
                    return ClipRRect(
                        borderRadius: idx == 0
                            ? BorderRadius.vertical(top: Radius.circular(18))
                            : idx == list.length - 1
                                ? BorderRadius.vertical(
                                    bottom: Radius.circular(18))
                                : BorderRadius.zero,
                        child: Container(
                            decoration: BoxDecoration(
                                color: Colors.white,
                                border: Border(
                                    bottom: BorderSide(
                                        color: Colors.black26, width: 0.5))),
                            child: Row(
                              children: <Widget>[
                                Expanded(
                                  child: ListTile(
                                    leading: ClipRRect(
                                        borderRadius: BorderRadius.circular(6),
                                        child: Image.asset(
                                          'assets/cover.jpg',
                                          height: ScreenUtil.getInstance()
                                              .getWidth(120),
                                        )),
                                    title: Text(list[idx].title,
                                        textScaleFactor: 1.0,
                                        style: TextStyle(
                                            fontSize: ScreenUtil.getInstance()
                                                .getWidth(48),
                                            fontWeight: FontWeight.bold)),
                                    subtitle: Text(list[idx].author,
                                        textScaleFactor: 1.0,
                                        style: TextStyle(
                                            fontSize: ScreenUtil.getInstance()
                                                .getWidth(36),
                                            color: Colors.black54)),
                                    onTap: () {
                                      String url = list[idx].url;

                                      Routes.router.navigateTo(context,
                                          'bookView?fileUrl=${url}&id=${url}');
                                    },
                                  ),
                                ),
                                Container(
                                  width: ScreenUtil.getInstance().getWidth(150),
                                  height: 56,
                                  alignment: Alignment.center,
                                  child: GestureDetector(
                                    child: Icon(Icons.add),
                                    onTap: () {
                                      WidgetUtil.getWidgetBounds(context);
                                      print(
                                          WidgetUtil.getWidgetBounds(context));
                                    },
                                  ),
                                )
                              ],
                            )));

                    // );
                  }),
            )));
  }
}
