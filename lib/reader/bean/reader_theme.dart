import 'package:flutter/material.dart';

class ReaderTheme {
  Color fontColor;
  Color backgroundColor;
  Color menuFontColor;
  Color innerBackgroundColor;
  Color iconBackgroundColor;
  Color catalogBackgroundColor;
  Color catalogFontColor;
  Color sliderActiveColor;
  Color sliderInactiveColor;
  Color buttonBgColor;
  Color buttonFrontColor;
  Color buttonDisabledColor;
  Color buttonActiveColor;
  Color tabActiveColor;
  Color markColor;
  Color titleColor;

  ReaderTheme({
    this.fontColor,
    this.backgroundColor,
    this.catalogBackgroundColor,
    this.catalogFontColor,
    this.iconBackgroundColor,
    this.innerBackgroundColor,
    this.menuFontColor,
    this.sliderActiveColor,
    this.sliderInactiveColor,
    this.buttonBgColor,
    this.buttonFrontColor,
    this.buttonDisabledColor,
    this.buttonActiveColor,
    this.tabActiveColor,
    this.markColor,
    this.titleColor,
  });
}
