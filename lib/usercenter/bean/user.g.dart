// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

User _$UserFromJson(Map<String, dynamic> json) {
  return User(
      id: json['id'] as int,
      name: json['name'] as String,
      email: json['email'] as String,
      stat: json['stat'] as int,
      approach: json['approach'] as String,
      birthday: json['birthday'] as String,
      city: json['city'] as String,
      fahao: json['fahao'] as String,
      gender: json['gender'] as String,
      headPortrait: json['headPortrait'] as String,
      headPortraitUrl: json['headPortraitUrl'] as String,
      token: json['token'] as String,
      province: json['province'] as String,
      signature: json['signature'] as String);
}

Map<String, dynamic> _$UserToJson(User instance) => <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'email': instance.email,
      'stat': instance.stat,
      'headPortrait': instance.headPortrait,
      'headPortraitUrl': instance.headPortraitUrl,
      'fahao': instance.fahao,
      'gender': instance.gender,
      'birthday': instance.birthday,
      'province': instance.province,
      'city': instance.city,
      'approach': instance.approach,
      'signature': instance.signature,
      'token': instance.token
    };
