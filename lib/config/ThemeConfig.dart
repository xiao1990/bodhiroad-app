import 'package:flutter/material.dart';

class ThemeConfig {
//  主文字颜色
  final Color mainTextColor;

//  内容区域背景色
  final Color sectionBgColor;

//  副标题文字颜色
  final Color subTitleTextColor;

//  主背景颜色
  final Color mainBgColor;

//  按钮激活颜色
  final Color buttonActiveColor;

//  按钮禁用颜色
  final Color buttonDisabledColor;

//  按钮颜色
  final Color buttonColor;

//  按钮颜色
  final Color buttonTextColor;
  //  按钮颜色
  final Color buttonActiveTextColor;

//  装饰颜色
  final Color embellishColorA;

//  装饰颜色
  final Color embellishColorB;

//  菜单背景颜色
  final Color innerMenuBgColorA;

//  菜单背景颜色
  final Color innerMenuBgColorB;

//  菜单背景颜色
  final Color innerMenuBgColorC;

//  菜单文字颜色
  final Color innerMenuTextColor;

//  弹出菜单颜色
  final Color popMenuColor;

//  弹出菜单文字颜色
  final Color popMenuTextColor;

//  边框颜色
  final Color mainBorderColor;

//  边框颜色
  final Color lightBorderColor;

//  分割线颜色
  final Color dividerColor;

//  阴影颜色
  final Color shadowColor;

//  激活导航菜单颜色
  final Color navigatorActiveColor;

//  操作成功提示颜色
  final Color successColor;

//  操作失败提示色
  final Color failColor;
  // 当前选中背景色 《今日功过格》
  final Color selectedBgColor;

  final Color placeholderColor;

//  标题栏背景色
  final Color titleBarBgColor;
//  标题栏文字颜色
  final Color titleBarFontColor;
//  功过统计 功线条色
  final Color meritsColorA;
//  功过统计 过线条色
  final Color meritsColorB;
  ThemeConfig(
      {this.mainTextColor,
      this.mainBorderColor,
      this.sectionBgColor,
      this.buttonActiveColor,
      this.buttonColor,
      this.buttonDisabledColor,
      this.buttonTextColor,
      this.buttonActiveTextColor,
      this.dividerColor,
      this.embellishColorA,
      this.embellishColorB,
      this.innerMenuBgColorA,
      this.innerMenuBgColorB,
      this.innerMenuBgColorC,
      this.innerMenuTextColor,
      this.lightBorderColor,
      this.mainBgColor,
      this.popMenuColor,
      this.popMenuTextColor,
      this.shadowColor,
      this.subTitleTextColor,
      this.navigatorActiveColor,
      this.successColor,
      this.failColor,
      this.selectedBgColor,
      this.placeholderColor,
      this.titleBarBgColor,
      this.titleBarFontColor,
      this.meritsColorA,
      this.meritsColorB});

  factory ThemeConfig.flt() {
    return ThemeConfig(
        mainTextColor: Colors.black87,
        mainBorderColor: Colors.black26,
        mainBgColor: Color(0xFFF2F2F2),
        sectionBgColor: Colors.white,
        buttonActiveColor: Color(0xFFF2F2F2),
        buttonColor: Colors.transparent,
        buttonDisabledColor: Colors.black12,
        buttonTextColor: Colors.black87,
        buttonActiveTextColor: Color(0xFFA70400),
        dividerColor: Colors.black12,
        embellishColorA: Color(0xFFA70400),
        embellishColorB: Color(0xFF2B7497),
        innerMenuBgColorA: Color(0xFF2B7497),
        innerMenuBgColorB: Color(0xFFF2F2F2),
        innerMenuBgColorC: Color(0xFF2B7497),
        innerMenuTextColor: Colors.white,
        lightBorderColor: Colors.black12,
        popMenuColor: Color(0xFFB5A79E),
        popMenuTextColor: Colors.white,
        shadowColor: Color(0x5538344E).withOpacity(0.1),
        subTitleTextColor: Colors.black26,
        navigatorActiveColor: Color(0xFFA70400),
        successColor: Colors.green,
        failColor: Colors.red,
        selectedBgColor: Color(0xFFF2F2F2),
        placeholderColor: Color(0xFFE0E0E0),
        titleBarBgColor: Color(0xFFF5F5F5),
        titleBarFontColor: Colors.black87,
        meritsColorA: Colors.redAccent,
        meritsColorB: Colors.blueAccent);
  }

  factory ThemeConfig.white() {
    return ThemeConfig(
        mainTextColor: Colors.black87,
        mainBorderColor: Colors.black26,
        mainBgColor: Colors.red,
        sectionBgColor: Colors.white,
        buttonActiveColor: Color(0xFFF2F2F2),
        buttonColor: Colors.transparent,
        buttonDisabledColor: Colors.black12,
        buttonTextColor: Colors.black87,
        buttonActiveTextColor: Color(0xFFA70400),
        dividerColor: Colors.black12,
        embellishColorA: Color(0xFFA70400),
        embellishColorB: Color(0xFFB5A79E),
        innerMenuBgColorA: Color(0xFF2B7497),
        innerMenuBgColorB: Color(0xFFF2F2F2),
        innerMenuBgColorC: Color(0xFF2B7497),
        innerMenuTextColor: Colors.white,
        lightBorderColor: Colors.black12,
        popMenuColor: Color(0xFFB5A79E),
        popMenuTextColor: Colors.white,
        shadowColor: Color(0x5538344E),
        subTitleTextColor: Colors.black26,
        navigatorActiveColor: Color(0xFFA70400),
        selectedBgColor: Color(0xFFF2F2F2),
        placeholderColor: Color(0xFFE0E0E0),
        titleBarBgColor: Color(0xFF5F5F5),
        titleBarFontColor: Colors.black87,
        meritsColorA: Colors.redAccent,
        meritsColorB: Colors.blueAccent);
  }
}
