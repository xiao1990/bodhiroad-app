import 'package:json_annotation/json_annotation.dart';

import 'book_catalog.dart';
import 'book_mark.dart';
import 'book_page.dart';

part 'book_info.g.dart';

@JsonSerializable()
class BookInfoBean {
  List<CatalogBean> catelogList;
  List<PageBean> pageList;
  List<BookmarkBean> bookMarkList;

  String title;
  String author;
  String fileUrl;
  String id;

  BookInfoBean({this.catelogList, this.pageList, this.bookMarkList, this.author, this.title, this.fileUrl, this.id});

  factory BookInfoBean.fromJson(Map<String, dynamic> json) {
    return _$BookInfoBeanFromJson(json);
  }
  Map<String, dynamic> toJson() => _$BookInfoBeanToJson(this);
}
