import 'dart:ui';

import 'package:bodhiroad/usercenter/bloc/download_bloc.dart';
import 'package:bodhiroad/utils/Route.dart';
import 'package:flustars/flustars.dart';
import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

class Recovery extends StatelessWidget {
  final DownloadBloc downloadBloc;
  final String title;
  Recovery({this.downloadBloc, this.title});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.transparent,
        body: Center(
          child: Stack(
            children: <Widget>[
              Container(
                  width: ScreenUtil.getInstance().screenWidth * 0.8,
                  height: ScreenUtil.getInstance().screenHeight * 0.75,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(15),
                  ),
                  child: Column(
                    children: <Widget>[
                      Container(
                        height: ScreenUtil.getInstance().getWidth(84),
                        width: ScreenUtil.getInstance().screenWidth * 0.6,
                        alignment: Alignment.topLeft,
                        padding: EdgeInsets.only(left: 15),
                        child: Text('纠错反馈', style: TextStyle(fontSize: ScreenUtil.getInstance().getSp(48), fontWeight: FontWeight.bold), overflow: TextOverflow.ellipsis),
                      ),
                      Expanded(
                          child: Container(
                        padding: EdgeInsets.only(left: 15, right: 15),
                        child: ListView(physics: BouncingScrollPhysics(), children: <Widget>[]),
                      ))
//    )
                    ],
                  )),
              Positioned(
                  right: 0,
                  top: 0,
                  child: ButtonTheme(
                    padding: EdgeInsets.all(0),
                    minWidth: ScreenUtil.getInstance().getWidth(84),
                    child: IconButton(
                        padding: EdgeInsets.all(0),
                        icon: Icon(
                          MdiIcons.closeCircle,
                          color: Colors.black54,
                        ),
                        onPressed: () {
                          Routes.router.pop(context);
                        }),
                  ))
            ],
          ),
        ));
  }
}
