import 'package:json_annotation/json_annotation.dart';

import 'merits_statistic_day.dart';
part 'merits_statistic.g.dart';

@JsonSerializable()
class MeritsStatistic {
  int meritsType;
  List<MeritsStatisticDay> list;

  MeritsStatistic({
    this.meritsType,
    this.list,
  });
  factory MeritsStatistic.fromJson(Map<String, dynamic> json) {
    return _$MeritsStatisticFromJson(json);
  }
  Map<String, dynamic> toJson() => _$MeritsStatisticToJson(this);
}
