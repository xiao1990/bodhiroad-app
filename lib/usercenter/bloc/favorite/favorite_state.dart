import 'package:bodhiroad/reader/bean/favorite.dart';
import 'package:equatable/equatable.dart';

abstract class FavoriteState extends Equatable {
  FavoriteState([props = const []]) : super([]);
}

class FavoriteInitState extends FavoriteState {
  FavoriteInitState() : super();
}

class FavoriteLoadState extends FavoriteState {
  final List<Favorite> list;
  final int tsp;
  FavoriteLoadState(this.list, this.tsp) : super([list, tsp]);
}
