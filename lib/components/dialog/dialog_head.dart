import 'package:flustars/flustars.dart';
import 'package:flutter/material.dart';

class DialogHead extends StatelessWidget {
  final String title;
  const DialogHead({Key key, this.title});
  @override
  Widget build(BuildContext context) {
    return Container(
      height: ScreenUtil.getInstance().getWidth(120),
      alignment: Alignment.center,
      child: Text(title ?? '提示信息', textScaleFactor: 1.0, style: TextStyle(fontSize: ScreenUtil.getInstance().getSp(42), color: Colors.black87, fontWeight: FontWeight.bold)),
      decoration: BoxDecoration(border: Border(bottom: BorderSide(color: Colors.black12, width: 0.5))),
    );
  }
}
