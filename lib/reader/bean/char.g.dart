// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'char.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Char _$CharFromJson(Map<String, dynamic> json) {
  return Char(
      id: json['id'] as int,
      chars: json['chars'] as String,
      isMarked: json['isMarked'] as bool,
      maskType: json['maskType'] as int,
      isSpell: json['isSpell'] as bool,
      rareText: json['rareText'] as bool,
      spell: json['spell'] as String,
      actualChars: json['actualChars'] as String,
      isTitle: json['isTitle'] as bool,
      bold: json['bold'] as bool,
      colored: json['colored'] as bool);
}

Map<String, dynamic> _$CharToJson(Char instance) => <String, dynamic>{
      'chars': instance.chars,
      'actualChars': instance.actualChars,
      'isMarked': instance.isMarked,
      'maskType': instance.maskType,
      'spell': instance.spell,
      'rareText': instance.rareText,
      'isSpell': instance.isSpell,
      'id': instance.id,
      'isTitle': instance.isTitle,
      'bold': instance.bold,
      'colored': instance.colored
    };
