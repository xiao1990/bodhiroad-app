import 'dart:async';
import 'package:bloc/bloc.dart';

class SukhavatiFrequencyBloc extends Bloc<double, double> {
  static final SukhavatiFrequencyBloc _sukhavatiFrequencyBlocSingleton =
      new SukhavatiFrequencyBloc._internal();
  factory SukhavatiFrequencyBloc() {
    return _sukhavatiFrequencyBlocSingleton;
  }
  SukhavatiFrequencyBloc._internal();

  double get initialState => 0;

  @override
  Stream<double> mapEventToState(
    double event,
  ) async* {
    try {
      yield event;
    } catch (_, stackTrace) {
      print('$_ $stackTrace');
      yield currentState;
    }
  }
}
