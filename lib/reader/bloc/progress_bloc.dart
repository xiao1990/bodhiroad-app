import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:bodhiroad/reader/bloc/reader_bloc.dart';

class ProgressBloc extends Bloc<double, double> {
  static final ProgressBloc _progressBlocSingleton = new ProgressBloc._internal();
  factory ProgressBloc() {
    return _progressBlocSingleton;
  }
  ProgressBloc._internal();

  double get initialState => 0;
  ReaderBloc readerBloc = ReaderBloc();
  @override
  Stream<double> mapEventToState(
    double event,
  ) async* {
    try {
      yield event;
    } catch (_) {
      yield currentState;
    }
  }
}
