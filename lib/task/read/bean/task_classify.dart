import 'package:json_annotation/json_annotation.dart';

import 'task.dart';
part 'task_classify.g.dart';

@JsonSerializable()
class TaskClassify {
  int taskTypeId;
  String taskTypeName;
  List<Task> list;
  TaskClassify({this.taskTypeId, this.taskTypeName, this.list});
  factory TaskClassify.fromJson(Map<String, dynamic> json) {
    return _$TaskClassifyFromJson(json);
  }
  Map<String, dynamic> toJson() => _$TaskClassifyToJson(this);
}
