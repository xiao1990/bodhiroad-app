import 'dart:math';

import 'package:bodhiroad/api/api.dart';
import 'package:flustars/flustars.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'calendar_widget.dart';

class CalendarMemorial extends StatefulWidget {
  @override
  _CalendarMemorialState createState() => _CalendarMemorialState();
}

class _CalendarMemorialState extends State<CalendarMemorial> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    CalendarHighlighter highlighter = (DateTime dt) {
      // randomly generate a boolean list of length monthLength + 1 (because months start at 1)
      return List.generate(Calendar.monthLength(dt) + 1, (index) {
        return (Random().nextDouble() < 0.3);
      });
    };
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text('佛历'),
        backgroundColor: Colors.white,
        elevation: 1,
      ),
      body: Container(
          decoration: BoxDecoration(
              image: DecorationImage(
            fit: BoxFit.cover,
            image: AssetImage('assets/texture/bgt2.jpg'),
          )),
          child: FutureBuilder<Map<dynamic, dynamic>>(
            future: SpUtil.getInt('hasLoadCalendar') != 1 ? Api.calendarMemorial() : null, // a previously-obtained Future<String> or null
            builder: (BuildContext context, AsyncSnapshot<Map<dynamic, dynamic>> snapshot) {
              switch (snapshot.connectionState) {
                case ConnectionState.none:
                  return Text('');
                case ConnectionState.active:
                case ConnectionState.waiting:
                  return SpinKitThreeBounce(
                    color: Colors.blueGrey,
                    size: 20.0,
                  );
                case ConnectionState.done:
                  if (snapshot.hasError) {
                    return Text('Error: ${snapshot.error}');
                  }
                  if (SpUtil.getInt('hasLoadCalendar') != 1) {
                    SpUtil.putObject('calendarMemorial', snapshot.data['data']);
                  }
                  return LayoutBuilder(
                    builder: (context, BoxConstraints constraints) {
                      return Container(
                        margin: EdgeInsets.only(top: 16),
                        alignment: Alignment.topCenter,
                        child: Calendar(
                          height: constraints.maxHeight,
                          width: MediaQuery.of(context).size.width - 32,
                          onTapListener: (DateTime dt) {
//                            final snackbar = SnackBar(
//                              content: Text('Clicked ${dt.month}/${dt.day}/${dt.year}!'),
//                            );
//                            Scaffold.of(context).showSnackBar(snackbar);
                          },
                          enableFuture: true,
                          highlighter: highlighter,
                        ),
                      );
                    },
                  );
              }
              return null; // unreachable
            },
          )),
    );
  }
}
