import 'dart:async';
import 'package:bloc/bloc.dart';

class ThemeBloc extends Bloc<int, int> {
  static final ThemeBloc _themeBlocSingleton = new ThemeBloc._internal();
  factory ThemeBloc() {
    return _themeBlocSingleton;
  }
  ThemeBloc._internal();

  int get initialState => 0;

  @override
  Stream<int> mapEventToState(
    int event,
  ) async* {
    try {
      yield event;
    } catch (_, stackTrace) {
      print('$_ $stackTrace');
      yield currentState;
    }
  }
}
