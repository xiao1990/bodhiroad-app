// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'bookmark.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

BookMark _$BookMarkFromJson(Map<String, dynamic> json) {
  return BookMark(
      id: json['id'] as int,
      articleId: json['articleId'] as int,
      type: json['type'] as int,
      title: json['title'] as String,
      actualStartIndex: json['actualStartIndex'] as int,
      actualEndIndex: json['actualEndIndex'] as int,
      startIndex: json['startIndex'] as int,
      endIndex: json['endIndex'] as int,
      pageIndex: json['pageIndex'] as int,
      chapterIndex: json['chapterIndex'] as int,
      paragraphIndex: json['paragraphIndex'] as int,
      yOffset: (json['yOffset'] as num)?.toDouble(),
      isFirst: json['isFirst'] as bool,
      markType: json['markType'] as int,
      isMarked: json['isMarked'] as bool);
}

Map<String, dynamic> _$BookMarkToJson(BookMark instance) => <String, dynamic>{
      'id': instance.id,
      'articleId': instance.articleId,
      'type': instance.type,
      'markType': instance.markType,
      'title': instance.title,
      'startIndex': instance.startIndex,
      'actualStartIndex': instance.actualStartIndex,
      'endIndex': instance.endIndex,
      'actualEndIndex': instance.actualEndIndex,
      'pageIndex': instance.pageIndex,
      'paragraphIndex': instance.paragraphIndex,
      'chapterIndex': instance.chapterIndex,
      'isMarked': instance.isMarked,
      'yOffset': instance.yOffset,
      'isFirst': instance.isFirst
    };
