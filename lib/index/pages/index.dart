import 'dart:io';
import 'dart:ui';

import 'package:bodhiroad/api/api.dart';
import 'package:bodhiroad/components/bodhiroad_dialog_confirm.dart';
import 'package:bodhiroad/components/inner_drawer.dart';
import 'package:bodhiroad/config/CommonConfig.dart';
import 'package:bodhiroad/config/GlobalColorCOnfig.dart';
import 'package:bodhiroad/index/bloc/hide_bloc.dart';
import 'package:bodhiroad/index/bloc/navigation_bloc.dart';
import 'package:bodhiroad/index/pages/merits_catalog.dart';
import 'package:bodhiroad/index/pages/task_list.dart';
import 'package:bodhiroad/theme/theme_bloc.dart';
import 'package:bodhiroad/theme/theme_state.dart';
import 'package:bodhiroad/usercenter/pages/user_center.dart';
import 'package:bodhiroad/utils/DateUtils.dart';
import 'package:bodhiroad/utils/GlobalConfigUtil.dart';
import 'package:bodhiroad/utils/Route.dart';
import 'package:bodhiroad/utils/ToastUtils.dart';
import 'package:flustars/flustars.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_downloader/flutter_downloader.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart' as prefix0;
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:package_info/package_info.dart';
import 'package:path_provider/path_provider.dart';

import 'buddhist_depositary.dart';

class AppIndexPage extends StatefulWidget {
  const AppIndexPage();

  @override
  _AppIndexReaderState createState() => _AppIndexReaderState();
}

class _AppIndexReaderState extends State<AppIndexPage> with TickerProviderStateMixin {
  Animation ani;
  NaviBloc naviBloc = NaviBloc();
  AnimationController animationController;
  Animation animation;
  final memorials = SpUtil.getObject('calendarMemorial') as Map<String, dynamic>;
  String key = DateUtils.getKey(DateTime.now());
  var _localPath;
  var taskId;

  void downLoadCallBack(id, status, progress) {
    print('Download task ($id) is in status ($status) and process ($progress)');
    if (progress == 100) {
      FlutterDownloader.open(taskId: id);
    }
  }

  prepare() async {
    FlutterDownloader.registerCallback(downLoadCallBack);
    Directory dir = await getExternalStorageDirectory();
    print(dir.path);
    _localPath = (dir.path) + '/Download';
    final savedDir = Directory(_localPath);
    bool hasExisted = await savedDir.exists();
    if (!hasExisted) {
      savedDir.create();
    }
  }

  void _requestDownload() async {
    taskId = await FlutterDownloader.enqueue(url: '${CommonConfig.DOMAIN_PROD}/apks/app-release.apk', savedDir: _localPath, showNotification: true, openFileFromNotification: true);
  }

  showUpdateDialog(BuildContext context, String description, String title, bool justClose) {
    showDialog(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context) {
          return StatefulBuilder(
            builder: (BuildContext context, setState) {
              return BodhiRoadConfirmDialog(
                title: title,
                confirmText: '立即更新',
                cancelText: justClose ? '关 闭' : '稍后更新',
                justClose: justClose,
                description: Text(description.replaceAll('-', '\n'), textScaleFactor: 1.0, style: TextStyle(fontSize: 14, color: GlobalColorConfig.mainTextColor)),
                onConfirm: () {
                  _requestDownload();
//                  Navigator.pop(context);
                },
              );
            },
          );
        });
  }

  void checkUpdate() async {
    await prepare();
    PackageInfo packageInfo = await PackageInfo.fromPlatform();
    String version = packageInfo.version;
    var res = await Api.checkVersion(data: {'version': version});
    if (res['code'] == 200) {
      if (res['data'] != null) {
        showUpdateDialog(context, res['data']['description'], '新版本 ' + res['data']['version'], false);
      }
    }
  }

  @override
  void initState() {
//    FlutterDownloader.cancelAll();
    checkUpdate();

    super.initState();
    animationController = AnimationController(vsync: this, duration: Duration(milliseconds: 200));
    animation = Tween(begin: 0.0, end: 100.0).animate(animationController);
  }

  @override
  void dispose() {
    super.dispose();
  }

  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  Widget getLeftAction() {
    return IconButton(
      icon: Icon(MdiIcons.account, size: ScreenUtil.getInstance().getSp(54)),
      onPressed: () {
        _toggle();
      },
    );
  }

  Widget getRightAction() {
    return IconButton(
      icon: Icon(
        MdiIcons.bookSearch,
        size: ScreenUtil.getInstance().getSp(54),
      ),
      onPressed: () {
        Routes.router.navigateTo(context, 'search');
      },
    );
  }

  Widget getCalendar() {
    return Stack(
      children: <Widget>[
        IconButton(
          icon: Icon(MdiIcons.calendarMonth),
          onPressed: () {
            Routes.router.navigateTo(context, 'calendarMemorial');
          },
        ),
        Positioned(
          right: 8,
          top: 8,
          child: Offstage(
              offstage: memorials == null || memorials[key] == null,
              child: Icon(
                MdiIcons.circle,
                size: 8,
                color: Colors.red,
              )),
        )
      ],
    );
  }

  GlobalKey key2 = GlobalKey();

//  Current State of InnerDrawerState
  final GlobalKey<InnerDrawerState> _innerDrawerKey = GlobalKey<InnerDrawerState>();

  void _toggle() {
    _innerDrawerKey.currentState.toggle(
        // direction is optional
        // if not set, the last direction will be used
        //InnerDrawerDirection.start OR InnerDrawerDirection.end
        direction: InnerDrawerDirection.start);
  }

  DateTime _lastPressedAt; //上次点击时间
  @override
  Widget build(BuildContext context) {
    MediaQuery.of(context);
    AppThemeBloc themeBloc = BlocProvider.of<AppThemeBloc>(context);
    return BlocBuilder(
      bloc: themeBloc,
      builder: (context, ThemeChangeState themeState) {
        return WillPopScope(
            child: Scaffold(
                backgroundColor: Colors.white,
                body: InnerDrawer(
                    key: _innerDrawerKey,
                    onTapClose: true,
                    swipe: true,
                    innerDrawerCallback: (a) => print(a),
                    leftOffset: 0.6,
                    rightOffset: 0.6,
                    leftScale: 0.9,
                    rightScale: 0.9,
                    boxShadow: [BoxShadow(color: Colors.black26, blurRadius: 30)],
                    proportionalChildArea: true,
                    // default true
                    borderRadius: 12,
                    // default 0
                    leftAnimationType: InnerDrawerAnimation.quadratic,
                    // default static
                    backgroundColor: Colors.white,
                    // default  Theme.of(context).backgroundColor
                    colorTransition: Colors.white,
                    onDragUpdate: (double val, InnerDrawerDirection direction) {},
                    leftChild: UserCenter(),
                    // required if rightChild is not set
                    backgroundDecoration: BoxDecoration(color: Colors.white, image: DecorationImage(fit: BoxFit.cover, alignment: Alignment.center, image: AssetImage('assets/texture/bgt3.jpg'))),
                    scaffold: Scaffold(
                      key: _scaffoldKey,
                      extendBody: true,
                      backgroundColor: themeState.themeConfig.mainBgColor,
                      bottomNavigationBar: BlocBuilder(
                        bloc: HideBloc(),
                        condition: (pre, cur) {
                          if (cur == 0.0) {
                            animationController.forward();
                          } else {
                            animationController.reverse();
                          }
                          return true;
                        },
                        builder: (context, st) {
                          return MultiBlocProvider(
                            providers: [
                              BlocProvider<NaviBloc>(
                                builder: (BuildContext context) => NaviBloc(),
                              ),
                            ],
                            child: SlideTransition(position: Tween(begin: Offset(0, 0), end: Offset(0, 1)).animate(animationController), child: BottomNavigator()),
                          );
                        },
                      ),
                      body: Container(
                          color: Colors.white,
                          child: BlocBuilder(
                            bloc: naviBloc,
                            builder: (context, naviSate) {
                              return IndexedStack(
                                index: naviSate,
                                children: <Widget>[
                                  MeritsCatalog(
                                    leftAction: getLeftAction(),
                                    rightAction: getCalendar(),
                                  ),
                                  TaskList(
                                    leftAction: getLeftAction(),
                                    rightAction: getCalendar(),
                                  ),
                                  BuddhistDepositary(
                                    leftAction: getLeftAction(),
                                    rightAction: getRightAction(),
                                  )
                                ],
                              );
                            },
                          )),
                    ))),
            onWillPop: () async {
              if (_lastPressedAt == null || DateTime.now().difference(_lastPressedAt) > Duration(seconds: 1)) {
                ToastUtils.success("再按一次退出");
                //两次点击间隔超过1秒则重新计时
                _lastPressedAt = DateTime.now();

                return false;
              }
              return true;
            });
      },
    );
  }
}

class BottomNavigator extends StatelessWidget {
  const BottomNavigator({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    NaviBloc naviBloc = BlocProvider.of<NaviBloc>(context);
    AppThemeBloc themeBloc = BlocProvider.of<AppThemeBloc>(context);
    GlobalKey key = GlobalKey();
    return BlocBuilder(
        bloc: themeBloc,
        builder: (context, ThemeChangeState appThemeSate) {
          return Container(
            decoration: BoxDecoration(color: Colors.white, boxShadow: [BoxShadow(blurRadius: 4, color: appThemeSate.themeConfig.shadowColor)]),
            child: BlocBuilder(
                bloc: naviBloc,
                builder: (context, naviSate) {
                  return Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Container(
                          width: MediaQuery.of(context).size.width * 0.6,
                          height: ScreenUtil.getInstance().getWidth(140),
                          color: Colors.white.withOpacity(0.2),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Expanded(
                                child: Material(
                                  color: Colors.transparent,
                                  child: InkWell(
                                    onTap: () {
                                      print(key.currentContext.size);
                                      naviBloc.dispatch(0);
                                    },
                                    child: Column(
                                      mainAxisAlignment: MainAxisAlignment.center,
                                      children: <Widget>[
                                        Icon(Icons.table_chart, color: naviSate == 0 ? appThemeSate.themeConfig.buttonActiveTextColor : appThemeSate.themeConfig.buttonTextColor),
                                        Text('功过格',
                                            key: key,
                                            textScaleFactor: 1.0,
                                            style: TextStyle(
                                                fontSize: GlobalConfigUtil.getBottomTabFontSize(),
                                                color: naviSate == 0 ? appThemeSate.themeConfig.buttonActiveTextColor : appThemeSate.themeConfig.buttonTextColor))
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                              Expanded(
                                child: Material(
                                    color: Colors.transparent,
                                    child: naviSate != 1
                                        ? InkWell(
                                            onTap: () {
                                              naviBloc.dispatch(1);
                                            },
                                            child: Column(
                                              mainAxisAlignment: MainAxisAlignment.center,
                                              children: <Widget>[
                                                Icon(Icons.book),
                                                Text(
                                                  '功课',
                                                  textScaleFactor: 1.0,
                                                  style: TextStyle(
                                                    fontSize: GlobalConfigUtil.getBottomTabFontSize(),
                                                  ),
                                                )
                                              ],
                                            ),
                                          )
                                        : Row(
                                            mainAxisAlignment: MainAxisAlignment.center,
                                            children: <Widget>[
                                              Container(
                                                width: ScreenUtil.getInstance().getWidth(125),
                                                height: ScreenUtil.getInstance().getWidth(125),
                                                decoration: BoxDecoration(
                                                  color: appThemeSate.themeConfig.mainBgColor,
                                                  borderRadius: BorderRadius.circular(ScreenUtil.getInstance().getWidth(63)),
                                                ),
                                                child: ButtonTheme(
                                                    height: 50,
                                                    minWidth: 50,
                                                    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(ScreenUtil.getInstance().getWidth(63))),
                                                    buttonColor: appThemeSate.themeConfig.embellishColorA,
                                                    child: IconButton(
                                                      color: Colors.black,
                                                      icon: Icon(
                                                        Icons.add,
                                                        color: appThemeSate.themeConfig.embellishColorA,
                                                      ),
                                                      onPressed: () {
                                                        Routes.router.navigateTo(context, 'addTask');
                                                      },
                                                    )),
                                              ),
                                            ],
                                          )),
                              ),
                              Expanded(
                                child: Material(
                                  color: Colors.transparent,
                                  child: InkWell(
                                    onTap: () {
                                      naviBloc.dispatch(2);
                                    },
                                    child: Column(
                                      mainAxisAlignment: MainAxisAlignment.center,
                                      children: <Widget>[
                                        Icon(Icons.local_library, color: naviSate == 2 ? appThemeSate.themeConfig.buttonActiveTextColor : appThemeSate.themeConfig.buttonTextColor),
                                        Text(
                                          '藏经阁',
                                          textScaleFactor: 1.0,
                                          style: TextStyle(
                                              fontSize: GlobalConfigUtil.getBottomTabFontSize(),
                                              color: naviSate == 2 ? appThemeSate.themeConfig.buttonActiveTextColor : appThemeSate.themeConfig.buttonTextColor),
                                        )
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          )),
                    ],
                  );
                }),
          );
        });
  }
}
