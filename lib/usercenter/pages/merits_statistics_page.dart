import 'dart:async';

import 'package:bodhiroad/merits/bean/merits_statistic_day.dart';
import 'package:bodhiroad/merits/bloc/merits_bloc.dart';
import 'package:bodhiroad/merits/bloc/merits_event.dart';
import 'package:bodhiroad/merits/bloc/merits_state.dart';
import 'package:bodhiroad/theme/bloc.dart';
import 'package:bodhiroad/utils/GlobalConfigUtil.dart';
import 'package:fl_chart/fl_chart.dart';
import 'package:flustars/flustars.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

int catalogId = 1;
int interval = 7;

class MeritsStatistic extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final AppThemeBloc appThemeBloc = AppThemeBloc();
    final MeritsBloc meritsBloc = MeritsBloc();
    catalogId = 1;
    interval = 7;
    return BlocBuilder(
      bloc: appThemeBloc,
      builder: (context, ThemeChangeState appThemeState) {
        return Scaffold(
          body: NestedScrollView(
            headerSliverBuilder: (context, boxIsScroll) {
              return [
                SliverAppBar(
                  backgroundColor: appThemeState.themeConfig.sectionBgColor,
                  title: Text(
                    '功过统计',
                    textScaleFactor: 1.0,
                    style: TextStyle(color: appThemeState.themeConfig.mainTextColor, fontSize: GlobalConfigUtil.getTitleFontSize()),
                  ),
                  pinned: true,
                  floating: true,
                  elevation: 4,
                  bottom: PreferredSize(
                      preferredSize: Size.fromHeight(ScreenUtil.getInstance().getWidth(700)),
                      child: Column(
                        children: <Widget>[
                          MeritsStatisticBody(appThemeBloc),
                          BlocBuilder(
                            bloc: meritsBloc,
                            builder: (context, meritsState) {
                              return Container(
                                  height: ScreenUtil.getInstance().getWidth(100),
                                  padding: EdgeInsets.symmetric(horizontal: 15),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                      ButtonTheme(
                                        minWidth: 32,
                                        height: 24,
                                        padding: EdgeInsets.symmetric(horizontal: ScreenUtil.getInstance().getSp(15)),
                                        child: FlatButton.icon(
                                          padding: EdgeInsets.symmetric(horizontal: ScreenUtil.getInstance().getSp(15)),
//                                  color: GlobalColorConfig.mainBackgroundColor,
                                          icon: Icon(
                                            Icons.swap_horizontal_circle,
                                            color: Color(0xFFA70400),
                                            size: ScreenUtil.getInstance().getSp(40),
                                          ),
                                          label: Text(catalogId == 1 ? '弟子规' : '感应篇',
                                              textScaleFactor: 1.0, style: TextStyle(fontSize: GlobalConfigUtil.getTabMainBodyFontSize(), color: Color(0xFFA70400))),
                                          onPressed: () {
                                            catalogId = catalogId == 1 ? 2 : 1;
                                            meritsBloc.dispatch(MeritsStatisticEvent(catalogId: catalogId, interval: interval));
                                          },
                                        ),
                                      ),
                                      Row(
                                        children: <Widget>[
                                          ButtonTheme(
                                            minWidth: ScreenUtil.getInstance().getWidth(80),
                                            height: ScreenUtil.getInstance().getWidth(60),
                                            padding: EdgeInsets.symmetric(horizontal: 6),
                                            child: FlatButton(
                                              color: appThemeState.themeConfig.buttonActiveColor.withOpacity(interval == 7 ? 1 : 0),
                                              child: Text(
                                                '近7天',
                                                textScaleFactor: 1.0,
                                                style: TextStyle(
                                                    fontSize: GlobalConfigUtil.getTabMainBodyFontSize(),
                                                    color: interval == 7 ? appThemeState.themeConfig.buttonActiveTextColor : appThemeState.themeConfig.mainTextColor),
                                              ),
                                              onPressed: () {
                                                interval = 7;
                                                meritsBloc.dispatch(MeritsStatisticEvent(catalogId: catalogId, interval: interval));
                                              },
                                            ),
                                          ),
                                          ButtonTheme(
                                            minWidth: ScreenUtil.getInstance().getWidth(80),
                                            height: ScreenUtil.getInstance().getWidth(60),
                                            padding: EdgeInsets.symmetric(horizontal: 6),
                                            child: FlatButton(
//                                      color:
//                                          GlobalColorConfig.mainBackgroundColor,
                                              color: appThemeState.themeConfig.buttonActiveColor.withOpacity(interval == 30 ? 1 : 0),
                                              child: Text(
                                                '近30天',
                                                textScaleFactor: 1.0,
                                                style: TextStyle(
                                                    fontSize: GlobalConfigUtil.getTabMainBodyFontSize(),
                                                    color: interval == 30 ? appThemeState.themeConfig.buttonActiveTextColor : appThemeState.themeConfig.mainTextColor),
                                              ),
                                              onPressed: () {
                                                interval = 30;
                                                meritsBloc.dispatch(MeritsStatisticEvent(catalogId: catalogId, interval: interval));
                                              },
                                            ),
                                          ),
                                          ButtonTheme(
                                            minWidth: ScreenUtil.getInstance().getWidth(80),
                                            height: ScreenUtil.getInstance().getWidth(60),
                                            padding: EdgeInsets.symmetric(horizontal: ScreenUtil.getInstance().getWidth(15)),
                                            child: FlatButton(
//                                      color:
//                                          GlobalColorConfig.mainBackgroundColor,
                                              color: appThemeState.themeConfig.buttonActiveColor.withOpacity(interval == 180 ? 1 : 0),
                                              child: Text(
                                                '近半年',
                                                textScaleFactor: 1.0,
                                                style: TextStyle(
                                                    fontSize: GlobalConfigUtil.getTabMainBodyFontSize(),
                                                    color: interval == 180 ? appThemeState.themeConfig.buttonActiveTextColor : appThemeState.themeConfig.mainTextColor),
                                              ),
                                              onPressed: () {
                                                interval = 180;
                                                meritsBloc.dispatch(MeritsStatisticEvent(catalogId: catalogId, interval: interval));
                                              },
                                            ),
                                          ),
                                        ],
                                      )
                                    ],
                                  ));
                            },
                          ),
                          Container(
                            height: ScreenUtil.getInstance().getWidth(100),
                            decoration: BoxDecoration(
                                border: Border(
                              bottom: BorderSide(color: appThemeState.themeConfig.lightBorderColor, width: 0.5),
                            )),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: <Widget>[
                                Text(
                                  '功',
                                  textScaleFactor: 1.0,
                                ),
                                Container(
                                  width: 0.5,
                                  color: appThemeState.themeConfig.mainBorderColor,
                                  height: ScreenUtil.getInstance().getWidth(60),
                                ),
                                Text(
                                  '过',
                                  textScaleFactor: 1.0,
                                ),
                              ],
                            ),
                          )
                        ],
                      )),
                )
              ];
            },
            body: Container(
              decoration: BoxDecoration(image: DecorationImage(fit: BoxFit.cover, image: AssetImage('assets/texture/bgt2.jpg'))),
              child: BlocBuilder(
                bloc: MeritsBloc(),
                builder: (context, state) {
                  if (state is MeritsStatisticLoadState) {
                    return Row(
                      children: <Widget>[
                        Expanded(
                          child: Container(
                              alignment: Alignment.topCenter,
                              child: ListView(
                                controller: ScrollController(),
                                physics: BouncingScrollPhysics(),
                                padding: EdgeInsets.symmetric(vertical: 10),
                                children: getTimeLine(state.list[0].list, appThemeState, 1),
                              )),
                        ),
                        Expanded(
                          child: Container(
                              child: ListView(
                            controller: ScrollController(),
                            physics: BouncingScrollPhysics(),
                            padding: EdgeInsets.symmetric(vertical: 10),
                            children: getTimeLine(state.list[1].list, appThemeState, 0),
                          )),
                        ),
                      ],
                    );
                  } else {
                    return Container();
                  }
                },
              ),
            ),

//        child: MeritsStatisticBody(),
          ),
        );
      },
    );
  }

  List<Widget> getTimeLine(List<MeritsStatisticDay> list, ThemeChangeState appThemeState, int type) {
    List<MeritsStatisticDay> filteredList = list.where((item) => item.list.length > 0).toList();
    List<Widget> items = filteredList.reversed
        .map(
          (item) => Container(
            alignment: Alignment.center,
            margin: EdgeInsets.only(bottom: 20),
            child: Container(
                width: ScreenUtil.getInstance().screenWidth * 0.5 - 25,
                decoration: BoxDecoration(
                  boxShadow: [BoxShadow(color: appThemeState.themeConfig.shadowColor, blurRadius: 8)],
                  borderRadius: BorderRadius.all(Radius.circular(4)),
                  color: appThemeState.themeConfig.sectionBgColor,
                ),
                child: Column(
                  mainAxisSize: MainAxisSize.max,
                  children: <Widget>[
                    Container(
                      height: ScreenUtil.getInstance().getWidth(10),
                    ),
                    Container(
                      height: ScreenUtil.getInstance().getWidth(96),
//                      padding: EdgeInsets.symmetric(horizontal: 8),
                      decoration: BoxDecoration(
//                        border: Border(left: BorderSide(width: 3, color: Colors.orange)),
                          ),
                      alignment: Alignment.centerLeft,
                      child: Row(
                        children: <Widget>[
                          Container(
                            width: 3,
                            height: 18,
                            color: type == 1 ? Colors.green : Color(0xFFA70400),
                          ),
                          Padding(
                            padding: EdgeInsets.only(left: 10),
                            child: Text(
                              item.day,
                              textScaleFactor: 1.0,
                              style:
                                  TextStyle(fontWeight: FontWeight.bold, color: appThemeState.themeConfig.titleBarFontColor, fontSize: GlobalConfigUtil.getTabMainBodyFontSize()),
                            ),
                          )
                        ],
                      ),
                    ),
                    Divider(
                      color: appThemeState.themeConfig.lightBorderColor,
                      height: 0.5,
                    )
                  ]..addAll(item.list
                      .where((e) => e.title != null)
                      .map((unit) => Container(
                            height: ScreenUtil.getInstance().getWidth(90),
                            alignment: Alignment.centerLeft,
                            padding: EdgeInsets.symmetric(horizontal: 12),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Text(
                                  unit.title ?? '无',
                                  textScaleFactor: 1.0,
                                  style: TextStyle(fontSize: GlobalConfigUtil.getTabMainBodyFontSize()),
                                ),
                              ],
                            ),
                            decoration: BoxDecoration(
                              border: Border(bottom: unit == item.list.last ? BorderSide.none : BorderSide(color: appThemeState.themeConfig.mainBorderColor, width: 0.2)),
//                              image: DecorationImage(
//                                  fit: BoxFit.cover,
//                                  image: AssetImage('assets/item-back2.jpg'))
                            ),
                          ))
                      .toList()),
                )),
            // ),
          ),
        )
        .toList();

    return items;
  }
}

class MeritsStatisticBody extends StatefulWidget {
  final AppThemeBloc appThemeBloc;
  MeritsStatisticBody(this.appThemeBloc);
  @override
  State<StatefulWidget> createState() => MeritsStatisticBodyState();
}

class MeritsStatisticBodyState extends State<MeritsStatisticBody> {
  StreamController<LineTouchResponse> controller;
  MeritsBloc _meritsBloc = MeritsBloc();
  @override
  void initState() {
    super.initState();
    _meritsBloc.dispatch(MeritsStatisticEvent(catalogId: catalogId, interval: 7));
    controller = StreamController();
    controller.stream.distinct().listen((LineTouchResponse response) {
      print('response: ${response.touchInput}');
    });
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder(
      bloc: widget.appThemeBloc,
      builder: (context, ThemeChangeState appThemeState) {
        return Container(
          height: ScreenUtil.getInstance().getWidth(500),
//            decoration:
//                BoxDecoration(color: appThemeState.themeConfig.sectionBgColor),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              SizedBox(
                height: ScreenUtil.getInstance().getWidth(48),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    Container(
                      height: 2,
                      width: 36,
                      color: Colors.green,
                    ),
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: 8),
                      child: Text(
                        '过',
                        textScaleFactor: 1.0,
                        style: TextStyle(color: appThemeState.themeConfig.mainTextColor),
                      ),
                    ),
                    Container(
                      height: 2,
                      width: 36,
                      color: appThemeState.themeConfig.meritsColorA,
                    ),
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: 8),
                      child: Text(
                        '功',
                        textScaleFactor: 1.0,
                        style: TextStyle(color: appThemeState.themeConfig.mainTextColor),
                      ),
                    )
                  ],
                ),
              ),
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.only(right: 16.0, left: 6.0),
                  child: BlocBuilder(
                    bloc: _meritsBloc,
                    condition: (prev, curt) {
                      if (curt is MeritsStatisticLoadState) {
                        return true;
                      } else {
                        return false;
                      }
                    },
                    builder: (context, state) {
                      if (state is MeritsStatisticLoadState) {
                        return FlChart(
                          chart: LineChart(
                            LineChartData(
                              lineTouchData: LineTouchData(
                                  touchResponseSink: controller.sink,
                                  touchTooltipData: TouchTooltipData(
                                    tooltipBgColor: Colors.blueGrey.withOpacity(0.8),
                                  )),
                              gridData: FlGridData(
                                show: true,
                                drawHorizontalGrid: true,
                                getDrawingVerticalGridLine: (value) {
                                  return FlLine(
                                    color: appThemeState.themeConfig.dividerColor,
                                    strokeWidth: 0.5,
                                  );
                                },
                                getDrawingHorizontalGridLine: (value) {
                                  return FlLine(
                                    color: appThemeState.themeConfig.dividerColor,
                                    strokeWidth: 0.5,
                                  );
                                },
                              ),
                              titlesData: FlTitlesData(
                                  bottomTitles: SideTitles(
                                      textStyle: TextStyle(
                                        color: appThemeState.themeConfig.mainTextColor,
                                        fontSize: 10,
                                      ),
                                      margin: 10,
                                      showTitles: true,
                                      getTitles: (index) {
                                        return state.list[0].list[index.toInt()].day.substring(8).replaceRange(1, 1, interval > 7 ? '\n' : '');
                                      }),
                                  leftTitles: SideTitles(
                                    textStyle: TextStyle(
                                      color: appThemeState.themeConfig.mainTextColor,
                                      fontSize: 10,
                                    ),
                                    margin: 8,
                                    showTitles: true,
                                    reservedSize: 30,
                                    getTitles: (value) {
                                      return value.toInt().toString();
                                    },
                                  )),
                              borderData: FlBorderData(
                                  show: true,
                                  border: Border(
                                    bottom: BorderSide(
                                      color: appThemeState.themeConfig.mainTextColor,
                                      width: 3,
                                    ),
                                    left: BorderSide(
                                      color: Colors.transparent,
                                    ),
                                    right: BorderSide(
                                      color: Colors.transparent,
                                    ),
                                    top: BorderSide(
                                      color: Colors.transparent,
                                    ),
                                  )),
                              minX: 0,
                              maxX: state.list[0].list.length.toDouble() - 1,
                              maxY: state.maxY.toDouble() == 0 ? interval.toDouble() : state.maxY.toDouble(),
                              minY: 0,
                              lineBarsData: [
                                LineChartBarData(
                                  spots: List.generate(state.list[0].list.length, (index) {
                                    return FlSpot(index.toDouble(), state.list[0].list[index].list.length.toDouble());
                                  }).toList(),
                                  isCurved: false,
                                  colors: [
                                    Colors.green,
                                  ],
                                  barWidth: 1,
                                  isStrokeCapRound: true,
                                  dotData: FlDotData(show: true, dotSize: 2, dotColor: appThemeState.themeConfig.embellishColorB),
                                  belowBarData: BelowBarData(show: true, colors: [Colors.black12.withOpacity(0.2)]),
                                ),
                                LineChartBarData(
                                  spots: List.generate(state.list[1].list.length, (index) {
                                    return FlSpot(index.toDouble(), state.list[1].list[index].list.length.toDouble());
                                  }).toList(),
                                  isCurved: false,
                                  colors: [
                                    appThemeState.themeConfig.meritsColorA,
                                  ],
                                  barWidth: 1,
                                  isStrokeCapRound: true,
                                  dotData: FlDotData(show: true, dotSize: 2, dotColor: appThemeState.themeConfig.embellishColorB),
                                  belowBarData: BelowBarData(show: true, colors: [Colors.black12.withOpacity(0.2)]),
                                ),
                              ],
                            ),
                          ),
                        );
                      } else {
                        return Container();
                      }
                    },
                  ),
                ),
              ),
            ],
          ),
        );
      },
    );
  }

  @override
  void dispose() {
    super.dispose();
    controller.close();
  }
}
