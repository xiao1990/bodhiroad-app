import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class StatisticEvent extends Equatable {
  StatisticEvent([props = const []]) : super(props);
  String toString() => 'StatisticEvent';
}

class StatisticInitFinshEvent extends StatisticEvent {
  @override
  String toString() => 'StatisticInitFinshEvent';
}

class StatisticLoadDataEvent extends StatisticEvent {
  @override
  String toString() => 'StatisticLoadDataEvent';
}

class StatisticChangeCataEvent extends StatisticEvent {
  final int taskType;
  StatisticChangeCataEvent({this.taskType}) : super([taskType]);
  @override
  String toString() => 'StatisticChangeCataEvent';
}
