// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'book_char.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CharBean _$CharBeanFromJson(Map<String, dynamic> json) {
  return CharBean(
      xOffset: (json['xOffset'] as num)?.toDouble(),
      yOffset: (json['yOffset'] as num)?.toDouble(),
      yOffset2: (json['yOffset2'] as num)?.toDouble(),
      char: json['char'] as String,
      fontSize: (json['fontSize'] as num)?.toDouble(),
      type: json['type'] as int,
      width: (json['width'] as num)?.toDouble(),
      height: (json['height'] as num)?.toDouble(),
      maxLine: json['maxLine'] as int,
      tag: json['tag'] as String,
      nodeIndex: json['nodeIndex'] as int,
      charIndex: json['charIndex'] as int,
      lastPageCharsHeight: (json['lastPageCharsHeight'] as num)?.toDouble(),
      marked: json['marked'] as int,
      prefix: (json['prefix'] as num)?.toDouble(),
      surfix: (json['surfix'] as num)?.toDouble(),
      maxCount: json['maxCount'] as int,
      paragraphHead: json['paragraphHead'] as int,
      paragraphEnd: json['paragraphEnd'] as bool,
      isPinyin: json['isPinyin'] as bool,
      rareText: json['rareText'] as bool)
    ..pinyinWidth = (json['pinyinWidth'] as num)?.toDouble()
    ..pinyin = json['pinyin'] as String;
}

Map<String, dynamic> _$CharBeanToJson(CharBean instance) => <String, dynamic>{
      'char': instance.char,
      'xOffset': instance.xOffset,
      'yOffset': instance.yOffset,
      'yOffset2': instance.yOffset2,
      'fontSize': instance.fontSize,
      'height': instance.height,
      'width': instance.width,
      'type': instance.type,
      'maxLine': instance.maxLine,
      'tag': instance.tag,
      'lastPageCharsHeight': instance.lastPageCharsHeight,
      'nodeIndex': instance.nodeIndex,
      'charIndex': instance.charIndex,
      'marked': instance.marked,
      'paragraphHead': instance.paragraphHead,
      'paragraphEnd': instance.paragraphEnd,
      'prefix': instance.prefix,
      'surfix': instance.surfix,
      'maxCount': instance.maxCount,
      'pinyinWidth': instance.pinyinWidth,
      'pinyin': instance.pinyin,
      'rareText': instance.rareText,
      'isPinyin': instance.isPinyin
    };
