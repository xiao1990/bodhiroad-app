import 'package:bodhiroad/config/CommonConfig.dart';
import 'package:bodhiroad/usercenter/bloc/download_bloc.dart';
import 'package:bodhiroad/usercenter/bloc/download_event.dart';
import 'package:bodhiroad/utils/GlobalConfigUtil.dart';
import 'package:bodhiroad/utils/Route.dart';
import 'package:flustars/flustars.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

class DictDownload extends StatefulWidget {
  @override
  _DictDownloadState createState() => _DictDownloadState();
}

class _DictDownloadState extends State<DictDownload> {
  DownloadBloc downloadBloc = DownloadBloc();

  @override
  void initState() {
    downloadBloc.dispatch(DictDownloadInitEvent());
    super.initState();
  }

  @override
  void dispose() {
    downloadBloc.dispatch(DisposeEvent());
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          '下载辞典数据',
          style: TextStyle(fontSize: GlobalConfigUtil.getTitleFontSize()),
        ),
        backgroundColor: Colors.white,
        elevation: 0,
        actions: <Widget>[
          FlatButton.icon(
              onPressed: () {
                Routes.router.pop(context);
                Routes.router.navigateTo(context, 'dictSearch');
              },
              icon: Icon(MdiIcons.bookSearchOutline, size: ScreenUtil.getInstance().getSp(45), color: Color(0xFFA70400)),
              label: Text(
                '去查辞',
                style: TextStyle(color: Color(0xFFA70400), fontSize: GlobalConfigUtil.getTabMainBodyFontSize()),
              ))
        ],
      ),
      body: Container(
        decoration: BoxDecoration(image: DecorationImage(image: AssetImage('assets/texture/bgt2.png'), fit: BoxFit.cover)),
        padding: EdgeInsets.symmetric(horizontal: ScreenUtil.getInstance().getWidth(40), vertical: ScreenUtil.getInstance().getWidth(50)),
        child: BlocBuilder(
            bloc: downloadBloc,
            builder: (context, state) {
              return ListView.builder(
                  itemCount: CommonConfig.dictList.length,
                  physics: BouncingScrollPhysics(),
                  itemBuilder: (context, index) {
                    return Container(
                        width: ScreenUtil.getInstance().screenWidth - ScreenUtil.getInstance().getWidth(80),
                        margin: EdgeInsets.only(top: ScreenUtil.getInstance().getWidth(40)),
                        decoration: BoxDecoration(
                            image: DecorationImage(image: AssetImage('assets/texture/item_bg.jpg'), fit: BoxFit.cover),
                            borderRadius: BorderRadius.circular(8),
                            boxShadow: [BoxShadow(color: Colors.black12, blurRadius: 12)]),
                        height: ScreenUtil.getInstance().getWidth(200),
                        child: Row(
                          children: <Widget>[
                            Expanded(
                                child: Column(
                              children: <Widget>[
                                Container(
                                  height: ScreenUtil.getInstance().getWidth(40),
                                  alignment: Alignment.centerLeft,
                                ),
                                Container(
                                  height: ScreenUtil.getInstance().getWidth(50),
                                  alignment: Alignment.centerLeft,
                                  padding: EdgeInsets.only(left: ScreenUtil.getInstance().getWidth(40)),
                                  decoration: BoxDecoration(border: Border(left: BorderSide(color: Color(0xFFA70400).withOpacity(0.4), width: 6))),
                                  child: Text(
                                    CommonConfig.dictList[index]['author'],
                                    style: TextStyle(fontSize: GlobalConfigUtil.getTabMainBodyFontSize(), color: Colors.black38),
                                  ),
                                ),
                                Container(
                                  height: ScreenUtil.getInstance().getWidth(110),
                                  alignment: Alignment.centerLeft,
                                  padding: EdgeInsets.only(left: ScreenUtil.getInstance().getWidth(40)),
                                  child: Text(
                                    CommonConfig.dictList[index]['name'],
                                    style: TextStyle(fontSize: GlobalConfigUtil.getSubTitleFontSize()),
                                  ),
                                )
                              ],
                            )),
                            Container(
                              alignment: Alignment.center,
                              padding: EdgeInsets.only(right: 10),
                              child: FlatButton(
                                  padding: EdgeInsets.all(0),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                      SpUtil.getInt(CommonConfig.dictList[index]['fileName'].toString() + '_status') == 1
                                          ? SpinKitRing(
                                              color: Color(0xFFB96652),
                                              lineWidth: 2,
                                              size: 16,
                                            )
                                          : SpUtil.getInt(CommonConfig.dictList[index]['fileName'].toString() + '_status') != 3
                                              ? Icon(
                                                  MdiIcons.cloudDownload,
                                                  size: ScreenUtil.getInstance().getSp(45),
                                                  color: Color(0xFFA70400),
                                                )
                                              : Icon(
                                                  Icons.done,
                                                  size: ScreenUtil.getInstance().getSp(45),
                                                  color: Colors.black38,
                                                ),
                                      Text(
                                        SpUtil.getInt(CommonConfig.dictList[index]['fileName'].toString() + '_status') == 1
                                            ? ' 已下载' + SpUtil.getInt(CommonConfig.dictList[index]['fileName'].toString() + '_progress').toString() + '%'
                                            : SpUtil.getInt(CommonConfig.dictList[index]['fileName'].toString() + '_status') == 2
                                                ? ' 重新下载'
                                                : SpUtil.getInt(CommonConfig.dictList[index]['fileName'].toString() + '_status') == 3 ? ' 已下载' : ' 下载',
                                        style: TextStyle(
                                            color: SpUtil.getInt(CommonConfig.dictList[index]['fileName'].toString() + '_status') == 3 ? Colors.black38 : Color(0xFFA70400),
                                            fontSize: GlobalConfigUtil.getTabMainBodyFontSize()),
                                      )
                                    ],
                                  ),
                                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(4)),
                                  onPressed: () {
                                    /// 0 下载完成 1 正在下载 2下载失败

                                    /// 1 下载失败 2未下载
                                    if (SpUtil.getInt(CommonConfig.dictList[index]['fileName'].toString() + '_status') == 2 ||
                                        SpUtil.getInt(CommonConfig.dictList[index]['fileName'].toString() + '_status') == 0) {
                                      downloadBloc.dispatch(DictDownloadEvent(path: CommonConfig.dictList[index]['fileName'], source: index + 1));
                                    }
                                  }),
                            )
                          ],
                        ));
                  });

//                children: List.generate(CommonConfig.dictList.length, (index) {
//                  return ;

//
//                    Container(
//                    decoration: BoxDecoration(border: Border.all(color: Colors.black12, width: 0.5), color: Colors.white, borderRadius: BorderRadius.circular(8)),
//                    child: Column(
//                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                      mainAxisSize: MainAxisSize.max,
//                      children: <Widget>[
//                        Row(crossAxisAlignment: CrossAxisAlignment.start, mainAxisAlignment: MainAxisAlignment.end, children: [
//                          Expanded(
//                              flex: 2,
//                              child: Container(
//                                  padding: EdgeInsets.only(top: 50),
//                                  child: Row(
//                                    mainAxisAlignment: MainAxisAlignment.end,
//                                    children: getVerticalText(CommonConfig.dictList[index]['author'], ScreenUtil.getInstance().getSp(36), Color(0xFF554B47)),
//                                  ))),
//                          Expanded(
//                              flex: 1,
//                              child: Row(
//                                  crossAxisAlignment: CrossAxisAlignment.start,
//                                  children: getVerticalText(CommonConfig.dictList[index]['name'], ScreenUtil.getInstance().getSp(48), Color(0xFFB96652)))),
//                        ]),
//                        Row(children: <Widget>[
//                          Expanded(
//                              child: ButtonTheme(
//                            child: FlatButton(
//                                padding: EdgeInsets.all(0),
//                                child: Row(
//                                  mainAxisAlignment: MainAxisAlignment.center,
//                                  children: <Widget>[
//                                    SpUtil.getInt(CommonConfig.dictList[index]['fileName'].toString() + '_status') == 1
//                                        ? SpinKitRing(
//                                            color: Color(0xFFB96652),
//                                            lineWidth: 2,
//                                            size: 16,
//                                          )
//                                        : SpUtil.getInt(CommonConfig.dictList[index]['fileName'].toString() + '_progress') < 100
//                                            ? Icon(
//                                                MdiIcons.cloudDownload,
//                                                size: 18,
//                                              )
//                                            : Icon(
//                                                Icons.done,
//                                                size: 18,
//                                              ),
//                                    Text(SpUtil.getInt(CommonConfig.dictList[index]['fileName'].toString() + '_status') == 1
//                                        ? ' 已下载' + SpUtil.getInt(CommonConfig.dictList[index]['fileName'].toString() + '_progress').toString() + '%'
//                                        : SpUtil.getInt(CommonConfig.dictList[index]['fileName'].toString() + '_progress') < 100 ? ' 下载' : ' 已下载')
//                                  ],
//                                ),
//                                shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(4)),
////                                borderSide: BorderSide(color: Colors.black12, width: 0.5),
//                                onPressed: () {
////                                  SpUtil.remove(CommonConfig.dictList[index]['fileName'].toString() + '_progress');
//                                  if (SpUtil.getInt(CommonConfig.dictList[index]['fileName'].toString() + '_status') == 0 ||
//                                      (SpUtil.getInt(CommonConfig.dictList[index]['fileName'].toString() + '_status') == 1 &&
//                                          SpUtil.getInt(CommonConfig.dictList[index]['fileName'].toString() + '_progress') != 100 &&
//                                          state.taskId != CommonConfig.dictList[index]['fileName'].toString())) {
//                                    downloadBloc.dispatch(DictDownloadEvent(path: CommonConfig.dictList[index]['fileName']));
//                                  }
//                                }),
//                          ))
//                        ]),
//                      ],
//                    ),
//                  );
//                }),
//              );
            }),
      ),
    );
  }
}
