import 'package:bodhiroad/api/api.dart';
import 'package:bodhiroad/utils/ToastUtils.dart';
import 'package:flutter/material.dart';

class EditorPage extends StatefulWidget {
  final String fileUrl;
  final int taskId;
  EditorPage({this.fileUrl, this.taskId});

  @override
  EditorPageState createState() => EditorPageState();
}

class EditorPageState extends State<EditorPage> {
  /// Allows to control the editor and the document.
  TextEditingController _controller;

  @override
  void initState() {
    super.initState();
    _controller = TextEditingController();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(
            "纠错反馈",
            style: TextStyle(color: Colors.black87),
          ),
          elevation: 0,
          actions: <Widget>[
            FlatButton.icon(
                icon: Icon(
                  Icons.done,
                  color: Colors.black87,
                ),
                onPressed: () async {
                  final res = await Api.feedback(data: {'content': _controller.text, 'fileUrl': widget.fileUrl, 'taskId': widget.taskId});
                  if (res['code'] == 200) {
                    Navigator.pop(context);
                    ToastUtils.success('提交成功');
                  } else {
                    ToastUtils.fail('提交失败');
                  }
                },
                label: Text(
                  '提交',
                  style: TextStyle(fontSize: 16, color: Colors.black87),
                ))
          ],
          backgroundColor: Colors.white,
        ),
        body: LayoutBuilder(
          builder: (context, constraints) {
            return Container(
              height: constraints.maxHeight,
              width: constraints.maxWidth,
              decoration: BoxDecoration(
                  image: DecorationImage(
                fit: BoxFit.cover,
                image: AssetImage('assets/texture/bgt2.jpg'),
              )),
              padding: EdgeInsets.all(15),
              child: TextField(
                controller: _controller,
                maxLines: 20,
                minLines: 5,
                decoration: new InputDecoration(
                  labelText: '请详细描述错误信息',
                  labelStyle: TextStyle(color: Colors.black54),
                  border: OutlineInputBorder(borderSide: BorderSide(color: Color(0xFFF2F2F2), width: 0.5)),
                ),
              ),
            );
          },
        ));
  }
}
