import 'dart:io';

import 'package:bodhiroad/components/datetime_picker/flutter_cupertino_date_picker.dart';
import 'package:bodhiroad/components/datetime_picker/src/date_time_formatter.dart';
import 'package:bodhiroad/config/CommonConfig.dart';
import 'package:bodhiroad/theme/bloc.dart';
import 'package:bodhiroad/usercenter/bean/user.dart';
import 'package:bodhiroad/usercenter/bloc/login/login_bloc.dart';
import 'package:bodhiroad/usercenter/bloc/login/login_event.dart';
import 'package:bodhiroad/usercenter/bloc/user/bloc.dart';
import 'package:bodhiroad/utils/GlobalConfigUtil.dart';
import 'package:bodhiroad/utils/Route.dart';
import 'package:bodhiroad/utils/parameter_convert_cn.dart';
import 'package:fluro/fluro.dart';
import 'package:flustars/flustars.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:global_configuration/global_configuration.dart';
import 'package:image_picker/image_picker.dart';
import 'package:bodhiroad/components/city_pickers/lib/city_pickers.dart';

class UserProfile extends StatefulWidget {
  @override
  _UserProfileState createState() => _UserProfileState();
}

class _UserProfileState extends State<UserProfile> {
  AppThemeBloc appThemeBloc = AppThemeBloc();
  UserBloc userBloc = UserBloc();
  File croppedFile;
  User usr = User();
  LoginBloc loginBloc = LoginBloc();
  @override
  void initState() {
    // userBloc.dispatch(InitialUserEvent());
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder(
      bloc: userBloc,
      condition: (previousState, currentState) {
        if (currentState is UserUpdateState) {
          // usr = currentState.user;
          return true;
        } else {
          return false;
        }
      },
      builder: (context, userState) {
        return BlocBuilder(
            bloc: appThemeBloc,
            builder: (context, ThemeChangeState appThemeState) {
              return Scaffold(
                appBar: AppBar(
                  backgroundColor: appThemeState.themeConfig.sectionBgColor,
                  title: Text(
                    '个人中心',
                    textScaleFactor: 1.0,
                    style: TextStyle(color: appThemeState.themeConfig.mainTextColor, fontSize: GlobalConfigUtil.getTitleFontSize()),
                  ),
                  elevation: 0,
                ),
                extendBody: true,
                body: Container(
                  decoration: BoxDecoration(image: DecorationImage(fit: BoxFit.cover, image: AssetImage('assets/texture/bgt2.jpg'))),
                  padding: EdgeInsets.symmetric(vertical: ScreenUtil.getInstance().getWidth(40)),
                  child: ListView(
                    children: <Widget>[
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Container(
                            height: ScreenUtil.getInstance().getWidth(195),
                            width: MediaQuery.of(context).size.width - ScreenUtil.getInstance().getWidth(80),
                            padding: EdgeInsets.symmetric(horizontal: ScreenUtil.getInstance().getWidth(40)),
                            decoration: BoxDecoration(
                                color: appThemeState.themeConfig.sectionBgColor,
                                borderRadius: BorderRadius.circular(8),
                                boxShadow: [BoxShadow(color: appThemeState.themeConfig.shadowColor, blurRadius: 15)]),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Expanded(
                                  child: GestureDetector(
                                      onTap: () {
                                        String title = ParameterConvertUtils.convertCN('修改昵称');
                                        String value = ParameterConvertUtils.convertCN(GlobalConfiguration().getString('name') ?? '');
                                        Routes.router.navigateTo(context, 'profileUpdate?title=${title}&properties=name&value=${value}');
                                      },
                                      child: Container(
                                        height: ScreenUtil.getInstance().getWidth(195),
                                        alignment: Alignment.centerLeft,
                                        child: Text(
                                          GlobalConfiguration().getString('name'),
                                          textScaleFactor: 1.0,
                                          overflow: TextOverflow.ellipsis,
                                          style: TextStyle(color: appThemeState.themeConfig.mainTextColor, fontSize: GlobalConfigUtil.getTabMainBodyFontSize()),
                                        ),
                                      )),
                                ),
                                Expanded(
                                  child: GestureDetector(
                                    onTap: () async {
                                      File _image = await ImagePicker.pickImage(source: ImageSource.gallery);

                                      GlobalConfiguration().loadFromMap({'filePath': _image.path});
                                      Routes.router.navigateTo(context, 'imageCrop', transition: TransitionType.cupertino);
                                    },
                                    child: Container(
                                      alignment: Alignment.centerRight,
                                      child: ClipOval(
                                        child: GlobalConfiguration().getString('headPortraitUrl') != null && GlobalConfiguration().getString('headPortraitUrl') != ''
                                            ? Image.file(
                                                File(GlobalConfiguration().getString('headPortraitUrl')),
                                                width: 48,
                                                height: 48,
                                              )
                                            : Image.asset(
                                                'assets/default.png',
                                                width: 48,
                                                height: 48,
                                              ),
                                      ),
                                    ),
                                  ),
                                )
                              ],
                            ),
                          ),
                        ],
                      ),
                      Container(
                        height: 20,
                      ),
                      Row(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
                        Container(
                            width: MediaQuery.of(context).size.width - ScreenUtil.getInstance().getWidth(80),
//                      padding: EdgeInsets.symmetric(horizontal: 15),
                            decoration: BoxDecoration(
                                color: appThemeState.themeConfig.sectionBgColor,
                                borderRadius: BorderRadius.circular(8),
                                boxShadow: [BoxShadow(color: appThemeState.themeConfig.shadowColor, blurRadius: 15)]),
                            child: Column(
                              children: <Widget>[
                                ListTile(
                                  title: Text('法号',
                                      textScaleFactor: 1.0, style: TextStyle(color: appThemeState.themeConfig.mainTextColor, fontSize: GlobalConfigUtil.getTabMainBodyFontSize())),
                                  trailing: Container(
                                    width: ScreenUtil.getInstance().getWidth(500),
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.end,
                                      children: <Widget>[
                                        Text(
                                          GlobalConfiguration().getString('fahao') ?? '',
                                          textScaleFactor: 1.0,
                                          style: TextStyle(fontSize: GlobalConfigUtil.getTabMainBodyFontSize()),
                                        ),
                                        Icon(Icons.keyboard_arrow_right, color: appThemeState.themeConfig.subTitleTextColor)
                                      ],
                                    ),
                                  ),
                                  onTap: () {
                                    String title = ParameterConvertUtils.convertCN('修改法号');
                                    String value = ParameterConvertUtils.convertCN(GlobalConfiguration().getString('fahao') ?? '');
                                    Routes.router.navigateTo(context, 'profileUpdate?title=${title}&properties=fahao&value=${value}');
                                  },
                                ),
//                          Divider(
//                            color: Colors.black12,
//                          ),
                                ListTile(
                                  onTap: () {
                                    showModalBottomSheet(
                                        context: context,
                                        builder: (context) {
                                          return GenderSelector();
                                        });
                                  },
                                  title: Text(
                                    '性别',
                                    textScaleFactor: 1.0,
                                    style: TextStyle(color: appThemeState.themeConfig.mainTextColor, fontSize: GlobalConfigUtil.getTabMainBodyFontSize()),
                                  ),
                                  trailing: Container(
                                    width: ScreenUtil.getInstance().getWidth(500),
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.end,
                                      children: <Widget>[
                                        Text(GlobalConfiguration().getString('gender') ?? '',
                                            textScaleFactor: 1.0,
                                            style: TextStyle(color: appThemeState.themeConfig.mainTextColor, fontSize: GlobalConfigUtil.getTabMainBodyFontSize())),
                                        Icon(Icons.keyboard_arrow_right, color: appThemeState.themeConfig.subTitleTextColor)
                                      ],
                                    ),
                                  ),
                                ),
//                          Divider(
//                            color: Colors.black12,
//                          ),
                                ListTile(
                                  onTap: () {
                                    DatePicker.showDatePicker(
                                      context,
                                      pickerTheme: DateTimePickerTheme(
                                        showTitle: true,
                                        confirm: Text('确定', textScaleFactor: 1.0, style: TextStyle(color: appThemeState.themeConfig.mainTextColor)),
                                        cancel: Text('取消', textScaleFactor: 1.0, style: TextStyle(color: appThemeState.themeConfig.mainTextColor)),
                                      ),
                                      minDateTime: DateTime.parse('1900-01-01'),
                                      maxDateTime: DateTime.parse('2900-01-01'),
                                      initialDateTime: DateTime.parse(GlobalConfiguration().getString('birthday') ?? DateTime.now().toString()),
                                      dateFormat: 'yyyy-MMMM-dd',
                                      locale: DateTimePickerLocale.zh_cn,
                                      onCancel: () {
                                        debugPrint('onCancel');
                                      },
                                      onChange: (dateTime, List<int> index) {},
                                      onConfirm: (dateTime, List<int> index) {
                                        print(DateTimeFormatter.formatDate(dateTime, 'yyyy-MM-dd', DateTimePickerLocale.zh_cn));
                                        userBloc.dispatch(
                                            UserUpdateEvent(map: <String, dynamic>{'birthday': DateTimeFormatter.formatDate(dateTime, 'yyyy-MM-dd', DateTimePickerLocale.zh_cn)}));
                                      },
                                    );
                                  },
                                  title: Text(
                                    '出生年月',
                                    textScaleFactor: 1.0,
                                    style: TextStyle(color: appThemeState.themeConfig.mainTextColor, fontSize: GlobalConfigUtil.getTabMainBodyFontSize()),
                                  ),
                                  trailing: Container(
                                    width: ScreenUtil.getInstance().getWidth(500),
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.end,
                                      children: <Widget>[
                                        Text(GlobalConfiguration().getString('birthday') ?? '',
                                            textScaleFactor: 1.0,
                                            style: TextStyle(color: appThemeState.themeConfig.mainTextColor, fontSize: GlobalConfigUtil.getTabMainBodyFontSize())),
                                        Icon(Icons.keyboard_arrow_right, color: appThemeState.themeConfig.subTitleTextColor)
                                      ],
                                    ),
                                  ),
                                ),
//                          Divider(
//                            color: Colors.black12,
//                          ),
                                ListTile(
                                  onTap: () async {
                                    Result result2 = await CityPickers.showCityPicker(
                                        height: 300,
                                        showType: ShowType.pc,
                                        context: context,
                                        itemExtent: 40,
                                        confirmWidget: Text('确定', style: TextStyle(fontSize: 14, fontFamily: 'songti')),
                                        cancelWidget: Text('取消', style: TextStyle(fontSize: 14, fontFamily: 'songti')),
                                        itemBuilder: (item, list, index) {
                                          return Center(
                                            child: Text(
                                              '${list[index]}',
                                              maxLines: 1,
                                              textScaleFactor: 1.0,
                                              style: TextStyle(fontSize: 14, fontFamily: 'songti'),
                                            ),
                                          );
                                        });
                                    if (result2 != null) {
                                      userBloc.dispatch(UserUpdateEvent(map: <String, dynamic>{'province': result2.provinceName, 'city': result2.cityName}));
                                    }
                                  },
                                  title: Text(
                                    '地区',
                                    textScaleFactor: 1.0,
                                    style: TextStyle(color: appThemeState.themeConfig.mainTextColor, fontSize: GlobalConfigUtil.getTabMainBodyFontSize()),
                                  ),
                                  trailing: Container(
                                    width: ScreenUtil.getInstance().getWidth(500),
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.end,
                                      children: <Widget>[
                                        Text(GlobalConfiguration().getString('province') ?? '' + GlobalConfiguration().getString('city') ?? '',
                                            overflow: TextOverflow.ellipsis,
                                            textScaleFactor: 1.0,
                                            style: TextStyle(color: appThemeState.themeConfig.mainTextColor, fontSize: GlobalConfigUtil.getTabMainBodyFontSize())),
                                        Icon(Icons.keyboard_arrow_right, color: appThemeState.themeConfig.subTitleTextColor)
                                      ],
                                    ),
                                  ),
                                ),
                                ListTile(
                                  onTap: () {
                                    String title = ParameterConvertUtils.convertCN('修学法门');
                                    Routes.router.navigateTo(context, 'profileUpdate?title=${title}&properties=approach', transition: TransitionType.cupertino);
                                  },
                                  title: Text(
                                    '法门',
                                    textScaleFactor: 1.0,
                                    style: TextStyle(color: appThemeState.themeConfig.mainTextColor, fontSize: GlobalConfigUtil.getTabMainBodyFontSize()),
                                  ),
                                  trailing: Container(
                                    width: ScreenUtil.getInstance().getWidth(500),
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.end,
                                      children: <Widget>[
                                        Text(GlobalConfiguration().getString('approach') ?? '',
                                            overflow: TextOverflow.ellipsis,
                                            textScaleFactor: 1.0,
                                            style: TextStyle(color: appThemeState.themeConfig.mainTextColor, fontSize: GlobalConfigUtil.getTabMainBodyFontSize())),
                                        Icon(Icons.keyboard_arrow_right, color: appThemeState.themeConfig.subTitleTextColor)
                                      ],
                                    ),
                                  ),
                                ),
                                ListTile(
                                  onTap: () {
                                    String title = ParameterConvertUtils.convertCN('一句提醒自己的话');
                                    String signature = ParameterConvertUtils.convertCN(GlobalConfiguration().getString('signature') ?? '');
                                    Routes.router.navigateTo(context, 'profileUpdate?title=${title}&properties=signature&value=${signature}');
                                  },
                                  title: Text(
                                    '一句提醒自己的话',
                                    textScaleFactor: 1.0,
                                    style: TextStyle(color: appThemeState.themeConfig.mainTextColor, fontSize: GlobalConfigUtil.getTabMainBodyFontSize()),
                                  ),
                                  trailing: Container(
                                    width: ScreenUtil.getInstance().getWidth(500),
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.end,
                                      children: <Widget>[
                                        Expanded(
                                            child:Container(
                                              alignment: Alignment.centerRight,
                                              child:  Text(GlobalConfiguration().getString('signature') ?? '',
                                                overflow: TextOverflow.ellipsis,
                                                textScaleFactor: 1.0,
                                                style: TextStyle(color: appThemeState.themeConfig.mainTextColor, fontSize: GlobalConfigUtil.getTabMainBodyFontSize())),)),
                                        Icon(Icons.keyboard_arrow_right, color: appThemeState.themeConfig.subTitleTextColor)
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ))
                      ]),
                      Container(
                        height: 20,
                      ),
                      Row(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
                        Container(
                            width: MediaQuery.of(context).size.width - ScreenUtil.getInstance().getWidth(80),
//                      padding: EdgeInsets.symmetric(horizontal: 15),
                            decoration: BoxDecoration(
                                color: appThemeState.themeConfig.sectionBgColor,
                                borderRadius: BorderRadius.circular(8),
                                boxShadow: [BoxShadow(color: appThemeState.themeConfig.shadowColor, blurRadius: 15)]),
                            child: Column(
                              children: <Widget>[
                                ListTile(
                                  title: Text('更改邮箱',
                                      textScaleFactor: 1.0, style: TextStyle(color: appThemeState.themeConfig.mainTextColor, fontSize: GlobalConfigUtil.getTabMainBodyFontSize())),
                                  trailing: Icon(Icons.keyboard_arrow_right, color: appThemeState.themeConfig.subTitleTextColor),
                                  onTap: () {
                                    String title = ParameterConvertUtils.convertCN('修改邮箱');
                                    String value = ParameterConvertUtils.convertCN(GlobalConfiguration().getString('email') ?? '');
                                    Routes.router.navigateTo(context, 'profileUpdate?title=${title}&properties=email&value=${value}');
                                  },
                                ),
//                          Divider(
//                            color: Colors.black12,
//                          ),
                                ListTile(
                                  onTap: () {
                                    String title = ParameterConvertUtils.convertCN('修改密码');
                                    String value = ParameterConvertUtils.convertCN(GlobalConfiguration().getString('password') ?? '');
                                    Routes.router.navigateTo(context, 'profileUpdate?title=${title}&properties=password&value=${value}');
                                  },
                                  title: Text(
                                    '更改密码',
                                    textScaleFactor: 1.0,
                                    style: TextStyle(color: appThemeState.themeConfig.mainTextColor, fontSize: GlobalConfigUtil.getTabMainBodyFontSize()),
                                  ),
                                  trailing: Icon(Icons.keyboard_arrow_right, color: appThemeState.themeConfig.subTitleTextColor),
                                ),
                              ],
                            ))
                      ]),
                      Container(
                        height: 20,
                      ),
                      Row(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
                        Container(
                          width: ScreenUtil.getInstance().screenWidth - ScreenUtil.getInstance().getWidth(80),
                          decoration: BoxDecoration(color: Colors.white, borderRadius: BorderRadius.circular(15), boxShadow: [BoxShadow(color: Colors.black12, blurRadius: 15)]),
                          child: ButtonTheme(
                            height: ScreenUtil.getInstance().getWidth(132),
                            child: FlatButton(
                                color: Color(0xFFFFFFFF),
                                padding: EdgeInsets.symmetric(horizontal: 12),
                                onPressed: () {
                                  SpUtil.getKeys().forEach((item) {
                                    if (item.contains(CommonConfig.BUDDHIST_READER_INDEX)) {
                                      SpUtil.remove(item);
                                    } else if (item.contains(CommonConfig.BUDDHIST_READER_POSITION)) {
                                      SpUtil.remove(item);
                                    } else if (item.contains('readerGlobalConfig')) {
                                      SpUtil.remove(item);
                                    } else if (item.contains('token')) {
                                      SpUtil.remove(item);
                                    } else if (item.contains('user')) {
                                      SpUtil.remove(item);
                                    } else if (item.contains('UNDERLINE')) {
                                      SpUtil.remove(item);
                                    }
                                  });
                                  loginBloc.dispatch(LoginOutEvent());
                                  Routes.router.navigateTo(context, 'login', clearStack: false, replace: false);
                                },
                                shape: RoundedRectangleBorder(side: BorderSide(color: Color(0xFFF2F2F2), width: 0.5), borderRadius: BorderRadius.circular(8)),
                                child: Text(
                                  '退出登录',
                                  style: TextStyle(fontSize: ScreenUtil.getInstance().getSp(42), color: Colors.black54),
                                )),
                          ),
                        )
                      ])
                    ],
                  ),
                ),
              );
            });
      },
    );
  }
}

//class UserProfile extends StatelessWidget {
//  @override
//  Widget build(BuildContext context) {
//    return Container(
//      color: Colors.purple,
//    );
//  }
//}
class GenderSelector extends StatefulWidget {
  @override
  _GenderSelectorState createState() => _GenderSelectorState();
}

class _GenderSelectorState extends State<GenderSelector> {
  UserBloc userBloc = UserBloc();

  @override
  Widget build(BuildContext context) {
    return BlocBuilder(
      bloc: AppThemeBloc(),
      builder: (context, ThemeChangeState appThemeState) {
        return BlocBuilder(
          bloc: userBloc,
          builder: (context, userState) {
            return Container(
              height: ScreenUtil.getInstance().getWidth(360),
              color: appThemeState.themeConfig.sectionBgColor,
              child: Column(
                children: <Widget>[
                  RadioListTile(
                    value: '善男子',
                    groupValue: GlobalConfiguration().getString('gender'),
                    onChanged: (value) {
                      userBloc.dispatch(UserUpdateEvent(map: <String, dynamic>{'gender': '善男子'}));
                    },
                    title: Text('善男子'),
                  ),
                  RadioListTile(
                    value: '善女人',
                    groupValue: GlobalConfiguration().getString('gender'),
                    onChanged: (value) {
//                      setState(() => GlobalConfiguration()
//                          .loadFromMap(<String, dynamic>{'gender': '善女人'}));
                      userBloc.dispatch(UserUpdateEvent(map: <String, dynamic>{'gender': '善女人'}));
                    },
                    title: Text('善女人'),
                  )
                ],
              ),
            );
          },
        );
      },
    );
  }
}
