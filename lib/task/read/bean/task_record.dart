import 'package:json_annotation/json_annotation.dart';

part 'task_record.g.dart';

@JsonSerializable()
class TaskRecord {
  int id;
  int taskId;
  int taskCount;
  int taskType;
  int state;
  int source;
  TaskRecord({this.id, this.taskId, this.taskCount, this.taskType, this.state, this.source});

  factory TaskRecord.fromJson(Map<String, dynamic> json) {
    return _$TaskRecordFromJson(json);
  }
  Map<String, dynamic> toJson() => _$TaskRecordToJson(this);
}
