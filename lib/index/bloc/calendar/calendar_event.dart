import 'dart:async';
import 'package:meta/meta.dart';

import 'calendar_bloc.dart';
import 'calendar_state.dart';

@immutable
abstract class CalendarEvent {
  Future<CalendarState> applyAsync(
      {CalendarState currentState, CalendarBloc bloc});
}

class LoadCalendarEvent extends CalendarEvent {
  @override
  String toString() => 'LoadCalendarEvent';

  @override
  Future<CalendarState> applyAsync(
      {CalendarState currentState, CalendarBloc bloc}) async {
    try {
      return new InCalendarState();
    } catch (_, stackTrace) {
      print('$_ $stackTrace');
      return new ErrorCalendarState(_?.toString());
    }
  }
}

class InitCalendarEvent extends CalendarEvent {
  @override
  String toString() => 'InitCalendarEvent';

  @override
  Future<CalendarState> applyAsync(
      {CalendarState currentState, CalendarBloc bloc}) async {
    try {
      return new UnCalendarState();
    } catch (_, stackTrace) {
      print('$_ $stackTrace');
      return new ErrorCalendarState(_?.toString());
    }
  }
}
