import 'package:bodhiroad/reader/bean/buddhist.dart';
import 'package:json_annotation/json_annotation.dart';

part 'buddhist_catalog.g.dart';

@JsonSerializable()
class BuddhistCatalogBean {
  String id;
  String title;
  String index;
  String juanCount;
  List<Buddhist> list;

  BuddhistCatalogBean({this.id, this.title, this.index, this.juanCount, this.list});

  factory BuddhistCatalogBean.fromJson(Map<String, dynamic> json) {
    return _$BuddhistCatalogBeanFromJson(json);
  }
  Map<String, dynamic> toJson() => _$BuddhistCatalogBeanToJson(this);
}
