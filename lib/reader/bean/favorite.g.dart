// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'favorite.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Favorite _$FavoriteFromJson(Map<String, dynamic> json) {
  return Favorite(
      id: json['id'] as int,
      buddhistId: json['buddhistId'] as String,
      title: json['title'] as String,
      url: json['url'] as String,
      author: json['author'] as String,
      type: json['type'] as int);
}

Map<String, dynamic> _$FavoriteToJson(Favorite instance) => <String, dynamic>{
      'id': instance.id,
      'buddhistId': instance.buddhistId,
      'title': instance.title,
      'url': instance.url,
      'author': instance.author,
      'type': instance.type
    };
