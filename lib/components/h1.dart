import 'package:bodhiroad/utils/GlobalConfigUtil.dart';
import 'package:flustars/flustars.dart';
import 'package:flutter/material.dart';

class TitleText {
  final String text;
  final Color color;
  TitleText(this.text, this.color);
  Widget get h1 {
    return Text(text, textScaleFactor: 1.0, style: TextStyle(color: color, fontSize: GlobalConfigUtil.getSubTitleFontSize(), fontWeight: FontWeight.bold));
  }

  Widget get h2 {
    return Text(text,
        maxLines: 2,
        overflow: TextOverflow.ellipsis,
        textScaleFactor: 1.0,
        style: TextStyle(color: color, fontSize: GlobalConfigUtil.getSubTitleFontSize(), fontWeight: FontWeight.bold));
  }

  Widget get h3 {
    return Text(text, textScaleFactor: 1.0, style: TextStyle(color: color, fontSize: ScreenUtil.getInstance().getSp(36), fontWeight: FontWeight.normal));
  }

  Widget get h4 {
    return Text(text, textScaleFactor: 1.0, style: TextStyle(color: color, fontSize: ScreenUtil.getInstance().getSp(28), fontWeight: FontWeight.bold));
  }
}
