import 'package:json_annotation/json_annotation.dart';

part 'merits_statistic_count_unit.g.dart';

@JsonSerializable()
class MeritsStatisticCountUnit {
  int meritsId;
  String title;
  int catalogId;
  String catalogName;
  int count;
  MeritsStatisticCountUnit({
    this.meritsId,
    this.title,
    this.catalogId,
    this.catalogName,
    this.count,
  });
  factory MeritsStatisticCountUnit.fromJson(Map<String, dynamic> json) {
    return _$MeritsStatisticCountUnitFromJson(json);
  }
  Map<String, dynamic> toJson() => _$MeritsStatisticCountUnitToJson(this);
}
