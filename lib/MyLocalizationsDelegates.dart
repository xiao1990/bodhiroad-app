import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class MyLocalizationsDelegates extends LocalizationsDelegate<MaterialLocalizations> {
  @override
  bool isSupported(Locale locale) {
//是否支持该locale，如果不支持会报异常
    if (locale == const Locale('zh', 'cn')) {
      return true;
    }
    return false;
  }

  @override //是否需要重载
  bool shouldReload(LocalizationsDelegate old) => false;

  @override
  Future<MaterialLocalizations> load(Locale locale) {
//加载本地化
    return new SynchronousFuture(new MyLocalizations(locale));
  }
}

//本地化实现，继承DefaultMaterialLocalizations
class MyLocalizations extends DefaultMaterialLocalizations {
  final Locale locale;
  MyLocalizations(
    this.locale,
  );
  @override
  String get okButtonLabel {
    if (locale == const Locale('zh', 'cn')) {
      return '好的';
    } else {
      return super.okButtonLabel;
    }
  }

  @override
  String get selectAllButtonLabel => '全选';

  @override
  String get pasteButtonLabel => '粘贴';

  @override
  String get copyButtonLabel => '复制';

  @override
  String get cutButtonLabel => '剪切';

  @override
  String get backButtonTooltip => '返回';

  @override
  String get closeButtonTooltip => '关闭';
}
