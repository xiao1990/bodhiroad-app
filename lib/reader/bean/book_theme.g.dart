// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'book_theme.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

BookTheme _$BookThemeFromJson(Map<String, dynamic> json) {
  return BookTheme(
      frontColor: json['frontColor'] as int,
      backColor: json['backColor'] as int);
}

Map<String, dynamic> _$BookThemeToJson(BookTheme instance) => <String, dynamic>{
      'frontColor': instance.frontColor,
      'backColor': instance.backColor
    };
