import 'dart:math' as math;

import 'package:bodhiroad/components/extended_text/lib/src/selection/extended_text_selection.dart';
import 'package:bodhiroad/config/ReaderThemeConfig.dart';
import 'package:bodhiroad/reader/bean/bookmark.dart';
import 'package:bodhiroad/reader/bean/reader_theme.dart';
import 'package:bodhiroad/reader/buddhist_reader_bloc_bloc.dart';
import 'package:bodhiroad/reader/toolbar_bloc.dart';
import 'package:bodhiroad/utils/ToastUtils.dart';
import 'package:flustars/flustars.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:global_configuration/global_configuration.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:bodhiroad/components/extended_text_library/lib/extended_text_library.dart';

// Minimal padding from all edges of the selection toolbar to all edges of the
// viewport.

const double _kToolbarScreenPadding = 8.0;
const double _kToolbarHeight = 44.0;
const double _kHandleSize = 22.0;

///
///  create by zmtzawqlp on 2019/8/3
///

class MyExtendedMaterialTextSelectionControls extends MaterialExtendedTextSelectionControls {
  final int fullTextIndex;
  final handleMyCut;
  final handleMyCopy;
  final handleMyPaste;
  final handleMySelectAll;
  final handleLike;
  final handleSearch;
  final handleShare;
  final handleMakeBookMark;
  final handleDrawUnderline;
  final handleRecovery;
  BuddhistReaderBlocBloc bloc;
  ToolbarBloc toolbarBloc;
  ExtendedTextSelectionState delegate0;
  MyExtendedMaterialTextSelectionControls(
      {this.fullTextIndex,
      this.handleMyCopy,
      this.handleMySelectAll,
      this.handleMyCut,
      this.handleMyPaste,
      this.handleLike,
      this.handleSearch,
      this.handleShare,
      this.handleMakeBookMark,
      this.handleDrawUnderline,
      this.handleRecovery,
      this.delegate0,
      this.toolbarBloc,
      this.bloc});

  TextSelectionDelegate get delegate1 => delegate0;
  @override
  Widget buildToolbar(
    BuildContext context,
    Rect globalEditableRegion,
    double textLineHeight,
    Offset position,
    List<TextSelectionPoint> endpoints,
    TextSelectionDelegate delegate,
  ) {
    assert(debugCheckHasMediaQuery(context));
    assert(debugCheckHasMaterialLocalizations(context));
    delegate0 = delegate;
    // The toolbar should appear below the TextField
    // when there is not enough space above the TextField to show it.
    final TextSelectionPoint startTextSelectionPoint = endpoints[0];
    final TextSelectionPoint endTextSelectionPoint = (endpoints.length > 1) ? endpoints[1] : null;
    final double x = (endTextSelectionPoint == null) ? startTextSelectionPoint.point.dx : (startTextSelectionPoint.point.dx + endTextSelectionPoint.point.dx) / 2.0;
    final double availableHeight = globalEditableRegion.top - MediaQuery.of(context).padding.top - _kToolbarScreenPadding;
    double y = (availableHeight < _kToolbarHeight)
        ? startTextSelectionPoint.point.dy + globalEditableRegion.height + _kToolbarHeight + _kToolbarScreenPadding
        : startTextSelectionPoint.point.dy - textLineHeight * 2.0;
//    y =
    y = (globalEditableRegion.top + startTextSelectionPoint.point.dy) > textLineHeight * 2.0 + MediaQuery.of(context).size.width * 0.18
        ? startTextSelectionPoint.point.dy - textLineHeight * 2.0
        : startTextSelectionPoint.point.dy + textLineHeight * 2.0 + MediaQuery.of(context).size.width * 0.18;
    final Offset preciseMidpoint = Offset(x, y);
    bloc ??= BuddhistReaderBlocBloc();
    return ConstrainedBox(
      constraints: BoxConstraints.tight(globalEditableRegion.size),
      child: CustomSingleChildLayout(
        delegate: MaterialExtendedTextSelectionToolbarLayout(
          MediaQuery.of(context).size,
          globalEditableRegion,
          preciseMidpoint,
        ),
        child: BlocBuilder(
            bloc: toolbarBloc,
            condition: (current, prev) {
              if (prev) {
                delegate.hideToolbar();

//                GlobalConfiguration().updateValue('toShow', true);
                toolbarBloc.dispatch(false);
              }
              return false;
            },
            builder: (context, state) {
              if (state) {
                delegate.hideToolbar();
                if (delegate is ExtendedTextSelectionState) {
                  delegate.clearSelection();
                }
                toolbarBloc.dispatch(false);
              }
              return _TextSelectionToolbar(
                  handleCut: canCut(delegate) ? () => handleCut(delegate) : null,
                  handlePaste: canPaste(delegate) ? () => handlePaste(delegate) : null,
                  handleSelectAll: canSelectAll(delegate) ? () => handleSelectAll(delegate) : null,
                  handleMakeBookMark: () {
                    final TextEditingValue value = delegate.textEditingValue;
                    int startIndex = fullTextIndex +
                        value.text.substring(0, value.selection.start).replaceAll('\u200B', '').replaceAll('\n', '').replaceAll('?', '？').replaceAll(' ', '').length;
                    String selectedText = value.selection.textInside(value.text).replaceAll('\u200B', '').replaceAll('\n', '').replaceAll(' ', '');
                    int endIndex = startIndex + selectedText.length;
                    delegate.hideToolbar();
                    handleMakeBookMark(BookMark(
                        actualStartIndex: startIndex,
                        actualEndIndex: endIndex,
                        yOffset: globalEditableRegion.top + startTextSelectionPoint.point.dy,
                        markType: 1,
                        title: selectedText));
                  },
                  handleRecovery: () {
                    final TextEditingValue value = delegate.textEditingValue;
                    delegate.hideToolbar();
                    handleRecovery(value.selection.textInside(value.text).replaceAll('\u200B', '').replaceAll('\n', '').replaceAll(' ', ''));
                  },
                  handleSearch: () {
                    final TextEditingValue value = delegate.textEditingValue;
                    handleSearch(value.selection.textInside(value.text).replaceAll('\u200B', '').replaceAll('\n', '').replaceAll(' ', ''));
                    delegate.hideToolbar();
                  },
                  handleDrawLine: () {
                    final TextEditingValue value = delegate.textEditingValue;
                    String selectedText = value.selection.textInside(value.text).replaceAll('\u200B', '').replaceAll('\n', '').replaceAll('?', '？');
                    int startIndex = fullTextIndex + value.text.substring(0, value.selection.start).replaceAll('\u200B', '').replaceAll('\n', '').replaceAll('?', '？').length;
                    int endIndex = startIndex + selectedText.length;
                    delegate.hideToolbar();
                    handleDrawUnderline(BookMark(
                        id: startIndex,
                        actualStartIndex: startIndex,
                        actualEndIndex: endIndex,
                        yOffset: globalEditableRegion.top + startTextSelectionPoint.point.dy,
                        markType: 2,
                        title: selectedText.substring(0, math.min(50, selectedText.length))));
                  },
                  handleShare: () {
                    final TextEditingValue value = delegate.textEditingValue;
                    String selectedText = value.selection.textInside(value.text).replaceAll('\u200B', '').replaceAll('\n', '').replaceAll(' ', '');
                    handleShare(selectedText);
                    delegate.hideToolbar();
                  },
                  handleLike: () {
                    //mailto:<email address>?subject=<subject>&body=<body>, e.g.
                    launch("mailto:zmtzawqlp@live.com?subject=extended_text_share&body=${delegate.textEditingValue.text}");
                    delegate.hideToolbar();
                    //clear selecction
                    delegate.textEditingValue = delegate.textEditingValue.copyWith(selection: TextSelection.collapsed(offset: delegate.textEditingValue.selection.end));
                  },
                  handleCopy: () {
                    final TextEditingValue value = delegate.textEditingValue;
                    int startIndex = fullTextIndex + value.text.substring(0, value.selection.start).replaceAll('\u200B', '').replaceAll('\n', '').replaceAll('?', '？').length;
                    int endIndex = startIndex + value.selection.textInside(value.text).replaceAll('\u200B', '').replaceAll('\n', '').length;

                    Clipboard.setData(ClipboardData(
                      text: value.selection.textInside(value.text),
                    ));
                    delegate.textEditingValue = TextEditingValue(
                      text: value.text,
                      selection: TextSelection.collapsed(offset: value.selection.end),
                    );
                    delegate.bringIntoView(delegate.textEditingValue.selection.extent);
                    delegate.hideToolbar();
//              handleMyCopy();
                    ToastUtils.success('已复制到剪贴板···');
                  });
            }),
      ),
    );
  }

  @override
  Widget buildHandle(BuildContext context, TextSelectionHandleType type, double textHeight) {
    final Widget handle = SizedBox(
      width: _kHandleSize,
      height: _kHandleSize,
      child: Image.asset("assets/icons/" + type.toString() + ".png"),
    );

    // [handle] is a circle, with a rectangle in the top left quadrant of that
    // circle (an onion pointing to 10:30). We rotate [handle] to point
    // straight up or up-right depending on the handle type.
    switch (type) {
      case TextSelectionHandleType.left:
        return Transform.rotate(
          angle: math.pi / 8,
          child: SizedBox(
            width: _kHandleSize,
            height: _kHandleSize,
            child: Image.asset("assets/icons/left.png"),
          ),
        );
      case TextSelectionHandleType.right: // points up-left
        return Transform.rotate(
          angle: -math.pi / 8,
          child: SizedBox(
            width: _kHandleSize,
            height: _kHandleSize,
            child: Image.asset("assets/icons/right.png"),
          ),
        );
      case TextSelectionHandleType.collapsed: // points up
        return handle;
    }
    assert(type != null);
    return null;
  }
}

/// Manages a copy/paste text selection toolbar.
class _TextSelectionToolbar extends StatelessWidget {
  const _TextSelectionToolbar({
    Key key,
    this.handleCopy,
    this.handleSelectAll,
    this.handleCut,
    this.handlePaste,
    this.handleLike,
    this.handleSearch,
    this.handleDrawLine,
    this.handleShare,
    this.handleMakeBookMark,
    this.handleRecovery,
  }) : super(key: key);

  final VoidCallback handleCut;
  final VoidCallback handleCopy;
  final VoidCallback handlePaste;
  final VoidCallback handleSelectAll;
  final VoidCallback handleLike;
  final VoidCallback handleSearch;
  final VoidCallback handleDrawLine;
  final VoidCallback handleShare;
  final VoidCallback handleMakeBookMark;
  final VoidCallback handleRecovery;

  @override
  Widget build(BuildContext context) {
    final List<Widget> items = <Widget>[];
    final ReaderTheme readerTheme = ReaderThemeConfig.instance.list[GlobalConfiguration().getInt('themeIndex')];
//    if (handleCut != null) items.add(FlatButton.icon(icon: Icon(MdiIcons.contentCopy), label: Text(localizations.cutButtonLabel), onPressed: handleCut));
    if (handleCopy != null) items.add(getButton(MdiIcons.contentCopy, '复制', handleCopy));
    if (handleDrawLine != null) items.add(getButton(MdiIcons.marker, '标记', handleDrawLine));
    if (handleSearch != null) items.add(getButton(MdiIcons.featureSearch, '查询', handleSearch));
    if (handleShare != null) items.add(getButton(MdiIcons.shareVariant, '分享', handleShare));
    if (handleMakeBookMark != null) items.add(getButton(MdiIcons.card, '卡片', handleMakeBookMark));
    if (handleRecovery != null) items.add(getButton(MdiIcons.checkboxMarkedCircle, '纠错', handleRecovery));
    if (handleSelectAll != null) items.add(getButton(MdiIcons.selectAll, '全选', handleSelectAll));
//    if (handlePaste != null)
//      items.add(FlatButton(
//        child: Text(localizations.pasteButtonLabel),
//        onPressed: handlePaste,
//      ));

//    if (handleLike != null) items.add(FlatButton(child: Icon(Icons.favorite), onPressed: handleLike));

    // If there is no option available, build an empty widget.
    if (items.isEmpty) {
      return Container(width: 0.0, height: 0.0);
    }

    return Container(
        width: MediaQuery.of(context).size.width * 0.8,
        height: MediaQuery.of(context).size.width * 0.8 * 0.25,
        decoration: BoxDecoration(color: readerTheme.backgroundColor, borderRadius: BorderRadius.circular(8), boxShadow: [BoxShadow(color: Colors.black54, blurRadius: 15)]),
        child: GridView.count(
          crossAxisCount: 4,
          children: items,
          mainAxisSpacing: 0,
          crossAxisSpacing: 0,
          childAspectRatio: 2.0,
        ));
//    );read
  }

  Widget getButton(IconData icon, String text, Function handler) {
    final ReaderTheme readerTheme = ReaderThemeConfig.instance.list[GlobalConfiguration().getInt('themeIndex')];
    return ButtonTheme(
      height: 40,
      minWidth: 72,
      padding: EdgeInsets.all(0),
      child: FlatButton.icon(
          padding: EdgeInsets.all(0),
          icon: Icon(
            icon,
            size: 16,
            color: readerTheme.fontColor,
          ),
          label: Text(text, style: TextStyle(color: readerTheme.fontColor, fontSize: ScreenUtil.getInstance().getSp(32))),
          onPressed: handler),
    );
  }
}
