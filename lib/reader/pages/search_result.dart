import 'dart:ui';

import 'package:bodhiroad/config/CommonConfig.dart';
import 'package:bodhiroad/usercenter/bloc/download_bloc.dart';
import 'package:bodhiroad/usercenter/bloc/download_event.dart';
import 'package:bodhiroad/usercenter/bloc/download_state.dart';
import 'package:bodhiroad/usercenter/pages/dict_search.dart';
import 'package:bodhiroad/utils/Route.dart';
import 'package:flustars/flustars.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

class SearchResult extends StatelessWidget {
  final DownloadBloc downloadBloc;
  final String title;
  SearchResult({this.downloadBloc, this.title});
  List list = [];
  int catalogIndex = 0;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.transparent,
        body: Center(
          child: Stack(
            children: <Widget>[
              Container(
                  width: ScreenUtil.getInstance().screenWidth * 0.8,
                  height: ScreenUtil.getInstance().screenHeight * 0.75,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(15),
                  ),
                  child: BlocBuilder(
                    bloc: downloadBloc,
                    condition: (prevSate, currentState) {
                      if (currentState is DictSearchState) {
                        list = currentState.resultList;
                        return true;
                      }
                    },
                    builder: (context, state) {
                      return Column(
                        children: <Widget>[
                          Container(
                            padding: EdgeInsets.all(15),
                            width: ScreenUtil.getInstance().screenWidth * 0.8,
                            decoration:
                                BoxDecoration(borderRadius: BorderRadius.vertical(top: Radius.circular(15)), color: Colors.white, boxShadow: [BoxShadow(color: Colors.black12, blurRadius: 15)]),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Container(
                                  height: ScreenUtil.getInstance().getWidth(84),
                                  width: ScreenUtil.getInstance().screenWidth * 0.6,
                                  alignment: Alignment.topLeft,
                                  padding: EdgeInsets.only(left: 15),
                                  child: Text(title, style: TextStyle(fontSize: ScreenUtil.getInstance().getSp(48), fontWeight: FontWeight.bold), overflow: TextOverflow.ellipsis),
                                ),
                                Container(
                                    height: ScreenUtil.getInstance().getWidth(100),
                                    child: ListView(
                                      scrollDirection: Axis.horizontal,
                                      children: [
                                        Container(
                                          padding: EdgeInsets.symmetric(vertical: 6, horizontal: 6),
                                          child: ButtonTheme(
                                              minWidth: ScreenUtil.getInstance().screenWidth * 0.8 * 0.2,
                                              height: ScreenUtil.getInstance().getWidth(48),
                                              child: FlatButton(
                                                  padding: EdgeInsets.all(6),
                                                  onPressed: () {
                                                    catalogIndex = 0;
                                                    downloadBloc.dispatch(DictSearchEvent(keyword: title, catalogIndex: 0));
//                                    search();
                                                  },
                                                  color: catalogIndex == 0 ? Color(0xFFF2F2F2) : Colors.transparent,
                                                  shape: RoundedRectangleBorder(side: BorderSide(color: Colors.black12, width: 0.5), borderRadius: BorderRadius.circular(4)),
                                                  child: Text(
                                                    '全部',
                                                    style: TextStyle(fontSize: ScreenUtil.getInstance().getSp(30)),
                                                  ))),
                                        )
                                      ]..addAll(List.generate(CommonConfig.dictList.length, (index) {
                                          return Container(
                                              padding: EdgeInsets.symmetric(vertical: 6, horizontal: 6),
                                              child: ButtonTheme(
                                                  minWidth: ScreenUtil.getInstance().screenWidth * 0.8 * 0.2,
                                                  height: ScreenUtil.getInstance().getWidth(48),
                                                  child: FlatButton(
                                                      padding: EdgeInsets.all(6),
                                                      color: catalogIndex == index + 1 ? Color(0xFFF2F2F2) : Colors.transparent,
                                                      onPressed: () {
                                                        catalogIndex = index + 1;
                                                        downloadBloc.dispatch(DictSearchEvent(keyword: title, catalogIndex: catalogIndex));
                                                      },
                                                      shape: RoundedRectangleBorder(side: BorderSide(color: Colors.black12, width: 0.5), borderRadius: BorderRadius.circular(4)),
                                                      child: Text(
                                                        CommonConfig.dictList[index]['name'],
                                                        style: TextStyle(fontSize: ScreenUtil.getInstance().getSp(30)),
                                                      ))));
                                        })),
                                    )),
                              ],
                            ),
                          ),
                          Expanded(
                              child: Container(
                            padding: EdgeInsets.only(left: 15, right: 15),
                            child: ListView(physics: BouncingScrollPhysics(), children: <Widget>[
                              Container(
                                child: MyPanel(
                                  title: '佛学大辞典',
                                  showMore: false,
                                  list: list.where((item) => item['source'] == 1).toList(),
                                  seeMore: (String title, String content) {
//                          seeMore(context, title, content);
                                  },
                                ),
                              ),
                              Container(
                                child: MyPanel(
                                  title: '佛光大辞典',
                                  showMore: false,
                                  list: list.where((item) => item['source'] == 2).toList(),
                                  seeMore: (String title, String content) {
//                          seeMore(context, title, content);
                                  },
                                ),
                              ),
                              Container(
//                        decoration: BoxDecoration(color: Colors.white, borderRadius: BorderRadius.circular(12), boxShadow: [BoxShadow(color: Colors.black12, blurRadius: 15)]),
                                child: MyPanel(
                                  title: '佛学常见词汇',
                                  showMore: false,
                                  list: list.where((item) => item['source'] == 3).toList(),
                                  seeMore: (String title, String content) {
//                          seeMore(context, title, content);
                                  },
                                ),
                              ),
                              Container(
                                child: MyPanel(
                                  title: '中国佛教',
                                  showMore: false,
                                  list: list.where((item) => item['source'] == 4).toList(),
                                  seeMore: (String title, String content) {
//                          seeMore(context, title, content);
                                  },
                                ),
                              ),
                            ]),
                          ))
//    )
                        ],
                      );
                    },
                  )),
              Positioned(
                  right: 0,
                  top: 0,
                  child: ButtonTheme(
                    padding: EdgeInsets.all(0),
                    minWidth: ScreenUtil.getInstance().getWidth(84),
                    child: IconButton(
                        padding: EdgeInsets.all(0),
                        icon: Icon(
                          MdiIcons.closeCircle,
                          color: Colors.black54,
                        ),
                        onPressed: () {
                          Routes.router.pop(context);
                        }),
                  ))
            ],
          ),
        ));
  }
}
