import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class CalendarState extends Equatable {
  CalendarState([Iterable props]) : super(props);

  /// Copy object for use in action
  CalendarState getStateCopy();
}

/// UnInitialized
class UnCalendarState extends CalendarState {
  @override
  String toString() => 'UnCalendarState';

  @override
  CalendarState getStateCopy() {
    return UnCalendarState();
  }
}

/// Initialized
class InCalendarState extends CalendarState {
  @override
  String toString() => 'InCalendarState';

  @override
  CalendarState getStateCopy() {
    return InCalendarState();
  }
}

class ErrorCalendarState extends CalendarState {
  final String errorMessage;

  ErrorCalendarState(this.errorMessage);
  
  @override
  String toString() => 'ErrorCalendarState';

  @override
  CalendarState getStateCopy() {
    return ErrorCalendarState(this.errorMessage);
  }
}
