import 'dart:async';
import 'package:bloc/bloc.dart';

class SelectDateBloc extends Bloc<Map, Map> {
  static final SelectDateBloc _selectDateBlocSingleton =
      new SelectDateBloc._internal();
  factory SelectDateBloc() {
    return _selectDateBlocSingleton;
  }
  SelectDateBloc._internal();

  Map get initialState {
    Map map = Map();
    map['weekIndex'] = 0;
    map['dayIndex'] = 0;
    return map;
  }

  @override
  Stream<Map> mapEventToState(
    Map event,
  ) async* {
    try {
      yield event;
    } catch (_, stackTrace) {
      print('$_ $stackTrace');
      yield currentState;
    }
  }
}
