import 'dart:io';
import 'dart:math' as math;
import 'dart:ui';

import 'package:bodhiroad/api/api.dart';
import 'package:bodhiroad/common/FontFamilyName.dart';
import 'package:bodhiroad/components/bodhiroad_dialog_confirm.dart';
import 'package:bodhiroad/components/dialog/MyDialog.dart';
import 'package:bodhiroad/config/CommonConfig.dart';
import 'package:bodhiroad/config/GlobalColorCOnfig.dart';
import 'package:bodhiroad/index/pages/brwh_catalog_list.dart';
import 'package:bodhiroad/theme/bloc.dart';
import 'package:bodhiroad/usercenter/bloc/login/login_bloc.dart';
import 'package:bodhiroad/utils/GlobalConfigUtil.dart';
import 'package:bodhiroad/utils/Route.dart';
import 'package:bodhiroad/utils/ToastUtils.dart';
import 'package:bodhiroad/utils/parameter_convert_cn.dart';
import 'package:fluro/fluro.dart';
import 'package:flustars/flustars.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_downloader/flutter_downloader.dart';
import 'package:global_configuration/global_configuration.dart';
import 'package:package_info/package_info.dart';
import 'package:path_provider/path_provider.dart';

class UserCenter extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
//          Container(
//            height: ScreenUtil.getInstance().getWidth(window.padding.top),
//            color: GlobalColorConfig.mainBackgroundColor,
//          ),
          Expanded(
            child: UserCenterBody(),
          ),
        ],
      ),
//      decoration: BoxDecoration(
//        image: DecorationImage(fit: BoxFit.cover, image: AssetImage('assets/body-back.jpg')),
//      ),
    );
  }
}

class UserCenterBody extends StatefulWidget {
  @override
  _UserCenterBodyState createState() => _UserCenterBodyState();
}

class _UserCenterBodyState extends State<UserCenterBody> {
  final LoginBloc loginBloc = LoginBloc();

  showUpdateDialog(BuildContext context, String description, String title, bool justClose) {
    showDialog(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context) {
          return StatefulBuilder(
            builder: (BuildContext context, setState) {
              return BodhiRoadConfirmDialog(
                title: title,
                confirmText: '立即更新',
                cancelText: justClose ? '关 闭' : '稍后更新',
                justClose: justClose,
                description: Text(description.replaceAll('-', '\n'), textScaleFactor: 1.0, style: TextStyle(fontSize: 14, color: GlobalColorConfig.mainTextColor)),
                onConfirm: () {
                  _requestDownload();
                  Navigator.pop(context);
                },
              );
            },
          );
        });
  }

  clearCache(BuildContext context, String description, String title, bool justClose) {
    showDialog(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context) {
          return StatefulBuilder(
            builder: (BuildContext context, setState) {
              return BodhiRoadConfirmDialog(
                title: title,
                confirmText: '立即清除',
                cancelText: justClose ? '关 闭' : '取消',
                justClose: justClose,
                description: Text('将会清除所有已下载的经文及文章目录信息！', textScaleFactor: 1.0, style: TextStyle(fontSize: 14, color: GlobalColorConfig.mainTextColor)),
                onConfirm: () {
                  Set keys = SpUtil.getKeys();
                  keys.forEach((key) {
                    if (key.toString().contains(CommonConfig.BUDDHIST_CAN_CLEAR)) {
                      SpUtil.remove(key.toString());
                    }
                  });
                  ToastUtils.success('已清除！');
                  Navigator.pop(context);
                },
              );
            },
          );
        });
  }

  var plateform;
  var _localPath;
  var taskId;

  @override
  void initState() {
    prepare();

    // TODO: implement initState

    super.initState();
  }

  prepare() async {
//    await FlutterDownloader.initialize();
    FlutterDownloader.registerCallback(downLoadCallBack);
    Directory dir = await getExternalStorageDirectory();
    print(dir.path);
    _localPath = (dir.path) + '/Download';
    final savedDir = Directory(_localPath);
    bool hasExisted = await savedDir.exists();
    print(hasExisted);
    if (!hasExisted) {
      savedDir.create();
    }
  }

  static void downLoadCallBack(id, status, progress) {
    print('Download task ($id) is in status ($status) and process ($progress)');
    if (progress == 100) {
      FlutterDownloader.open(taskId: id);
    }
  }

  void _requestDownload() async {
    taskId = await FlutterDownloader.enqueue(url: 'http://www.bodhiroad.cn/apks/app-release.apk', savedDir: _localPath, showNotification: true, openFileFromNotification: true);
  }

  downLoadDict(BuildContext context) {
    showDialog(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context) {
          return StatefulBuilder(
            builder: (BuildContext context, setState) {
              return MyDialog(
                title: '提示信息',
                confirmText: '选择下载',
                cancelText: '取消',
                justClose: false,
                height: ScreenUtil.getInstance().getWidth(640),
                content: Text(
                  '使用此功能需要先下载辞典数据，包括\n《佛学大辞典》\n《佛光大辞典》\n《佛学常见词汇》\n《中国佛教》\n四部，是否去下载？',
                  textAlign: TextAlign.center,
                  style: TextStyle(color: Colors.black87, fontSize: ScreenUtil.getInstance().getSp(36)),
                ),
                onConfirm: () {
                  Navigator.pop(context);
                  Routes.router.navigateTo(context, 'dictDownload', transition: TransitionType.native);
                },
                onCancel: () {
                  Navigator.pop(context);
                },
              );
            },
          );
        });
  }

  opermenu(int index, BuildContext context) async {
    switch (index) {
      case 1:
        ToastUtils.success('正在开发中···');
        break;
      case 2:
        if (SpUtil.getBool('dict_downloaded')) {
          Routes.router.navigateTo(context, 'dictSearch');
        } else {
          downLoadDict(context);
        }
        break;
      case 3:
        Navigator.of(context).push(MaterialPageRoute(builder: (BuildContext context) {
          return BrwhCatalogList(
            id: 'dict',
            from: 'dict',
            source: "5",
            taskType: "0",
            catalogName: ParameterConvertUtils.convertCN('浏览大辞典'),
          );
        }));
        break;
      case 4:
        ToastUtils.success('正在开发中···');
        break;
      case 5:
        clearCache(context, '将会清除所有已下载的经文及文章目录信息！', '清除缓存 ', false);
        break;
      case 6:
        Routes.router.navigateTo(context, 'share');
        break;
      case 7:
        Routes.router.navigateTo(context, 'feedback');
        break;
      case 8:
        ToastUtils.success('正在开发中···');
        break;
      case 9:
        PackageInfo.fromPlatform().then((PackageInfo packageInfo) async {
          String version = packageInfo.version;
          var res = await Api.checkVersion(data: {'version': version});
          if (res['code'] == 200) {
            if (res['data'] != null) {
              showUpdateDialog(context, res['data']['description'], '新版本 ' + res['data']['version'], false);
            } else {
              showUpdateDialog(context, '已经是最新版本', '提示信息', true);
            }
          }
        });
        break;
      case 10:
        ToastUtils.success('正在开发中···');
        break;
      default:
    }
  }

  List<Widget> getVerticalText(String text) {
    text = text.replaceAll("《", '').replaceAll("》", '');
//    text = text.substring(0, math.min(18, text.length));

    int cols = (text.length / 7).ceil();
    List<Widget> list = [];
    for (int i = 0; i < cols; i++) {
      String text2 = text.substring(i * 7, math.min(i * 7 + 7 - 1, text.length - 1) + 1).replaceAll('', '\n').replaceFirst('\n', '');
      text2 = text2.substring(0, text2.length - 1);
      list.add(Container(
        width: ScreenUtil.getInstance().getWidth(56),
        height: 100 + ScreenUtil.getInstance().screenHeight * 0.05,
        alignment: Alignment.center,
        child: Container(
          width: ScreenUtil.getInstance().getWidth(56),
          padding: EdgeInsets.symmetric(vertical: 4),
          decoration: BoxDecoration(
            color: Colors.white.withOpacity(0.3),
            borderRadius: BorderRadius.circular(3),
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                text2,
                style: TextStyle(
                    decoration: TextDecoration.none,
                    color: Colors.black,
                    fontSize: GlobalConfigUtil.getTabMainBodyFontSize(),
                    shadows: [Shadow(color: Colors.black26, blurRadius: 6)],
                    fontWeight: FontWeight.normal,
                    fontFamily: FontFamilyName.CHINESESTYLE),
              )
            ],
          ),
        ),
      ));
      list.add(Container(
        width: 8,
      ));
    }
    return list.reversed.toList();
  }

  @override
  Widget build(BuildContext context) {
    final AppThemeBloc themeBloc = BlocProvider.of<AppThemeBloc>(context);
    plateform = Theme.of(context).platform;
    return BlocBuilder(
      bloc: themeBloc,
      builder: (context, ThemeChangeState themeState) {
        return Scaffold(
            backgroundColor: Colors.transparent,
            extendBody: true,
            body: Container(
                padding: EdgeInsets.zero,
                child: Column(
                  children: <Widget>[
                    GestureDetector(
                        onTap: () {
                          String token = SpUtil.getString('token');
                          if (token == null || token.length == 0) {
                            Navigator.pop(context);
                            Routes.router.navigateTo(context, 'login', transition: TransitionType.cupertino);
                          } else {
                            Routes.router.navigateTo(context, 'userProfile', transition: TransitionType.cupertino);
                          }
                        },
                        child: Container(
                          height: ScreenUtil.getInstance().getWidth(250) + ScreenUtil.getInstance().screenHeight * 0.05,
//                          padding: EdgeInsets.only(top: ScreenUtil.getInstance().screenHeight * 0.05),
                          width: ScreenUtil.getInstance().screenWidth,
                          alignment: Alignment.centerLeft,
                          padding: EdgeInsets.only(right: ScreenUtil.getInstance().getWidth(25)),
                          margin: EdgeInsets.only(bottom: ScreenUtil.getInstance().getWidth(50)),
//                          decoration: BoxDecoration(image: DecorationImage(fit: BoxFit.fitWidth, alignment: Alignment.topCenter, image: AssetImage('assets/texture/banner6.jpg'))),
                          child: Row(
                            children: <Widget>[
                              Expanded(
                                  child: Container(
                                      padding: EdgeInsets.only(left: ScreenUtil.getInstance().getWidth(75)),
                                      alignment: Alignment.centerLeft,
                                      child: Row(mainAxisAlignment: MainAxisAlignment.start, children: getVerticalText(GlobalConfiguration().getString('signature'))))),
                              Container(
                                  width: ScreenUtil.getInstance().getWidth(60),
                                  padding: EdgeInsets.symmetric(vertical: ScreenUtil.getInstance().getWidth(15)),
                                  decoration:
                                      BoxDecoration(color: Color(0xFFA70400), borderRadius: BorderRadius.circular(6), boxShadow: [BoxShadow(color: Colors.black26, blurRadius: 8)]),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                      Text(
                                        GlobalConfiguration()
                                            .getString('name')
                                            .replaceAllMapped('', (match) => match.start == 0 || match.end == GlobalConfiguration().getString('name').length ? '' : '\n'),
                                        style: TextStyle(color: Colors.white, fontSize: GlobalConfigUtil.getTabMainBodyFontSize(), fontFamily: FontFamilyName.CHINESESTYLE),
                                      )
                                    ],
                                  )),
                              Container(
                                width: 64,
                                alignment: Alignment.center,
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                  children: <Widget>[
                                    ClipOval(
                                      child: CircleAvatar(
                                        radius: ScreenUtil.getInstance().getWidth(60),
                                        backgroundColor: Colors.white.withOpacity(0.3),
                                        child: GlobalConfiguration().getString('headPortraitUrl') != null && GlobalConfiguration().getString('headPortraitUrl').length > 0
                                            ? Image.file(File(GlobalConfiguration().getString('headPortraitUrl')))
                                            : Image.asset('assets/default.png'),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
//                              Text(GlobalConfiguration().getString('signature'), textScaleFactor: 1.0, style: TextStyle(color: themeState.themeConfig.mainTextColor)),
                            ],
                          ),
                        )),
                    Expanded(
                        child: ListView(
                            physics: BouncingScrollPhysics(),
                            padding: EdgeInsets.zero,
                            children: <Widget>[
                              Container(
                                height: ScreenUtil.getInstance().getWidth(150),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                                  children: <Widget>[
                                    GestureDetector(
                                      onTap: () {
                                        Routes.router.navigateTo(
                                          context,
                                          'userFavorite',
                                        );
                                      },
                                      child: AspectRatio(
                                        aspectRatio: 1.5,
                                        child: Container(
                                          decoration: BoxDecoration(
                                              color: Colors.white,
                                              image: DecorationImage(image: AssetImage('assets/texture/ss10.png')),
                                              borderRadius: BorderRadius.circular(12),
                                              boxShadow: [BoxShadow(color: Color(0xFFF2F2F2), blurRadius: 8)]),
                                          child: Column(
                                            crossAxisAlignment: CrossAxisAlignment.center,
                                            mainAxisAlignment: MainAxisAlignment.center,
                                            children: <Widget>[
                                              Icon(Icons.star, size: ScreenUtil.getInstance().getWidth(50), color: Colors.black54),
                                              Text('我的收藏', textScaleFactor: 1.0, style: TextStyle(color: Colors.black54, fontSize: ScreenUtil.getInstance().getWidth(28)))
                                            ],
                                          ),
                                        ),
                                      ),
                                    ),
                                    GestureDetector(
                                      onTap: () {},
                                      child: AspectRatio(
                                        aspectRatio: 1.5,
                                        child: GestureDetector(
                                          onTap: () {
                                            Navigator.pop(context);
                                            Routes.router.navigateTo(context, 'taskHistory', clearStack: false);
//                                    Navigator.push(
//                                      context,
//                                      new MaterialPageRoute(
//                                          builder: (context) =>
//                                              new TaskStatisticsPage()),
//                                    );
                                          },
                                          child: Container(
                                            decoration: BoxDecoration(
                                                color: Colors.white,
                                                image: DecorationImage(image: AssetImage('assets/texture/ss10.png')),
                                                borderRadius: BorderRadius.circular(12),
                                                boxShadow: [BoxShadow(color: Color(0xFFF2F2F2), blurRadius: 8)]),
                                            child: Column(
                                              crossAxisAlignment: CrossAxisAlignment.center,
                                              mainAxisAlignment: MainAxisAlignment.center,
                                              children: <Widget>[
                                                Icon(Icons.map, size: ScreenUtil.getInstance().getWidth(50), color: Colors.black54),
                                                Text('修学轨迹', textScaleFactor: 1.0, style: TextStyle(color: Colors.black54, fontSize: ScreenUtil.getInstance().getSp(28)))
                                              ],
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                    GestureDetector(
                                      onTap: () {
                                        Routes.router.navigateTo(
                                          context,
                                          'meritsStatistic',
                                        );
                                      },
                                      child: AspectRatio(
                                        aspectRatio: 1.5,
                                        child: Container(
                                          decoration: BoxDecoration(
                                              color: Colors.white,
                                              image: DecorationImage(image: AssetImage('assets/texture/ss10.png')),
                                              borderRadius: BorderRadius.circular(12),
                                              boxShadow: [BoxShadow(color: Color(0xFFF2F2F2), blurRadius: 8)]),
                                          child: Column(
                                            crossAxisAlignment: CrossAxisAlignment.center,
                                            mainAxisAlignment: MainAxisAlignment.center,
                                            children: <Widget>[
                                              Icon(Icons.bubble_chart, size: ScreenUtil.getInstance().getWidth(60), color: Colors.black54),
                                              Text('功过统计', textScaleFactor: 1.0, style: TextStyle(color: Colors.black54, fontSize: ScreenUtil.getInstance().getSp(28)))
                                            ],
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ]..addAll(
                                CommonConfig.userMenus
                                    .map((item) => Container(
                                          height: ScreenUtil.getInstance().getWidth(150),
                                          padding: EdgeInsets.only(left: ScreenUtil.getInstance().getWidth(40)),
                                          child: Column(
                                            mainAxisAlignment: MainAxisAlignment.center,
                                            children: <Widget>[
                                              Expanded(
                                                  child: Material(
                                                color: Colors.transparent,
                                                child: Ink(
                                                  child: InkWell(
                                                    onTap: () {
                                                      opermenu(int.parse(item['id']), context);
                                                    },
                                                    child: Row(
                                                      crossAxisAlignment: CrossAxisAlignment.center,
//                                                      mainAxisAlignment: MainAxisAlignment.center,
                                                      children: <Widget>[
                                                        Container(
                                                          width: ScreenUtil.getInstance().getWidth(150),
                                                          alignment: Alignment.center,
                                                          child: Container(
                                                            width: ScreenUtil.getInstance().getWidth(60),
                                                            height: ScreenUtil.getInstance().getWidth(60),
                                                            decoration: BoxDecoration(color: Color(0xFFF2F2F2), borderRadius: BorderRadius.circular(15)),
                                                            child: Icon(
                                                              item['icon'],
                                                              size: ScreenUtil.getInstance().getWidth(35),
                                                              color: Color(0xFFA70400),
                                                            ),
                                                          ),
                                                        ),
                                                        Expanded(
                                                            child: Container(
                                                                width: ScreenUtil.getInstance().getWidth(150),
                                                                child: Text(
                                                                  item['key'],
                                                                  textScaleFactor: 1.0,
                                                                  style: TextStyle(color: Colors.black54, fontSize: GlobalConfigUtil.getTabMainBodyFontSize()),
                                                                ))),
                                                        Icon(
                                                          Icons.keyboard_arrow_right,
                                                          color: Colors.black12,
                                                        )
                                                      ],
                                                    ),
                                                  ),
                                                ),
                                              )),
                                              Divider(
                                                height: 0.5,
                                                color: themeState.themeConfig.lightBorderColor,
                                              ),
                                            ],
                                          ),
                                        ))
                                    .toList(),
                              )))
                  ],
                )));
      },
    );
  }
}
