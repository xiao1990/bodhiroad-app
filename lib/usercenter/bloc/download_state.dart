abstract class DownloadState {
  int get status => 0;
  DownloadState() : super();
}

/// 初始化状态
class InitialDownloadState extends DownloadState {
  InitialDownloadState() : super();
  @override
  int get status => 0;
}

/// 初始化状态
class DictDownloadState extends DownloadState {
  DictDownloadState() : super();
  @override
  int get status => 1;
}

/// 初始化状态
class DictDownloadingState extends DownloadState {
  final String taskId;
  DictDownloadingState({this.taskId}) : super();
  @override
  int get status => 2;
}

/// 初始化状态
class DictDownloadCompleteState extends DownloadState {
  final String taskId;
  DictDownloadCompleteState({this.taskId}) : super();
  @override
  int get status => 3;
}

class DictSearchState extends DownloadState {
  final List resultList;
  DictSearchState({this.resultList}) : super();
  @override
  int get status => 0;
}

/// 重载阅读器事件
class SelectCatalogState extends DownloadState {
  final int index;
  SelectCatalogState({this.index});
}

/// 重载阅读器事件
class SimpleSelectCatalogState extends DownloadState {
  final int index;
  SimpleSelectCatalogState({this.index});
}
