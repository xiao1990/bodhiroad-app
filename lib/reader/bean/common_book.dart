import 'package:flutter/material.dart';
import 'package:json_annotation/json_annotation.dart';

import 'buddhist.dart';

part 'common_book.g.dart';

@JsonSerializable()
class CommonBook {
  final String id;
  final String title;
  final List<Buddhist> list;

  const CommonBook({Key key, this.id, this.list = const [], this.title = ''});

  factory CommonBook.fromJson(Map<String, dynamic> json) {
    return _$CommonBookFromJson(json);
  }
  Map<String, dynamic> toJson() => _$CommonBookToJson(this);
}
