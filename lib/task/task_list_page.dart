import 'package:bodhiroad/components/h1.dart';
import 'package:bodhiroad/config/GlobalColorCOnfig.dart';
import 'package:bodhiroad/task/read/bean/task.dart';
import 'package:bodhiroad/theme/bloc.dart';
import 'package:flustars/flustars.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class TaskList extends StatefulWidget {
  const TaskList({Key key, @required this.list, @required this.from, this.appThemeBloc, this.rightAction = false, this.centerClick, this.rightClick}) : super(key: key);

  final List<Task> list;
  final String from;
  final centerClick;
  final rightClick;
  final bool rightAction;
  final AppThemeBloc appThemeBloc;
  @override
  _TaskListState createState() => _TaskListState();
}

class _TaskListState extends State<TaskList> {
  @override
  Widget build(BuildContext context) {
    // final AppThemeBloc appThemeBloc = BlocProvider.of<AppThemeBloc>(context);
    return BlocBuilder(
      bloc: widget.appThemeBloc,
      builder: (context, ThemeChangeState appThemeState) {
        return ListView.builder(
          itemCount: widget.list.length,
          physics: BouncingScrollPhysics(),
          itemBuilder: (BuildContext context, index) {
            return Container(
                height: ScreenUtil.getInstance().getWidth(260),
                width: ScreenUtil.getInstance().screenWidth,
                alignment: Alignment.center,
                child: Material(
                  color: Colors.transparent,
                  child: Ink(
                      width: MediaQuery.of(context).size.width - ScreenUtil.getInstance().getWidth(80),
                      height: ScreenUtil.getInstance().getWidth(200),
                      child: InkWell(
                        splashColor: GlobalColorConfig.mainBorderColor.withOpacity(0.5),
                        highlightColor: Colors.white.withOpacity(0.2),
                        borderRadius: BorderRadius.circular(8),
                        child: Container(
                          width: MediaQuery.of(context).size.width - 30,
                          height: ScreenUtil.getInstance().getWidth(200),
                          padding: EdgeInsets.only(left: ScreenUtil.getInstance().getWidth(40)),
                          decoration: BoxDecoration(
                              color: Colors.white,
                              image: DecorationImage(image: AssetImage('assets/texture/item_bg.jpg')),
                              borderRadius: BorderRadius.circular(12),
                              boxShadow: [BoxShadow(color: Colors.black12, blurRadius: 15)]),
                          child: Row(
                            children: <Widget>[
                              Expanded(
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    TitleText(widget.list[index].taskName, GlobalColorConfig.mainTextColor).h2,
                                  ],
                                ),
                              ),
                              Offstage(
                                offstage: !widget.rightAction,
                                child: InkWell(
                                  splashColor: GlobalColorConfig.mainBorderColor.withOpacity(0.5),
                                  highlightColor: Colors.white.withOpacity(0.2),
                                  borderRadius: BorderRadius.circular(8),
                                  child: Container(
                                    alignment: Alignment.center,
                                    width: ScreenUtil.getInstance().getWidth(180),
                                    height: ScreenUtil.getInstance().getWidth(200),
                                    child: Icon(
                                      widget.list[index].state == 1 ? Icons.done : Icons.add,
                                      color: Color(0xFFA70400),
                                    ),
                                  ),
                                  onTap: () {
                                    widget.rightClick(widget.list[index].taskId, index, widget.list[index].state);
                                  },
                                ),
                              )
                            ],
                          ),
                        ),
                        onTap: () {
                          widget.centerClick(widget.list[index]);
                        },
                      )),
                ));
          },
        );
      },
    );
  }
}
