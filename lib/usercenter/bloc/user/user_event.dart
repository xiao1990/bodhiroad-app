import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class UserEvent extends Equatable {
  UserEvent([List props = const []]) : super(props);
}

class InitialUserEvent extends UserEvent {}

class UserUpdateEvent extends UserEvent {
  final Map map;
  UserUpdateEvent({this.map}) : super([map]);
}
