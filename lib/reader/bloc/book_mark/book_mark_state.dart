import 'package:bodhiroad/reader/bean/book_mark.dart';
import 'package:bodhiroad/reader/bean/book_page.dart';
import 'package:equatable/equatable.dart';

abstract class BookMarkState extends Equatable {
  BookMarkState([Iterable props]) : super(props);

  /// Copy object for use in action
  BookMarkState getStateCopy();
}

/// UnInitialized
class UnBookMarkState extends BookMarkState {
  @override
  String toString() => 'UnBookMarkState';

  @override
  BookMarkState getStateCopy() {
    return UnBookMarkState();
  }
}

/// Initialized
class InBookMarkState extends BookMarkState {
  @override
  String toString() => 'InBookMarkState';
  List<BookmarkBean> bList;
  List<PageBean> pageList;
  InBookMarkState({this.bList, this.pageList});
  @override
  BookMarkState getStateCopy() {
    return InBookMarkState();
  }
}

class ErrorBookMarkState extends BookMarkState {
  final String errorMessage;

  ErrorBookMarkState(this.errorMessage);

  @override
  String toString() => 'ErrorBookMarkState';

  @override
  BookMarkState getStateCopy() {
    return ErrorBookMarkState(this.errorMessage);
  }
}
