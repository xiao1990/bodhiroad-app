// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'text_layout_size.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

TextLayoutSize _$TextLayoutSizeFromJson(Map<String, dynamic> json) {
  return TextLayoutSize(
      width: (json['width'] as num)?.toDouble(),
      heightHorizontal: (json['heightHorizontal'] as num)?.toDouble(),
      heightVertical: (json['heightVertical'] as num)?.toDouble(),
      fontSize: (json['fontSize'] as num)?.toDouble(),
      fontHeight: (json['fontHeight'] as num)?.toDouble());
}

Map<String, dynamic> _$TextLayoutSizeToJson(TextLayoutSize instance) =>
    <String, dynamic>{
      'heightHorizontal': instance.heightHorizontal,
      'heightVertical': instance.heightVertical,
      'width': instance.width,
      'fontSize': instance.fontSize,
      'fontHeight': instance.fontHeight
    };
