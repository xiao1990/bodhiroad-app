library easy_dialog;

import 'package:bodhiroad/components/bodhiroad_dialog.dart';
import 'package:flutter/material.dart';

class EasyDialog {
  final ImageProvider topImage;
  final Text title;
  final Text description;
  final bool closeButton;
  final double height;
  final double width;
  final double fogOpacity;
  final double cornerRadius;
  List<Widget> contentList;
  EdgeInsets contentPadding;
  EdgeInsets titlePadding;
  EdgeInsets descriptionPadding;
  final onConfirm;

  EasyDialog(
      {Key key,
      this.topImage,
      this.title,
      this.description,
      this.closeButton = true,
      this.height = 140,
      this.width = 300,
      this.cornerRadius = 8.0,
      this.fogOpacity = 0.37,
      this.contentList,
      this.contentPadding,
      this.descriptionPadding = const EdgeInsets.all(0.0),
      this.titlePadding = const EdgeInsets.only(bottom: 12.0),
      this.onConfirm})
      : assert(fogOpacity >= 0 && fogOpacity <= 1.0);

  insertByIndex(EdgeInsets padding, Widget child, int index) {
    contentList.insert(
        index,
        Container(
          padding: padding,
          child: child,
          alignment: Alignment.center,
        ));
  }

  show(BuildContext context) {
    ClipRRect image;
    if (topImage != null) {
      image = ClipRRect(
        child: new Image(image: topImage),
        borderRadius: BorderRadius.only(
            topLeft: Radius.circular(cornerRadius),
            topRight: Radius.circular(cornerRadius)),
      );
    }
    if (contentList == null) {
      contentList = new List();
    }
    if (title != null && description != null) {
      insertByIndex(titlePadding, title, 0);
      insertByIndex(descriptionPadding, description, 1);
    }
    if (title != null && description == null) {
      insertByIndex(titlePadding, title, 0);
    }
    if (description != null && title == null) {
      insertByIndex(descriptionPadding, description, 0);
    }
    if (contentPadding == null) {
      contentPadding = EdgeInsets.fromLTRB(17.5, 12.0, 17.5, 13.0);
    }
    return showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return BodhiRoadDialog(
          title: title,
          description: description,
          onConfirm: (text) {
            onConfirm(text);
            // Navigator.pop(context);
          },
        );
      },
    );
  }
}
