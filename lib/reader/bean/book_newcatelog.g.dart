// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'book_newcatelog.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

NewCatalogBean _$NewCatalogBeanFromJson(Map<String, dynamic> json) {
  return NewCatalogBean(
      unit: json['unit'] == null
          ? null
          : BookParagraphUnitBean.fromJson(
              json['unit'] as Map<String, dynamic>),
      indexedPage: json['indexedPage'] == null
          ? null
          : PageBean.fromJson(json['indexedPage'] as Map<String, dynamic>));
}

Map<String, dynamic> _$NewCatalogBeanToJson(NewCatalogBean instance) =>
    <String, dynamic>{
      'unit': instance.unit,
      'indexedPage': instance.indexedPage
    };
