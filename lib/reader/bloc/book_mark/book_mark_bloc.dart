import 'dart:async';
import 'package:bloc/bloc.dart';

import 'book_mark_event.dart';
import 'book_mark_state.dart';

class BookMarkBloc extends Bloc<BookMarkEvent, BookMarkState> {
  static final BookMarkBloc _bookMarkBlocSingleton =
      new BookMarkBloc._internal();
  factory BookMarkBloc() {
    return _bookMarkBlocSingleton;
  }
  BookMarkBloc._internal();

  BookMarkState get initialState => new UnBookMarkState();

  @override
  Stream<BookMarkState> mapEventToState(
    BookMarkEvent event,
  ) async* {
    try {
      if (event is LoadBookMarkEvent) {
        switch (event.action) {
          case 'add':
            event.bList.add(event.bookMark);
            event.pageList[event.currentPageIndex].marked = 1;
            yield InBookMarkState(bList: event.bList, pageList: event.pageList);
            break;
          case 'remove':
            break;
          default:
        }
      }

      yield await event.applyAsync(currentState: currentState, bloc: this);
    } catch (_, stackTrace) {
      print('$_ $stackTrace');
      yield currentState;
    }
  }
}
