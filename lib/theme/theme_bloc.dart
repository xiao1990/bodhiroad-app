import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:bodhiroad/config/ThemeConfig.dart';

import './bloc.dart';

class AppThemeBloc extends Bloc<ThemeEvent, ThemeChangeState> {
  @override
  ThemeChangeState get initialState =>
      ThemeChangeState(themeConfig: ThemeConfig.flt(), pureColor: true);

  @override
  Stream<ThemeChangeState> mapEventToState(
    ThemeEvent event,
  ) async* {
    if (event is ThemeChangeEvent) {
      if (event.themeName == 'default') {
        yield ThemeChangeState(themeConfig: ThemeConfig.flt(), pureColor: true);
      } else if (event.themeName == 'white') {
        yield ThemeChangeState(
            themeConfig: ThemeConfig.white(), pureColor: true);
      }
    }
  }
}
