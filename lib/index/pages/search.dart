import 'package:bodhiroad/reader/bean/brwh_catalog.dart';
import 'package:bodhiroad/reader/bean/buddhist.dart';
import 'package:bodhiroad/reader/pages/book_view_page.dart';
import 'package:bodhiroad/utils/DatabaseUtils.dart';
import 'package:bodhiroad/utils/GlobalConfigUtil.dart';
import 'package:bodhiroad/utils/ToastUtils.dart';
import 'package:bodhiroad/utils/parameter_convert_cn.dart';
import 'package:flustars/flustars.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:sqflite/sqflite.dart';

import 'brwh_cover_index.dart';

class SearchPage extends StatefulWidget {
  @override
  _SearchPageState createState() => _SearchPageState();
}

class _SearchPageState extends State<SearchPage> {
  List list = [];
  int searchType = 0;
  void loadAllBuddhists() async {
    String keyword = _controller.text;
//    if (keyword.trim().length == 0) {
//      ToastUtils.fail('还没输入关键字呢！');
//      return;
//    }
    if (keyword.trim() == '经') {
      ToastUtils.fail('全是经，你要找哪一部？');
      return;
    }
    String sql = "select * from ${searchType == 0 ? 'qldzj' : 'brwh'} where ${searchType == 1 ? ' parentId=0 and ' : ''} title like ?";
    Database db = await DatabaseUtils.getCurrentDatabase();
    List list2 = await db.rawQuery(sql, ['%' + keyword + '%']);
    if (keyword.trim().length > 0) {
      if (searchType == 0) {
        list = list2.map((item) => Buddhist.fromJson(item)).toList();
      } else {
        list = list2.map((item) => BrwhCatalogBean.fromJson(item)).toList();
      }
    }

    setState(() {});
  }

  TextEditingController _controller = TextEditingController();
  WidgetsBinding widgetsBinding;
  @override
  void initState() {
    // TODO: implement initState
    widgetsBinding = WidgetsBinding.instance;
    widgetsBinding.addPostFrameCallback((callback) {
      print("addPostFrameCallback be invoke");
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('搜索'),
        backgroundColor: Colors.white,
        elevation: 0,
        actions: <Widget>[
          ButtonTheme(
              minWidth: ScreenUtil.getInstance().getWidth(240),
              height: ScreenUtil.getInstance().getWidth(90),
              child: FlatButton.icon(
                  icon: Icon(
                    Icons.done,
                    color: searchType == 0 ? Color(0xFFA70400) : Colors.transparent,
                    size: ScreenUtil.getInstance().getSp(40),
                  ),
                  padding: EdgeInsets.all(6),
                  onPressed: () {
                    searchType = 0;
                    loadAllBuddhists();
                  },
//                                color: catalogIndex == 0 ? Color(0xFFF2F2F2) : Colors.transparent,
//                  shape: RoundedRectangleBorder(side: BorderSide(color: Colors.black12, width: 0.5), borderRadius: BorderRadius.circular(4)),
                  label: Text(
                    '乾隆大藏经',
                    style: TextStyle(
                      fontSize: GlobalConfigUtil.getTabMainBodyFontSize(),
                      color: searchType == 0 ? Color(0xFFA70400) : Colors.black87,
                    ),
                  ))),
          ButtonTheme(
              minWidth: ScreenUtil.getInstance().getWidth(240),
              height: ScreenUtil.getInstance().getWidth(90),
              child: FlatButton.icon(
                  icon: Icon(
                    Icons.done,
                    color: searchType == 1 ? Color(0xFFA70400) : Colors.transparent,
                    size: ScreenUtil.getInstance().getSp(40),
                  ),
                  padding: EdgeInsets.all(6),
                  onPressed: () {
                    searchType = 1;
                    loadAllBuddhists();
                  },
//                                color: catalogIndex == 0 ? Color(0xFFF2F2F2) : Colors.transparent,
//                  shape: RoundedRectangleBorder(side: BorderSide(color: Colors.black12, width: 0.5), borderRadius: BorderRadius.circular(4)),
                  label: Text(
                    '般若文海',
                    style: TextStyle(fontSize: GlobalConfigUtil.getTabMainBodyFontSize(), color: searchType == 1 ? Color(0xFFA70400) : Colors.black87),
                  )))
        ],
      ),
      body: Container(
          decoration: BoxDecoration(image: DecorationImage(image: AssetImage('assets/texture/bgt2.png'), fit: BoxFit.cover)),
          child: Column(
            children: <Widget>[
              Container(
                padding: EdgeInsets.only(left: 15, right: 15, bottom: 15),
                height: ScreenUtil.getInstance().getWidth(360),
//                decoration: BoxDecoration(color: Colors.white, border: Border(bottom: BorderSide(color: Colors.black12, width: 0.5))),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    TextField(
                      controller: _controller,
                      maxLines: 1,
                      minLines: 1,
                      cursorColor: Color(0xFFB96652),
                      style: TextStyle(fontSize: GlobalConfigUtil.getSubTitleFontSize(), color: Colors.black38),
                      textAlignVertical: TextAlignVertical.top,
                      onSubmitted: (value) {
                        print(value);
                      },
                      showCursor: true,
                      decoration: new InputDecoration(
                        hintText: '输入关键字',
                        hintStyle: TextStyle(color: Colors.black38),
                        border: InputBorder.none,
                      ),
                    ),
                    Row(mainAxisSize: MainAxisSize.max, mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: [
                      Expanded(
                          child: ButtonTheme(
                        padding: EdgeInsets.all(0),
                        minWidth: ScreenUtil.getInstance().getWidth(30),
                        buttonColor: Color(0xFFB96652),
                        height: ScreenUtil.getInstance().getWidth(118),
                        child: RaisedButton(
                            color: Color(0xFFA70400),
                            elevation: 8,
                            padding: EdgeInsets.symmetric(horizontal: 12),
                            onPressed: () {
                              loadAllBuddhists();
                            },
                            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(6)),
                            child: Text(
                              '搜  索',
                              style: TextStyle(fontSize: GlobalConfigUtil.getSubTitleFontSize(), color: Colors.white),
                            )),
                      ))
                    ]),
                  ],
                ),
              ),

              Expanded(
                  child: Container(
                      child: ListView.builder(
                          itemCount: list.length,
                          physics: BouncingScrollPhysics(),
                          itemExtent: ScreenUtil.getInstance().getWidth(250),
                          itemBuilder: (BuildContext context, index) {
                            return Container(
                              margin: EdgeInsets.symmetric(vertical: 6),
                              alignment: Alignment.center,
                              child: GestureDetector(
                                  onTap: () {
                                    if (searchType == 1) {
                                      Navigator.of(context).push(MaterialPageRoute(builder: (BuildContext context) {
                                        return BrwhCoverIndex(
                                          from: 'brwh',
                                          source: '2',
                                          taskType: '8',
                                          article: BrwhCatalogBean(
                                              id: list[index].id,
                                              title: list[index].title,
                                              author: list[index].author,
                                              url: list[index].url,
                                              catalogId: list[index].catalogId,
                                              desc: list[index].desc,
                                              parentId: list[index].parentId,
                                              subTitle: []),
                                        );
                                      }));
                                    } else {
                                      String bookName = ParameterConvertUtils.convertCN(list[index].title);
                                      Navigator.of(context).push(MaterialPageRoute(builder: (BuildContext context) {
                                        return BookView(
                                          fileUrl: list[index].url,
                                          source: 1,
                                          id: list[index].id,
                                          taskType: 1,
                                          bookName: bookName,
                                          version: 0,
                                        );
                                      }));
                                    }
                                  },
                                  child: Container(
                                      width: ScreenUtil.getInstance().screenWidth - 30,
                                      decoration: BoxDecoration(
                                          image: DecorationImage(image: AssetImage('assets/texture/item_bg.jpg'), fit: BoxFit.cover),
                                          borderRadius: BorderRadius.circular(8),
                                          boxShadow: [BoxShadow(color: Colors.black12, blurRadius: 12)]),
                                      child: Row(
                                        children: <Widget>[
                                          Expanded(
                                              child: Column(
                                            children: <Widget>[
                                              Container(
                                                height: ScreenUtil.getInstance().getWidth(40),
                                                alignment: Alignment.centerLeft,
                                              ),
                                              Container(
                                                height: ScreenUtil.getInstance().getWidth(50),
                                                alignment: Alignment.centerLeft,
                                                padding: EdgeInsets.only(left: 15),
                                                decoration: BoxDecoration(border: Border(left: BorderSide(color: Color(0xFFA70400).withOpacity(0.4), width: 6))),
                                                child: Text(
                                                  list[index].author ?? '失',
                                                  style: TextStyle(fontSize: GlobalConfigUtil.getTabMainBodyFontSize(), color: Colors.black38),
                                                ),
                                              ),
                                              Expanded(
                                                child: Container(
                                                  height: ScreenUtil.getInstance().getWidth(160),
                                                  alignment: Alignment.centerLeft,
                                                  padding: EdgeInsets.only(left: 15),
                                                  child: Text(
                                                    list[index].title,
                                                    style: TextStyle(fontSize: GlobalConfigUtil.getSubTitleFontSize()),
                                                  ),
                                                ),
                                              )
                                            ],
                                          )),
                                          Container(
                                            width: ScreenUtil.getInstance().getWidth(150),
                                            alignment: Alignment.center,
                                            child: Icon(
                                              Icons.keyboard_arrow_right,
                                              color: Colors.black26,
                                            ),
                                          )
                                        ],
                                      ))),
                            );
                          })))
//    )
            ],
          )),
    );
  }
}
