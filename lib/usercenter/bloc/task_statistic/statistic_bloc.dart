import 'dart:async';
import 'dart:convert';

import 'package:bloc/bloc.dart';
import 'package:bodhiroad/api/api.dart';
import 'package:bodhiroad/usercenter/bean/task_statistic.dart';
import 'package:bodhiroad/usercenter/bean/task_statistic_day.dart';
import 'package:bodhiroad/usercenter/bloc/task_statistic/statistic_event.dart';
import 'package:bodhiroad/usercenter/bloc/task_statistic/statistic_state.dart';

class StatisticBloc extends Bloc<StatisticEvent, StatisticState> {
  static final StatisticBloc _statisticBlocSingleton = new StatisticBloc._internal();
  factory StatisticBloc() {
    return _statisticBlocSingleton;
  }
  StatisticBloc._internal();

  StatisticState get initialState => new UnStatisticState([], [], 1);
  List<TaskStatistic> list = [];
  @override
  Stream<StatisticState> mapEventToState(
    StatisticEvent event,
  ) async* {
    try {
      if (event is StatisticLoadDataEvent) {
        var rest = await Api.taskStatistics();
        if (rest['code'] == 200) {
          List list1 = rest['data']['taskStatistics'];
          List list2 = rest['data']['taskStatisticByDays'];
          List<TaskStatistic> lt = list1.map((item) => TaskStatistic.fromJson(item)).toList();
          List<TaskStatisticDays> lt2 = list2.map((item) => TaskStatisticDays.fromJson(item)).toList();
          List<TaskStatisticDays> lt3 = lt2.sublist(0);
          list = lt;
          lt2.sort((a, b) => a.totalDays.compareTo(b.totalDays));
          yield StatisticLoadDataState(lt);
          yield StatisticLoadDataByCataState(lt3, lt2.last.totalDays);
        }
      } else if (event is StatisticChangeCataEvent) {
        yield StatisticChangeCataState(taskType: event.taskType);
        if (event.taskType == -1) {
          yield StatisticLoadDataState(list);
        } else {
          List<TaskStatistic> opList = getList(list);
          opList.retainWhere((item) {
            item.list.retainWhere((value) {
              return value.taskType == event.taskType;
            });
            return item.list.length > 0;
          });
          yield StatisticLoadDataState(opList);
        }
      }
    } catch (_, stackTrace) {
      print('$_ $stackTrace');
      yield currentState;
    }
  }

  List<TaskStatistic> getList(List<TaskStatistic> list) {
    List tlist = jsonDecode(jsonEncode(list));
    return tlist.map((item) => TaskStatistic.fromJson(item)).toList();
  }
}
