import 'dart:math' as math;

import 'package:bodhiroad/common/FontFamilyName.dart';
import 'package:bodhiroad/reader/bean/brwh_catalog.dart';
import 'package:bodhiroad/reader/pages/book_view_page.dart';
import 'package:bodhiroad/utils/DatabaseUtils.dart';
import 'package:bodhiroad/utils/GlobalConfigUtil.dart';
import 'package:bodhiroad/utils/parameter_convert_cn.dart';
import 'package:flustars/flustars.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:global_configuration/global_configuration.dart';

class BrwhCoverIndex extends StatefulWidget {
  final String from;
  BrwhCatalogBean article;
  final String source;
  final String taskType;
  BrwhCoverIndex({this.from, this.article, this.taskType, this.source});
  @override
  _BrwhCoverIndexState createState() => _BrwhCoverIndexState();
}

class _BrwhCoverIndexState extends State<BrwhCoverIndex> {
  int isFavorite = SpUtil.getInt('favorite_' + GlobalConfiguration().getInt('id').toString() + GlobalConfiguration().getInt('source').toString());
  @override
  void initState() {
    if (widget.article.url == null || widget.article.url == '') {
      String sql = "select * from brwh where parentId=?";
      DatabaseUtils.getCurrentDatabase().then((db) {
        db.rawQuery(sql, [widget.article.id]).then((value) {
          widget.article.subTitle = value.map((item) => BrwhCatalogBean.fromJson(item)).toList();
          setState(() {});
        });
      });
    }
    super.initState();
  }

  List<Widget> getVerticalText(String text) {
    text = text.replaceAll("《", '').replaceAll("》", '');
    text = text.substring(0, math.min(14, text.length));

    int cols = (text.length / 7).ceil();
    List<Widget> list = [];
    for (int i = 0; i < cols; i++) {
      String text2 = text.substring(i * 7, math.min(i * 7 + 7 - 1, text.length - 1) + 1).replaceAll('', '\n');
      list.add(Container(
        width: ScreenUtil.getInstance().getWidth(48),
        height: ScreenUtil.getInstance().screenHeight * 0.5,
        alignment: Alignment.topCenter,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Container(
              width: ScreenUtil.getInstance().getWidth(48),
//              decoration: BoxDecoration(border: Border(left: BorderSide(color: Colors.black26, width: 0.5))),
              alignment: i == 0 ? Alignment.topCenter : Alignment.topRight,
              child: Text(
                text2,
                style: TextStyle(
                    decoration: TextDecoration.none,
                    color: Colors.black87,
                    fontSize: ScreenUtil.getInstance().getSp(30),
                    shadows: [Shadow(color: Colors.black26, blurRadius: 6)],
                    fontWeight: FontWeight.normal,
                    fontFamily: FontFamilyName.SONGTI),
              ),
            )
          ],
        ),
      ));
    }
    return list.reversed.toList();
  }

  toRead(int index) {
    String bookName = ParameterConvertUtils.convertCN(widget.article.subTitle.length > 0 ? widget.article.subTitle[index].title : widget.article.title);
    Navigator.of(context).push(MaterialPageRoute(builder: (BuildContext context) {
      return BookView(
        fileUrl: widget.article.subTitle.length > 0 ? widget.article.subTitle[index].url : widget.article.url,
        source: int.parse(widget.source),
        id: widget.article.subTitle.length > 0 ? widget.article.subTitle[index].id : widget.article.id,
        taskType: 0,
        bookName: bookName,
        version: widget.article.subTitle.length > 0 ? widget.article.subTitle[index].version ?? 0 : widget.article.version ?? 0,
      );
    }));
  }

  @override
  Widget build(BuildContext context) {
//    final AppThemeBloc appThemeBloc = BlocProvider.of<AppThemeBloc>(context);
    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          backgroundColor: Colors.white,
          elevation: 0,
          title: Text(
            widget.article.title,
            style: TextStyle(fontSize: GlobalConfigUtil.getTitleFontSize()),
          ),
        ),
        body: Column(
          children: <Widget>[
            Hero(
                tag: widget.article.catalogId + widget.article.id.toString(),
                child: Container(
                    child: Container(
                  height: ScreenUtil.getInstance().getWidth(360),
                  padding: EdgeInsets.symmetric(horizontal: 15),
                  child: Row(
                    children: <Widget>[
                      Container(
                        width: ScreenUtil.getInstance().getWidth(234),
                        height: ScreenUtil.getInstance().getWidth(330),
                        decoration: BoxDecoration(
                            color: Colors.white,
                            border: Border.all(color: Color(0xFFdbd8d9), width: 1),
                            boxShadow: [BoxShadow(color: Colors.black12, blurRadius: 6)],
                            image: DecorationImage(
                                fit: BoxFit.cover,
                                image: AssetImage(
                                  'assets/texture/cover2.jpg',
                                ))),
//                                    color: Colors.deepOrangeAccent,
                        padding: EdgeInsets.only(right: 10),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: getVerticalText(widget.article.title),
                        ),
                      ),
                      Expanded(
                          child: Container(
                        height: ScreenUtil.getInstance().getWidth(330),
                        padding: EdgeInsets.only(left: ScreenUtil.getInstance().getWidth(30)),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
//                            Container(
//                              height: ScreenUtil.getInstance().getWidth(75),
//                              child: Text(
//                                widget.article.title,
//                                style: TextStyle(
//                                    fontSize: GlobalConfigUtil.getSubTitleFontSize(),
//                                    fontWeight: FontWeight.bold,
//                                    decoration: TextDecoration.none,
//                                    color: Colors.black87,
//                                    fontFamily: FontFamilyName.SONGTI),
//                              ),
//                            ),
                            Container(
                              height: ScreenUtil.getInstance().getWidth(50),
                              child: Text(
                                widget.article.author == null || widget.article.author.length == 0 ? '【失】' : '【' + widget.article.author + '】',
                                style: TextStyle(
                                    fontSize: GlobalConfigUtil.getTabMainBodyFontSize(),
                                    decoration: TextDecoration.none,
                                    fontWeight: FontWeight.bold,
                                    color: Colors.black54,
                                    fontFamily: FontFamilyName.SONGTI),
                              ),
                            ),
                            Expanded(
                                child: Container(
                              child: SingleChildScrollView(
                                physics: BouncingScrollPhysics(),
                                child: Text(
                                  widget.article.desc ?? '',
                                  softWrap: true,
//                              overflow: TextOverflow.ellipsis,
                                  style: TextStyle(
                                      fontSize: GlobalConfigUtil.getTabMainBodyFontSize(),
                                      color: Colors.black54,
                                      decoration: TextDecoration.none,
                                      fontWeight: FontWeight.normal,
                                      fontFamily: FontFamilyName.SONGTI),
                                ),
                              ),
                            ))
                          ],
                        ),
                      ))
                    ],
                  ),
                ))),
            Container(
              height: ScreenUtil.getInstance().getWidth(150),
              margin: EdgeInsets.only(top: ScreenUtil.getInstance().getWidth(20)),
              color: Colors.white,
              alignment: Alignment.center,
              child: Stack(
                alignment: Alignment.center,
                children: <Widget>[
                  Image.asset(
                    'assets/texture/bk3.png',
                    color: Color(0xFFc0bbb2),
                    width: ScreenUtil.getInstance().screenWidth - ScreenUtil.getInstance().getWidth(80),
                  ),
                  Text(
                    '目录',
                    style: TextStyle(fontSize: GlobalConfigUtil.getSubTitleFontSize()),
                  )
                ],
              ),
            ),
            Expanded(
                child: ListView.separated(
                    physics: BouncingScrollPhysics(),
                    itemBuilder: (context, index) {
                      return Container(
                          height: ScreenUtil.getInstance().getWidth(120),
                          child: Material(
                            color: Colors.white,
                            child: Ink(
                              child: InkWell(
                                onTap: () {
                                  toRead(index);
                                },
                                child: Container(
                                  height: ScreenUtil.getInstance().getWidth(120),
                                  width: ScreenUtil.getInstance().screenWidth,
                                  alignment: Alignment.centerLeft,
                                  child: Row(
                                    children: <Widget>[
                                      Container(
                                          height: ScreenUtil.getInstance().getWidth(120),
                                          width: ScreenUtil.getInstance().getWidth(120),
                                          child: Row(
                                            mainAxisAlignment: MainAxisAlignment.center,
                                            children: <Widget>[
                                              Container(
                                                height: ScreenUtil.getInstance().getWidth(60),
                                                width: ScreenUtil.getInstance().getWidth(60),
                                                decoration: BoxDecoration(color: Colors.black12, borderRadius: BorderRadius.circular(12)),
                                                alignment: Alignment.center,
                                                child: Text(
                                                  (index + 1).toString(),
                                                  style: TextStyle(fontSize: GlobalConfigUtil.getTabMainBodyFontSize()),
                                                ),
                                              )
                                            ],
                                          )),
                                      Expanded(
                                          child: Text(
                                        widget.article.subTitle.length == 0 ? widget.article.title : widget.article.subTitle[index].title,
                                        style: TextStyle(fontSize: GlobalConfigUtil.getTabMainBodyFontSize()),
                                      )),
                                      Container(
                                        width: ScreenUtil.getInstance().getWidth(120),
                                        child: Icon(
                                          Icons.keyboard_arrow_right,
                                          color: Colors.black26,
                                          size: ScreenUtil.getInstance().getWidth(50),
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ));
                    },
                    separatorBuilder: (context, index) {
                      return Container(
                        height: 0.5,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: List.generate(32, (index) {
                            return Container(
                              width: 4,
                              height: 0.5,
                              color: Colors.black12,
                            );
                          }).toList(),
                        ),
                      );
                    },
                    itemCount: widget.article.subTitle.length == 0 ? 1 : widget.article.subTitle.length)),
            Container(
              height: ScreenUtil.getInstance().getWidth(150),
              decoration: BoxDecoration(color: Colors.white, boxShadow: [BoxShadow(color: Colors.black12, blurRadius: 15)]),
              padding: EdgeInsets.symmetric(horizontal: ScreenUtil.getInstance().getWidth(40)),
              child: Row(
                children: <Widget>[
                  Expanded(
                      flex: 4,
                      child: ButtonTheme(
                        minWidth: ScreenUtil.getInstance().screenWidth / 4,
                        height: 42,
                        child: FlatButton(
                          onPressed: () {
                            toRead(0);
                          },
                          child: Text(
                            '开始阅读',
                            style: TextStyle(color: Colors.white, fontSize: GlobalConfigUtil.getTabMainBodyFontSize()),
                          ),
                          color: Colors.black54,
                          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(24)),
                        ),
                      )),
                ],
              ),
            )
          ],
        ));
//      },
//    );
  }
}
