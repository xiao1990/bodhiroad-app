import 'dart:async';
import 'package:bloc/bloc.dart';

class TaskScrollBloc extends Bloc<double, double> {
  static final TaskScrollBloc _taskScrollBlocSingleton =
      new TaskScrollBloc._internal();
  factory TaskScrollBloc() {
    return _taskScrollBlocSingleton;
  }
  TaskScrollBloc._internal();

  double get initialState => 0.0;

  @override
  Stream<double> mapEventToState(
    double event,
  ) async* {
    try {
      yield event > 100 ? 100 : event;
    } catch (_, stackTrace) {
      print('$_ $stackTrace');
      yield currentState;
    }
  }
}
