import 'package:bodhiroad/components/bodhiroad_dialog.dart';
import 'package:bodhiroad/drawer/calendar.dart';
import 'package:bodhiroad/index/pages/buddhist_depositary.dart';
import 'package:bodhiroad/index/pages/search.dart';
import 'package:bodhiroad/index/pages/task_list.dart';
import 'package:bodhiroad/reader/pages/book_view_page.dart';
import 'package:bodhiroad/reader/pages/buddhist_article_list.dart';
import 'package:bodhiroad/task/add_task_page.dart';
import 'package:bodhiroad/task/sukhavati/pages/sukhavati.dart';
import 'package:bodhiroad/task/zen/pages/zen.dart';
import 'package:bodhiroad/usercenter/pages/dict_download.dart';
import 'package:bodhiroad/usercenter/pages/dict_search.dart';
import 'package:bodhiroad/usercenter/pages/feedback.dart';
import 'package:bodhiroad/usercenter/pages/image_croper_page.dart';
import 'package:bodhiroad/usercenter/pages/merits_statistics_page.dart';
import 'package:bodhiroad/usercenter/pages/profile_update_page.dart';
import 'package:bodhiroad/usercenter/pages/recovery.dart';
import 'package:bodhiroad/usercenter/pages/register_login.dart';
import 'package:bodhiroad/usercenter/pages/share.dart';
import 'package:bodhiroad/usercenter/pages/task_statistics_page.dart';
import 'package:bodhiroad/usercenter/pages/user_favorite.dart';
import 'package:bodhiroad/usercenter/pages/user_profile.dart';
import 'package:bodhiroad/utils/parameter_convert_cn.dart';
import 'package:fluro/fluro.dart';

import '../main.dart';

class Routes {
  static Router router;
  static String home = '/';
  static String bookIndex = '/bookIndex';
  static String bookView = '/bookView';
  static String search = '/search';
  static String dzjCatelog2 = '/dzjCatelog2';
  static String addTask = '/addTask';
  static String sukhavati = '/sukhavati';
  static String zen = '/zen';
  static String login = '/login';
  static String taskView = '/taskView';
  static String taskHistory = '/taskHistory';
  static String inputDialog2 = '/inputDialog2';
  static String meritsStatistic = '/meritsStatistic';
  static String userFavorite = '/userFavorite';
  static String userProfile = '/userProfile';
  static String imageCrop = '/imageCrop';
  static String profileUpdate = '/profileUpdate';
  static String calendarMemorial = '/calendarMemorial';
  static String dictSearch = '/dictSearch';
  static String dictDownload = '/dictDownload';
  static String recovery = '/recovery';
  static String share = '/share';
  static String feedback = '/feedback';

  static void configureRoutes(Router router) {
    router.define(home,
        transitionType: TransitionType.native,
        handler: Handler(handlerFunc: (context, params) => MyApp()));
    router.define(bookIndex,
        transitionType: TransitionType.native,
        handler:
            Handler(handlerFunc: (context, params) => BuddhistDepositary()));
    router.define(bookView, transitionType: TransitionType.native,
        handler: Handler(handlerFunc: (context, params) {
      String fileUrl = params['fileUrl']?.first; //取出传参
      String source = params['source']?.first; //取出传参
      String id = params['id']?.first;
      String version = params['version']?.first;
      String taskType = params['taskType']?.first;
      String bookName = params['bookName']?.first;
      return BookView(
          fileUrl: fileUrl,
          id: id.length > 0 ? int.parse(id) : 0,
          taskType: int.parse(taskType),
          source: int.parse(source),
          bookName: bookName,
          version: int.parse(version));
    }));
    router.define(search, transitionType: TransitionType.native,
        handler: Handler(handlerFunc: (context, params) {
      return SearchPage();
    }));
    router.define(dzjCatelog2, transitionType: TransitionType.native,
        handler: Handler(handlerFunc: (context, params) {
      String catelogId = params['catelogId']?.first; //取出传参
      String from = params['from']?.first; //取出传参
      String source = params['source']?.first; //取出传参
      String taskType = params['taskType']?.first; //取出传参
      String version = params['version']?.first; //取出传参
      return BuddhistArticleList(
          catelogId: catelogId,
          from: from,
          source: source,
          taskType: taskType,
          version: int.parse(version));
    }));
    router.define(addTask, transitionType: TransitionType.nativeModal,
        handler: Handler(handlerFunc: (context, params) {
      return AddTask();
    }));
    router.define(sukhavati, transitionType: TransitionType.native,
        handler: Handler(handlerFunc: (context, params) {
      int count = int.parse(params['count']?.first);
      int taskId = int.parse(params['taskId']?.first);
      String taskName = params['taskName']?.first;
      return Sukhavati(
          count: count,
          taskId: taskId,
          taskName: ParameterConvertUtils.parseParameterCN(taskName));
    }));
    router.define(zen, transitionType: TransitionType.native,
        handler: Handler(handlerFunc: (context, params) {
      int duration = int.parse(params['duration']?.first);
      int taskId = int.parse(params['taskId']?.first);
      return ZenPage(duration: duration, taskId: taskId);
    }));
    router.define(login, transitionType: TransitionType.native,
        handler: Handler(handlerFunc: (context, params) {
      return RegisterPage();
    }));
    router.define(taskHistory, transitionType: TransitionType.native,
        handler: Handler(handlerFunc: (context, params) {
      return TaskStatisticsPage();
    }));
    router.define(taskView, transitionType: TransitionType.fadeIn,
        handler: Handler(handlerFunc: (context, params) {
      int currentIndex = int.parse(params['currentIndex']?.first);
      return TaskSlideView(currentIndex: currentIndex);
    }));
    router.define(inputDialog2, transitionType: TransitionType.native,
        handler: Handler(handlerFunc: (context, params) {
      return BodhiRoadDialog();
    }));
    router.define(meritsStatistic, transitionType: TransitionType.native,
        handler: Handler(handlerFunc: (context, params) {
      return MeritsStatistic();
    }));
    router.define(userFavorite, transitionType: TransitionType.native,
        handler: Handler(handlerFunc: (context, params) {
      return UserFavorite();
    }));
    router.define(userProfile, transitionType: TransitionType.native,
        handler: Handler(handlerFunc: (context, params) {
      return UserProfile();
    }));
    router.define(imageCrop, transitionType: TransitionType.native,
        handler: Handler(handlerFunc: (context, params) {
      return ImageCropPage();
    }));
    router.define(calendarMemorial, transitionType: TransitionType.native,
        handler: Handler(handlerFunc: (context, params) {
      return CalendarMemorial();
    }));
    router.define(dictSearch, transitionType: TransitionType.native,
        handler: Handler(handlerFunc: (context, params) {
      return DictSearch();
    }));
    router.define(dictDownload, transitionType: TransitionType.native,
        handler: Handler(handlerFunc: (context, params) {
      return DictDownload();
    }));
    router.define(share, transitionType: TransitionType.native,
        handler: Handler(handlerFunc: (context, params) {
      return SharePage();
    }));
    router.define(feedback, transitionType: TransitionType.native,
        handler: Handler(handlerFunc: (context, params) {
      return Feedback();
    }));
    router.define(profileUpdate, transitionType: TransitionType.native,
        handler: Handler(handlerFunc: (context, params) {
      String title = params['title']?.first;
      String properties = params['properties']?.first;
      String value = params['value']?.first;
      return ProfileUpdatePage(
        title: title,
        properties: properties,
        value: value,
      );
    }));
    router.define(recovery,
        transitionType: TransitionType.cupertinoFullScreenDialog,
        handler: Handler(handlerFunc: (context, params) {
      String value = params['value']?.first;
      return Recovery(value: value);
    }));
    Routes.router = router;
  }
}
