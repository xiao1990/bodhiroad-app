import 'package:json_annotation/json_annotation.dart';
part 'task_nianfo.g.dart';

@JsonSerializable()
class TaskNianfo {
  int taskId;
  String taskName;
  int count;
  double frequency;
  int followedId;
  bool current;
  TaskNianfo({this.taskId, this.taskName, this.count, this.frequency, this.followedId,this.current=false});
  factory TaskNianfo.fromJson(Map<String, dynamic> json) {
    return _$TaskNianfoFromJson(json);
  }
  Map<String, dynamic> toJson() => _$TaskNianfoToJson(this);
}
