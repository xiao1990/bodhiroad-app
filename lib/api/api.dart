import 'package:bodhiroad/utils/HttpUtils.dart';

/// api请求
class Api {
  /// 登录
  static const String LOGIN = '/login';
  static const String REGISTER = '/register';

  /// 功课
  static const String TASK_ALL = '/task';
  static const String TASK_RECORD_ADD = '/authorize/task/record';
  static const String TASK_RECORD_UPDATE = '/authorize/task/record';
  static const String TASK_REPORT = '/authorize/task/report';
  static const String TASK_STATISTICS = '/authorize/task/statistics';
  static const String TASK_ADD = '/authorize/task/user';
  static const String TASK_DELETE = '/authorize/task/user';
  static const String TASK_VALIDATE = '/authorize/task/validate';
  static const String TASK_USER = '/authorize/task/user';

  /// 日历
  static const String CALENDAR_MEMORIAL = '/calendar/memorialDay';

  /// 反馈
  static const String FEEDBACK = '/feedBack';

  /// 功过格
  static const String MERITS = '/merits';
  static const String MERITS_UPDATE = '/authorize/merits';
  static const String MERITS_STATISTICS = '/authorize/merits/statistics';

  /// 收藏
  static const String FAVORITE_GET = '/authorize/favorite';
  static const String FAVORITE_ADD = '/authorize/favorite';
  static const String FAVORITE_DELETE = '/authorize/favorite';
  static const String FAVORITE_VALIDATE = '/authorize/favorite/validate';

  /// 文件上传
  static const String FILE_UPLOAD_HEADPORTRAIT = '/authorize/upload/headPortrait';

  /// 用户信息
  static const String USER_INFO = '/authorize/user';
  static const String USER_PROFILE_UPDATE = '/authorize/user';

  /// 经文
  static const String BUDDHIST = '/buddhist';
  static const String BUDDHIST_CHECKUPDATE = '/buddhist/checkUpdate';
  static const String BUDDHIST_MARK = '/authorize/buddhist/mark';

  /// 资源
  static const String RESOURCES_ZEN_MUSIC = '/authorize/resource/zenMusicList';

  /// 版本检查
  static const String VERSION_CHECK = '/version';

  /// 注册
  static Future<Map> register({data}) async {
    return HttpUtils.post(REGISTER, data: data);
  }

  /// 登录
  static Future<Map> login({data}) async {
    return HttpUtils.post(LOGIN, data: data);
  }

  /// 查询用户信息
  static Future<Map> getUserInfo({data}) async {
    return HttpUtils.get(USER_INFO, data: data);
  }

  /// 添加功课记录
  static Future<Map> taskRecordAdd({data}) async {
    return HttpUtils.post(TASK_RECORD_ADD, data: data);
  }

  /// 修改功课记录
  static Future<Map> taskRecordUpdate({data}) async {
    return HttpUtils.put(TASK_RECORD_UPDATE, data: data);
  }

  /// 报数
  static Future<Map> taskReport({data}) async {
    return HttpUtils.post(TASK_REPORT, data: data);
  }

  /// 功课统计
  static Future<Map> taskStatistics({data}) async {
    return HttpUtils.get(TASK_STATISTICS, data: data);
  }

  /// 查询功课列表
  static Future<Map> getAllTask({data}) async {
    return HttpUtils.get(TASK_ALL, data: data);
  }

  /// 查询功课列表
  static Future<Map> getUserTask({data}) async {
    return HttpUtils.get(TASK_USER, data: data);
  }

  /// 添加功课
  static Future<Map> addTask({data}) async {
    return HttpUtils.post(TASK_ADD, data: data);
  }

  /// 删除功课
  static Future<Map> deleteTask({data}) async {
    return HttpUtils.delete(TASK_DELETE, data: data);
  }

  /// 验证是否添加过该功课
  static Future<Map> validateTask({data}) async {
    return HttpUtils.get(TASK_VALIDATE, data: data);
  }

  /// 查询收藏
  static Future<Map> getFavorite({data}) async {
    return HttpUtils.get(FAVORITE_GET, data: data);
  }

  /// 添加收藏
  static Future<Map> addFavorite({data}) async {
    return HttpUtils.post(FAVORITE_ADD, data: data);
  }

  /// 删除收藏
  static Future<Map> deleteFavorite({data}) async {
    return HttpUtils.delete(FAVORITE_DELETE, data: data);
  }

  /// 验证是否收藏过
  static Future<Map> validateFavorite({data}) async {
    return HttpUtils.get(FAVORITE_VALIDATE, data: data);
  }

  /// 上传头像
  static Future<Map> uploadHeadPortrait({data}) async {
    return HttpUtils.post(FILE_UPLOAD_HEADPORTRAIT, data: data);
  }

  /// 修改个人信息
  static Future<Map> updateUserProfile({data}) async {
    return HttpUtils.put(USER_PROFILE_UPDATE, data: data);
  }

  /// 查询常用佛经
  static Future<Map> getBuddhist({data}) async {
    return HttpUtils.get(BUDDHIST, data: data);
  }

  /// 查询常用佛经
  static Future<Map> getBuddhistByCatalogId({data}) async {
    String url = BUDDHIST + '/' + data['catalogId'] + '/' + data['classifyId'];
    return HttpUtils.get(url, data: null);
  }

  /// 查询禅修音乐列表
  static Future<Map> getZenMusicList({data}) async {
    return HttpUtils.get(RESOURCES_ZEN_MUSIC, data: data);
  }

  /// 查询功过格
  static Future<Map> getMerits({data}) async {
    return HttpUtils.get(MERITS, data: data);
  }

  /// 新增功过记录
  static Future<Map> addUserMerits({data}) async {
    return HttpUtils.post(MERITS_UPDATE, data: data);
  }

  /// 修改功过记录
  static Future<Map> updateUserMerits({data}) async {
    return HttpUtils.put(MERITS_UPDATE, data: data);
  }

  /// 功过统计
  static Future<Map> userMeritsStatistics({data}) async {
    return HttpUtils.get(MERITS_STATISTICS, data: data);
  }

  /// 版本检查
  static Future<Map> checkVersion({data}) async {
    return HttpUtils.get(VERSION_CHECK, data: data);
  }

  /// 纠错反馈
  static Future<Map> feedback({data}) async {
    return HttpUtils.post(FEEDBACK, data: data);
  }

  /// 查询日历
  static Future<Map> calendarMemorial() async {
    return HttpUtils.get(CALENDAR_MEMORIAL);
  }

  /// 检查经文更新
  static Future<Map> buddhistCheckUpdate({data}) async {
    return HttpUtils.get(BUDDHIST_CHECKUPDATE, data: data);
  }

  /// 新增标记
  static Future<Map> addBuddhistMark({data}) async {
    return HttpUtils.post(BUDDHIST_MARK, data: data);
  }

  /// 删除标记
  static Future<Map> deleteBuddhistMark({data}) async {
    return HttpUtils.delete(BUDDHIST_MARK, data: data);
  }

  /// 查询标记
  static Future<Map> getBuddhistMark({data}) async {
    return HttpUtils.get(BUDDHIST_MARK, data: data);
  }
}
