import 'package:json_annotation/json_annotation.dart';
part 'taskStatisticSimple.g.dart';

@JsonSerializable()
class TaskStatisticSimpleBean {
  int taskType;
  int total;

  TaskStatisticSimpleBean({this.taskType, this.total});

  factory TaskStatisticSimpleBean.fromJson(Map<String, dynamic> json) {
    return _$TaskStatisticSimpleBeanFromJson(json);
  }
  Map<String, dynamic> toJson() => _$TaskStatisticSimpleBeanToJson(this);
}
