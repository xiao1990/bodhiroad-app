import 'package:bodhiroad/usercenter/bean/user.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class LoginEvent extends Equatable {
  LoginEvent([List props = const []]) : super(props);
}

/// 注册/登录中事件
class RegisteringEvent extends LoginEvent {
  @override
  String toString() => 'LoginClickEvent';
}

class RegisterSuccessEvent extends LoginEvent {
  final User user;
  RegisterSuccessEvent({this.user});
  @override
  String toString() => 'LoginClickEvent';
}

class RegisterErrorEvent extends LoginEvent {
  @override
  String toString() => 'LoginClickEvent';
}

/// 未登录事件
class LoggingInEvent extends LoginEvent {
  @override
  String toString() => 'UnLoginEvent';
}

/// 已登录事件
class LoggedOnEvent extends LoginEvent {
  @override
  String toString() => 'LoggedOnEvent';
  final User user;
  LoggedOnEvent({this.user}) : super([user]);
}

/// 已登录事件
class LoginErrorEvent extends LoginEvent {
  @override
  String toString() => 'LoginErrorEvent';
}

class LoginOutEvent extends LoginEvent {
  @override
  String toString() => 'LoginErrorEvent';
}

class ExitEvent extends LoginEvent {
  @override
  String toString() => 'LoginErrorEvent';
}
