import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'dart:isolate';
import 'dart:ui';

import 'package:bloc/bloc.dart';
import 'package:bodhiroad/config/CommonConfig.dart';
import 'package:bodhiroad/usercenter/bloc/download_event.dart';
import 'package:bodhiroad/utils/DatabaseUtils.dart';
import 'package:bodhiroad/utils/ToastUtils.dart';
import 'package:flustars/flustars.dart';
import 'package:flutter_downloader/flutter_downloader.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';

import 'download_state.dart';

class DownloadBloc extends Bloc<DownloadEvent, DownloadState> {
  static final DownloadBloc _downloadBloc = new DownloadBloc._internal();

  factory DownloadBloc() {
    return _downloadBloc;
  }

  DownloadBloc._internal();

  var _localPath;
  var taskId;

  DownloadState get initialState => InitialDownloadState();
  ReceivePort _port = ReceivePort();
  prepare() async {
    print('准备下载配置');
    Directory dir = await getExternalStorageDirectory();
    print(dir.path);
    _localPath = (dir.path) + '/buddhistdict';
    final savedDir = Directory(_localPath);
    bool hasExisted = await savedDir.exists();
    print(hasExisted);
    if (!hasExisted) {
      savedDir.create(recursive: true);
    }

    bool listened = IsolateNameServer.registerPortWithName(_port.sendPort, 'downloader_send_port');
    print(listened);
//    if (!listened) {
    _port.listen((dynamic data) {
      String id = data[0];
      DownloadTaskStatus status = data[1];
      int progress = data[2];
      if (status == DownloadTaskStatus.complete) {
        _downloadBloc.dispatch(DictDownloadCompleteEvent(taskId: SpUtil.getString(id)));
        SpUtil.putInt(SpUtil.getString(id) + '_status', 3);
        insert(SpUtil.getString(id), SpUtil.getInt(id + '_source'));
      } else {
        if (progress == -1) {
          // 下载失败
          SpUtil.putInt(SpUtil.getString(id) + '_status', 2);
        } else {
          SpUtil.putInt(SpUtil.getString(id) + '_status', 1);
          _downloadBloc.dispatch(DictDownloadingEvent(taskId: SpUtil.getString(id), progress: progress.toDouble()));
        }
      }
      print('Download task ($id) is in status ($status) and process ($progress)');
      SpUtil.putInt(SpUtil.getString(id) + '_progress', progress);
    }, cancelOnError: true, onDone: () {});
    FlutterDownloader.registerCallback(downLoadCallBack);
//    }
  }

  static void downLoadCallBack(id, status, progress) {
    final SendPort send = IsolateNameServer.lookupPortByName('downloader_send_port');
    send.send([id, status, progress]);
//    print('Download task ($id) is in status ($status) and process ($progress)');
  }

  void _requestDownload(String path, int source) async {
    taskId = await FlutterDownloader.enqueue(url: '${CommonConfig.DOMAIN_PROD}/dict/${path}', savedDir: _localPath, showNotification: false, openFileFromNotification: false);
    SpUtil.putString(taskId, path);
    SpUtil.putInt(taskId + '_source', source);
  }

  void insert(String filePath, int source) async {
    Database database = await DatabaseUtils.getCurrentDatabase();
    if (!await DatabaseUtils.isTableExits('dict')) {
      await database.execute('CREATE TABLE dict (id INTEGER , entry TEXT, paraphrase TEXT, source INTEGER)');
    }
    String sql = "select count(*) from dict where source = ?";
    List list = await database.rawQuery(sql, [source]);
    if (list.length > 0) {
      await database.delete('dict', where: 'source=?', whereArgs: [source]);
    }

    //AUTOINCREMENT
    Batch batch = database.batch();
    File file = File(_localPath + '/' + filePath);
    if (!file.parent.existsSync()) {
      file.parent.createSync();
    }
    file.readAsStringSync();
    List map = json.decode(file.readAsStringSync());
    Map newMap;
    for (int j = 0; j < map.length; j++) {
      batch.insert('dict', map[j]);
    }
    await batch.commit(noResult: true);
    print('插入: ' + map.length.toString());
    SpUtil.putBool('dict_downloaded', true);
    SpUtil.putBool('dict_downloaded_' + filePath, true);
  }

  @override
  Stream<DownloadState> mapEventToState(
    DownloadEvent event,
  ) async* {
    try {
      if (event is DictDownloadInitEvent) {
        IsolateNameServer.removePortNameMapping('downloader_send_port');
        prepare();
        yield InitialDownloadState();
      } else if (event is DictDownloadEvent) {
        _requestDownload(event.path, event.source);
      } else if (event is DictDownloadCompleteEvent) {
        yield DictDownloadCompleteState(taskId: event.taskId);
      } else if (event is DictDownloadingEvent) {
        yield DictDownloadingState(taskId: event.taskId);
      } else if (event is DisposeEvent) {
        IsolateNameServer.removePortNameMapping('downloader_send_port');
      } else if (event is DictSearchEvent) {
        Database db = await DatabaseUtils.getCurrentDatabase();
        String sql = 'select * from dict where entry like ? order by id,source';
        List params = ['%' + event.keyword + '%'];
        if (event.catalogIndex != 0) {
          sql = 'select * from dict where source = ? and entry like ? order by id,source';
          params.insert(0, event.catalogIndex);
        }
        List result = await db.rawQuery(sql, params);
        if (result.length == 0) {
          ToastUtils.fail('未查询到结果');
          return;
        }
        yield DictSearchState(resultList: result);
      } else if (event is SimpleSelectCatalogEvent) {
        yield SimpleSelectCatalogState(index: event.index);
      } else if (event is SelectCatalogEvent) {
        yield SelectCatalogState(index: event.index);
      } else if (event is SelectCatalogEvent) {
        yield SelectCatalogState(index: event.index);
      }
    } catch (_, stackTrace) {
      print('$_ $stackTrace');
      yield currentState;
    }
  }
}
