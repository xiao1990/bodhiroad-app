// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'taskStatisticSimple.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

TaskStatisticSimpleBean _$TaskStatisticSimpleBeanFromJson(
    Map<String, dynamic> json) {
  return TaskStatisticSimpleBean(
      taskType: json['taskType'] as int, total: json['total'] as int);
}

Map<String, dynamic> _$TaskStatisticSimpleBeanToJson(
        TaskStatisticSimpleBean instance) =>
    <String, dynamic>{'taskType': instance.taskType, 'total': instance.total};
