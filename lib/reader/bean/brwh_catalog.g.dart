// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'brwh_catalog.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

BrwhCatalogBean _$BrwhCatalogBeanFromJson(Map<String, dynamic> json) {
  return BrwhCatalogBean(
      id: json['id'] as int,
      author: json['author'] as String,
      title: json['title'] as String,
      url: json['url'] as String,
      catalogId: json['catalogId'] as String,
      desc: json['desc'] as String,
      parentId: json['parentId'] as int,
      subTitle: (json['subTitle'] as List)
          ?.map((e) => e == null
              ? null
              : BrwhCatalogBean.fromJson(e as Map<String, dynamic>))
          ?.toList(),
      version: json['version'] as int);
}

Map<String, dynamic> _$BrwhCatalogBeanToJson(BrwhCatalogBean instance) =>
    <String, dynamic>{
      'id': instance.id,
      'parentId': instance.parentId,
      'title': instance.title,
      'author': instance.author,
      'url': instance.url,
      'desc': instance.desc,
      'catalogId': instance.catalogId,
      'version': instance.version,
      'subTitle': instance.subTitle
    };
