import 'dart:io';

import 'package:bodhiroad/api/api.dart';
import 'package:bodhiroad/usercenter/bean/user.dart';
import 'package:bodhiroad/usercenter/bloc/user/bloc.dart';
import 'package:dio/dio.dart';
import 'package:flustars/flustars.dart';
import 'package:flutter/material.dart';
import 'package:global_configuration/global_configuration.dart';
import 'package:image_crop/image_crop.dart';

class ImageCropPage extends StatefulWidget {
  // final String filePath;
  // ImageCropPage(this.filePath);
  @override
  _ImageCropPageState createState() => _ImageCropPageState();
}

class _ImageCropPageState extends State<ImageCropPage> {
  final cropKey = GlobalKey<CropState>();
  File _image = File(GlobalConfiguration().getString('filePath'));
  UserBloc userBloc = UserBloc();
  File _file;
  File _sample;
  File _lastCropped;
  @override
  void initState() {
    // initCrop();
    super.initState();
    WidgetsBinding widgetsBinding = WidgetsBinding.instance;
    widgetsBinding.addPostFrameCallback((callback) {
      // _cropImage(_image);
    });
  }

  Widget _buildCropImage() {
    return Container(
      color: Colors.black,
      padding: const EdgeInsets.all(20.0),
      child: Crop(
        key: cropKey,
        image: FileImage(_image),
        aspectRatio: 1.0,
        maximumScale: 1.0,
      ),
    );
  }

  Future<void> _cropImage() async {
    final scale = cropKey.currentState.scale;
    final area = cropKey.currentState.area;
    if (area == null) {
      // cannot crop, widget is not setup
      return;
    }

    // scale up to use maximum possible number of pixels
    // this will sample image in higher resolution to make cropped image larger
    final sample = await ImageCrop.sampleImage(
      file: _image,
      preferredSize: (2000 / scale).round(),
    );

    final croppedFile = await ImageCrop.cropImage(
      file: sample,
      area: area,
    );

    sample.delete();

    _lastCropped?.delete();
    _lastCropped = croppedFile;

    debugPrint('$croppedFile');
    User user = SpUtil.getObj('user', (v) => User.fromJson(v));
    user.headPortraitUrl = croppedFile.path;
    SpUtil.putString('headPortraitUrl', croppedFile.path);
    GlobalConfiguration().loadFromMap({'headPortraitUrl': croppedFile.path});
    userBloc.dispatch(UserUpdateEvent());
    // setState(() {});
//    MultipartFile.fromFile(croppedFile.path);
    Navigator.pop(context);
    var res = await Api.uploadHeadPortrait(data: {'file':await MultipartFile.fromFile(croppedFile.path)});
    if (res['code'] == 200) {
      user.headPortrait = croppedFile.path;
    }
    SpUtil.putObject('user', user);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        title: Text('裁切', style: TextStyle(color: Colors.black87)),
        actions: <Widget>[
          Container(
            width: 56,
            height: 56,
            child: IconButton(
              icon: Icon(Icons.done, color: Colors.black87),
              onPressed: () {
                _cropImage();
              },
            ),
          )
        ],
      ),
      body: Container(
        child: _buildCropImage(),
      ),
    );
  }
}
