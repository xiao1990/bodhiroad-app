// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user_merits.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UserMerits _$UserMeritsFromJson(Map<String, dynamic> json) {
  return UserMerits(
      id: json['id'] as int,
      userId: json['userId'] as String,
      meritsId: json['meritsId'] as int,
      lastUpdate: json['lastUpdate'] as String,
      isGood: json['isGood'] as int);
}

Map<String, dynamic> _$UserMeritsToJson(UserMerits instance) =>
    <String, dynamic>{
      'id': instance.id,
      'userId': instance.userId,
      'meritsId': instance.meritsId,
      'lastUpdate': instance.lastUpdate,
      'isGood': instance.isGood
    };
