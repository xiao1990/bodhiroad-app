abstract class SukhavatiSetState {
  SukhavatiSetState([props = const []]) : super();
}

class SukhavatiInitState extends SukhavatiSetState {
  @override
  String toString() => 'SukhavatiInitState';
}

class SukhavatiCountState extends SukhavatiSetState {
  int count;
  List<int> list;
  SukhavatiCountState({this.count, this.list}) : super([count, list]);
  @override
  String toString() => 'SukhavatiCountState';
}

class SukhavatiFrequencyState extends SukhavatiSetState {
  final double frequency;
  SukhavatiFrequencyState({this.frequency = 3}) : super([frequency]);
  @override
  String toString() => 'SukhavatiFrequencyState';
}

class SukhavatiVolumnState extends SukhavatiSetState {
  final double volume;
  SukhavatiVolumnState({this.volume = 0.5}) : super([volume]);
  @override
  String toString() => 'SukhavatiVolumnState';
}

class SukhavatiInitFinishState extends SukhavatiSetState {
  @override
  String toString() => 'SukhavatiInitFinishState';
}

class SukhavatiStartState extends SukhavatiSetState {
  final int interval;
  SukhavatiStartState({this.interval = 500}) : super([interval]);
  @override
  String toString() => 'SukhavatiStartState';
}

class SukhavatiUnderwayState extends SukhavatiSetState {
  final int count;
  SukhavatiUnderwayState({this.count}) : super([count]);
  @override
  String toString() => 'SukhavatiUnderwayState';
}

class SukhavatiFrequencyPreviewState extends SukhavatiSetState {
  SukhavatiFrequencyPreviewState() : super();
  @override
  String toString() => 'SukhavatiFrequencyPreviewState';
}

class SukhavatiFrequencyPreviewStopState extends SukhavatiSetState {
  SukhavatiFrequencyPreviewStopState() : super();
  @override
  String toString() => 'SukhavatiFrequencyPreviewState';
}

class SukhavatiFrequencyPreviewFinishState extends SukhavatiSetState {
  SukhavatiFrequencyPreviewFinishState() : super();
  @override
  String toString() => 'SukhavatiFrequencyPreviewState';
}

class SukhavatiPauseState extends SukhavatiSetState {
  final int reduceCount;
  SukhavatiPauseState({this.reduceCount}) : super([reduceCount]);
  @override
  String toString() => 'SukhavatiPauseState';
}

class SukhavatiStopState extends SukhavatiSetState {
  SukhavatiStopState() : super();
  @override
  String toString() => 'SukhavatiStopState';
}

class SukhavatiFinishState extends SukhavatiSetState {
  @override
  String toString() => 'SukhavatiFinishState';
}

class SukhavatiGoingState extends SukhavatiSetState {
  @override
  String toString() => 'SukhavatiGoingState';
}
