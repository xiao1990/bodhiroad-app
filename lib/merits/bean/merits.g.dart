// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'merits.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Merits _$MeritsFromJson(Map<String, dynamic> json) {
  return Merits(
      id: json['id'] as int,
      title: json['title'] as String,
      catalogId: json['catalogId'] as int,
      catalogName: json['catalogName'] as String,
      lastUpdate: json['lastUpdate'] as String,
      isGood: json['isGood'] as int,
      list: (json['list'] as List)
          ?.map((e) =>
              e == null ? null : Merits.fromJson(e as Map<String, dynamic>))
          ?.toList(),
      userMerits: (json['userMerits'] as List)
          ?.map((e) =>
              e == null ? null : UserMerits.fromJson(e as Map<String, dynamic>))
          ?.toList());
}

Map<String, dynamic> _$MeritsToJson(Merits instance) => <String, dynamic>{
      'id': instance.id,
      'title': instance.title,
      'catalogId': instance.catalogId,
      'catalogName': instance.catalogName,
      'lastUpdate': instance.lastUpdate,
      'isGood': instance.isGood,
      'list': instance.list,
      'userMerits': instance.userMerits
    };
