// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'book_catalog.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CatalogBean _$CatalogBeanFromJson(Map<String, dynamic> json) {
  return CatalogBean(
      title: json['title'] as String,
      subTitle: json['subTitle'] as String,
      pageIndex: json['pageIndex'] as int,
      level: json['level'] as int,
      nodeIndex: json['nodeIndex'] as int,
      indexedPage: json['indexedPage'] == null
          ? null
          : PageBean.fromJson(json['indexedPage'] as Map<String, dynamic>),
      yOffset: (json['yOffset'] as num)?.toDouble(),
      fullTextIndex: json['fullTextIndex'] as int,
      tag: json['tag'] as String);
}

Map<String, dynamic> _$CatalogBeanToJson(CatalogBean instance) =>
    <String, dynamic>{
      'title': instance.title,
      'subTitle': instance.subTitle,
      'pageIndex': instance.pageIndex,
      'level': instance.level,
      'nodeIndex': instance.nodeIndex,
      'indexedPage': instance.indexedPage,
      'yOffset': instance.yOffset,
      'tag': instance.tag,
      'fullTextIndex': instance.fullTextIndex
    };
