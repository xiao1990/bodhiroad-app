import 'dart:io';
import 'dart:math' as math;
import 'dart:ui';

import 'package:after_layout/after_layout.dart';
import 'package:bodhiroad/api/api.dart';
import 'package:bodhiroad/common/FontFamilyName.dart';
import 'package:bodhiroad/components/popup_menu/popup_menu.dart';
import 'package:bodhiroad/components/reader_drawer.dart';
import 'package:bodhiroad/config/GlobalColorCOnfig.dart';
import 'package:bodhiroad/config/ReaderThemeConfig.dart';
import 'package:bodhiroad/reader/bean/book_catalog.dart';
import 'package:bodhiroad/reader/bean/book_page.dart';
import 'package:bodhiroad/reader/bean/book_paragraph_unit.dart';
import 'package:bodhiroad/reader/bean/bookmark.dart';
import 'package:bodhiroad/reader/bean/reader_theme.dart';
import 'package:bodhiroad/reader/bloc.dart';
import 'package:bodhiroad/reader/bloc/light_ajust_bloc.dart';
import 'package:bodhiroad/reader/bloc/progress_bloc.dart';
import 'package:bodhiroad/reader/bloc/reader_theme_bloc.dart';
import 'package:bodhiroad/reader/bottom_slide.dart';
import 'package:bodhiroad/reader/pages/search_result.dart';
import 'package:bodhiroad/task/read/bloc/task_bloc.dart';
import 'package:bodhiroad/task/read/bloc/task_event.dart';
import 'package:bodhiroad/theme/bloc.dart';
import 'package:bodhiroad/theme/theme_bloc.dart';
import 'package:bodhiroad/usercenter/bloc/download_bloc.dart';
import 'package:bodhiroad/usercenter/bloc/download_event.dart';
import 'package:bodhiroad/utils/DateUtils.dart';
import 'package:bodhiroad/utils/GlobalConfigUtil.dart';
import 'package:bodhiroad/utils/Route.dart';
import 'package:bodhiroad/utils/ToastUtils.dart';
import 'package:bodhiroad/utils/parameter_convert_cn.dart';
import 'package:esys_flutter_share/esys_flutter_share.dart';
import 'package:flustars/flustars.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:global_configuration/global_configuration.dart';
import 'package:image_picker_saver/image_picker_saver.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:screenshot/screenshot.dart';
import 'package:sliding_up_panel/sliding_up_panel.dart';

import 'VerticalScrollPage.dart';
import 'bottom_menu.dart';
import 'horizontalScrollPage.dart';

class BookView extends StatefulWidget {
  BookView({Key key, this.fileUrl, this.id, this.source, this.taskType = 0, this.bookName, this.version}) : super(key: key);

  final String bookName;
  final String fileUrl;
  final int id;
  final int source;
  final int taskType;
  final int version;

  @override
  _BookViewState createState() => _BookViewState();
}

class _BookViewState extends State<BookView> with TickerProviderStateMixin, AfterLayoutMixin<BookView> {
  // 分页集合
  List<PageBean> pageList = [];
  // 书签列表
  List<BookMark> bookMarkList = [];
  // 划线列表
  List<BookMark> underLineList = [];
  // 目录列表
  List<CatalogBean> catalogList = [];
  // 书籍段落集合
  List<BookParagraphUnitBean> unitList = [];
  // 经文名称
  String bookName = '';
  BuddhistReaderBlocBloc _buddhistReaderBlocBloc;

  BottomSlideBloc slideBloc;
  // 进度条Bloc
  ProgressBloc progressBloc = ProgressBloc();
  // 亮度调节Bloc
  LightAjustBloc lightAjustBloc = LightAjustBloc();
  // APP 主题bloc
  AppThemeBloc appThemeBloc = AppThemeBloc();
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey();
  // 页面滚动控制器
  ReaderThemeBloc readerThemeBloc = ReaderThemeBloc();
  TaskBloc _taskBloc = TaskBloc();
  int initPage = 0;
  double initPosition = 0.0;
  PanelController _panelController;
  DownloadBloc downloadBloc = DownloadBloc();
  @override
  void dispose() {
    setDesignWHD(1080, 2160);
    SystemChrome.setEnabledSystemUIOverlays([SystemUiOverlay.top]);
    super.dispose();
  }

  @override
  void initState() {
    _buddhistReaderBlocBloc = BuddhistReaderBlocBloc();
    _panelController = PanelController();
    slideBloc = BottomSlideBloc();

    ///字符串解码 经文名称
    bookName = ParameterConvertUtils.parseParameterCN(widget.bookName);
    // 用户阅读配置
    GlobalConfiguration().updateValue('fileUrl', widget.fileUrl);
    GlobalConfiguration().updateValue('id', widget.id);
    GlobalConfiguration().updateValue('version', widget.version);
    GlobalConfiguration().updateValue('source', widget.source);
    GlobalConfiguration().updateValue('taskType', widget.taskType);
    GlobalConfiguration().updateValue('readFinished', false);
    if (widget.taskType == 4) {
      GlobalConfiguration().updateValue('textLayOutDirection', 0);
      GlobalConfiguration().updateValue('flipType', 0);
    }
    _buddhistReaderBlocBloc.dispatch(InitReaderBlocEvent());
    _buddhistReaderBlocBloc.dispatch(ReloadReaderEvent());

//    阅读页面全屏
    SystemChrome.setEnabledSystemUIOverlays([]);
    // 添加功课开始记录
    _taskBloc.dispatch(AddTaskRecordEvent());
    progressBloc.dispatch(0.0);
    super.initState();
  }

  @override
  void afterFirstLayout(BuildContext context) {
    slideBloc.dispatch(0);
  }

  back2Index() {
    setDesignWHD(1080, 2160);
    Navigator.of(context).pop();
  }

  /// 添加收藏
  addFavorite(int value) {
    _buddhistReaderBlocBloc.dispatch(AddFavoriteEvent(status: value));
  }

  /// 进度跳转
  seek(double position) {
    if (GlobalConfiguration().getInt('flipType') < 2) {
      if (GlobalConfiguration().getInt('textLayOutDirection') == 1) {
        position = pageList.length - position;
      }
      _buddhistReaderBlocBloc.dispatch(ChapterJumpEvent(index: position.toInt() > pageList.length ? position.toInt() - 1 : position.toInt()));
      if (position.toInt() == pageList.length) {
        _buddhistReaderBlocBloc.dispatch(ReaderReachBottomEvent());
      } else {
        _buddhistReaderBlocBloc.dispatch(ReaderUnReachBottomEvent());
      }
    } else {
      _buddhistReaderBlocBloc.dispatch(ChapterJumpEvent(position: position));
      if (pageList.last.charList.first.yOffset < position) {
        _buddhistReaderBlocBloc.dispatch(ReaderReachBottomEvent());
      } else {
        _buddhistReaderBlocBloc.dispatch(ReaderUnReachBottomEvent());
      }
    }
    GlobalConfigUtil.saveReaderProgress(position);
  }
//    progressBloc.dispatch(position);

  /// 跳转上一章节
  goToPrevChapter() {
    if (GlobalConfiguration().getInt('flipType') < 2) {
      /// 当前页面index
      int currentPageIndex = GlobalConfiguration().getInt('currentPageIndex');

      /// 当前页面章节index
      int currentChapterIndex = pageList[currentPageIndex].chapterIndex;

      if (currentChapterIndex == 0) {
        ToastUtils.fail('已经是第一章！');
        return;
      } else {
        int prevChapterPageIndex = getPrevChapterIndexedPage(currentPageIndex, currentChapterIndex);
        if (prevChapterPageIndex == currentPageIndex) {
          ToastUtils.fail('已经是第一章！');
          return;
        } else {
          _buddhistReaderBlocBloc.dispatch(ChapterJumpEvent(index: prevChapterPageIndex));
          progressBloc.dispatch(prevChapterPageIndex.toDouble());
          GlobalConfigUtil.saveReaderProgress(prevChapterPageIndex.toDouble());
          _buddhistReaderBlocBloc.dispatch(ReaderUnReachBottomEvent());
        }
      }
    } else {
      for (var i = 0; i < catalogList.length; i++) {
        if (GlobalConfiguration().getDouble('scrollOffset') <= catalogList[i].yOffset && i > 0 && catalogList[i].level == 2) {
          double position = catalogList[i - 1].level == 1 ? catalogList[i - 2].yOffset : catalogList[i - 1].yOffset;
          goToDestination(position);
          break;
        }
      }
    }
  }

  /// 滚动到指定章节
  goToDestination(double position) {
    _buddhistReaderBlocBloc.dispatch(ChapterJumpEvent(position: position));
    GlobalConfigUtil.saveReaderProgress(position);
    GlobalConfiguration().updateValue('scrollOffset', position);
    _buddhistReaderBlocBloc.dispatch(ReaderUnReachBottomEvent());
  }

  goChapterStart() {
    seek(0);
  }

  /// 跳转下一章节
  goToNextChapter() {
    if (GlobalConfiguration().getInt('flipType') < 2) {
      /// 当前页面index
      int currentPageIndex = GlobalConfiguration().getInt('currentPageIndex');

      /// 当前页面章节index
      int currentChapterIndex = pageList[currentPageIndex].chapterIndex;
      if (currentChapterIndex + 1 > catalogList.length) {
        ToastUtils.fail('已经是最后一章！');
      } else {
        int nextChapterPageIndex = getNextChapterIndexedPage(currentPageIndex, currentChapterIndex);
        if (nextChapterPageIndex == currentPageIndex) {
          ToastUtils.fail('已经是最后一章！');
          return;
        } else {
          _buddhistReaderBlocBloc.dispatch(ChapterJumpEvent(index: nextChapterPageIndex));
          progressBloc.dispatch(nextChapterPageIndex.toDouble());
          GlobalConfigUtil.saveReaderProgress(nextChapterPageIndex.toDouble());
        }
      }
    } else {
      for (var i = 0; i < catalogList.length; i++) {
        if (GlobalConfiguration().getDouble('scrollOffset') < catalogList[i].yOffset && catalogList[i].level == 2) {
          goToDestination(catalogList[i].yOffset);
          break;
        } else {
          _buddhistReaderBlocBloc.dispatch(ReaderUnReachBottomEvent());
        }
      }
    }
  }

  /// 递归获取上一章节所在页面
  int getPrevChapterIndexedPage(int currentPageIndex, int currentChapterIndex) {
    currentChapterIndex--;
    if (currentChapterIndex < 0) {
      ToastUtils.fail('已经是第一章！');
      return currentPageIndex;
    } else {
      int prevChapterPageIndex = catalogList[currentChapterIndex].pageIndex;
      if (prevChapterPageIndex == currentPageIndex) {
        prevChapterPageIndex = getPrevChapterIndexedPage(currentPageIndex, currentChapterIndex);
      }
      return prevChapterPageIndex;
    }
  }

  /// 递归获取下一章节所在页面
  int getNextChapterIndexedPage(int currentPageIndex, int currentChapterIndex) {
    currentChapterIndex++;
    if (currentChapterIndex > catalogList.length - 1) {
      ToastUtils.fail('已经是最后一章！');
      return catalogList.last.pageIndex;
    }
    int nextChapterPageIndex = catalogList[currentChapterIndex].pageIndex;
    if (nextChapterPageIndex == currentPageIndex) {
      nextChapterPageIndex = getNextChapterIndexedPage(currentPageIndex, currentChapterIndex);
    }
    return nextChapterPageIndex;
  }

  String loadingText = '初始化···';
  bool reversed = false;
  bool reachEnd = false;

  List<Widget> getVerticalText(String text) {
    int cols = (text.length / 20).ceil();
    List<Widget> list = [];
    for (int i = 0; i < cols; i++) {
      String text2 = text.substring(i * 20, math.min(i * 20 + 20 - 1, text.length - 1) + 1).replaceAll('', '\n');
      text2.substring(1, text2.length - 1);
      list.add(Container(
        width: ScreenUtil.getInstance().getWidth(72),
        height: ScreenUtil.getInstance().screenHeight * 0.5,
        alignment: Alignment.topLeft,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Container(
              width: ScreenUtil.getInstance().getWidth(72),
              child: Text(
                text2,
                style: TextStyle(
                    decoration: TextDecoration.none,
                    color: Colors.black87,
                    fontSize: ScreenUtil.getInstance().getSp(42),
                    shadows: [Shadow(color: Colors.black26, blurRadius: 6)],
                    fontWeight: FontWeight.normal,
                    fontFamily: FontFamilyName.CHINESESTYLE),
              ),
            )
          ],
        ),
      ));
    }
    return list.reversed.toList();
  }

  ScreenshotController screenshotController = ScreenshotController();
  double opc = 0;

  openCatalogDialog(BuildContext context) {
    showDialog(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context) {
          return StatefulBuilder(builder: (BuildContext context, setState) {
            return Material(
              color: Colors.transparent,
              child: Dialog(
                shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(50)),
                child: ReaderDrawer(bookName: '', titleList: catalogList, pageList: pageList, bookMarkList: bookMarkList, underLineList: underLineList, bloc: _buddhistReaderBlocBloc),
              ),
            );
          });
        });
  }

  showBookmarkDialog(BuildContext context, String text) {
    String dateStr = DateUtils.getLunar(DateTime.now()).toString().replaceRange(3, 9, '').replaceAll('', '\n');
    showDialog(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context) {
          return StatefulBuilder(
            builder: (BuildContext context, setState) {
              return Container(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Screenshot(
                        controller: screenshotController,
                        child: Container(
                          height: ScreenUtil.getInstance().screenHeight * 0.7,
                          width: ScreenUtil.getInstance().screenWidth * 0.8,
                          decoration: BoxDecoration(
                              color: Colors.white,
                              image: DecorationImage(fit: BoxFit.none, alignment: Alignment.bottomCenter, image: AssetImage('assets/texture/ss13.jpg')),
                              borderRadius: BorderRadius.circular(15)),
                          child: Row(
                            children: <Widget>[
                              Expanded(
                                  flex: 1,
                                  child: Container(
                                    alignment: Alignment.bottomCenter,
                                    child: Column(
                                      mainAxisAlignment: MainAxisAlignment.end,
                                      children: [
                                        Text(
                                          GlobalConfigUtil.getString('name').replaceAll('', '\n') + '·\n' + dateStr.substring(1, dateStr.length - 1),
                                          style: TextStyle(
                                              decoration: TextDecoration.none,
                                              color: Colors.black54,
                                              fontSize: ScreenUtil.getInstance().getSp(42),
                                              fontWeight: FontWeight.normal,
                                              fontFamily: FontFamilyName.CHINESESTYLE),
                                        ),
                                        Image.asset(
                                          'assets/default.png',
                                          width: 20,
                                        ),
                                        Container(
                                          height: 20,
                                          width: 1,
                                        )
                                      ],
                                    ),
                                  )),
                              Expanded(
                                  flex: 4,
                                  child: Container(
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.center,
                                      children: getVerticalText(text),
                                    ),
                                  )),
                              Expanded(
                                  flex: 1,
                                  child: Container(
                                    height: ScreenUtil.getInstance().screenHeight * 0.7,
                                    alignment: Alignment.topLeft,
                                    child: Container(
                                      padding: EdgeInsets.only(left: 10),
                                      margin: EdgeInsets.only(top: 20),
                                      decoration: BoxDecoration(border: Border(left: BorderSide(color: Colors.black26, width: 0.5))),
                                      child: Text(
                                        bookName.replaceAll('', '\n'),
                                        style: TextStyle(
                                            decoration: TextDecoration.none,
                                            color: Colors.black54,
//                                        shadows: [Shadow(color: Colors.black26, blurRadius: 6)],
                                            fontSize: ScreenUtil.getInstance().getSp(60),
                                            fontWeight: FontWeight.normal,
                                            fontFamily: FontFamilyName.CHINESESTYLE),
                                      ),
                                    ),
                                  )),
                            ],
                          ),
                        )),
                    Container(
                      height: 20,
                      width: 300,
                    ),
                    Container(
                      height: ScreenUtil.getInstance().getWidth(150),
                      width: ScreenUtil.getInstance().screenWidth * 0.8,
                      decoration: BoxDecoration(color: Colors.white, borderRadius: BorderRadius.circular(15)),
                      child: Row(
                        children: <Widget>[
                          Expanded(
                              child: ButtonTheme(
                                  height: ScreenUtil.getInstance().getWidth(150),
                                  child: FlatButton.icon(
                                    onPressed: () {
                                      screenshotController.capture(pixelRatio: 2).then((File image) async {
                                        String filePath = await ImagePickerSaver.saveFile(fileData: image.readAsBytesSync());
                                        if (filePath != null && filePath.length > 0) {
                                          ToastUtils.success('保存成功');
                                        }
                                      }).catchError((onError) {
                                        print(onError);
                                      });
                                    },
                                    icon: Icon(
                                      MdiIcons.contentSave,
                                      size: ScreenUtil.getInstance().getWidth(48),
                                    ),
                                    label: Text(
                                      '保存',
                                      style: TextStyle(fontSize: ScreenUtil.getInstance().getSp(42)),
                                    ),
                                  ))),
                          Expanded(
                              child: ButtonTheme(
                            height: ScreenUtil.getInstance().getWidth(150),
                            child: FlatButton.icon(
                              onPressed: () {
                                screenshotController.capture(pixelRatio: 2).then((File image) async {
                                  await Share.file('分享到', 'esys.png', image.readAsBytesSync(), 'image/png', text: '');
                                }).catchError((onError) {
                                  print(onError);
                                });
                              },
                              icon: Icon(
                                MdiIcons.shareCircle,
                                size: ScreenUtil.getInstance().getWidth(48),
                              ),
                              label: Text(
                                '分享',
                                style: TextStyle(fontSize: ScreenUtil.getInstance().getSp(42)),
                              ),
                            ),
                          ))
                        ],
                      ),
                    )
                  ],
                ),
              );
            },
          );
        });
  }

  void handleMakeBookmark(BuildContext context, String content) {
    if (content.length > 100) {
      ToastUtils.fail('字数过长，不能超过100个字符！');
      return;
    }
    showBookmarkDialog(context, content);
  }

  void handleShare(String content) {
    Share.text('分享', content, 'text/plain');
  }

  void handleDrawUnderline(BookMark content) {
    _buddhistReaderBlocBloc.dispatch(AddBookMarkEvent(bookMark: content));
    content.chapterIndex = getMarkedChapterIndex(content.actualStartIndex);
    if (SpUtil.getString('token') != null && SpUtil.getString('token').length > 0) {
      Api.addBuddhistMark(data: {
        'articleId': widget.id,
        'articleType': widget.source,
        'chapterId': content.chapterIndex,
        'title': content.title.substring(0, math.min(50, content.title.length)),
        'startIndex': content.actualStartIndex,
        'endIndex': content.actualEndIndex
      });
    }
  }

  int getMarkedChapterIndex(int startIndex) {
    int chapterIndex = 0;
    for (int i = 0; i < catalogList.length; i++) {
      if (startIndex <= catalogList[i].fullTextIndex) {
        chapterIndex = i - 1;
        break;
      }
    }
    return chapterIndex;
  }

  void handleMyCopy(String content) {
    Clipboard.setData(ClipboardData(
      text: content,
    ));
    ToastUtils.success('已复制到剪贴板···');
  }

  void openMarkedValue(Rect rect, int id, String content) {
    PopupMenu menu = PopupMenu(
        backgroundColor: readerTheme.menuFontColor,
        isLast: false,
        maxColumn: 4,
        lineColor: Colors.transparent,
        items: [
          MenuItem(
              title: '复制',
              index: 0,
              textStyle: TextStyle(fontSize: ScreenUtil.getInstance().getSp(28), color: readerTheme.backgroundColor),
              image: Icon(
                MdiIcons.contentCopy,
                color: Colors.white,
                size: 16,
              )),
          MenuItem(
              title: '取消标记',
              index: 1,
              textStyle: TextStyle(fontSize: ScreenUtil.getInstance().getSp(28), color: readerTheme.backgroundColor),
              image: Icon(
                MdiIcons.marker,
                color: Colors.white,
                size: 16,
              )),
          MenuItem(
              title: '卡片',
              index: 2,
              textStyle: TextStyle(fontSize: ScreenUtil.getInstance().getSp(28), color: readerTheme.backgroundColor),
              image: Icon(
                MdiIcons.bookmarkCheck,
                color: Colors.white,
                size: 16,
              )),
          MenuItem(
              title: '分享',
              index: 3,
              textStyle: TextStyle(fontSize: ScreenUtil.getInstance().getSp(28), color: readerTheme.backgroundColor),
              image: Icon(
                MdiIcons.shareVariant,
                color: Colors.white,
                size: 16,
              )),
        ],
        onClickMenu: (item) {
          if (item is MenuItem) {
            if (item.index == 1) {
              _buddhistReaderBlocBloc.dispatch(RemoveUnderLineEvent(id));
            } else if (item.index == 2) {
              handleMakeBookmark(context, content);
            } else if (item.index == 3) {
              handleShare(content);
            } else if (item.index == 0) {
              handleMyCopy(content);
            }
          }
        },
        onDismiss: () {
          GlobalConfiguration().updateValue('specialTapped', true);
        });
    menu.show(rect: rect);
  }

  /// 查询
  search(BuildContext context, String value) {
    downloadBloc.dispatch(DictSearchEvent(keyword: value, catalogIndex: 0));
    showDialog(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context) {
          return SearchResult(
            title: value,
            downloadBloc: downloadBloc,
          );
        });
  }

  /// 纠错
  recovery(BuildContext context, String value) {
//    downloadBloc.dispatch(DictSearchEvent(keyword: value, catalogIndex: 0));
    showDialog(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context) {
          return SearchResult(
            title: value,
            downloadBloc: downloadBloc,
          );
        });
  }

  ReaderTheme readerTheme = ReaderThemeConfig.instance.list[GlobalConfigUtil.getInt('themeIndex')];
  @override
  Widget build(BuildContext context) {
//    if (Platform.isAndroid) {
//      SystemUiOverlayStyle systemUiOverlayStyle = SystemUiOverlayStyle(statusBarColor: Colors.transparent);
//      SystemChrome.setSystemUIOverlayStyle(systemUiOverlayStyle);
//    }

    return BlocBuilder(
        bloc: AppThemeBloc(),
        builder: (context, ThemeChangeState appThemeState) {
          return BlocBuilder(
              bloc: readerThemeBloc,
              builder: (context, ReaderThemeState readerThemeState) {
                return BlocBuilder(
                    bloc: _buddhistReaderBlocBloc,
                    condition: (previousState, currentState) {
                      if (currentState is ReloadReaderState) {
                        pageList = currentState.pageList;
                        catalogList = currentState.catalogList;
                        bookMarkList = currentState.bookMarkList2;
                        unitList = currentState.unitList;
                        if (pageList.length == 1) {
                          _buddhistReaderBlocBloc.dispatch(ReaderReachBottomEvent());
                        }
                      } else if (currentState is BuddhistReaderLoadingState) {
                        loadingText = currentState.description;
                      } else if (currentState is DownloadingBuddhistReaderBlocState) {
                        loadingText = currentState.description;
                      } else if (currentState is ParsingBuddhistReaderBlocState) {
                        loadingText = currentState.description;
                      } else if (currentState is ReaderReachBottomState) {
                        if (reachEnd) {
                          return false;
                        } else {
                          reachEnd = true;
                          print('到达底部！');
                          return true;
                        }
                      } else if (currentState is ReaderUnReachBottomState) {
                        if (reachEnd) {
                          reachEnd = false;
                          print('未到达底部！');
                          return true;
                        } else {
                          return false;
                        }
                      } else if (currentState is SimpleReloadReaderState) {
                        _buddhistReaderBlocBloc.dispatch(SimpleReloadFinishReaderEvent());
                      } else if (currentState is OpenBottomMenuState) {
                        _buddhistReaderBlocBloc.dispatch(OpenBottomMenuSuccessEvent());
                      } else if (currentState is BottomMenuSlidingState) {
                        opc = currentState.progress;
                      }
                      return currentState.reload;
                    },
                    builder: (context, pageState) {
                      return Scaffold(
                              key: _scaffoldKey,
                              backgroundColor: readerTheme.backgroundColor,
                              endDrawer:
                                  ReaderDrawer(bookName: '', titleList: catalogList, pageList: pageList, bookMarkList: bookMarkList, underLineList: underLineList, bloc: _buddhistReaderBlocBloc),
                              body: SlidingUpPanel(
                                  controller: _panelController,
                                  minHeight: 20,
                                  parallaxEnabled: true,
                                  maxHeight: ScreenUtil.getInstance().screenHeight * 0.5,
                                  parallaxOffset: 0.3,
                                  renderPanelSheet: false,
                                  color: Colors.transparent,
                                  borderRadius: BorderRadius.circular(24),
                                  backdropEnabled: true,
                                  onPanelSlide: (progress) {
                                    slideBloc.dispatch(math.min(progress * 6, 1));
                                  },
                                  panel: BlocBuilder(
                                    bloc: slideBloc,
                                    builder: (context, state) {
                                      return Opacity(
                                          opacity: state,
                                          child: BottomMenu(
                                            readerBlocBloc: _buddhistReaderBlocBloc,
                                            progressBloc: progressBloc,
                                            readerThemeBloc: readerThemeBloc,
                                            lightAdjustBloc: lightAjustBloc,
                                            appThemeBloc: appThemeBloc,
                                            backToList: () {
                                              _panelController.close();
                                              _panelController.open();
                                              Navigator.pop(context);
                                            },
                                            addFavorite: (value) {
                                              addFavorite(value);
                                            },
                                            openDrawer: (value) {
                                              if (GlobalConfigUtil.getInt('drawerType') != null) {
                                                GlobalConfigUtil.updateValue('drawerType', value);
                                              } else {
                                                GlobalConfiguration().addValue('drawerType', value);
                                              }
                                              _panelController.close();
                                              _scaffoldKey.currentState.openEndDrawer();
                                            },
                                            chapterSliderMax: GlobalConfiguration().getInt('flipType') == 2 && pageList.length > 0
                                                ? pageList.last.charList.last.yOffset
                                                : pageList.length == 0 ? 1.0 : pageList.length.toDouble(),
                                            chapterSliderMin: 0.0,
                                            onChapterChange: (value) {},
                                            onChapterChangeEnd: (double value) {
                                              seek(value);
                                            },
                                            onReaderThemeChanged: (index) {
                                              // 切换主题
                                              GlobalConfigUtil.updateValue('themeIndex', index);
                                              _buddhistReaderBlocBloc.dispatch(SimpleReloadReaderEvent());
                                              readerTheme = ReaderThemeConfig.instance.list[index];
                                            },
                                            onFontSizeChange: (double value) {
                                              // 字体大小调整
                                              GlobalConfigUtil.updateValue('fontSize', value.toInt());
                                              _buddhistReaderBlocBloc.dispatch(ReloadReaderEvent());
                                            },
                                            onFlipTypeChange: (int index) {
                                              // 翻页方式切换
                                              int preIndex = GlobalConfiguration().getInt('flipType');
                                              if (preIndex == index) {
                                                return;
                                              }
                                              if (GlobalConfiguration().getInt('taskType') == 4 && index == 2) {
                                                ToastUtils.fail('该功课暂不支持该翻页方式！');
                                                return;
                                              }
                                              GlobalConfigUtil.updateValue('flipType', index);
                                              if (index == 2) {
                                                GlobalConfigUtil.updateValue('textLayOutDirection', 0);
                                              }
                                              if (index == 2 || preIndex == 2) {
                                                pageList = [];
                                                _buddhistReaderBlocBloc.dispatch(ReloadReaderEvent());
                                                progressBloc.dispatch(0);
                                              } else {
                                                _buddhistReaderBlocBloc.dispatch(SimpleReloadReaderEvent());
                                              }
                                            },
                                            onTextDirectionChanged: (bool value) {
                                              if (GlobalConfiguration().getInt('taskType') == 4 && !value) {
                                                ToastUtils.fail('该功课暂不支持切换文字排版方式！');
                                                return;
                                              }
                                              // 切换文字排版方向
                                              GlobalConfigUtil.updateValue('textLayOutDirection', value ? 0 : 1);
                                              GlobalConfigUtil.saveReaderProgress(0);
                                              if (!value && GlobalConfiguration().getInt('flipType') == 2) {
                                                GlobalConfigUtil.updateValue('flipType', 0);
                                              }
                                              _buddhistReaderBlocBloc.dispatch(ReloadReaderEvent());
                                              _buddhistReaderBlocBloc.dispatch(ChapterJumpEvent(index: GlobalConfiguration().getInt('textLayOutDirection') == 1 ? -1 : 0, position: 0.0));
                                            },
                                            onLanguageTypeChange: (bool value) {
                                              // 简繁转换
                                              GlobalConfigUtil.updateValue('simplifiedChinese', value);
                                              _buddhistReaderBlocBloc.dispatch(SimpleReloadReaderEvent());
                                            },
                                            goLastChapter: () {
                                              // 上一章
                                              goToPrevChapter();
                                            },
                                            goNextChapter: () {
                                              // 下一章
                                              goToNextChapter();
                                            },
                                          ));
                                    },
                                  ),
                                  body: pageList.length > 0
                                      ? Container(
                                          color: ReaderThemeConfig.instance.list[readerThemeState.readConfig.themeIndex].backgroundColor,
                                          height: MediaQuery.of(context).size.height,
                                          width: MediaQuery.of(context).size.width,
                                          child: Stack(children: <Widget>[
                                            GlobalConfiguration().get('flipType') == 2
                                                ? VerticalScrollPage(
                                                    pageList: pageList,
                                                    bloc: _buddhistReaderBlocBloc,
                                                    progressBloc: progressBloc,
                                                    openBottomMenu: () {
                                                      _panelController.open();
                                                    },
                                                    handleDrawUnderline: (BookMark bookmark) {
                                                      handleDrawUnderline(bookmark);
                                                    },
                                                    handleMakeBookMark: (String content) {
                                                      handleMakeBookmark(context, content);
                                                    },
                                                    handleShare: (String content) {
                                                      handleShare(content);
                                                    },
                                                    handleMyCopy: (String content) {
                                                      handleMyCopy(content);
                                                    },
                                                    openMarkedMenu: (Rect rect, int id, String content) {
                                                      openMarkedValue(rect, id, content);
                                                    },
                                                    handleSearch: (String value) {
                                                      search(context, value);
                                                    },
                                                    handleRecovery: (String value) {
                                                      Routes.router.navigateTo(context, 'recovery');
                                                    },
                                                  )
                                                :
//                                            LiquidPullToRefresh(
//                                                    springAnimationDurationInMilliseconds: 0,
//                                                    showChildOpacityTransition: false,
//                                                    height: 60,
//                                                    onRefresh: () async {}, // refresh callback
//                                                    child:

                                                ListView(
                                                    padding: EdgeInsets.all(0),
                                                    children: [
                                                      HorizontalScrollPage(
                                                          pageList: pageList,
                                                          title: bookName,
                                                          bloc: _buddhistReaderBlocBloc,
                                                          progressBloc: progressBloc,
                                                          openBottomMenu: () {
                                                            _panelController.open();
                                                          },
                                                          openMarkedMenu: (Rect rect, int id, String content) {
                                                            openMarkedValue(rect, id, content);
                                                          },
                                                          onPageChanged: (int index) {
                                                            GlobalConfigUtil.updateValue('currentPageIndex', index);
                                                            if (GlobalConfiguration().getInt('textLayOutDirection') == 1) {
                                                              progressBloc.dispatch(pageList.length - 1 - index.toDouble());
                                                              if (index == 0) {
                                                                _buddhistReaderBlocBloc.dispatch(ReaderReachBottomEvent());
                                                              } else {
                                                                _buddhistReaderBlocBloc.dispatch(ReaderUnReachBottomEvent());
                                                              }
                                                            } else {
                                                              progressBloc.dispatch(index.toDouble());
                                                              if (index == pageList.length - 1) {
                                                                _buddhistReaderBlocBloc.dispatch(ReaderReachBottomEvent());
                                                              } else {
                                                                _buddhistReaderBlocBloc.dispatch(ReaderUnReachBottomEvent());
                                                              }
                                                            }
                                                            GlobalConfigUtil.saveReaderProgress(index.toDouble());

                                                            if (GlobalConfiguration().getInt('textLayOutDirection') == 0) {
                                                                progressBloc.dispatch(index.toDouble());
                                                              if (pageList.length - 1 == index) {
                                                                _buddhistReaderBlocBloc.dispatch(ReaderReachBottomEvent());
                                                                // 最后一页 && 直排文字，跳转到第一页
                                                              } else {
                                                                _buddhistReaderBlocBloc.dispatch(ReaderUnReachBottomEvent());
                                                              }
                                                            }else{
                                                              progressBloc.dispatch((pageList.length - index).toDouble());
                                                              if (index == 0) {
                                                                _buddhistReaderBlocBloc.dispatch(ReaderReachBottomEvent());
                                                              } else {
                                                                _buddhistReaderBlocBloc.dispatch(ReaderUnReachBottomEvent());
                                                              }
                                                            }
                                                          },
                                                          handleMakeBookmark: (String value) {
                                                            handleMakeBookmark(context, value);
                                                          },
                                                          handleDrawUnderline: (value) {
                                                            handleDrawUnderline(value);
                                                          },
                                                          handleMyCopy: (String value) {
                                                            handleMyCopy(value);
                                                          },
                                                          handleMySelectAll: (value) {
                                                            print(value);
                                                          },
                                                          handleMyCut: (value) {
                                                            print(value);
                                                          },
                                                          handleMyPaste: (value) {
                                                            print(value);
                                                          },
                                                          handleSearch: (value) {
                                                            search(context, value);
                                                          },
                                                          handleShare: (String value) {
                                                            handleShare(value);
                                                          },
                                                          handleRecovery: (value) {
                                                            value = ParameterConvertUtils.convertCN(value);
                                                            Routes.router.navigateTo(context, '/recovery?value=' + value);
                                                          })
                                                    ],
                                                  )
//                                            )
//                                            )
                                            ,
                                            Positioned(
                                              right: 20,
                                              bottom: 70,
                                              child: Offstage(
                                                  offstage: !reachEnd || widget.source > 0,
                                                  child: FloatingActionButton(
                                                    backgroundColor: GlobalConfiguration().getBool('readFinished') ? Colors.green : GlobalColorConfig.mainRedColor,
                                                    onPressed: () {
                                                      if (GlobalConfiguration().getBool('readFinished')) {
                                                        GlobalConfigUtil.updateValue('readFinished', false);
                                                        _buddhistReaderBlocBloc.dispatch(ReaderRestartEvent());
                                                        _taskBloc.dispatch(AddTaskRecordEvent());
                                                      } else {
                                                        GlobalConfigUtil.updateValue('readFinished', true);
                                                        _taskBloc.dispatch(UpdateTaskRecordEvent());
                                                      }
                                                      setState(() {});
                                                    },
                                                    child: GlobalConfiguration().getBool('readFinished') ? Icon(Icons.replay) : Icon(Icons.done),
                                                  )),
                                            )
                                            // 悬浮按钮
                                          ]))
                                      : Center(
                                          child: Column(
                                            mainAxisAlignment: MainAxisAlignment.center,
                                            children: <Widget>[
                                              SpinKitThreeBounce(
                                                color: Colors.blueGrey,
                                                size: 20.0,
                                              ),
                                              Text(loadingText)
                                            ],
                                          ),
                                        )))
//                      )
                          ;
                    });
              });
        });
  }
}
