import 'package:json_annotation/json_annotation.dart';
part 'task.g.dart';

@JsonSerializable()
class Task {
  int id;
  int userId;
  int taskId;
  String taskName;
  int taskTypeId;
  String taskTypeName;
  String taskContentUrl;
  int state;
  String originalId;
  String description;
  String author;
  int count;
  int totalCount;
  int times;
  int version;
  Task(
      {this.id,
      this.userId,
      this.taskId,
      this.taskName,
      this.taskTypeId,
      this.taskTypeName,
      this.taskContentUrl,
      this.originalId,
      this.description,
      this.author,
      this.count,
      this.totalCount,
      this.state,
      this.times,
      this.version});
  factory Task.fromJson(Map<String, dynamic> json) {
    return _$TaskFromJson(json);
  }
  Map<String, dynamic> toJson() => _$TaskToJson(this);
}
