import 'package:bodhiroad/config/GlobalColorCOnfig.dart';
import 'package:bodhiroad/usercenter/bloc/user/bloc.dart';
import 'package:bodhiroad/utils/parameter_convert_cn.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';

class ProfileUpdatePage extends StatefulWidget {
  final String properties;
  final String value;
  final String title;
  ProfileUpdatePage({this.properties, this.title, this.value});
  String get prop => properties;
  @override
  _ProfileUpdatePageState createState() => _ProfileUpdatePageState();
}

class _ProfileUpdatePageState extends State<ProfileUpdatePage> {
  String titleCN = '';
  final GlobalKey<FormBuilderState> _fbKey = GlobalKey<FormBuilderState>();
  TextEditingController _textEditingController;
  UserBloc userBloc = UserBloc();
  bool obscureText = true;
  @override
  void initState() {
    titleCN = ParameterConvertUtils.parseParameterCN(widget.title);
    _textEditingController = TextEditingController(text: ParameterConvertUtils.parseParameterCN(widget.value));
    super.initState();
  }

  @override
  void dispose() {
//    _textEditingController.dispose();
    super.dispose();
  }

  Widget getWidget() {
    switch (widget.properties) {
      case 'name':
        return FormBuilderTextField(
          controller: _textEditingController,
          attribute: widget.properties,
          cursorColor: Colors.black87,
          validators: [
            FormBuilderValidators.required(errorText: '不能为空！'),
            (val) {
              if (val.length > 15) {
                return "请不要超过15个字符";
              }
            },
          ],
          onEditingComplete: () {
            _fbKey.currentState.save();
            if (_fbKey.currentState.validate()) {
              print(_fbKey.currentState.value);
            }
          },
          maxLines: 1,
          autofocus: true,
          style: TextStyle(fontSize: 18, color: Colors.black38),
          decoration: InputDecoration(
            filled: true,
            fillColor: Color(0xFFF2F2F2).withOpacity(0.3),
            alignLabelWithHint: false,
            hasFloatingPlaceholder: true,
            hintText: ParameterConvertUtils.parseParameterCN(widget.value),
            isDense: false,
            focusColor: GlobalColorConfig.mainBackgroundColor.withOpacity(0.0),
            hoverColor: Colors.white,
            focusedBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.black12, width: 0.5), borderRadius: BorderRadius.circular(12)),
            enabledBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.black12, width: 0.5), borderRadius: BorderRadius.circular(12)),
          ),
        );
      case 'fahao':
        return FormBuilderTextField(
          controller: _textEditingController,
          attribute: widget.properties,
          cursorColor: Colors.black87,
          validators: [
            FormBuilderValidators.required(errorText: '不能为空！'),
            (val) {
              if (val.length > 30) {
                return "请不要超过30个字符";
              }
            },
          ],
          onEditingComplete: () {
            _fbKey.currentState.save();
            if (_fbKey.currentState.validate()) {
              print(_fbKey.currentState.value);
            }
          },
          maxLines: 1,
          autofocus: true,
          style: TextStyle(fontSize: 18, color: Colors.black38),
          decoration: InputDecoration(
            filled: true,
            fillColor: Color(0xFFF2F2F2).withOpacity(0.3),
            alignLabelWithHint: false,
            hasFloatingPlaceholder: true,
            hintText: ParameterConvertUtils.parseParameterCN(widget.value),
            isDense: false,
            focusColor: GlobalColorConfig.mainBackgroundColor.withOpacity(0.0),
            hoverColor: Colors.white,
            focusedBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.black12, width: 0.5), borderRadius: BorderRadius.circular(12)),
            enabledBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.black12, width: 0.5), borderRadius: BorderRadius.circular(12)),
          ),
        );
      case 'signature':
        return FormBuilderTextField(
          controller: _textEditingController,
          attribute: widget.properties,
          cursorColor: Color(0xFFB96652),
          validators: [
            FormBuilderValidators.required(errorText: '不能为空！'),
            (val) {
              if (val.length > 30) {
                return "请不要超过30个字符";
              }
            },
          ],
          onEditingComplete: () {
            _fbKey.currentState.save();
            if (_fbKey.currentState.validate()) {
              print(_fbKey.currentState.value);
            }
          },
          maxLines: 4,
          autofocus: true,
          style: TextStyle(fontSize: 18, color: Colors.black38),
          decoration: InputDecoration(
            filled: true,
            fillColor: Color(0xFFF2F2F2).withOpacity(0.3),
            alignLabelWithHint: false,
            hasFloatingPlaceholder: true,
            hintText: ParameterConvertUtils.parseParameterCN(widget.value),
            isDense: false,
            focusColor: GlobalColorConfig.mainBackgroundColor.withOpacity(0.0),
            hoverColor: Colors.white,
            focusedBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.black12, width: 0.5), borderRadius: BorderRadius.circular(12)),
            enabledBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.black12, width: 0.5), borderRadius: BorderRadius.circular(12)),
          ),
        );
      case 'email':
        return FormBuilderTextField(
          controller: _textEditingController,
          attribute: widget.properties,
          cursorColor: Color(0xFFB96652),
          validators: [FormBuilderValidators.email(errorText: '邮箱格式不正确')],
          onEditingComplete: () {
            _fbKey.currentState.save();
            if (_fbKey.currentState.validate()) {
              print(_fbKey.currentState.value);
            }
          },
          maxLines: 1,
          autofocus: true,
          style: TextStyle(fontSize: 18, color: Colors.black38),
          decoration: InputDecoration(
            filled: true,
            fillColor: Color(0xFFF2F2F2).withOpacity(0.3),
            alignLabelWithHint: false,
            hasFloatingPlaceholder: true,
            hintText: ParameterConvertUtils.parseParameterCN(widget.value),
            isDense: false,
            focusColor: GlobalColorConfig.mainBackgroundColor.withOpacity(0.0),
            hoverColor: Colors.white,
            focusedBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.black12, width: 0.5), borderRadius: BorderRadius.circular(12)),
            enabledBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.black12, width: 0.5), borderRadius: BorderRadius.circular(12)),
          ),
        );
      case 'password':
        return Container(
            decoration: BoxDecoration(color: Color(0xFFF2F2F2).withOpacity(0.3), border: Border.all(color: Colors.black12, width: 0.5), borderRadius: BorderRadius.circular(6)),
            padding: EdgeInsets.only(left: 15),
            child: Row(
              children: <Widget>[
                Expanded(
                    child: FormBuilderTextField(
                  attribute: widget.properties,
                  cursorColor: Color(0xFFB96652),
                  obscureText: obscureText,
                  validators: [
                    FormBuilderValidators.required(errorText: '密码不能为空！'),
                    (val) {
                      RegExp pt = RegExp(r"^[a-zA-Z]\w{5,17}$");
                      if (!pt.hasMatch(val)) return "以字母开头,可以包含大小写字母、数字、下划线,长度6-18位";
                    },
                  ],
                  onEditingComplete: () {
                    _fbKey.currentState.save();
                    if (_fbKey.currentState.validate()) {
                      print(_fbKey.currentState.value);
                    }
                  },
                  maxLines: 1,
                  autofocus: true,
                  style: TextStyle(fontSize: 18, color: Colors.black38),
                  decoration: InputDecoration(
                    alignLabelWithHint: false,
                    hasFloatingPlaceholder: true,
                    hintText: '请输入新密码',
                    isDense: false,
                    focusColor: GlobalColorConfig.mainBackgroundColor.withOpacity(0.0),
                    hoverColor: Colors.white,
                    focusedBorder: InputBorder.none,
                    enabledBorder: InputBorder.none,
                  ),
                )),
                GestureDetector(
                    onTap: () {
                      obscureText = !obscureText;
                      setState(() {});
                    },
                    child: Container(
                      width: 60,
                      height: 60,
                      child: obscureText
                          ? Icon(
                              Icons.visibility_off,
                              color: Colors.black54,
                              size: 18,
                            )
                          : Icon(
                              Icons.visibility,
                              color: Colors.black54,
                              size: 18,
                            ),
                    ))
              ],
            ));
    }
  }

  @override
  Widget build(BuildContext context) {
    Widget widget;

    return Scaffold(
        appBar: AppBar(
          elevation: 0,
          backgroundColor: Colors.white,
          title: Text(titleCN, style: TextStyle(color: Colors.black87)),
          actions: <Widget>[
            Container(
              width: 56,
              height: 56,
              child: IconButton(
                icon: Icon(Icons.done, color: Color(0xFFA70400)),
                onPressed: () async {
                  _fbKey.currentState.save();
                  Map map = _fbKey.currentState.value;
                  userBloc.dispatch(UserUpdateEvent(map: map));
                  Navigator.pop(context);
                },
              ),
            )
          ],
        ),
        body: LayoutBuilder(
          builder: (context, constraint) {
            return Container(
              height: constraint.maxHeight,
              width: constraint.maxWidth,
              decoration: BoxDecoration(image: DecorationImage(image: AssetImage('assets/texture/bgt2.jpg'), fit: BoxFit.cover)),
              alignment: Alignment.topCenter,
              child: FormBuilder(key: _fbKey, child: Container(width: constraint.maxWidth, padding: EdgeInsets.symmetric(horizontal: 15, vertical: 15), child: getWidget())),
            );
          },
        ));
  }
}
