import 'dart:core';
import 'dart:math';

import 'package:bodhiroad/config/ReaderThemeConfig.dart';
import 'package:bodhiroad/reader/bean/book_catalog.dart';
import 'package:bodhiroad/reader/bean/book_char.dart';
import 'package:bodhiroad/reader/bean/book_mark.dart';
import 'package:bodhiroad/reader/bean/book_page.dart';
import 'package:bodhiroad/reader/bean/book_paragraph_unit.dart';
import 'package:bodhiroad/reader/bean/book_singlemark.dart';
import 'package:bodhiroad/reader/bean/read_config.dart';
import 'package:bodhiroad/reader/bean/text_layout_size.dart';
import 'package:bodhiroad/reader/buddhist_reader_bloc_state.dart';
import 'package:flutter/painting.dart';

class PageUtils {
  ///  文字垂直/水平翻页
  static ReloadReaderState textVerticalPage2(
    List<BookParagraphUnitBean> unitlist,
    ReloadReaderState book,
  ) {
    Map<String, TextLayoutSize> map = book.map;
    // 文字起始偏移量
    double xoffset = book.configuration.get('viewWidth');
    // 每一页段落集合
    List<CharBean> pageCharList = [];

    Map<String, int> maxLineMap = Map();
    maxLineMap['h1'] = (book.configuration.get('viewHeight') / map['h1'].heightVertical).floor();
    maxLineMap['h2'] = ((book.configuration.get('viewHeight') - map['h1'].heightVertical * 2) / map['h2'].heightVertical).floor();
    maxLineMap['h3'] = ((book.configuration.get('viewHeight') - map['h1'].heightVertical * 3) / map['h3'].heightVertical).floor();
    maxLineMap['h4'] = ((book.configuration.get('viewHeight') - map['h4'].heightVertical * 2) / map['h4'].heightVertical).floor();
    maxLineMap['p'] = (book.configuration.get('viewHeight') / map['p'].heightVertical).floor();
    maxLineMap['h5'] = (book.configuration.get('viewHeight') / map['h5'].heightVertical).floor();
    maxLineMap['font'] = (book.configuration.get('viewHeight') / map['font'].heightVertical).floor();
    Map<String, double> yoffsetMap = Map();
    yoffsetMap['h1'] = 0;
    yoffsetMap['h2'] = map['h1'].heightVertical * 2;
    yoffsetMap['h3'] = map['h1'].heightVertical * 3;
    yoffsetMap['h4'] = map['h4'].heightVertical * 2;
    yoffsetMap['p'] = map['p'].heightVertical * 2;
    yoffsetMap['h5'] = map['h5'].heightVertical * 2;
    yoffsetMap['font'] = map['font'].heightVertical * 2;
    yoffsetMap['p0'] = 0;

// 段落分页集合
    List<PageBean> pList = [];
    int chapterIndex = 0;
    List<BookmarkBean> bookmarkList = [];
    List<BookSingleMarkBean> bookmarkPageList = [];
    int marked = 0;
    int paraMarked = 0;

    CatalogBean catalog = CatalogBean(title: '', level: 1, subTitle: '');
    CatalogBean f = CatalogBean();
    CatalogBean l = CatalogBean();
    for (var k = 0; k < unitlist.length; k++) {
      /// 段落开始
      // print(i.toString() + '------------' + k.toString());
      BookParagraphUnitBean bookParagraphUnitBean = unitlist[k];
      String charStr = bookParagraphUnitBean.text;
      // 开始截取位置
      int startIndex = 0;
      // 结束截取位置
      int endIndex = 0;
      // 本段落列数
      int pColumnCount = 0;
      catalog = null;
      if (bookParagraphUnitBean.tag == 'h1' || bookParagraphUnitBean.tag == 'h2') {
        chapterIndex++;
        catalog = CatalogBean(
          title: charStr,
          subTitle: charStr,
          level: bookParagraphUnitBean.tag == 'h1' ? 1 : 2,
        );
      }

      /// 每一段文字有多少列
      pColumnCount = ((bookParagraphUnitBean.tag == 'p' ? (charStr.length + 2) : charStr.length) / maxLineMap[bookParagraphUnitBean.tag]).ceil();

      /// 截取出每一列保存
      for (int pColumnIndex = 0; pColumnIndex < pColumnCount; pColumnIndex++) {
        if (pColumnIndex == 0 && bookParagraphUnitBean.tag == 'p') {
          endIndex = charStr.length > maxLineMap[bookParagraphUnitBean.tag] - 2 ? maxLineMap[bookParagraphUnitBean.tag] - 2 : charStr.length;
        } else {
          endIndex = startIndex + maxLineMap[bookParagraphUnitBean.tag] > charStr.length ? charStr.length : startIndex + maxLineMap[bookParagraphUnitBean.tag];
        }

        /// 是否是最后一列
        bool lastLine = k == unitlist.length - 1 && pColumnIndex == pColumnCount - 1;

        /// 需要换页
        if (xoffset - map[bookParagraphUnitBean.tag].width * 2 < 0) {
          PageBean pageBean = PageBean(
            list: pageCharList,
            marked: marked,
            bookMarkList: bookmarkList,
            bookMarkPageList: bookmarkPageList,
            chapterIndex: chapterIndex,
            pageTitle: book.catalogList.last.title,
          );

          pList.insert(0, pageBean);
          xoffset = book.configuration.get('viewWidth');
          pageCharList = [];
          marked = 0;
          bookmarkPageList = [];
          if (1 == bookParagraphUnitBean.marked) {
            for (var t = 0; t < bookParagraphUnitBean.markIndexs.length; t++) {
              if (bookParagraphUnitBean.markIndexs[t] >= startIndex && bookParagraphUnitBean.markIndexs[t] < endIndex) {
                marked = 1;
                paraMarked = 1;
                bookmarkList.add(BookmarkBean(title: charStr.substring(bookParagraphUnitBean.markIndexs[t], endIndex), pageIndex: pList.length));
                bookmarkPageList.add(BookSingleMarkBean(nodeIndex: k, position: pageCharList.length, charIndex: bookParagraphUnitBean.markIndexs[t]));
              }
            }
          }
          pageCharList.add(CharBean(
              char: charStr.substring(startIndex, endIndex),
              xOffset: xoffset - map[bookParagraphUnitBean.tag].width * 1.5,
              yOffset: bookParagraphUnitBean.tag == 'p' && pColumnIndex > 0 ? 0 : yoffsetMap[bookParagraphUnitBean.tag],
              width: map[bookParagraphUnitBean.tag].width * 2,
              fontSize: map[bookParagraphUnitBean.tag].fontSize,
              nodeIndex: k,
              charIndex: startIndex,
              marked: paraMarked,
              paragraphHead: bookParagraphUnitBean.tag == 'p' && pColumnIndex == 0 ? 1 : 0,
              tag: bookParagraphUnitBean.tag,
              maxLine: maxLineMap[bookParagraphUnitBean.tag])); // 行数

          paraMarked = 0;
          startIndex += pageCharList.last.char.length;
          xoffset -= map[bookParagraphUnitBean.tag].width * 2;

          /// 不需要换页
        } else if (xoffset - map[bookParagraphUnitBean.tag].width * 2 > 0) {
          if (1 == bookParagraphUnitBean.marked) {
            for (var t = 0; t < bookParagraphUnitBean.markIndexs.length; t++) {
              if (bookParagraphUnitBean.markIndexs[t] >= startIndex && bookParagraphUnitBean.markIndexs[t] < endIndex) {
                marked = 1;
                paraMarked = 1;
                bookmarkList.add(BookmarkBean(title: charStr.substring(bookParagraphUnitBean.markIndexs[t], endIndex), pageIndex: pList.length));
                bookmarkPageList.add(BookSingleMarkBean(nodeIndex: k, position: pageCharList.length, charIndex: bookParagraphUnitBean.markIndexs[t]));
              }
            }
          }
          pageCharList.add(CharBean(
              char: charStr.substring(startIndex, endIndex),
              xOffset: xoffset - map[bookParagraphUnitBean.tag].width * 1.5,
              yOffset: bookParagraphUnitBean.tag == 'p' && pColumnIndex > 0 ? 0 : yoffsetMap[bookParagraphUnitBean.tag],
              width: map[bookParagraphUnitBean.tag].width * 2,
              fontSize: map[bookParagraphUnitBean.tag].fontSize,
              nodeIndex: k,
              charIndex: startIndex,
              marked: paraMarked,
              paragraphHead: bookParagraphUnitBean.tag == 'p' && pColumnIndex == 0 ? 1 : 0,
              tag: bookParagraphUnitBean.tag,
              maxLine: maxLineMap[bookParagraphUnitBean.tag]));
          paraMarked = 0;
          startIndex += pageCharList.last.char.length;

          xoffset -= map[bookParagraphUnitBean.tag].width * 2;
        } else if (xoffset - map[bookParagraphUnitBean.tag].width * 2 == 0) {
          if (1 == bookParagraphUnitBean.marked) {
            for (var t = 0; t < bookParagraphUnitBean.markIndexs.length; t++) {
              if (bookParagraphUnitBean.markIndexs[t] >= startIndex && bookParagraphUnitBean.markIndexs[t] < endIndex) {
                marked = 1;
                paraMarked = 1;
                bookmarkList.add(BookmarkBean(title: charStr.substring(bookParagraphUnitBean.markIndexs[t], endIndex), pageIndex: pList.length));
                bookmarkPageList.add(BookSingleMarkBean(nodeIndex: k, position: pageCharList.length, charIndex: bookParagraphUnitBean.markIndexs[t]));
              }
            }
          }
          pageCharList.add(CharBean(
              char: charStr.substring(startIndex, endIndex),
              xOffset: xoffset - map[bookParagraphUnitBean.tag].width * 1.5,
              yOffset: bookParagraphUnitBean.tag == 'p' && pColumnIndex > 0 ? 0 : yoffsetMap[bookParagraphUnitBean.tag],
              width: map[bookParagraphUnitBean.tag].width * 2,
              nodeIndex: k,
              charIndex: startIndex,
              marked: paraMarked,
              paragraphHead: bookParagraphUnitBean.tag == 'p' && pColumnIndex == 0 ? 1 : 0,
              fontSize: map[bookParagraphUnitBean.tag].fontSize,
              tag: bookParagraphUnitBean.tag,
              maxLine: maxLineMap[bookParagraphUnitBean.tag]));
          paraMarked = 0;
          startIndex += pageCharList.last.char.length;
          PageBean pageBean = PageBean(
            list: pageCharList,
            marked: marked,
            bookMarkList: bookmarkList,
            bookMarkPageList: bookmarkPageList,
            chapterIndex: chapterIndex,
            pageTitle: book.catalogList.last.title,
          );
          pList.insert(0, pageBean);
          xoffset = book.configuration.get('viewWidth');
          pageCharList = [];
          marked = 0;
          bookmarkPageList = [];
        }
//          添加标题
        if (catalog != null) {
          catalog.pageIndex = pList.length;
          book.catalogList.add(catalog);
        }
        if (lastLine) {
          PageBean pageBean = PageBean(
            list: pageCharList,
            marked: marked,
            bookMarkList: bookmarkList,
            bookMarkPageList: bookmarkPageList,
            chapterIndex: chapterIndex,
            pageTitle: book.catalogList.last.title,
          );
          pList.insert(0, pageBean);
          marked = 0;
          bookmarkPageList = [];
        }
      }
    }
    book.pageList = pList;
    List<PageBean> nList = [];
    List<CatalogBean> cList = [];

    CatalogBean first = null;
    CatalogBean last = null;
    for (int i = pList.length - 1; i >= 0; i--) {
      /// 从头开始分页
      PageBean pageBean = pList[i];

      /// 页面中所有列
      List<CharBean> columnCharList = pageBean.list;
      List<CharBean> nColumnCharList = [];
      columnCharList.forEach((columnChar) {
        nColumnCharList.insert(0, columnChar);

        /// 判断标题，加入目录列表
        if (columnChar.tag == 'h1' || columnChar.tag == 'h2') {
          CatalogBean temp = CatalogBean(tag: columnChar.tag, title: columnChar.char, pageIndex: pList.length - 1 - i);
          if (first == null) {
            first = temp;
          } else {
            last = temp;
          }
          cList.insert(0, temp);
        }
      });

      if (first == null) {
        pageBean.firstCatalog = cList.first;
        pageBean.lastCatalog = cList.first;
      } else if (last == null) {
        pageBean.firstCatalog = first;
        pageBean.lastCatalog = first;
      } else {
        pageBean.firstCatalog = first;
        pageBean.lastCatalog = last;
      }
      pageBean.list = nColumnCharList;
      first = null;
      last = null;
//      nList.add(pageBean);
    }

    book.bookMarkList = bookmarkList;
    return book;
  }

  ///  文字垂直/水平翻页
  static ReloadReaderState textVerticalPage(
    List<BookParagraphUnitBean> unitlist,
    ReloadReaderState book,
  ) {
    Map<String, TextLayoutSize> map = book.map;
    // 文字起始偏移量
    double xoffset = book.configuration.get('viewWidth');
    // 每一页段落集合
    List<CharBean> pageCharList = [];

    Map<String, int> maxLineMap = Map();
    maxLineMap['h1'] = (book.configuration.get('viewHeight') / map['h1'].heightVertical).floor();
    maxLineMap['h2'] = ((book.configuration.get('viewHeight') - map['h1'].heightVertical * 2) / map['h2'].heightVertical).floor();
    maxLineMap['h3'] = ((book.configuration.get('viewHeight') - map['h1'].heightVertical * 3) / map['h3'].heightVertical).floor();
    maxLineMap['h4'] = ((book.configuration.get('viewHeight') - map['h4'].heightVertical * 2) / map['h4'].heightVertical).floor();
    maxLineMap['p'] = (book.configuration.get('viewHeight') / map['p'].heightVertical).floor();
    maxLineMap['h5'] = (book.configuration.get('viewHeight') / map['h5'].heightVertical).floor();
    maxLineMap['font'] = (book.configuration.get('viewHeight') / map['font'].heightVertical).floor();
    Map<String, double> yoffsetMap = Map();
    yoffsetMap['h1'] = 0;
    yoffsetMap['h2'] = map['h1'].heightVertical * 2;
    yoffsetMap['h3'] = map['h1'].heightVertical * 3;
    yoffsetMap['h4'] = map['h4'].heightVertical * 2;
    yoffsetMap['p'] = map['p'].heightVertical * 2;
    yoffsetMap['h5'] = map['h5'].heightVertical * 2;
    yoffsetMap['font'] = map['font'].heightVertical * 2;
    yoffsetMap['p0'] = 0;

// 段落分页集合
    List<PageBean> pList = [];
    int chapterIndex = -1;
    List<BookmarkBean> bookmarkList = [];
    List<BookSingleMarkBean> bookmarkPageList = [];
    int marked = 0;
    int paraMarked = 0;

    CatalogBean catalog = CatalogBean(title: '', level: 1, subTitle: '');
    for (var k = 0; k < unitlist.length; k++) {
      // print(i.toString() + '------------' + k.toString());
      BookParagraphUnitBean bookParagraphUnitBean = unitlist[k];
      String charStr = bookParagraphUnitBean.text;
      // 开始截取位置
      int startIndex = 0;
      // 结束截取位置
      int endIndex = 0;
      // 本段落列数
      int pColumnCount = 0;
      catalog = null;
      if (bookParagraphUnitBean.tag == 'h1' || bookParagraphUnitBean.tag == 'h2') {
        chapterIndex++;
        catalog = CatalogBean(
          title: charStr,
          subTitle: charStr,
          level: bookParagraphUnitBean.tag == 'h1' ? 1 : 2,
        );
      }

      /// 每一段文字有多少列
      pColumnCount = ((bookParagraphUnitBean.tag == 'p' ? (charStr.length + 2) : charStr.length) / maxLineMap[bookParagraphUnitBean.tag]).ceil();

      /// 截取出每一列保存
      for (int pColumnIndex = 0; pColumnIndex < pColumnCount; pColumnIndex++) {
        if (pColumnIndex == 0 && bookParagraphUnitBean.tag == 'p') {
          endIndex = charStr.length > maxLineMap[bookParagraphUnitBean.tag] - 2 ? maxLineMap[bookParagraphUnitBean.tag] - 2 : charStr.length;
        } else {
          endIndex = startIndex + maxLineMap[bookParagraphUnitBean.tag] > charStr.length ? charStr.length : startIndex + maxLineMap[bookParagraphUnitBean.tag];
        }

        /// 是否是最后一列
        bool lastLine = k == unitlist.length - 1 && pColumnIndex == pColumnCount - 1;

        /// 需要换页
        if (xoffset - map[bookParagraphUnitBean.tag].width * 2 < 0) {
          PageBean pageBean = PageBean(
            list: pageCharList,
            marked: marked,
            bookMarkList: bookmarkList,
            bookMarkPageList: bookmarkPageList,
            chapterIndex: chapterIndex,
            pageTitle: book.catalogList.last.title,
          );

          pList.insert(0, pageBean);
          xoffset = book.configuration.get('viewWidth');
          pageCharList = [];
          marked = 0;
          bookmarkPageList = [];
          if (1 == bookParagraphUnitBean.marked) {
            for (var t = 0; t < bookParagraphUnitBean.markIndexs.length; t++) {
              if (bookParagraphUnitBean.markIndexs[t] >= startIndex && bookParagraphUnitBean.markIndexs[t] < endIndex) {
                marked = 1;
                paraMarked = 1;
                bookmarkList.add(BookmarkBean(title: charStr.substring(bookParagraphUnitBean.markIndexs[t], endIndex), pageIndex: pList.length));
                bookmarkPageList.add(BookSingleMarkBean(nodeIndex: k, position: pageCharList.length, charIndex: bookParagraphUnitBean.markIndexs[t]));
              }
            }
          }
          pageCharList.add(CharBean(
              char: charStr.substring(startIndex, endIndex),
              xOffset: xoffset - map[bookParagraphUnitBean.tag].width * 1.5,
              yOffset: bookParagraphUnitBean.tag == 'p' && pColumnIndex > 0 ? 0 : yoffsetMap[bookParagraphUnitBean.tag],
              width: map[bookParagraphUnitBean.tag].width * 2,
              fontSize: map[bookParagraphUnitBean.tag].fontSize,
              nodeIndex: k,
              charIndex: startIndex,
              marked: paraMarked,
              paragraphHead: bookParagraphUnitBean.tag == 'p' && pColumnIndex == 0 ? 1 : 0,
              tag: bookParagraphUnitBean.tag,
              maxLine: maxLineMap[bookParagraphUnitBean.tag])); // 行数

          paraMarked = 0;
          startIndex += pageCharList.last.char.length;
          xoffset -= map[bookParagraphUnitBean.tag].width * 2;

          /// 不需要换页
        } else if (xoffset - map[bookParagraphUnitBean.tag].width * 2 > 0) {
          if (1 == bookParagraphUnitBean.marked) {
            for (var t = 0; t < bookParagraphUnitBean.markIndexs.length; t++) {
              if (bookParagraphUnitBean.markIndexs[t] >= startIndex && bookParagraphUnitBean.markIndexs[t] < endIndex) {
                marked = 1;
                paraMarked = 1;
                bookmarkList.add(BookmarkBean(title: charStr.substring(bookParagraphUnitBean.markIndexs[t], endIndex), pageIndex: pList.length));
                bookmarkPageList.add(BookSingleMarkBean(nodeIndex: k, position: pageCharList.length, charIndex: bookParagraphUnitBean.markIndexs[t]));
              }
            }
          }
          pageCharList.add(CharBean(
              char: charStr.substring(startIndex, endIndex),
              xOffset: xoffset - map[bookParagraphUnitBean.tag].width * 1.5,
              yOffset: bookParagraphUnitBean.tag == 'p' && pColumnIndex > 0 ? 0 : yoffsetMap[bookParagraphUnitBean.tag],
              width: map[bookParagraphUnitBean.tag].width * 2,
              fontSize: map[bookParagraphUnitBean.tag].fontSize,
              nodeIndex: k,
              charIndex: startIndex,
              marked: paraMarked,
              paragraphHead: bookParagraphUnitBean.tag == 'p' && pColumnIndex == 0 ? 1 : 0,
              tag: bookParagraphUnitBean.tag,
              maxLine: maxLineMap[bookParagraphUnitBean.tag]));
          paraMarked = 0;
          startIndex += pageCharList.last.char.length;

          xoffset -= map[bookParagraphUnitBean.tag].width * 2;
        } else if (xoffset - map[bookParagraphUnitBean.tag].width * 2 == 0) {
          if (1 == bookParagraphUnitBean.marked) {
            for (var t = 0; t < bookParagraphUnitBean.markIndexs.length; t++) {
              if (bookParagraphUnitBean.markIndexs[t] >= startIndex && bookParagraphUnitBean.markIndexs[t] < endIndex) {
                marked = 1;
                paraMarked = 1;
                bookmarkList.add(BookmarkBean(title: charStr.substring(bookParagraphUnitBean.markIndexs[t], endIndex), pageIndex: pList.length));
                bookmarkPageList.add(BookSingleMarkBean(nodeIndex: k, position: pageCharList.length, charIndex: bookParagraphUnitBean.markIndexs[t]));
              }
            }
          }
          pageCharList.add(CharBean(
              char: charStr.substring(startIndex, endIndex),
              xOffset: xoffset - map[bookParagraphUnitBean.tag].width * 1.5,
              yOffset: bookParagraphUnitBean.tag == 'p' && pColumnIndex > 0 ? 0 : yoffsetMap[bookParagraphUnitBean.tag],
              width: map[bookParagraphUnitBean.tag].width * 2,
              nodeIndex: k,
              charIndex: startIndex,
              marked: paraMarked,
              paragraphHead: bookParagraphUnitBean.tag == 'p' && pColumnIndex == 0 ? 1 : 0,
              fontSize: map[bookParagraphUnitBean.tag].fontSize,
              tag: bookParagraphUnitBean.tag,
              maxLine: maxLineMap[bookParagraphUnitBean.tag]));
          paraMarked = 0;
          startIndex += pageCharList.last.char.length;
          PageBean pageBean = PageBean(
            list: pageCharList,
            marked: marked,
            bookMarkList: bookmarkList,
            bookMarkPageList: bookmarkPageList,
            chapterIndex: chapterIndex,
            pageTitle: book.catalogList.last.title,
          );
          pList.insert(0, pageBean);
          xoffset = book.configuration.get('viewWidth');
          pageCharList = [];
          marked = 0;
          bookmarkPageList = [];
        }
//          添加标题
        if (catalog != null) {
          catalog.pageIndex = pList.length;
          book.catalogList.add(catalog);
        }
        if (lastLine) {
          PageBean pageBean = PageBean(
            list: pageCharList,
            marked: marked,
            bookMarkList: bookmarkList,
            bookMarkPageList: bookmarkPageList,
            chapterIndex: chapterIndex,
            pageTitle: book.catalogList.last.title,
          );
          pList.insert(0, pageBean);
          marked = 0;
          bookmarkPageList = [];
        }
      }
    }
    book.pageList = pList;
    List<PageBean> nList = [];
    List<CatalogBean> cList = [];

    CatalogBean first = null;
    CatalogBean last = null;
    for (int i = 0; i < pList.length; i++) {
      /// 从头开始分页
      PageBean pageBean = pList[i];

      /// 页面中所有列
      List<CharBean> columnCharList = pageBean.list;
      List<CharBean> nColumnCharList = [];
      columnCharList.forEach((columnChar) {
        nColumnCharList.insert(0, columnChar);

        /// 判断标题，加入目录列表
        if (columnChar.tag == 'h1' || columnChar.tag == 'h2') {
          CatalogBean temp = CatalogBean(tag: columnChar.tag, title: columnChar.char, pageIndex: i);
//          if (first == null) {
//            first = temp;
//          } else {
//            last = temp;
//          }
          cList.insert(0, temp);
        }
      });

//      if (first == null) {
//        pageBean.firstCatalog = cList.first;
//        pageBean.lastCatalog = cList.first;
//      } else if (last == null) {
//        pageBean.firstCatalog = first;
//        pageBean.lastCatalog = first;
//      } else {
//        pageBean.firstCatalog = first;
//        pageBean.lastCatalog = last;
//      }
      pageBean.list = nColumnCharList;
//      first = null;
//      last = null;
      nList.add(pageBean);
    }
    book.pageList = nList;
    book.catalogList = cList;
    book.bookMarkList = bookmarkList;
    return book;
  }

  /// 文字水平/垂直滚动
  static ReloadReaderState textVerticalScrollPage(
    List<BookParagraphUnitBean> unitlist,
    ReadConfig readConfig,
    ReloadReaderState book,
  ) {
    Map<String, TextLayoutSize> map = book.map;
    // 文字起始偏移量
    double yoffset = 0;
    double yoffset2 = 0;
    // 每一页段落集合
    List<CharBean> pageCharList = [];

    // 段落分页集合
    List<PageBean> pList = [];
    List<BookmarkBean> bookmarkList = [];
    List<BookSingleMarkBean> bookmarkPageList = [];
    int marked = 0;
    int paraMarked = 0;
    // 段落标题
    CatalogBean catalog = CatalogBean(title: '', level: 1, subTitle: '');
    for (var k = 0; k < unitlist.length; k++) {
      // print(chapterIndex.toString() + '------------' + k.toString());
      BookParagraphUnitBean bookParagraphUnitBean = unitlist[k];
      String charStr = bookParagraphUnitBean.text;
      int maxCharCount = (book.configuration.get('viewWidth') / map[bookParagraphUnitBean.tag].width).floor();
      // 本段落行数
      int pLineCount = bookParagraphUnitBean.tag == 'p' ? ((charStr.length + 2) / maxCharCount).ceil() : (charStr.length / maxCharCount).ceil();
      // 开始截取位置
      int startIndex = 0;
      int endIndex = 0;
      catalog = null;
      if (bookParagraphUnitBean.tag == 'h1' || bookParagraphUnitBean.tag == 'h2') {
        catalog = CatalogBean(
          title: charStr,
          subTitle: charStr,
          level: bookParagraphUnitBean.tag == 'h1' ? 1 : 2,
        );
      }
      for (var pColumnIndex = 0; pColumnIndex < pLineCount; pColumnIndex++) {
        if (pColumnIndex == 0 && bookParagraphUnitBean.tag == 'p') {
          endIndex = charStr.length > maxCharCount - 2 ? maxCharCount - 2 : charStr.length;
        } else {
          endIndex = startIndex + maxCharCount > charStr.length ? charStr.length : startIndex + maxCharCount;
        }
        double xOffset = (book.configuration.get('viewWidth') - maxCharCount * map[bookParagraphUnitBean.tag].width) / 2;
        // 是否是最后一行
        bool lastLine = k == unitlist.length - 1 && pColumnIndex == pLineCount - 1;

        if (pageCharList.length > 32) {
          pList.add(PageBean(list: pageCharList, chapterIndex: 0, pageTitle: ''));
          pageCharList = [];
          yoffset = 0;
        }
        if (1 == bookParagraphUnitBean.marked) {
          for (var t = 0; t < bookParagraphUnitBean.markIndexs.length; t++) {
            if (bookParagraphUnitBean.markIndexs[t] >= startIndex && bookParagraphUnitBean.markIndexs[t] < endIndex) {
              marked = 1;
              paraMarked = 1;
              bookmarkList.add(BookmarkBean(title: charStr.substring(bookParagraphUnitBean.markIndexs[t], endIndex), position: yoffset2));
              bookmarkPageList.add(BookSingleMarkBean(nodeIndex: k, position: pageCharList.length, charIndex: bookParagraphUnitBean.markIndexs[t]));
            }
          }
        }
        pageCharList.add(CharBean(
            char: charStr.substring(startIndex, endIndex),
            xOffset: bookParagraphUnitBean.tag == 'p'
                ? pColumnIndex == 0 ? map[bookParagraphUnitBean.tag].width * 2 + xOffset : xOffset
                : (book.configuration.get('viewWidth') - (endIndex - startIndex) * map[bookParagraphUnitBean.tag].width) / 2,
            yOffset: yoffset,
            yOffset2: yoffset2,
            nodeIndex: k,
            marked: paraMarked,
            paragraphHead: pColumnIndex == 0 && bookParagraphUnitBean.tag == 'p' ? 1 : 0,
            charIndex: startIndex,
            fontSize: map[bookParagraphUnitBean.tag].fontSize,
            type: 3,
            tag: bookParagraphUnitBean.tag,
            height: map[bookParagraphUnitBean.tag].heightHorizontal,
            maxLine: 1));
        paraMarked = 0;
        yoffset += map[bookParagraphUnitBean.tag].heightHorizontal;
        yoffset2 += map[bookParagraphUnitBean.tag].heightHorizontal;
        if (catalog != null) {
          catalog.yOffset = yoffset2 - map[bookParagraphUnitBean.tag].heightHorizontal * 0.25;
          book.catalogList.add(catalog);
        }
        startIndex += pageCharList.last.char.length;
        if (lastLine) {
          pList.add(PageBean(list: pageCharList, chapterIndex: 0, pageTitle: ''));
          pageCharList = [];
        }
      }
    }
    book.bookMarkList = bookmarkList;
    book.pageList.addAll(pList);
    return book;
  }

  /// 文字水平/水平翻页
  static ReloadReaderState textHorizontalPage(
    List<BookParagraphUnitBean> unitlist,
    ReloadReaderState book,
  ) {
//    Map<String, TextLayoutSize> map = readConfig.textLayoutSize;
//    Map<String, TextLayoutSize> map = book.map;
    Map<String, TextLayoutSize> map = ReaderThemeConfig.textSizeMap;
    Map<String, TextLayoutSize> map3 = ReaderThemeConfig.initialFontSizeList()[0];
    // 文字起始偏移量
    double yoffset = 0;
    // 每一页段落集合
    List<CharBean> pageCharList = [];
    // 段落分页集合
    int chapterIndex = 0;
    List<PageBean> pList = [];
    List<BookmarkBean> bookmarkList = [];
    List<BookSingleMarkBean> bookmarkPageList = [];
    int marked = 0;
    int paraMarked = 0;
    if (unitlist == null) {
      unitlist = [];
    }
    // 段落标题
    CatalogBean catalog = CatalogBean(title: '', level: 1, subTitle: '');
    String prefix = ReaderThemeConfig.fontSizeList[book.configuration.getInt('fontSize')].toString();
    for (var k = 0; k < unitlist.length; k++) {
      BookParagraphUnitBean bookParagraphUnitBean = unitlist[k];
      String charStr = bookParagraphUnitBean.text;
      int maxCharCount = (book.configuration.get('viewWidth') / map[prefix + bookParagraphUnitBean.tag].width).floor();
      double xOffset = (book.configuration.get('viewWidth') - maxCharCount * map[prefix + bookParagraphUnitBean.tag].width) / 2;
      // 本段落行数
      int pLineCount = ((charStr.length + 2) / (bookParagraphUnitBean.tag == 'h3' ? maxCharCount : maxCharCount)).ceil();
      // 开始截取位置
      int startIndex = 0;
      int endIndex = 0;
      catalog = null;
      if (bookParagraphUnitBean.tag == 'h1' || bookParagraphUnitBean.tag == 'h2') {
        catalog = CatalogBean(
          title: charStr,
          subTitle: charStr,
          level: bookParagraphUnitBean.tag == 'h1' ? 1 : 2,
        );
        if (bookParagraphUnitBean.tag == 'h2') {
          chapterIndex++;
        }
      }
      for (var pColumnIndex = 0; pColumnIndex < pLineCount; pColumnIndex++) {
        if (pColumnIndex == 0 && bookParagraphUnitBean.tag == 'p') {
          endIndex = charStr.length > maxCharCount - 2 ? maxCharCount - 2 : charStr.length;
        } else {
          endIndex = startIndex + maxCharCount > charStr.length ? charStr.length : startIndex + maxCharCount;
        }
        // 是否是最后一行
        bool lastLine = k == unitlist.length - 1 && pColumnIndex == pLineCount - 1;
        if (book.configuration.get('viewHeight') < yoffset + map[prefix + bookParagraphUnitBean.tag].heightHorizontal) {
          pList.add(PageBean(
              list: pageCharList,
              marked: marked,
              bookMarkList: bookmarkList,
              bookMarkPageList: bookmarkPageList,
              chapterIndex: chapterIndex,
              pageTitle: book.catalogList.last.title,
              status: 1));
          yoffset = 0;
          marked = 0;
          bookmarkPageList = [];
          pageCharList = [];
          // **************************************
          if (1 == bookParagraphUnitBean.marked) {
            for (var t = 0; t < bookParagraphUnitBean.markIndexs.length; t++) {
              if (bookParagraphUnitBean.markIndexs[t] >= startIndex && bookParagraphUnitBean.markIndexs[t] < endIndex) {
                marked = 1;
                paraMarked = 1;
                bookmarkList.add(BookmarkBean(title: charStr.substring(bookParagraphUnitBean.markIndexs[t], endIndex), pageIndex: pList.length));
                bookmarkPageList.add(BookSingleMarkBean(nodeIndex: k, position: pageCharList.length, charIndex: bookParagraphUnitBean.markIndexs[t]));
              }
            }
          }
          double xof = 0;
          if (bookParagraphUnitBean.tag == 'p') {
            xof = pColumnIndex == 0 ? map[prefix + bookParagraphUnitBean.tag].width * 2 + xOffset : xOffset;
          } else if (bookParagraphUnitBean.tag == 'h5') {
            xof = book.configuration.get('viewWidth') - (endIndex - startIndex) * map[prefix + bookParagraphUnitBean.tag].width;
          } else {
            xof = (book.configuration.get('viewWidth') - (endIndex - startIndex) * map[prefix + bookParagraphUnitBean.tag].width) / 2;
          }
          pageCharList.add(CharBean(
              char: charStr.substring(startIndex, endIndex),
              xOffset: xof,
              yOffset: yoffset,
              height: map[prefix + bookParagraphUnitBean.tag].heightHorizontal,
              marked: paraMarked,
              paragraphHead: pColumnIndex == 0 && bookParagraphUnitBean.tag == 'p' ? 1 : 0,
              nodeIndex: k,
              charIndex: startIndex,
              fontSize: map[prefix + bookParagraphUnitBean.tag].fontSize,
              type: 3,
              tag: bookParagraphUnitBean.tag,
              maxLine: 1));
          paraMarked = 0;
          startIndex += pageCharList.last.char.length;
          yoffset += map[prefix + bookParagraphUnitBean.tag].heightHorizontal;
        } else if (book.configuration.get('viewHeight') > yoffset + map[prefix + bookParagraphUnitBean.tag].heightHorizontal) {
          // **************************************
          if (1 == bookParagraphUnitBean.marked) {
            for (var t = 0; t < bookParagraphUnitBean.markIndexs.length; t++) {
              if (bookParagraphUnitBean.markIndexs[t] >= startIndex && bookParagraphUnitBean.markIndexs[t] < endIndex) {
                marked = 1;
                paraMarked = 1;
                bookmarkList.add(BookmarkBean(title: charStr.substring(bookParagraphUnitBean.markIndexs[t], endIndex), pageIndex: pList.length));
                bookmarkPageList.add(BookSingleMarkBean(nodeIndex: k, position: pageCharList.length, charIndex: bookParagraphUnitBean.markIndexs[t]));
              }
            }
          }
          double xof = 0;
          if (bookParagraphUnitBean.tag == 'p') {
            xof = pColumnIndex == 0 ? map[prefix + bookParagraphUnitBean.tag].width * 2 + xOffset : xOffset;
          } else if (bookParagraphUnitBean.tag == 'h5') {
            xof = book.configuration.get('viewWidth') - (endIndex - startIndex) * map[prefix + bookParagraphUnitBean.tag].width;
          } else {
            xof = (book.configuration.get('viewWidth') - (endIndex - startIndex) * map[prefix + bookParagraphUnitBean.tag].width) / 2;
          }
          pageCharList.add(CharBean(
              char: charStr.substring(startIndex, endIndex),
              xOffset: xof,
              yOffset: yoffset,
              height: map[prefix + bookParagraphUnitBean.tag].heightHorizontal,
              paragraphHead: pColumnIndex == 0 && bookParagraphUnitBean.tag == 'p' ? 1 : 0,
              marked: paraMarked,
              nodeIndex: k,
              charIndex: startIndex,
              fontSize: map[prefix + bookParagraphUnitBean.tag].fontSize,
              type: 3,
              tag: bookParagraphUnitBean.tag,
              maxLine: 1));
          paraMarked = 0;
          startIndex += pageCharList.last.char.length;
          yoffset += map[prefix + bookParagraphUnitBean.tag].heightHorizontal;
        } else if (book.configuration.get('viewHeight') == yoffset + map[prefix + bookParagraphUnitBean.tag].heightHorizontal) {
          if (1 == bookParagraphUnitBean.marked) {
            for (var t = 0; t < bookParagraphUnitBean.markIndexs.length; t++) {
              if (bookParagraphUnitBean.markIndexs[t] >= startIndex && bookParagraphUnitBean.markIndexs[t] < endIndex) {
                marked = 1;
                paraMarked = 1;
                bookmarkList.add(BookmarkBean(title: charStr.substring(bookParagraphUnitBean.markIndexs[t], endIndex), pageIndex: pList.length));
                bookmarkPageList.add(BookSingleMarkBean(nodeIndex: k, position: pageCharList.length, charIndex: bookParagraphUnitBean.markIndexs[t]));
              }
            }
          }
          double xof = 0;
          if (bookParagraphUnitBean.tag == 'p') {
            xof = pColumnIndex == 0 ? map[prefix + bookParagraphUnitBean.tag].width * 2 + xOffset : xOffset;
          } else if (bookParagraphUnitBean.tag == 'h5') {
            xof = book.configuration.get('viewWidth') - (endIndex - startIndex) * map[prefix + bookParagraphUnitBean.tag].width;
          } else {
            xof = (book.configuration.get('viewWidth') - (endIndex - startIndex) * map[prefix + bookParagraphUnitBean.tag].width) / 2;
          }

          pageCharList.add(CharBean(
              char: charStr.substring(startIndex, endIndex),
              xOffset: xof,
              yOffset: yoffset,
              height: map[prefix + bookParagraphUnitBean.tag].heightHorizontal,
              paragraphHead: pColumnIndex == 0 && bookParagraphUnitBean.tag == 'p' ? 1 : 0,
              marked: paraMarked,
              nodeIndex: k,
              charIndex: startIndex,
              fontSize: map[prefix + bookParagraphUnitBean.tag].fontSize,
              type: 3,
              tag: bookParagraphUnitBean.tag,
              maxLine: 1));
          paraMarked = 0;
          startIndex += pageCharList.last.char.length;
          pList.add(PageBean(
              list: pageCharList,
              marked: marked,
              bookMarkList: bookmarkList,
              bookMarkPageList: bookmarkPageList,
              chapterIndex: chapterIndex,
              pageTitle: book.catalogList.last.title,
              status: 1));
          yoffset = 0;
          pageCharList = [];
          marked = 0;
          bookmarkPageList = [];
        }
        if (catalog != null) {
          catalog.pageIndex = pList.length;
          book.catalogList.add(catalog);
        }
        if (lastLine) {
          pList.add(PageBean(
              list: pageCharList,
              marked: marked,
              bookMarkList: bookmarkList,
              bookMarkPageList: bookmarkPageList,
              chapterIndex: chapterIndex,
              pageTitle: book.catalogList.last.title,
              status: 1));
          pageCharList = [];
          marked = 0;
          bookmarkPageList = [];
        }
      }
    }
    book.bookMarkList = bookmarkList;
    book.pageList = pList;
    return book;
  }

  /// 咒
  static ReloadReaderState zhou(
    List<BookParagraphUnitBean> unitlist,
    ReloadReaderState book,
  ) {
    Map<String, TextLayoutSize> map = book.map;
    String prefix = ReaderThemeConfig.fontSizeList[book.configuration.getInt('fontSize')].toString()+'-';
    // 文字起始偏移量
    double yoffset = 0;
    double xoffset = map[prefix+'p'].width * 2;
    // 每一页段落集合
    List<CharBean> pageCharList = [];
    // 段落分页集合
    int chapterIndex = -1;
    List<PageBean> pList = [];
    List<BookmarkBean> bookmarkList = [];
    List<BookSingleMarkBean> bookmarkPageList = [];
    int marked = 0;
    int paraMarked = 0;
    // 段落标题
    CatalogBean catalog = CatalogBean(title: '', level: 1, subTitle: '');
    for (var k = 0; k < unitlist.length; k++) {
      BookParagraphUnitBean paragraphUnitBean = unitlist[k];
      //
      List<CharBean> chars = paragraphUnitBean.chars;
      bool lastLine = false;

      if (paragraphUnitBean.tag != 'p') {
        lastLine = k == unitlist.length - 1;
        if (yoffset + map[prefix+paragraphUnitBean.tag].heightHorizontal > book.configuration.get('viewHeight')) {
          pList.add(PageBean(list: pageCharList, marked: 0, bookMarkList: [], bookMarkPageList: [], chapterIndex: chapterIndex, pageTitle: '', status: 1));
          xoffset = map[prefix+'p'].width * 2;
          yoffset = 0;
          pageCharList = [];
        }

        CharBean char = CharBean(
          xOffset: (book.configuration.get('viewWidth') - paragraphUnitBean.text.length * map[prefix+paragraphUnitBean.tag].width) / 2,
          yOffset: yoffset,
          width: paragraphUnitBean.chars.length * map[prefix+paragraphUnitBean.tag].width,
          char: paragraphUnitBean.text,
          height: map[prefix+paragraphUnitBean.tag].heightHorizontal,
          paragraphHead: 0,
          nodeIndex: k,
          fontSize: map[prefix+paragraphUnitBean.tag].fontSize,
          type: 3,
          tag: paragraphUnitBean.tag,
        );
        pageCharList.add(char);
        yoffset += map[prefix+paragraphUnitBean.tag].heightHorizontal;
        xoffset = map[prefix+'p'].width * 2;
      } else {
        if (yoffset + map[prefix+paragraphUnitBean.tag].heightHorizontal > book.configuration.get('viewHeight')) {
          pList.add(PageBean(list: pageCharList, marked: 0, bookMarkList: [], bookMarkPageList: [], chapterIndex: chapterIndex, pageTitle: '', status: 1));
          xoffset = 0;
          yoffset = 0;
          pageCharList = [];
        }

        for (var j = 0; j < chars.length; j++) {
          lastLine = k == unitlist.length - 1 && j == chars.length - 1;
          CharBean char = chars[j];
          double charWidth = max(char.pinyinWidth, map[prefix+char.tag].width);

          if (xoffset + charWidth > book.configuration.get('viewWidth')) {
            xoffset = 0;
            yoffset += map[prefix+char.tag].heightHorizontal * 1.2;
            if (yoffset + map[prefix+char.tag].heightHorizontal > book.configuration.get('viewHeight')) {
              pList.add(PageBean(list: pageCharList, marked: 0, bookMarkList: [], bookMarkPageList: [], chapterIndex: chapterIndex, pageTitle: '', status: 1));
              xoffset = 0;
              yoffset = 0;
              pageCharList = [];
            }
          }

          char.xOffset = xoffset + (charWidth - map[prefix+char.tag].width) / 2;
          char.yOffset = yoffset + map[prefix+char.tag].heightHorizontal * 0.4; // 汉字向下偏移0.4，留给拼音
          char.width = charWidth;
          char.height = map[prefix+char.tag].heightHorizontal;
          char.tag = char.tag;
          char.paragraphHead = 0;
          char.fontSize = map[prefix+char.tag].fontSize;
          char.type = 3;
          char.maxLine = 1;
          char.isPinyin = false;
//        print(char);
          pageCharList.add(char);
          pageCharList.add(CharBean(
              width: charWidth,
              char: char.pinyin == '·' ? '' : char.pinyin,
              xOffset: xoffset + (charWidth - char.pinyinWidth) / 2,
              yOffset: yoffset + map[prefix+char.tag].heightHorizontal * 0.1, //拼音向下偏移0.2
              height: map[prefix+char.tag].heightHorizontal * 0.6,
              isPinyin: true,
              paragraphHead: 0,
              nodeIndex: k,
              fontSize: map[prefix+char.tag].fontSize * 0.6,
              type: 3,
              tag: char.tag,
              maxLine: 1));
          xoffset += charWidth + charWidth * 0.2;
        }
        xoffset = map[prefix+paragraphUnitBean.tag].width * 2;
        yoffset += map[prefix+paragraphUnitBean.tag].heightHorizontal * 1.5;
      }
      if (lastLine) {
        pList.add(PageBean(
            list: pageCharList,
            // marked: marked,
            // bookMarkList: bookmarkList,
            // bookMarkPageList: bookmarkPageList,
            // chapterIndex: chapterIndex,
            pageTitle: '',
            status: 1));
        pageCharList = [];
        marked = 0;
        bookmarkPageList = [];
      }
    }
//    book.bookMarkList = bookmarkList;
    book.pageList = pList;
    return book;
  }

  getPinyin(String text) {
    // text.
    TextPainter textPainter1 = TextPainter(text: TextSpan(text: 'char', style: TextStyle(fontFamily: 'zwizz', fontSize: 16)), textDirection: TextDirection.ltr);
    textPainter1.layout();
    print(textPainter1.size);
  }
}
