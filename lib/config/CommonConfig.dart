import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

class CommonConfig {
  static const String DOMAIN_DEV = 'https://bodhiroad-file.cpolar.cn';
  static const String DOMAIN_PROD = 'https://www.bodhiroad.store';

  static const List<String> taskCatalogList = ['诵经', '念佛', '禅修', '持咒', '拜忏'];
  static const List<Map<String, dynamic>> userMenus = [
    {"id": "1", "key": "数据同步", "url": "", "icon": Icons.cached},
    {"id": "2", "key": "查询大辞典", "url": "", "icon": Icons.search},
    {"id": "3", "key": "浏览大辞典", "url": "", "icon": Icons.chrome_reader_mode},
    {"id": "4", "key": "切换主题", "url": "", "icon": Icons.palette},
    {"id": "5", "key": "清除缓存", "url": "", "icon": Icons.clear_all},
    {"id": "10", "key": "使用帮助", "url": "", "icon": MdiIcons.help},
    {"id": "6", "key": "分享菩提路", "url": "", "icon": Icons.share},
    {"id": "7", "key": "问题反馈", "url": "", "icon": Icons.sms},
    {"id": "8", "key": "关于菩提路", "url": "", "icon": Icons.child_care},
    {"id": "9", "key": "检查更新", "url": "", "icon": Icons.update},
  ];
  static const List<String> catalogList = [
    '诵经',
    '念佛',
    '打坐',
    '持咒',
    '拜忏',
  ];
  static const Map<int, String> taskCountUnit = {1: '部', 2: '遍', 3: '分', 4: '遍', 5: '次'};
  static const List<int> zenDurations = [3, 5, 15, 30, 45, 60, 90, 120, 150, 180];
  static const List<Map<String, String>> dictList = [
    {'name': '佛学大辞典', 'fileName': '01.json', 'author': '丁福保'},
    {'name': '佛光大辞典', 'fileName': '02.json', 'author': '佛光山宗委会'},
    {'name': '佛学常见词汇', 'fileName': '03.json', 'author': '陈义孝'},
    {'name': '中国佛教', 'fileName': '04.json', 'author': '中国佛教协会'}
  ];

  static const String BUDDHIST_CAN_CLEAR = 'canClear';
  static const String BUDDHIST_PARAGRAPH_PREFIX = 'canClear_unitList_';
  static const String BUDDHIST_HTML = 'canClear_book_html_';
  static const String BUDDHIST_READER_POSITION = 'reader_last_position_';
  static const String BUDDHIST_READER_INDEX = 'reader_last_index_';
  static const String BUDDHIST_CATALOG = 'canClear_reader_catalog_';
  static const String BUDDHIST_ARTICLE_LIST = 'canClear_reader_catalog_article_';
  static const String BUDDHIST_SYNC = 'reader_mark_sync_';
  static const String BRWH_CATALOG = 'canClear_BRWH_CAT_';
  static const String QLDZJ_CATALOG = 'canClear_QLDZJ_CAT_';
  static const String TASK = 'task_';

  static const List<String> shareImages = ['assets/share1.jpg', 'assets/share2.jpg', 'assets/share1.jpg'];
  static const List<Map<String, int>> fohaoNum = [
    {"count": 10, "type": 1},
    {"count": 100, "type": 1},
    {"count": 500, "type": 1},
    {"count": 800, "type": 1},
    {"count": 1000, "type": 1},
    {"count": 3000, "type": 1},
    {"count": 5000, "type": 1},
    {"count": 8000, "type": 1},
    {"count": 10000, "type": 1},
    {"count": 30000, "type": 1},
    {"count": 50000, "type": 1},
    {"count": 80000, "type": 1}
  ];
  static const List<Map> fohaoFollowed = [
    {"name": '木鱼', "duration": 3},
    {"name": '木鱼2', "duration": 3},
    {"name": '木鱼3', "duration": 3},
  ];
}
