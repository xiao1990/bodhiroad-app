// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'merits_statistic_count_unit.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

MeritsStatisticCountUnit _$MeritsStatisticCountUnitFromJson(
    Map<String, dynamic> json) {
  return MeritsStatisticCountUnit(
      meritsId: json['meritsId'] as int,
      title: json['title'] as String,
      catalogId: json['catalogId'] as int,
      catalogName: json['catalogName'] as String,
      count: json['count'] as int);
}

Map<String, dynamic> _$MeritsStatisticCountUnitToJson(
        MeritsStatisticCountUnit instance) =>
    <String, dynamic>{
      'meritsId': instance.meritsId,
      'title': instance.title,
      'catalogId': instance.catalogId,
      'catalogName': instance.catalogName,
      'count': instance.count
    };
