import 'package:json_annotation/json_annotation.dart';
part 'task_statistic_day.g.dart';

@JsonSerializable()
class TaskStatisticDays {
  int taskType;
  int totalDays;
  String catalogName;
  TaskStatisticDays({
    this.taskType,
    this.totalDays,
    this.catalogName,
  });
  factory TaskStatisticDays.fromJson(Map<String, dynamic> json) {
    return _$TaskStatisticDaysFromJson(json);
  }
  Map<String, dynamic> toJson() => _$TaskStatisticDaysToJson(this);
}
