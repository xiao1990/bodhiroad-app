import 'dart:convert';
import 'dart:io';
import 'package:archive/archive.dart';
import 'package:archive/archive_io.dart';
import 'package:flutter/services.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';

class DatabaseUtils {
  static const _VERSION = 1;

  static const _NAME = "bodhiroad.db";

  static Database _database;

  ///初始化
  static init() async {
    var databasesPath = await getDatabasesPath();

    String path = join(databasesPath, _NAME);

    _database = await openDatabase(path, version: _VERSION, onCreate: (Database db, int version) async {});
  }

  ///判断表是否存在
  static Future<bool> isTableExits(String tableName) async {
    await getCurrentDatabase();
    var res = await _database.rawQuery("select * from Sqlite_master where type = 'table' and name = '$tableName'");
    return res != null && res.length > 0;
  }

  ///获取当前数据库对象
  static Future<Database> getCurrentDatabase() async {
    if (_database == null) {
      await init();
    }
    return _database;
  }

  ///关闭
  static close() {
    _database?.close();
    _database = null;
  }

  static void initArticleTables() async {
    bool qldzjExist = await isTableExits('qldzj');
    bool brwhExist = await isTableExits('brwh');
    if (!qldzjExist) {
      String qldzj = '''
          CREATE TABLE qldzj 
          (
            id INTEGER, 
            title TEXT, 
            author TEXT,
            juanCount TEXT, 
            sectionIndex TEXT, 
            url TEXT, 
            catalogId TEXT, 
            catalogName TEXT
            )
          ''';
      await _database.execute(qldzj);
    }
    if (!brwhExist) {
      String brwh = '''
        CREATE TABLE brwh 
        (
          id INTEGER, 
          title TEXT, 
          author TEXT,
          url TEXT, 
          catalogId TEXT, 
          parentId INTEGER, 
          desc TEXT)
        ''';
      await _database.execute(brwh);
    }

    if (qldzjExist && brwhExist) {
      return;
    }
    ByteData bates = await rootBundle.load('assets/db/db.zip');
    InputStream inputStream = InputStream(bates);
    Archive archive = ZipDecoder().decodeBuffer(inputStream);
    Directory tempDir = await getTemporaryDirectory();
    String tempPath = tempDir.path;
    // Extract the contents of the Zip archive to disk.
    for (ArchiveFile file in archive) {
      Batch batch = _database.batch();
      String filename = file.name;
      if (file.isFile) {
        List<int> data = file.content;
        File(tempPath + 'db/' + filename)
          ..createSync(recursive: true)
          ..writeAsBytesSync(data);
        File file2 = File(tempPath + 'db/' + filename);
        file2.readAsStringSync();
        List list = json.decode(file2.readAsStringSync());
        list.forEach((item) {
          batch.insert(filename.substring(0, filename.indexOf('.')), item);
        });
        await batch.commit(noResult: true);
        print('插入' + filename + ' ' + list.length.toString() + ' 条数据');
      }
    }
  }
}
