// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'merits_statistic_count.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

MeritsStatisticCount _$MeritsStatisticCountFromJson(Map<String, dynamic> json) {
  return MeritsStatisticCount(
      meritsType: json['meritsType'] as int,
      list: (json['list'] as List)
          ?.map((e) => e == null
              ? null
              : MeritsStatisticCountUnit.fromJson(e as Map<String, dynamic>))
          ?.toList());
}

Map<String, dynamic> _$MeritsStatisticCountToJson(
        MeritsStatisticCount instance) =>
    <String, dynamic>{'meritsType': instance.meritsType, 'list': instance.list};
