import 'package:dio/dio.dart';
import 'package:flustars/flustars.dart';

class HttpUtils {
  /// global dio object
  static Dio dio;

  /// default options
//  static const String API_PREFIX = 'http://192.168.1.6:8080';
//  static const String API_PREFIX = 'http://10.10.174.152:8080';

  static const String API_PREFIX = 'http://www.bodhiroad.cn';
  static const int CONNECT_TIMEOUT = 10000;
  static const int RECEIVE_TIMEOUT = 120000;

  /// http request methods
  static const String GET = 'get';
  static const String POST = 'post';
  static const String PUT = 'put';
  static const String PATCH = 'patch';
  static const String DELETE = 'delete';

  /// Get Method
  static Future<Map> get(String url, {data}) async {
    Dio dio = createInstance();
    var result;
    try {
      print("【请求地址为】" + url);
      Response response = await dio.get(url, queryParameters: data);
      result = response.data;
      print('【响应数据】' + response.toString());
    } on DioError catch (e) {
      /// 打印请求失败相关信息
      print('请求出错：' + e.toString());
      print('请求出错地址：' + url);
//      ToastUtils.fail('暂未登录！');
    }
    return result;
  }

  static Future<Map> post(String url, {data}) async {
    Dio dio = createInstance();
    var result;
    try {
      Response response = await dio.post(url, data: data != null ? FormData.fromMap(data) : null);
      var token = response.headers['token'];
      if (token != null) {
//        GlobalConfiguration().loadFromMap({'token': token});
        SpUtil.putString('token', token[0]);
        HttpUtils.forceCreateInstance();
      }
      result = response.data;

      print('响应数据：' + response.toString());
    } on DioError catch (e) {
      /// 打印请求失败相关信息
      print('请求出错：' + e.toString());
      print('请求出错地址：' + url);
//      ToastUtils.fail('暂未登录！');
    }
    return result;
  }

  static Future<Map> put(String url, {data}) async {
    Dio dio = createInstance();
    var result;
    try {
      Response response = await dio.put(url, data: data != null ? FormData.fromMap(data) : null);
      result = response.data;
      print('响应数据：' + response.toString());
    } on DioError catch (e) {
      print('请求出错：' + e.toString());
      print('请求出错地址：' + url);
//      ToastUtils.fail('暂未登录！');
    }
    return result;
  }

  static Future<Map> delete(String url, {data}) async {
    Dio dio = createInstance();
    var result;
    try {
      Response response = await dio.delete(url, data: data != null ? FormData.fromMap(data) : null);
      result = response.data;
      print('响应数据：' + response.toString());
    } on DioError catch (e) {
      print('请求出错：' + e.toString());
      print('请求出错地址：' + url);
//      ToastUtils.fail('暂未登录！');
    }
    return result;
  }

  static Future<Response> download(String url, String savePath, {data, onReceiveProgress}) async {
    Dio dio = createInstance();
    Response response;
    try {
      response = await dio.download(url, savePath, data: data != null ? FormData.fromMap(data) : null, onReceiveProgress: onReceiveProgress);
    } on DioError catch (e) {
      print(e);
    }
    return response;
  }

//  getAsStream(String url) async {
//    Response<ResponseBody> rs = await Dio().get<ResponseBody>(
//      url,
//      options: Options(responseType: ResponseType.stream), //设置接收类型为stream
//    );
//    print(rs.data.stream); //响应流
//    return rs.data.stream;
//  }

  /// 创建 dio 实例对象
  static Dio createInstance() {
    if (dio == null) {
      /// 全局属性：请求前缀、连接超时时间、响应超时时间
      BaseOptions options = new BaseOptions(
        baseUrl: API_PREFIX,
        connectTimeout: CONNECT_TIMEOUT,
        receiveTimeout: RECEIVE_TIMEOUT,
        headers: {'Authorization': SpUtil.getString('token')},
      );
      dio = new Dio(options);
    }
    return dio;
  }

  static void forceCreateInstance() {
    /// 全局属性：请求前缀、连接超时时间、响应超时时间
    BaseOptions options = new BaseOptions(
      baseUrl: API_PREFIX,
      connectTimeout: CONNECT_TIMEOUT,
      receiveTimeout: RECEIVE_TIMEOUT,
      headers: {'Authorization': SpUtil.getString('token')},
    );

    dio = new Dio(options);
  }

  /// 清空 dio 对象
  static clear() {
    dio = null;
  }
}
