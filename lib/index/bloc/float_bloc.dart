import 'dart:async';
import 'package:bloc/bloc.dart';

class FloatBloc extends Bloc<double, double> {
  static final FloatBloc _floatBlocSingleton = new FloatBloc._internal();
  factory FloatBloc() {
    return _floatBlocSingleton;
  }
  FloatBloc._internal();

  double get initialState => 0.0;

  @override
  Stream<double> mapEventToState(
    double event,
  ) async* {
    try {
      yield event;
    } catch (_, stackTrace) {
      print('$_ $stackTrace');
      yield currentState;
    }
  }
}
