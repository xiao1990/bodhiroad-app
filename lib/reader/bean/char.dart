import 'package:json_annotation/json_annotation.dart';

part 'char.g.dart';

/// 书签类
/// 文本行
///  阅读器布局计算以行为单位

/// 文本字符类
@JsonSerializable()
class Char {
  /// 字符串
  String chars;

  /// 字符串
  String actualChars;

  /// 是否标记为书签
  bool isMarked;

  /// 标记类型 1 书签 2 划线
  int maskType;

  /// 汉语拼音
  String spell;

  /// 是否为生僻字
  bool rareText;

  /// 是否为拼音
  bool isSpell;
  int id;
  bool isTitle;
  bool bold;
  bool colored;
  Char(
      {this.id,
      this.chars,
      this.isMarked = false,
      this.maskType,
      this.isSpell,
      this.rareText,
      this.spell,
      this.actualChars,
      this.isTitle = false,
      this.bold = false,
      this.colored = false});
  factory Char.fromJson(Map<String, dynamic> json) {
    return _$CharFromJson(json);
  }
  Map<String, dynamic> toJson() => _$CharToJson(this);
}
