// 页面前景色
import 'package:bodhiroad/components/transformerPageview/transformer_page_view.dart';
import 'package:bodhiroad/reader/bean/reader_theme.dart';
import 'package:bodhiroad/reader/bean/text_layout_size.dart';
import 'package:bodhiroad/reader/pages/tranType.dart';
import 'package:flutter/material.dart';

List<int> frontColors = [
  0xFF252525,
  0xFF252525,
  0xFF616e77,
  0xFF252525,
  0xFF95938f,
];
// 页面背景色
List<int> backColors = [
  0xFFF9F8F3,
  0xFFF3EFE5,
  0xFF051c2c,
  0xFFB3DDBF,
  0xFF393335,
];
// 书签背景色
List<int> markBackColors = [
  0xFFDBDAD5,
  0xFFD5D1C7,
  0xFF233A4A,
  0xFFD1FBDD,
  0xFF575153,
];
// 菜单背景色
List<int> menuColors = [
  0xFFFBFAF5,
  0xFFF6F2E8,
  0xFF0F2636,
  0xFFBDE7C9,
  0xFF433D3F,
];
List<List<int>> colors = [
  [
    0xFF364771, // 0文字颜色
    0xFFFFFFFF, // 1背景颜色
    0xFF364771, // 2菜单文字颜色
    0xFFF2F2F2, // 3内部局部背景色
    0xFFFFFFFF, // 4图标背景色
    0xFFFFFFFF, // 5抽屉目录背景色
    0xFF364771, // 6抽屉文字颜色
    0xFF364771, // 7活动滑块颜色
    0xFFFFFFFF, // 8未激活滑块颜色
    0xFF4C6399, // 12按钮激活色
    0xFF8A5F1F, // 13TAB激活文字颜色
    0xFFB5BAC7, // 标记背景色
    0xFF4C6399, // 标题色
  ],
  [
    0xFF201A1C, // 文字颜色
    0xFFFEF8F0, // 背景颜色
    0xFF201A1C, // 菜单文字颜色
    0xFFf3eee8, // 内部局部背景色
    0xFFFEF8F0, // 图标背景色
    0xFFFEF8F0, // 抽屉目录背景色
    0xFF201A1C, // 抽屉文字颜色
    0xFFDEBA2A, // 活动滑块颜色
    0xFFFEF8F0, // 未激活滑块颜色
    0xFFDEBA2A, // 按钮激活色
    0xFF8A5F1F, // 13TAB激活文字颜色
    0xFFD1D3D4, // 标记背景色
    0xFF90A3AE, // 标题色
  ],
  [
    0xFF261811, // 文字颜色
    0xFFB0D1B2, // 背景颜色
    0xFF261811, // 菜单文字颜色
    0xFFA2C4A7, // 内部局部背景色
    0xFFB0D1B2, // 图标背景色
    0xFFB0D1B2, // 抽屉目录背景色
    0xFF261811, // 抽屉文字颜色
    0xFF773A34, // 活动滑块颜色
    0xFFB0D1B2, // 未激活滑块颜色
    0xFF773A34, // 按钮激活色
    0xFF8A5F1F, // 13TAB激活文字颜色
    0xFFD6D997, // 标记背景色
    0xFF773A34, // 标题色
  ],
  [
    0xFF73C2DF, // 文字颜色
    0xFF156F9B, // 背景颜色
    0xFF73C2DF, // 菜单文字颜色
    0xFF15668e, // 内部局部背景色
    0xFF156F9B, // 图标背景色
    0xFF156F9B, // 抽屉目录背景色
    0xFF73C2DF, // 抽屉文字颜色
    0xFF8AC9D3, // 活动滑块颜色
    0xFF156F9B, // 未激活滑块颜色
    0xFF8AC9D3, // 按钮激活色
    0xFF8A5F1F, // 13TAB激活文字颜色
    0xFF1D557C, // 标记背景色
    0xFF8AC9D3, // 标题色
  ],
  [
    0xFF78A086, // 文字颜色
    0xFFF2F0E3, // 背景颜色
    0xFF78A086, // 菜单文字颜色
    0xFFdddbcf, // 内部局部背景色
    0xFFF2F0E3, // 图标背景色
    0xFFF2F0E3, // 抽屉目录背景色
    0xFF78A086, // 抽屉文字颜色
    0xFF5F4232, // 活动滑块颜色
    0xFFF2F0E3, // 未激活滑块颜色
    0xFF5F4232, // 按钮激活色
    0xFF8A5F1F, // 13TAB激活文字颜色
    0xFFC9D1C0, // 标记背景色
    0xFFD4B745, // 标题色
  ],
  [
    0xFF4F7F71, // 文字颜色
    0xFFCAD7E0, // 背景颜色
    0xFF4F7F71, // 菜单文字颜色
    0xFFc2cdd4, // 内部局部背景色
    0xFFCAD7E0, // 图标背景色
    0xFFCAD7E0, // 抽屉目录背景色
    0xFF4F7F71, // 抽屉文字颜色
    0xFF5B493D, // 活动滑块颜色
    0xFFCAD7E0, // 未激活滑块颜色
    0xFF5B493D, // 按钮激活色
    0xFF8A5F1F, // 13TAB激活文字颜色
    0xFFC9D1C0, // 标记背景色
    0xFFC19F86, // 标题色
  ],
  [
    0xFFB8955E, // 文字颜色
    0xFF413F41, // 背景颜色
    0xFFB8955E, // 菜单文字颜色
    0xFF313031, // 内部局部背景色
    0xFF413F41, // 图标背景色
    0xFF051C2C, // 抽屉目录背景色
    0xFFB8955E, // 抽屉文字颜色
    0xFFB8955E, // 活动滑块颜色
    0xFF051C2C, // 未激活滑块颜色
    0xFFB8955E, // 按钮激活色
    0xFF8A5F1F, // 13TAB激活文字颜色
    0xFF4B625F, // 标记背景色
    0xFFBC7F67, // 标题色
  ],
  [
    0xFF84919D, // 文字颜色
    0xFFD7DFEA, // 背景颜色
    0xFF84919D, // 菜单文字颜色
    0xFFC7D4E4, // 内部局部背景色
    0xFFD7DFEA, // 图标背景色
    0xFFD7DFEA, // 抽屉目录背景色
    0xFF84919D, // 抽屉文字颜色
    0xFF84919D, // 活动滑块颜色
    0xFFD7DFEA, // 未激活滑块颜色
    0xFF637079, // 按钮激活色
    0xFF8A5F1F, // 13TAB激活文字颜色
    0xFFC7D4E4, // 标记背景色
    0xFF727E8A, // 标题色
  ],
];

enum TextLayOutDirection {
  HORIZONTAL,
  VERTICAL,
}
enum PagingDirection { HORIZONTAL, VERTICAL }
enum ScreenDirection { HORIZONTAL, VERTICAL }
enum FlipTypeEnum { HORIZONTAL_SLIDE, HORIZONTAL_STACK, VERTICAL }

class ReaderThemeConfig {
  static List<PageTransformer> transformers = [null, DeepthPageTransformer()];
  static List flipType = ['水平', '层叠', '垂直'];
  List<ReaderTheme> list = [];
  static List<double> fontSizeList = [14, 16, 18, 20, 22, 24, 28];
  static Map<String, TextLayoutSize> textSizeMap = Map();
  static Map<String, TextLayoutSize> verticalTextSizeMap = Map();
//  static List<>
  /// h1 一级标题
  /// h2 二级标题
  /// h3 作者
  /// h4 偈
  /// h5

  static Map<String, double> fontScaleList = {'h1': 1.2, 'h2': 1.1, 'h3': 0.8, 'h4': 1.0, 'p': 1.0, 'h5': 0.8, 'font': 1.1};
  static Map<String, double> fontHeightList = {'h1': 1.8, 'h2': 2.5, 'h3': 1.8, 'h4': 1.8, 'p': 1.8, 'h5': 1.8, 'font': 1.8};
  static ReaderThemeConfig readerThemeConfig;
  static ReaderThemeConfig get instance => _getInstance();
  factory ReaderThemeConfig() => _getInstance();
  static List<Map<String, TextLayoutSize>> textFontLayOutSizeList = [];
  ReaderThemeConfig._internal() {
    ReaderTheme readerTheme;
    textFontLayOutSizeList = initialFontSizeList();
    colors.forEach((item) {
      readerTheme = ReaderTheme();
      readerTheme.fontColor = Color(item[0]);
      readerTheme.backgroundColor = Color(item[1]);
      readerTheme.menuFontColor = Color(item[2]);
      readerTheme.innerBackgroundColor = Color(item[3]);
      readerTheme.iconBackgroundColor = Color(item[4]);
      readerTheme.catalogBackgroundColor = Color(item[5]);
      readerTheme.catalogFontColor = Color(item[6]);
      readerTheme.sliderActiveColor = Color(item[7]);
      readerTheme.sliderInactiveColor = Color(item[8]);
      readerTheme.buttonActiveColor = Color(item[9]);
      readerTheme.tabActiveColor = Color(item[10]);
      readerTheme.markColor = Color(item[11]);
      readerTheme.titleColor = Color(item[12]);
      list.add(readerTheme);
    });
  }
//  Map<String, TextLayoutSize> get textmap => textSizeMap;

  static ReaderThemeConfig _getInstance() {
    if (readerThemeConfig == null) {
      readerThemeConfig = ReaderThemeConfig._internal();
    }
    return readerThemeConfig;
  }

  static List<Map<String, TextLayoutSize>> initialFontSizeList() {
//    textSizeMap['h1'] =

    if (textFontLayOutSizeList.length > 0) {
      return textFontLayOutSizeList;
    }
    fontSizeList.forEach((fs) {
      Map<String, TextLayoutSize> map = Map();
      fontScaleList.forEach((key, value) {
        TextPainter textPainter = TextPainter(text: TextSpan(text: '中', style: TextStyle(fontFamily: 'songti', fontSize: fs * value, height: 1.0)), textDirection: TextDirection.ltr);
        textPainter.layout();
        TextLayoutSize textLayoutSize = TextLayoutSize(width: textPainter.size.width, heightVertical: textPainter.size.height, heightHorizontal: textPainter.size.height * 1.8, fontSize: fs * value);
        map[key] = textLayoutSize;
      });
      textFontLayOutSizeList.add(map);
    });
    return textFontLayOutSizeList;
//    SpUtil.putObjectList('fontSizeList', textLayoutSizeList);
  }
}
