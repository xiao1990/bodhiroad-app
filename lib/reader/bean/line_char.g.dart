// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'line_char.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

LineChar _$LineCharFromJson(Map<String, dynamic> json) {
  return LineChar(
      tag: json['tag'] as String,
      xOffset: (json['xOffset'] as num)?.toDouble(),
      yOffset: (json['yOffset'] as num)?.toDouble(),
      fontSize: (json['fontSize'] as num)?.toDouble(),
      lineGroup: (json['lineGroup'] as List)
          ?.map((e) =>
              e == null ? null : Char.fromJson(e as Map<String, dynamic>))
          ?.toList(),
      fullTextIndex: json['fullTextIndex'] as int);
}

Map<String, dynamic> _$LineCharToJson(LineChar instance) => <String, dynamic>{
      'tag': instance.tag,
      'lineGroup': instance.lineGroup,
      'xOffset': instance.xOffset,
      'fontSize': instance.fontSize,
      'yOffset': instance.yOffset,
      'fullTextIndex': instance.fullTextIndex
    };
