import 'package:json_annotation/json_annotation.dart';

part 'user.g.dart';

@JsonSerializable()
class User {
  int id;
  String name;
  String email;
  int stat;
  String headPortrait;
  String headPortraitUrl;
  String fahao;
  String gender;
  String birthday;
  String province;
  String city;
  String approach;
  String signature;
  String token;

  User(
      {this.id = 0,
      this.name = '未登录',
      this.email = '',
      this.stat = 0,
      this.approach = '未设置',
      this.birthday = '未设置',
      this.city = '未设置',
      this.fahao = '未设置',
      this.gender = '未设置',
      this.headPortrait = 'default.png',
      this.headPortraitUrl,
      this.token = '',
      this.province = '未设置',
      this.signature = '未设置'});

  factory User.fromJson(Map<String, dynamic> json) {
    return _$UserFromJson(json);
  }
  Map<String, dynamic> toJson() => _$UserToJson(this);
}
