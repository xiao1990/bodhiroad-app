// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'task_statistic_unit.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

TaskStatisticUnit _$TaskStatisticUnitFromJson(Map<String, dynamic> json) {
  return TaskStatisticUnit(
      taskId: json['taskId'] as int,
      taskName: json['taskName'] as String,
      taskCount: json['taskCount'] as int,
      taskDuration: json['taskDuration'] as int,
      taskType: json['taskType'] as int);
}

Map<String, dynamic> _$TaskStatisticUnitToJson(TaskStatisticUnit instance) =>
    <String, dynamic>{
      'taskId': instance.taskId,
      'taskName': instance.taskName,
      'taskCount': instance.taskCount,
      'taskDuration': instance.taskDuration,
      'taskType': instance.taskType
    };
