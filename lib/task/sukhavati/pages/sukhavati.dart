import 'dart:ui';

import 'package:bodhiroad/components/bodhiroad_dialog_confirm.dart';
import 'package:bodhiroad/components/flutter_xlider.dart';
import 'package:bodhiroad/config/CommonConfig.dart';
import 'package:bodhiroad/config/GlobalColorCOnfig.dart';
import 'package:bodhiroad/task/read/bean/task_nianfo.dart';
import 'package:bodhiroad/task/sukhavati/bloc/sukhavati_set_bloc.dart';
import 'package:bodhiroad/task/sukhavati/bloc/sukhavati_set_event.dart';
import 'package:bodhiroad/task/sukhavati/bloc/sukhavati_set_state.dart';
import 'package:bodhiroad/utils/GlobalConfigUtil.dart';
import 'package:bodhiroad/utils/ToastUtils.dart';
import 'package:flip_card/flip_card.dart';
import 'package:flustars/flustars.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:global_configuration/global_configuration.dart';

class Sukhavati extends StatefulWidget {
  final int taskId;
  final String taskName;
  final int count;
  Sukhavati({this.count, this.taskId, this.taskName});
  @override
  _SukhavatiState createState() => _SukhavatiState();
}

class _SukhavatiState extends State<Sukhavati> with TickerProviderStateMixin {
  final GlobalKey<FormBuilderState> _fbKey = GlobalKey<FormBuilderState>();
  SukhavatiSetBloc _sukhavatiSetBloc = SukhavatiSetBloc();
  TextEditingController countEditingController;
  int totalCount = 0;
  List<TaskNianfo> goals = [];
  TaskNianfo currentTask;
  double currentFrequency = 3.0;
  int reduceCount = 10;
  double tempFrequency = 3.0;
  @override
  void initState() {
    goals = SpUtil.getObjList('nianfo_goals_' + widget.taskId.toString(), (item) => TaskNianfo.fromJson(item)) ?? [];
    if (goals.length > 0) {
      List<TaskNianfo> where = goals.where((value) => value.current ?? false).toList();
      if (where.length > 0) {
        currentTask = where.first;
      } else {
        currentTask = TaskNianfo(taskId: widget.taskId, taskName: widget.taskName, count: 10, current: true, frequency: SpUtil.getDouble('frequency_' + widget.taskId.toString()), followedId: 0);
      }
      currentFrequency = currentTask.frequency;
      tempFrequency = currentFrequency;
      reduceCount = currentTask.count;
    }
    countEditingController = TextEditingController(text: currentTask == null ? '10' : currentTask.count.toString());
    if (SpUtil.getDouble('frequency_' + widget.taskId.toString()) == 0) {
      SpUtil.putDouble('frequency_' + widget.taskId.toString(), 3);
    }
    SystemChrome.setEnabledSystemUIOverlays([]);
    GlobalConfiguration().updateValue('id', widget.taskId);
    super.initState();
  }

  // TextEditingController _controller = TextEditingController();
  int index = 1;
  void countEditComplete(String count) {}
  void frequencyEditComplete(String frequenc) {}
  void volumeEditComplete(String volume) {}
  int playingState = 0;
  int previewState = 0;
  String _myActivity = '';

  @override
  void dispose() {
    SystemChrome.setEnabledSystemUIOverlays([SystemUiOverlay.top]);
    super.dispose();
  }

  // 隐藏键盘
//  Container(
//  padding: EdgeInsets.symmetric(vertical: ScreenUtil.getInstance().getWidth(150)),
//  child: Image.asset(
//  'assets/pusa/${widget.taskId}.png',
//  fit: BoxFit.contain,
//  ),
//  )
//  FocusScope.of(context).requestFocus(FocusNode());
  bool goalsExists(int goal) {
    bool flag = false;
    for (int i = 0; i < goals.length; i++) {
      if (goal == goals[i].count && SpUtil.getDouble('frequency_' + widget.taskId.toString()) == goals[i].frequency) {
        currentTask = goals[i];
        flag = true;
        currentTask.current = true;
        SpUtil.putObjectList('nianfo_goals_' + widget.taskId.toString(), goals);
      } else {
        currentTask.current = false;
      }
    }
    if (flag) {
      return true;
    } else {
      TaskNianfo taskNianfo =
          TaskNianfo(taskId: widget.taskId, taskName: widget.taskName, count: goal, current: true, frequency: SpUtil.getDouble('frequency_' + widget.taskId.toString()), followedId: 0);
      currentTask = taskNianfo;
      if (goals.length == 0) {
        goals = [];
      }
      goals.add(taskNianfo);
      SpUtil.putObjectList('nianfo_goals_' + widget.taskId.toString(), goals);
      return false;
    }
  }

  String getTimeStr(int time) {
    int hour = (time / 3600).floor();
    int min = ((time % 3600) / 60).floor();
    int sec = time % 60;
    String str = '';
    if (hour > 0) {
      str += (hour.toString() + '小时');
    }
    if (min > 0) {
      str += (min.toString() + '分');
    }
    if (sec > 0) {
      str += (sec.toString() + '秒');
    }
    return str;
  }

  int fohaoFollowedIndex = 0;
  show() {
    showModalBottomSheet(
        backgroundColor: Colors.transparent,
        context: context,
        builder: (BuildContext context) {
          return StatefulBuilder(builder: (context, state) {
            return Container(
              height: 300,
              decoration: BoxDecoration(color: Colors.white, borderRadius: BorderRadius.vertical(top: Radius.circular(15))),
              child: ListView.builder(
                itemCount: CommonConfig.fohaoFollowed.length,
                itemExtent: ScreenUtil.getInstance().getWidth(120),
                itemBuilder: (context, index) {
                  return Material(
                      color: Colors.transparent,
                      borderRadius: BorderRadius.circular(15),
                      child: Ink(
                        child: InkWell(
                          onTap: () {
                            state(() {
                              fohaoFollowedIndex = index;
                              SpUtil.putInt('fohaoFollowed', index);
                            });
                            setState(() {});
                            Navigator.pop(context);
                          },
                          child: Container(
                            decoration: BoxDecoration(border: Border(bottom: BorderSide(color: Color(0xFFF2F2F2), width: 0.5))),
                            padding: EdgeInsets.symmetric(horizontal: 15),
                            alignment: Alignment.centerLeft,
                            child: Row(
                              children: <Widget>[
                                Expanded(
                                  child: Text(
                                    CommonConfig.fohaoFollowed[index]['name'],
                                    style: TextStyle(fontSize: ScreenUtil.getInstance().getSp(36)),
                                  ),
                                ),
                                Offstage(
                                  offstage: index != fohaoFollowedIndex,
                                  child: Container(
                                    width: ScreenUtil.getInstance().getWidth(120),
                                    child: Icon(
                                      Icons.done,
                                      size: ScreenUtil.getInstance().getSp(48),
                                      color: Color(0xFFA70400),
                                    ),
                                  ),
                                )
                              ],
                            ),
                          ),
                        ),
                      ));
                },
              ),
            );
          });
        });
  }

  showMyGoals() {
    showModalBottomSheet(
        backgroundColor: Colors.transparent,
        context: context,
        builder: (BuildContext context) {
          return StatefulBuilder(builder: (context, state) {
            return Container(
              height: 300,
              decoration: BoxDecoration(color: Colors.white, borderRadius: BorderRadius.vertical(top: Radius.circular(15))),
              child: ListView.builder(
                itemCount: goals.length,
                itemExtent: ScreenUtil.getInstance().getWidth(120),
                itemBuilder: (context, index) {
                  return Material(
                      color: Colors.transparent,
                      borderRadius: BorderRadius.circular(15),
                      child: Ink(
                        child: InkWell(
                          onTap: () {
                            state(() {
                              currentTask = goals[index];
                              countEditingController.value = countEditingController.value.copyWith(
                                text: currentTask.count.toString(),
                              );
                            });
                            setState(() {});
                            Navigator.pop(context);
                          },
                          child: Container(
                            decoration: BoxDecoration(border: Border(bottom: BorderSide(color: Color(0xFFF2F2F2), width: 0.5))),
                            padding: EdgeInsets.symmetric(horizontal: 15),
                            alignment: Alignment.centerLeft,
                            child: Row(
                              children: <Widget>[
                                Expanded(
                                  child: Text(
                                    '目标：' + goals[index].count.toString() + '/' + goals[index].frequency.toString(),
                                    style: TextStyle(fontSize: ScreenUtil.getInstance().getSp(36)),
                                  ),
                                ),
                                Offstage(
                                  offstage: goals[index].count != currentTask.count || goals[index].frequency != currentTask.frequency,
                                  child: Container(
                                    width: ScreenUtil.getInstance().getWidth(120),
                                    child: Icon(
                                      Icons.done,
                                      size: ScreenUtil.getInstance().getSp(48),
                                      color: Color(0xFFA70400),
                                    ),
                                  ),
                                )
                              ],
                            ),
                          ),
                        ),
                      ));
                },
              ),
            );
          });
        });
  }

  confirmBack(BuildContext context, String description, String title, bool justClose) {
    showDialog(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context) {
          return StatefulBuilder(
            builder: (BuildContext context, setState) {
              return BodhiRoadConfirmDialog(
                title: title,
                confirmText: '退出功课',
                cancelText: justClose ? '关 闭' : '取消',
                justClose: justClose,
                description: Text(description, textScaleFactor: 1.0, style: TextStyle(fontSize: 14, color: GlobalColorConfig.mainTextColor)),
                onConfirm: () {
                  _sukhavatiSetBloc.dispatch(SukhavatiStopEvent(taskId: widget.taskId));
                  Navigator.pop(context);
                },
              );
            },
          );
        });
  }

  GlobalKey<FlipCardState> cardKey = GlobalKey<FlipCardState>();
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: () async {
          if (playingState == 1) {
            confirmBack(context, '功课进行中，如果返回，功课将终止！\n是否确认终止？', '退出确认', false);
            return false;
          } else {
            return true;
          }
        },
        child: BlocBuilder(
            bloc: _sukhavatiSetBloc,
            condition: (prevState, curtState) {
              if (curtState is SukhavatiUnderwayState) {
                reduceCount = curtState.count;
                playingState = 1;
                return true;
              } else if (curtState is SukhavatiPauseState) {
                ToastUtils.success('已暂停');
                playingState = 2;
                return true;
              } else if (curtState is SukhavatiFinishState) {
                playingState = 3;
                ToastUtils.success('本次功课已完成！');
                return true;
              } else if (curtState is SukhavatiFrequencyState) {
                tempFrequency = curtState.frequency;
                return true;
              } else if (curtState is SukhavatiFrequencyPreviewState) {
                previewState = 1;
                return true;
              } else if (curtState is SukhavatiFrequencyPreviewFinishState) {
                previewState = 2;
                return true;
              } else if (curtState is SukhavatiFrequencyPreviewStopState) {
                previewState = 0;
                return true;
              }
              return false;
            },
            builder: (context, state) {
              return Scaffold(
                  appBar: AppBar(
                    elevation: 0,
                    backgroundColor: Colors.white,
                    centerTitle: true,
                    title: Text(widget.taskName, style: TextStyle(fontSize: GlobalConfigUtil.getTitleFontSize())),
                    actions: <Widget>[
                      IconButton(
                        icon: Icon(
                          Icons.settings,
                          size: ScreenUtil.getInstance().getSp(48),
                          color: Color(0xFFA70400),
                        ),
                        onPressed: () {
                          if (playingState == 1) {
                            ToastUtils.fail('请先停止功课!');
                            return;
                          }
                          cardKey.currentState.toggleCard();
                        },
                      )
                    ],
                  ),
                  bottomNavigationBar: BottomAppBar(
                    //底部工具栏
                    color: Colors.white,
                    elevation: 15,
                    shape: CircularNotchedRectangle(), //圆形缺口
                    child: Row(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: <Widget>[
                        Container(
                          height: ScreenUtil.getInstance().getSp(140),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Icon(
                                Icons.home,
                                color: Color(0xFFA70400),
                                size: ScreenUtil.getInstance().getSp(60),
                              ),
                              Text('念佛', style: TextStyle(color: Color(0xFFA70400), fontSize: ScreenUtil.getInstance().getSp(30)))
                            ],
                          ),
                        ),
                        Container(
                          height: ScreenUtil.getInstance().getSp(140),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Icon(
                                Icons.hearing,
                                size: ScreenUtil.getInstance().getSp(60),
                              ),
                              Text(
                                '听佛',
                                style: TextStyle(fontSize: ScreenUtil.getInstance().getSp(30)),
                              )
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                  floatingActionButton: FloatingActionButton(
                    backgroundColor: Color(0xFFA70400),
                    child: Icon(playingState == 1 ? Icons.pause : playingState == 3 ? Icons.refresh : Icons.play_arrow),
                    onPressed: () {
                      if (!cardKey.currentState.isFront) {
                        ToastUtils.fail('请先完成设置!');
                        return;
                      }
                      if (currentTask == null) {
                        ToastUtils.fail('还没有设置目标！');
                        return;
                      }

                      if (playingState == 1) {
                        _sukhavatiSetBloc.dispatch(SukhavatiPauseEvent());
                      } else if (playingState == 2) {
                        ToastUtils.success('开始念佛！');
                        _sukhavatiSetBloc.dispatch(SukhavatiContinueEvent(interval: (currentTask.frequency * 100).toInt(), count: reduceCount, taskId: widget.taskId));
                      } else if (playingState == 3 || playingState == 0) {
                        ToastUtils.success('开始念佛！');
                        _sukhavatiSetBloc.dispatch(SukhavatiStartEvent(interval: (currentTask.frequency * 100).toInt(), count: currentTask.count, taskId: widget.taskId));
                      }
                    },
                    isExtended: true,
                  ),
                  resizeToAvoidBottomPadding: false,
                  floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
                  body: Container(
                      decoration: BoxDecoration(image: DecorationImage(image: AssetImage('assets/texture/bgt2.jpg'), fit: BoxFit.cover)),
                      child: LayoutBuilder(
                        builder: (context, constraints) {
                          return Column(
                            children: <Widget>[
                              Expanded(
                                  child: Container(
                                      alignment: Alignment.center,
                                      child: FlipCard(
                                        key: cardKey,
                                        flipOnTouch: false,
                                        direction: FlipDirection.HORIZONTAL, // default
                                        front: Container(
                                          width: ScreenUtil.getInstance().screenWidth * 0.85,
                                          height: (constraints.maxHeight - ScreenUtil.getInstance().getWidth(150)) * 0.9,
                                          decoration: BoxDecoration(color: Colors.white, borderRadius: BorderRadius.circular(12), boxShadow: [BoxShadow(color: Colors.black12, blurRadius: 15)]),
                                          child: Column(
                                            children: <Widget>[
                                              Expanded(
                                                child: Stack(
                                                  children: <Widget>[
                                                    GestureDetector(
                                                      onTap: () {
                                                        cardKey.currentState.toggleCard();
                                                      },
                                                      child: Container(
                                                        width: ScreenUtil.getInstance().screenWidth * 0.85,
                                                        padding: EdgeInsets.symmetric(vertical: 10),
                                                        child: Hero(
                                                            tag: '2_' + widget.taskId.toString(),
                                                            flightShuttleBuilder: (flightContext, animation, direction, fromContext, toContext) {
                                                              return ScaleTransition(
                                                                scale: animation.drive(Tween(begin: 1.0, end: 1.0)),
                                                                child: Image.asset(
                                                                  'assets/pusa/${widget.taskId}.png',
                                                                  fit: BoxFit.contain,
                                                                ),
                                                              );
                                                            },
                                                            child: Image.asset(
                                                              'assets/pusa/${widget.taskId}.png',
                                                              fit: BoxFit.contain,
                                                            )),
                                                      ),
                                                    )
                                                  ],
                                                ),
                                              ),
                                              Container(
                                                height: ScreenUtil.getInstance().getWidth(180),
                                                child: Row(
                                                  crossAxisAlignment: CrossAxisAlignment.center,
                                                  children: <Widget>[
                                                    Expanded(
                                                      child: Container(
                                                        child: Column(
                                                          mainAxisAlignment: MainAxisAlignment.center,
                                                          children: <Widget>[
                                                            Text('目标'),
                                                            Text(
                                                              currentTask == null ? '0' : currentTask.count.toString(),
                                                              style: TextStyle(fontSize: ScreenUtil.getInstance().getSp(60), color: Color(0xFFA70400)),
                                                            ),
                                                          ],
                                                        ),
                                                      ),
                                                    ),
                                                    Expanded(
                                                      child: Container(
                                                        child: Column(
                                                          mainAxisAlignment: MainAxisAlignment.center,
                                                          children: <Widget>[
                                                            Text('当前'),
                                                            Text(
                                                              currentTask == null ? '0' : (currentTask.count - reduceCount).toString(),
                                                              style: TextStyle(fontSize: ScreenUtil.getInstance().getSp(60), color: Colors.green),
                                                            ),
                                                          ],
                                                        ),
                                                      ),
                                                    ),
                                                    Expanded(
                                                      child: Container(
                                                        child: Column(
                                                          mainAxisAlignment: MainAxisAlignment.center,
                                                          children: <Widget>[
                                                            Text('剩余'),
                                                            Text(
                                                              currentTask == null ? '0' : reduceCount.toString(),
                                                              style: TextStyle(fontSize: ScreenUtil.getInstance().getSp(60), color: Colors.orange),
                                                            ),
                                                          ],
                                                        ),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                        back: Container(
                                          width: ScreenUtil.getInstance().screenWidth * 0.85,
                                          padding: EdgeInsets.symmetric(horizontal: ScreenUtil.getInstance().getWidth(40)),
                                          height: (constraints.maxHeight - ScreenUtil.getInstance().getWidth(150)) * 0.9,
                                          decoration: BoxDecoration(
                                              color: Colors.white,
                                              image: DecorationImage(image: AssetImage('assets/texture/ss10.png'), alignment: Alignment.bottomCenter),
                                              borderRadius: BorderRadius.circular(12),
                                              boxShadow: [BoxShadow(color: Colors.black12, blurRadius: 15)]),
                                          child: Column(
                                            children: <Widget>[
                                              Container(
                                                height: ScreenUtil.getInstance().getWidth(120),
                                                decoration: BoxDecoration(border: Border(bottom: BorderSide(color: Color(0xFFF2F2F2), width: 0.5))),
                                                child: Row(
                                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                  children: <Widget>[
                                                    IconButton(
                                                        onPressed: () {
                                                          cardKey.currentState.toggleCard();
                                                        },
                                                        icon: Icon(
                                                          Icons.close,
                                                          size: ScreenUtil.getInstance().getWidth(48),
                                                        )),
                                                    Text(
                                                      '目标设置',
                                                      style: TextStyle(fontSize: GlobalConfigUtil.getSubTitleFontSize(), fontWeight: FontWeight.bold),
                                                    ),
                                                    IconButton(
                                                        onPressed: () {
                                                          _fbKey.currentState.save();
                                                          bool valid = _fbKey.currentState.validate();
                                                          if (valid) {
                                                            var val = int.parse(_fbKey.currentState.value['count']);
                                                            print(val);
                                                            bool exist = goalsExists(val);
                                                            if (!exist) {
                                                              _sukhavatiSetBloc.dispatch(SukhavatiCountEvent(count: val));
                                                            }
                                                            reduceCount = val;
                                                            cardKey.currentState.toggleCard();
                                                            setState(() {});
                                                          }
                                                        },
                                                        icon: Icon(
                                                          Icons.done,
                                                          size: ScreenUtil.getInstance().getWidth(48),
                                                          color: Color(0xFFA70400),
                                                        ))
                                                  ],
                                                ),
                                              ),
                                              FormBuilder(
                                                  key: _fbKey,
                                                  child: Expanded(
                                                      child: ListView(
                                                    physics: BouncingScrollPhysics(),
                                                    children: <Widget>[
                                                      Container(
                                                        height: ScreenUtil.getInstance().getWidth(120),
                                                        alignment: Alignment.centerLeft,
                                                        child: Text(
                                                          '自定义数量',
                                                          style: TextStyle(fontSize: GlobalConfigUtil.getTabMainBodyFontSize(), color: Colors.black54),
                                                        ),
                                                      ),
                                                      ClipRRect(
                                                          borderRadius: BorderRadius.circular(10),
                                                          child: Container(
                                                              height: ScreenUtil.getInstance().getWidth(120),
                                                              decoration: BoxDecoration(color: Color(0xFFF2F2F2).withOpacity(0.5)),
                                                              child: Row(
                                                                children: <Widget>[
                                                                  Expanded(
                                                                      child: FormBuilderTextField(
//                                                              initialValue: currentTask == null ? '10' : currentTask.count.toString(),
                                                                    controller: countEditingController,
                                                                    attribute: 'count',
                                                                    onChanged: (val) {
                                                                      print(countEditingController.text);
                                                                    },
                                                                    onFieldSubmitted: (val) {
                                                                      FocusScope.of(context).requestFocus(FocusNode());
                                                                      SystemChrome.setEnabledSystemUIOverlays([]);
                                                                    },
                                                                    validators: [FormBuilderValidators.numeric(errorText: '仅支持输入数字')],
                                                                    autovalidate: true,
                                                                    enableInteractiveSelection: false,
                                                                    textAlign: TextAlign.left,
                                                                    keyboardType: TextInputType.number,
                                                                    decoration: InputDecoration(
                                                                        hintText: '输入目标佛号声数',
                                                                        filled: false,
                                                                        contentPadding:
                                                                            EdgeInsets.symmetric(horizontal: ScreenUtil.getInstance().getWidth(40), vertical: ScreenUtil.getInstance().getWidth(36)),
                                                                        enabledBorder: InputBorder.none,
                                                                        focusedBorder: InputBorder.none),
                                                                  )),
                                                                  GestureDetector(
                                                                    onTap: () {
                                                                      showMyGoals();
                                                                    },
                                                                    child: Container(
                                                                        height: ScreenUtil.getInstance().getWidth(120),
                                                                        width: ScreenUtil.getInstance().getWidth(210),
                                                                        child: Row(
                                                                          children: <Widget>[
                                                                            Container(
                                                                              decoration: BoxDecoration(border: Border(right: BorderSide(color: Color(0xFFEFEFEF), width: 0.5))),
                                                                              width: ScreenUtil.getInstance().getWidth(90),
                                                                              alignment: Alignment.center,
                                                                              child: Text('声'),
                                                                            ),
                                                                            Expanded(
                                                                                child: Container(
                                                                              alignment: Alignment.center,
                                                                              child: Icon(
                                                                                Icons.arrow_drop_down,
                                                                                size: ScreenUtil.getInstance().getSp(72),
                                                                                color: Colors.black54,
                                                                              ),
                                                                            )),
                                                                          ],
                                                                        )),
                                                                  ),
                                                                ],
                                                              ))),
                                                      Container(
                                                        height: ScreenUtil.getInstance().getWidth(90),
                                                        alignment: Alignment.centerLeft,
                                                        child: Text('快速设置', style: TextStyle(fontSize: GlobalConfigUtil.getTabMainBodyFontSize(), color: Colors.black54)),
                                                      ),
                                                      Wrap(
                                                        spacing: 6,
                                                        runSpacing: 6,
                                                        children: List.generate(
                                                            CommonConfig.fohaoNum.length,
                                                            (index) => ButtonTheme(
                                                                minWidth: ScreenUtil.getInstance().getWidth(120),
                                                                height: ScreenUtil.getInstance().getWidth(72),
                                                                layoutBehavior: ButtonBarLayoutBehavior.padded,
                                                                materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                                                                child: FlatButton(
                                                                    onPressed: () {
//                                                                      DateUtil.convertToZHDateTimeString('2018-12-25', ':');
                                                                      countEditingController.value = countEditingController.value.copyWith(
                                                                        text: CommonConfig.fohaoNum[index]['count'].toString(),
                                                                      );
                                                                      _sukhavatiSetBloc.dispatch(SukhavatiFrequencyEvent(frequency: tempFrequency));
                                                                    },
                                                                    color: Color(0xFFF2F2F2).withOpacity(0.5),
                                                                    padding: EdgeInsets.symmetric(horizontal: 8, vertical: 0),
                                                                    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(ScreenUtil.getInstance().getWidth(45))),
                                                                    child: Text(
                                                                      CommonConfig.fohaoNum[index]['count'].toString(),
                                                                      style: TextStyle(fontSize: GlobalConfigUtil.getTabMainBodyFontSize()),
                                                                    )))),
                                                      ),
                                                      Container(
                                                        height: ScreenUtil.getInstance().getWidth(90),
                                                        alignment: Alignment.centerLeft,
                                                        child: Text('频率', style: TextStyle(fontSize: GlobalConfigUtil.getTabMainBodyFontSize(), color: Colors.black54)),
                                                      ),
                                                      Container(
                                                        height: ScreenUtil.getInstance().getWidth(60),
                                                        alignment: Alignment.center,
                                                        child: Text(
                                                          '完成功课大约需要' + getTimeStr(int.parse(countEditingController.text) * tempFrequency * 100 ~/ 1000),
                                                          style: TextStyle(fontSize: GlobalConfigUtil.getTabMainBodyFontSize()),
                                                        ),
                                                      ),
                                                      Container(
                                                        child: Row(
                                                          children: <Widget>[
                                                            Expanded(
                                                                child: Container(
                                                              child: FlutterSlider(
                                                                values: [currentTask == null ? 3.0 : currentTask.frequency],
                                                                max: 50,
                                                                min: 3,
                                                                handlerWidth: 20,
                                                                handlerHeight: 20,
                                                                trackBar: FlutterSliderTrackBar(
                                                                    activeTrackBar: BoxDecoration(color: Color(0xFFA70400)),
                                                                    inactiveTrackBar: BoxDecoration(color: Colors.black12),
                                                                    activeTrackBarHeight: 2.5,
                                                                    inactiveTrackBarHeight: 2),
                                                                tooltip: FlutterSliderTooltip(disabled: true),
                                                                onDragging: (handlerIndex, lowerValue, upperValue) async {
                                                                  _sukhavatiSetBloc.dispatch(SukhavatiFrequencyEvent(frequency: lowerValue));
                                                                },
                                                                handler: FlutterSliderHandler(
                                                                    decoration: BoxDecoration(
                                                                        color: Color(0xFFA70400),
                                                                        borderRadius: BorderRadius.circular(10),
                                                                        boxShadow: [BoxShadow(color: Colors.black26, blurRadius: 8)]),
                                                                    child: Container(
                                                                      height: 20,
                                                                      width: 20,
                                                                      alignment: Alignment.center,
                                                                      child: Icon(
                                                                        Icons.keyboard_arrow_right,
                                                                        color: Colors.white,
                                                                        size: 16,

//                                            color: readerThemeA.iconBackgroundColor,
                                                                      ),
                                                                    )),
                                                                onDragCompleted: (handlerIndex, lowerValue, upperValue) async {
                                                                  print(lowerValue);
                                                                  _sukhavatiSetBloc.dispatch(SukhavatiFrequencyEvent(frequency: lowerValue));

//                                      widget.onChapterChangeEnd(lowerValue);
                                                                },
                                                              ),
                                                            )),
                                                            Container(
                                                              width: ScreenUtil.getInstance().getWidth(150),
                                                              child: ButtonTheme(
                                                                materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                                                                height: ScreenUtil.getInstance().getWidth(72),
                                                                child: FlatButton.icon(
                                                                    shape: RoundedRectangleBorder(side: BorderSide(color: Color(0xFFF2F2F2), width: 0.5), borderRadius: BorderRadius.circular(8)),
                                                                    padding: EdgeInsets.all(0),
                                                                    color: Color(0xFFF2F2F2).withOpacity(0.5),
                                                                    icon: Icon(
                                                                      previewState == 1 ? Icons.pause : Icons.play_circle_filled,
                                                                      color: previewState == 1 ? Color(0xFFA70400) : Colors.black26,
                                                                      size: ScreenUtil.getInstance().getSp(42),
                                                                    ),
                                                                    label: Text(
                                                                      '预览',
                                                                      style: TextStyle(fontSize: GlobalConfigUtil.getTabMainBodyFontSize()),
                                                                    ),
                                                                    onPressed: () {
                                                                      if (previewState == 1) {
                                                                        _sukhavatiSetBloc.dispatch(SukhavatiFrequencyPreviewStopEvent());
                                                                      } else {
                                                                        _sukhavatiSetBloc.dispatch(SukhavatiFrequencyPreviewEvent(frequency: tempFrequency * 100));
                                                                      }
                                                                    }),
                                                              ),
                                                            )
                                                          ],
                                                        ),
                                                      ),
                                                      Container(
                                                        height: ScreenUtil.getInstance().getWidth(120),
                                                        alignment: Alignment.centerLeft,
                                                        child: Text('跟随选择', style: TextStyle(fontSize: GlobalConfigUtil.getTabMainBodyFontSize(), color: Colors.black54)),
                                                      ),
                                                      GestureDetector(
                                                          onTap: () {
                                                            show();
                                                          },
                                                          child: Container(
                                                            height: ScreenUtil.getInstance().getWidth(120),
                                                            alignment: Alignment.centerLeft,
                                                            padding: EdgeInsets.only(left: 15),
                                                            decoration: BoxDecoration(
                                                              color: Color(0xFFF2F2F2).withOpacity(0.5),
                                                              borderRadius: BorderRadius.circular(8),
                                                            ),
                                                            child: Row(
                                                              children: <Widget>[
                                                                Expanded(child: Text(CommonConfig.fohaoFollowed[fohaoFollowedIndex]['name'])),
                                                                GestureDetector(
                                                                  onTap: () {
                                                                    show();
                                                                  },
                                                                  child: Container(
                                                                    height: ScreenUtil.getInstance().getWidth(120),
                                                                    width: ScreenUtil.getInstance().getWidth(120),
                                                                    decoration: BoxDecoration(border: Border(left: BorderSide(color: Color(0xFFF2F2F2).withOpacity(0.5), width: 0.5))),
                                                                    child: Icon(
                                                                      Icons.arrow_drop_down,
                                                                      size: ScreenUtil.getInstance().getSp(72),
                                                                      color: Colors.black54,
                                                                    ),
                                                                  ),
                                                                ),
                                                              ],
                                                            ),
                                                          )),
                                                    ],
                                                  )))
                                            ],
                                          ),
                                        ),
                                      )))
                            ],
                          );
                        },
                      )));
            }));
  }
}
