import 'package:json_annotation/json_annotation.dart';

part 'buddhist.g.dart';

@JsonSerializable()
class Buddhist {
  int id;
  String title;
  String author;
  String juanCount;
  String sectionIndex;
  String url;
  String catalogId;
  String catalogName;
  int classify;
  int version;

  Buddhist({this.id, this.title, this.juanCount, this.sectionIndex, this.url, this.catalogId, this.catalogName, this.classify = 2, this.version = 0});

  factory Buddhist.fromJson(Map<String, dynamic> json) {
    return _$BuddhistFromJson(json);
  }
  Map<String, dynamic> toJson() => _$BuddhistToJson(this);
}
