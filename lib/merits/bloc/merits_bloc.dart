import 'dart:async';
import 'dart:core';

import 'package:bloc/bloc.dart';
import 'package:bodhiroad/api/api.dart';
import 'package:bodhiroad/merits/bean/merits.dart';
import 'package:bodhiroad/merits/bean/merits_statistic.dart';
import 'package:bodhiroad/merits/bean/merits_statistic_count.dart';
import 'package:flustars/flustars.dart';

import 'merits_event.dart';
import 'merits_state.dart';

class MeritsBloc extends Bloc<MeritsEvent, MeritsState> {
  static final MeritsBloc _meritsBlocSingleton = new MeritsBloc._internal();
  factory MeritsBloc() {
    return _meritsBlocSingleton;
  }
  MeritsBloc._internal();

  MeritsState get initialState => MeritsInitState();
  List<Merits> belong1 = [];
  List<Merits> belong2 = [];
  @override
  Stream<MeritsState> mapEventToState(
    MeritsEvent event,
  ) async* {
    try {
      if (event is MeritsSlideEvent) {
        yield MeritsSlideState(page: event.page);
      } else if (event is MeritsUpdateEvent) {
        List<List<Merits>> list = [];
        print(DateTime.now().millisecondsSinceEpoch);
        List dyList0 = SpUtil.getObjectList('merits0');
        List dyList1 = SpUtil.getObjectList('merits1');
        print(DateTime.now().millisecondsSinceEpoch);
        if (dyList0 == null || event.forceUpdate) {
          list = await getMerits();
          yield MeritsUpdateState(list, DateTime.now().millisecondsSinceEpoch.toString());
          SpUtil.putObjectList('merits0', list[0]);
          SpUtil.putObjectList('merits1', list[1]);
        } else {
          List<Merits> lst0 = dyList0.map((item) => Merits.fromJson(item)).toList();
          List<Merits> lst1 = dyList1.map((item) => Merits.fromJson(item)).toList();
          list.add(lst0);
          list.add(lst1);
          yield MeritsUpdateState(list, DateTime.now().millisecondsSinceEpoch.toString());
        }
      } else if (event is SwitchTypeEvent) {
        yield SwitchTypeState(belong: event.belong);
      } else if (event is AddUserMeritsEvent) {
        print(DateTime.now().millisecondsSinceEpoch);
        Merits merits = event.merits;
        merits.list[event.rowId].userMerits[event.columnId].isGood = event.isGood;
        yield MarkMeritsState(merits, DateTime.now().millisecondsSinceEpoch.toString());
        print(DateTime.now().millisecondsSinceEpoch);
        var list = SpUtil.getObjectList('merits${event.belong}');
        list[event.catalogId] = merits.toJson();
        SpUtil.putObjectList('merits${event.belong}', list);
        if (event.operation == 'add') {
          Api.addUserMerits(data: {'meritsId': event.meritId, 'isGood': event.isGood, 'lastUpdate': event.date, 'belong': event.belong + 1});
        } else {
          Api.updateUserMerits(data: {'meritsId': event.meritId, 'isGood': event.isGood, 'lastUpdate': event.date, 'belong': event.belong + 1});
        }
      } else if (event is MeritsStatisticEvent) {
        yield MeritsLoadingState();
        var res = await Api.userMeritsStatistics(data: {'belong': event.catalogId, 'interval': event.interval});
        if (res['code'] == 200) {
          var resData = res['data'];
          List dlist1 = resData['statistics'];
          List dlist2 = resData['meritsCounts'];
          List<MeritsStatistic> list = dlist1.map((item) => MeritsStatistic.fromJson(item)).toList();
          List<MeritsStatisticCount> list2 = dlist2.map((item) => MeritsStatisticCount.fromJson(item)).toList();
          List<int> sortlist = [];
          list[0].list.forEach((item) {
            print(item.list.length);
            sortlist.add(item.list.length);
          });
          list[1].list.forEach((item) {
            sortlist.add(item.list.length);
          });
          sortlist.sort((a, b) => a.compareTo(b));
          this.dispatch(MeritsStatisticLoadEvent(list: list, maxY: sortlist.last, list2: list2));
        }
      } else if (event is MeritsStatisticLoadEvent) {
        yield MeritsStatisticLoadState(list: event.list, tsp: DateTime.now().millisecondsSinceEpoch, maxY: event.maxY);
      }
    } catch (_, stackTrace) {
      print('$_ $stackTrace');
      yield currentState;
    }
  }

  Future<List<List<Merits>>> getMerits() async {
    List<List<Merits>> list = [];
//    var rest = await HttpUtils.get('/merits/getAllMerits');
    var rest = await Api.getMerits();
    if (rest['code'] == 200) {
      List cataloglist = rest['data'];
      cataloglist.forEach((item) {
        List catalog = item['list'];
        List<Merits> lt = catalog.map((merits) => Merits.fromJson(merits)).toList();
        list.add(lt);
      });
      return list;
    } else {
      return [];
    }
  }
}
