// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'book_mark.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

BookmarkBean _$BookmarkBeanFromJson(Map<String, dynamic> json) {
  return BookmarkBean(
      bookId: json['bookId'] as String,
      title: json['title'] as String,
      pageIndex: json['pageIndex'] as int,
      position: (json['position'] as num)?.toDouble());
}

Map<String, dynamic> _$BookmarkBeanToJson(BookmarkBean instance) =>
    <String, dynamic>{
      'bookId': instance.bookId,
      'title': instance.title,
      'pageIndex': instance.pageIndex,
      'position': instance.position
    };
