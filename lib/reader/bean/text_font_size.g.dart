// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'text_font_size.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

TextFontSize _$TextFontSizeFromJson(Map<String, dynamic> json) {
  return TextFontSize(
      tagName: json['tagName'] as String,
      size: json['size'] == null
          ? null
          : TextLayoutSize.fromJson(json['size'] as Map<String, dynamic>));
}

Map<String, dynamic> _$TextFontSizeToJson(TextFontSize instance) =>
    <String, dynamic>{'tagName': instance.tagName, 'size': instance.size};
