import 'package:bodhiroad/api/api.dart';
import 'package:bodhiroad/reader/bean/bookmark.dart';
import 'package:flustars/flustars.dart';
import 'package:global_configuration/global_configuration.dart';

class GlobalConfigUtil {
  static const String PREFIX_PAGE = 'LASTPAGE_';
  static const String PREFIX_POSITION = 'LASTPOSITION_';
  static const String PREFIX_BOOKMARK = 'BOOKMARK_';
  static const String PREFIX_UNDERLINE = 'UNDERLINE_';

  ///获取书签键
  static String getBookmarkKey() {
    return PREFIX_BOOKMARK + GlobalConfigUtil.getInt('source').toString() + '_' + GlobalConfigUtil.getInt('id').toString();
  }

  ///获取划线键
  static String getUnderlineKey() {
    return PREFIX_UNDERLINE + GlobalConfigUtil.getInt('source').toString() + '_' + GlobalConfigUtil.getInt('id').toString();
  }

  ///获取书签集合
  static List<BookMark> getBookmarkList() {
    return SpUtil.getObjList(getBookmarkKey(), (v) => BookMark.fromJson(v)).toList();
  }

  ///获取标记集合
  static List<BookMark> getUnderLineList() {
//    List<BookMark> list = SpUtil.getObjList(getUnderlineKey(), (v) => BookMark.fromJson(v)).toList();
    return SpUtil.getObjList(getUnderlineKey(), (v) => BookMark.fromJson(v)).toList();
  }

  ///获取书签或划线
  static List<BookMark> getUnderLineOrBookmarkList() {
    return SpUtil.getObjList(getUnderlineKey(), (v) => BookMark.fromJson(v)).toList();
  }

  /// 保存书签或划线
  static void saveUnderLineOrBookmarkList(List<BookMark> list) {
    SpUtil.putObjectList(getUnderlineKey(), list);
  }

  /// 保存阅读配置信息
  static void updateValue(String key, dynamic value) {
    GlobalConfiguration().updateValue(key, value);
    SpUtil.putObject('readerGlobalConfig', GlobalConfiguration().appConfig);
  }

  static int getInt(String key) {
    return GlobalConfiguration().getInt(key);
  }

  static double getDouble(String key) {
    return GlobalConfiguration().getDouble(key);
  }

  static String getString(String key) {
    return GlobalConfiguration().getString(key);
  }

  /// 保存阅读进度
  static void saveReaderProgress(double value) {
    /// 水平翻页
    if (GlobalConfiguration().get('flipType') < 2) {
      SpUtil.putInt(PREFIX_PAGE + GlobalConfiguration().getInt('source').toString() + GlobalConfiguration().getInt('id').toString(), value.toInt());
    } else {
      SpUtil.putDouble(PREFIX_POSITION + GlobalConfiguration().getInt('source').toString() + GlobalConfiguration().getInt('id').toString(), value);
    }
  }

  /// 获取适配后字体大小
  static double getTitleFontSize() {
    return ScreenUtil.getInstance().getSp(48);
  }

  static double getSubTitleFontSize() {
    return ScreenUtil.getInstance().getSp(42);
  }

  static double getTabFontSize() {
    return ScreenUtil.getInstance().getSp(36);
  }

  static double getTabMainBodyFontSize() {
    return ScreenUtil.getInstance().getSp(32);
  }

  static double getCoverTitleFontSize() {
    return ScreenUtil.getInstance().getSp(16);
  }

  static double getBottomTabFontSize() {
    return ScreenUtil.getInstance().getSp(24);
  }

  static void setObject(String key, dynamic object) {
    if (GlobalConfiguration().get(key) == null) {
      GlobalConfiguration().loadFromMap({key: object});
      if (GlobalConfiguration().get(key) == null) {
        setObject(key, object);
      }
    } else {
      GlobalConfiguration().loadFromMap({key: object});
    }
  }
}
