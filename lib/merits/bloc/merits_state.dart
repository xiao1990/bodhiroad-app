import 'package:bodhiroad/merits/bean/merits.dart';
import 'package:bodhiroad/merits/bean/merits_statistic.dart';
import 'package:bodhiroad/merits/bean/merits_statistic_count.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class MeritsState extends Equatable {
  MeritsState([Iterable props]) : super(props);
}

/// 初始化
class MeritsInitState extends MeritsState {
  @override
  String toString() => 'MeritsInitState';
}

/// 翻页
class MeritsSlideState extends MeritsState {
  final int page;
  MeritsSlideState({this.page}) : super([page]);
  @override
  String toString() => 'MeritsSlideState';
}

/// 更新
class MeritsUpdateState extends MeritsState {
  final List<List<Merits>> list;
  final String randomStr;
  MeritsUpdateState(this.list, this.randomStr) : super([list, randomStr]);
  @override
  String toString() => 'MeritsSlideState';
}

class MarkMeritsState extends MeritsState {
  final Merits merits;
  final String randomStr;
  MarkMeritsState(this.merits, this.randomStr) : super([merits, randomStr]);
  @override
  String toString() => 'MeritsSlideState';
}

/// 翻页
class MeritsExpandState extends MeritsState {
  final int index;
  MeritsExpandState({this.index}) : super([index]);
  @override
  String toString() => 'MeritsExpandState';
}

class MeritsLoadingState extends MeritsState {
  final int index;
  MeritsLoadingState({this.index}) : super([index]);
  @override
  String toString() => 'MeritsExpandState';
}

class MeritsStatisticLoadState extends MeritsState {
  final List<MeritsStatistic> list;
  final int tsp;
  final int maxY;
  final List<MeritsStatisticCount> list2;
  MeritsStatisticLoadState({this.list, this.tsp, this.maxY, this.list2})
      : super([list, tsp, maxY, list2]);
  @override
  String toString() => 'MeritsStatisticLoadState';
}

class SwitchTypeState extends MeritsState {
  final int belong;
  SwitchTypeState({this.belong}) : super([belong]);
  @override
  String toString() => 'MeritsExpandState';
}
