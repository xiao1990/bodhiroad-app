import 'package:flutter/material.dart';
import 'package:oktoast/oktoast.dart';

class ToastUtils {
  static void success(String message) {
    showToast(
      message,
      textPadding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
      duration: Duration(seconds: 2),
      position: ToastPosition.center,
      backgroundColor: Colors.black.withOpacity(0.5),
      radius: 30,
      textStyle: TextStyle(fontSize: 14.0),
    );
  }

  static void fail(String message) {
    showToast(
      message,
      textPadding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
      duration: Duration(seconds: 2),
      position: ToastPosition.center,
      backgroundColor: Colors.red.withOpacity(0.5),
      radius: 30,
      textStyle: TextStyle(fontSize: 14.0),
    );
  }
}
