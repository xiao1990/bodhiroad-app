import 'dart:io';

import 'package:bodhiroad/api/api.dart';
import 'package:bodhiroad/components/extended_text/lib/extended_text.dart';
import 'package:bodhiroad/utils/GlobalConfigUtil.dart';
import 'package:bodhiroad/utils/Route.dart';
import 'package:bodhiroad/utils/ToastUtils.dart';
import 'package:flustars/flustars.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:image_picker_saver/image_picker_saver.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

class Feedback extends StatefulWidget {
  @override
  _FeedbackState createState() => _FeedbackState();
}

class _FeedbackState extends State<Feedback> {
  TextEditingController _controller = TextEditingController();
  TextEditingController _controller2 = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBody: true,
      appBar: AppBar(
        title: Text(
          '问题反馈',
          style: TextStyle(fontSize: GlobalConfigUtil.getTitleFontSize()),
        ),
        elevation: 0,
        backgroundColor: Colors.white,
        actions: <Widget>[
          FlatButton.icon(
              onPressed: () async {
                String issues = _controller.text.trim();
                String idea = _controller2.text.trim();
                if (issues.length > 10 || idea.length > 10) {
                  Map result = await Api.feedback(data: {'content': issues, 'idea': idea});
                  if (result['code'] == 200) {
                    Routes.router.pop(context);
                    ToastUtils.success('提交成功,感谢反馈！');
                  } else {
                    ToastUtils.success('提交失败！');
                  }
                } else {
                  ToastUtils.success('至少需要10个字符的描述！');
                }
              },
              icon: Icon(
                Icons.done,
                color: Color(0xFFA70400),
              ),
              label: Text(
                '提交',
                style: TextStyle(color: Color(0xFFA70400)),
              ))
        ],
      ),
      body: LayoutBuilder(
        builder: (context, constraint) {
          return Container(
            height: constraint.maxHeight,
            width: constraint.maxWidth,
            padding: EdgeInsets.symmetric(horizontal: ScreenUtil.getInstance().getWidth(40)),
            decoration: BoxDecoration(image: DecorationImage(image: AssetImage('assets/texture/bgt2.jpg'), fit: BoxFit.cover)),
            alignment: Alignment.topCenter,
            child: ListView(
              physics: BouncingScrollPhysics(),
              children: <Widget>[
                Container(
                  height: ScreenUtil.getInstance().getWidth(150),
                  alignment: Alignment.centerLeft,
                  child: Row(
                    children: <Widget>[
                      Container(
                        width: ScreenUtil.getInstance().getWidth(70),
                        height: ScreenUtil.getInstance().getWidth(70),
                        margin: EdgeInsets.only(right: 4),
                        decoration: BoxDecoration(borderRadius: BorderRadius.circular(14), color: Color(0xFFF2F2F2)),
                        child: Icon(
                          MdiIcons.bug,
                          color: Color(0xFFA70400),
                          size: ScreenUtil.getInstance().getSp(45),
                        ),
                      ),
                      Text('使用过程中遇到的问题')
                    ],
                  ),
                ),
                TextField(
                  controller: _controller,
                  maxLines: 12,
                  minLines: 6,
                  cursorColor: Color(0xFFB96652),
                  style: TextStyle(fontSize: 16, color: Colors.black38),
                  textAlignVertical: TextAlignVertical.top,
                  onSubmitted: (value) {
                    print(value);
                  },
                  showCursor: true,
                  decoration: new InputDecoration(
                    hintText: '由于资源有限，我们没能够在所有机型上进行测试，所以，不同手机用户难免出现各种问题，如您在使用过程中，遇到某项功能有问题，或者出现崩溃，卡死等影响您正常使用的情况，希望您能给我们积极反馈，我们将尽快修复！',
                    fillColor: Color(0xF2F2F2).withOpacity(0.5),
                    filled: true,
                    hintStyle: TextStyle(color: Colors.black38),
                    enabledBorder: OutlineInputBorder(borderSide: BorderSide(color: Color(0xFFF2F2F2), width: 0.5), borderRadius: BorderRadius.circular(8)),
                    focusedBorder: OutlineInputBorder(borderSide: BorderSide(color: Color(0xFFF2F2F2), width: 0.5), borderRadius: BorderRadius.circular(8)),
                  ),
                ),
                Container(
                  height: ScreenUtil.getInstance().getWidth(150),
                  alignment: Alignment.centerLeft,
                  child: Row(
                    children: <Widget>[
                      Container(
                        width: ScreenUtil.getInstance().getWidth(70),
                        height: ScreenUtil.getInstance().getWidth(70),
                        margin: EdgeInsets.only(right: 4),
                        decoration: BoxDecoration(borderRadius: BorderRadius.circular(14), color: Color(0xFFF2F2F2)),
                        child: Icon(
                          MdiIcons.origin,
                          color: Color(0xFFA70400),
                          size: ScreenUtil.getInstance().getSp(45),
                        ),
                      ),
                      Text('好的想法或创意')
                    ],
                  ),
                ),
                TextField(
                  controller: _controller2,
                  maxLines: 12,
                  minLines: 6,
                  cursorColor: Color(0xFFB96652),
                  style: TextStyle(fontSize: GlobalConfigUtil.getSubTitleFontSize(), color: Colors.black38),
                  textAlignVertical: TextAlignVertical.top,
                  onSubmitted: (value) {
                    print(value);
                  },
                  showCursor: true,
                  decoration: new InputDecoration(
                    hintText: '如果你有好的想法，让菩提路更加方便辅助同修们精进,可以融入我们的APP，非常欢迎您把想法传递给我，如果可行，我会把您的想法在我们的APP中实现。',
                    hintStyle: TextStyle(color: Colors.black38),
                    fillColor: Color(0xFFF2F2F2).withOpacity(0.8),
                    filled: true,
                    enabledBorder: OutlineInputBorder(borderSide: BorderSide(color: Color(0xFFF2F2F2), width: 0.5), borderRadius: BorderRadius.circular(8)),
                    focusedBorder: OutlineInputBorder(borderSide: BorderSide(color: Color(0xFFF2F2F2), width: 0.5), borderRadius: BorderRadius.circular(8)),
                  ),
                ),
                Container(
                  height: ScreenUtil.getInstance().getWidth(150),
                  alignment: Alignment.centerLeft,
                  child: Row(
                    children: <Widget>[
                      Container(
                        width: ScreenUtil.getInstance().getWidth(70),
                        height: ScreenUtil.getInstance().getWidth(70),
                        margin: EdgeInsets.only(right: 4),
                        decoration: BoxDecoration(borderRadius: BorderRadius.circular(14), color: Color(0xFFF2F2F2)),
                        child: Icon(
                          MdiIcons.wechat,
                          color: Color(0xFFA70400),
                          size: 18,
                        ),
                      ),
                      Expanded(
                          child: Text(
                        '当然，您可以直接加我的微信，直接与我沟通！',
                        maxLines: 2,
                      ))
                    ],
                  ),
                ),
                Container(
                  height: ScreenUtil.getInstance().getWidth(500),
                  decoration: BoxDecoration(
                      color: Colors.white,
                      image: DecorationImage(image: AssetImage('assets/texture/ss10.png'), fit: BoxFit.cover),
                      borderRadius: BorderRadius.circular(8),
                      boxShadow: [BoxShadow(color: Colors.black12, blurRadius: 12)]),
//                  padding: EdgeInsets.symmetric(vertical: 20),
                  child: Row(
                    children: <Widget>[
                      Container(
                        width: ScreenUtil.getInstance().getWidth(350),
                        height: ScreenUtil.getInstance().getWidth(425),
//                        color: Colors.orange,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: <Widget>[
                            Image.asset(
                              'assets/wechate1.png',
                              height: ScreenUtil.getInstance().getWidth(300),
                            ),
                            ButtonTheme(
                              height: ScreenUtil.getInstance().getWidth(75),
                              minWidth: ScreenUtil.getInstance().getWidth(250),
                              padding: EdgeInsets.all(0),
                              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(4), side: BorderSide(color: Colors.black12, width: 0.5)),
                              child: FlatButton(
                                  padding: EdgeInsets.all(0),
                                  onPressed: () async {
                                    File file = File('assets/wechate1.png');
                                    ByteData image = await rootBundle.load('assets/wechate1.png');
                                    String filePath = await ImagePickerSaver.saveFile(fileData: image.buffer.asUint8List());
                                    if (filePath != null && filePath.length > 0) {
                                      ToastUtils.success('保存成功');
                                    }
                                  },
                                  child: Text('保存二维码')),
                            )
                          ],
                        ),
                      ),
                      Container(
                        width: 0.5,
                        height: ScreenUtil.getInstance().getWidth(500),
                        color: Color(0xFFF2F2F2),
                      ),
                      Expanded(
                          child: Container(
                        padding: EdgeInsets.only(left: 15),
                        height: ScreenUtil.getInstance().getWidth(375),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            ExtendedText.rich(TextSpan(children: [TextSpan(text: '微信号：'), TextSpan(text: 'bodhiroad2019', style: TextStyle(height: 2, color: Color(0xFFA70400)))])),
                            ExtendedText.rich(TextSpan(children: [TextSpan(text: '菩提名：'), TextSpan(text: '菩提路', style: TextStyle(height: 2, color: Color(0xFFA70400)))])),
                            ExtendedText.rich(
                                TextSpan(children: [TextSpan(text: '邮   箱：'), TextSpan(text: 'bodhiroad@163.com', style: TextStyle(height: 2, color: Color(0xFFA70400)))]))
                          ],
                        ),
                      ))
                    ],
                  ),
                )
              ],
            ),
          );
        },
      ),
    );
  }
}
