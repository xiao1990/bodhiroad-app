import 'package:bodhiroad/components/dialog/dialog_body.dart';
import 'package:bodhiroad/components/dialog/dialog_head.dart';
import 'package:flustars/flustars.dart';
import 'package:flutter/material.dart';

import 'dialog_foot.dart';

class MyDialog extends StatefulWidget {
  final String title;
  final String confirmText;
  final String cancelText;
  final String closeText;
  final bool justClose;
  final double height;
  final VoidCallback onConfirm;
  final VoidCallback onCancel;
  final VoidCallback onClose;
  Widget content;
  MyDialog({Key key, this.height, this.title, this.closeText, this.confirmText, this.cancelText, this.justClose, this.content, this.onConfirm, this.onCancel, this.onClose});
  @override
  _MyDialogState createState() => _MyDialogState();
}

class _MyDialogState extends State<MyDialog> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.transparent,
      body: Center(
          child: ClipRRect(
              borderRadius: BorderRadius.circular(12),
              child: Container(
                height: widget.height ?? ScreenUtil.getInstance().getWidth(480),
                color: Colors.white,
                width: ScreenUtil.getInstance().screenWidth * 0.75,
                child: Column(
                  children: <Widget>[
                    DialogHead(title: widget.title),
                    Expanded(
                        child: DialogBody(
                      child: widget.content,
                    )),
                    DialogFoot(
                      confirmText: widget.confirmText,
                      cancelText: widget.cancelText,
                      closeText: widget.closeText,
                      justClose: widget.justClose,
                      onConfirm: () {
                        widget.onConfirm();
                      },
                      onCancel: () {
                        widget.onCancel();
                      },
                      onClose: () {
                        widget.onClose();
                      },
                    ),
                  ],
                ),
              ))),
    );
  }
}
