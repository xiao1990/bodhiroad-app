import 'dart:math';
import 'dart:ui';

import 'package:bodhiroad/common/FontFamilyName.dart';
import 'package:bodhiroad/common/tab_bloc.dart';
import 'package:bodhiroad/components/md2_tab_indicator.dart';
import 'package:bodhiroad/config/GlobalColorCOnfig.dart';
import 'package:bodhiroad/index/bloc/hide_bloc.dart';
import 'package:bodhiroad/index/pages/qldzj_catalog_list.dart';
import 'package:bodhiroad/reader/bloc/buddhist/buddhist_bloc.dart';
import 'package:bodhiroad/reader/bloc/buddhist/buddhist_event.dart';
import 'package:bodhiroad/reader/bloc/buddhist/buddhist_state.dart';
import 'package:bodhiroad/theme/bloc.dart';
import 'package:bodhiroad/utils/GlobalConfigUtil.dart';
import 'package:flustars/flustars.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

import 'brwh_topcatalog_list.dart';
import 'common_buddhist.dart';

class BuddhistDepositary extends StatefulWidget {
  final Widget leftAction;
  final Widget rightAction;
  BuddhistDepositary({this.leftAction, this.rightAction});
  @override
  _BuddhistDepositaryState createState() => _BuddhistDepositaryState();
}

class _BuddhistDepositaryState extends State<BuddhistDepositary> with SingleTickerProviderStateMixin, AutomaticKeepAliveClientMixin {
  ScrollController _scrollController;
  double _movePercent = 0.0;
  TabBloc _tabBloc = TabBloc();
  BuddhistBloc _buddhistBloc = BuddhistBloc();
  HideBloc hideBloc = HideBloc();
  double lastPosition = 0.0;
  AxisDirection currntDirection = AxisDirection.down;
  TabController tabController;
  @override
  void initState() {
    Random().nextInt(7);
    super.initState();
    tabController = TabController(length: 3, vsync: this);
    _buddhistBloc.dispatch(LoadBuddhistEvent());
    _scrollController = ScrollController(initialScrollOffset: 0);
  }

  @override
  bool get wantKeepAlive => true;

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder(
      bloc: AppThemeBloc(),
      builder: (context, ThemeChangeState appThemeState) {
        return Container(
          decoration: BoxDecoration(
              color: Colors.white,
              image: DecorationImage(
                fit: BoxFit.cover,
                image: AssetImage('assets/texture/bgt2.jpg'),
              )),
          child: NotificationListener<ScrollNotification>(
            child: NestedScrollView(
                controller: _scrollController,
                headerSliverBuilder: (context, isScroll) {
                  return [
                    SliverPadding(
                        padding: EdgeInsets.only(top: 0),
                        sliver: SliverAppBar(
                          backgroundColor: appThemeState.themeConfig.sectionBgColor,
                          centerTitle: true,
                          title: Text('藏经阁',
                              textScaleFactor: 1.0,
                              style: TextStyle(color: appThemeState.themeConfig.mainTextColor, fontSize: GlobalConfigUtil.getTitleFontSize(), fontFamily: FontFamilyName.CHINESESTYLE)),
                          pinned: true,
                          floating: true,
                          elevation: 4,
                          forceElevated: isScroll,
                          leading: widget.leftAction,
                          actions: <Widget>[widget.rightAction],
                          bottom: PreferredSize(
                            preferredSize: Size.fromHeight(ScreenUtil.getInstance().getWidth(120)),
                            child: BlocBuilder(
                                bloc: _tabBloc,
                                builder: (BuildContext context, indicatorWidth) {
                                  return Padding(
                                      padding: EdgeInsets.symmetric(horizontal: 0),
                                      child: Container(
                                        height: ScreenUtil.getInstance().getWidth(120),
                                        // padding: EdgeInsets.all(10),
                                        decoration: BoxDecoration(
                                          color: appThemeState.themeConfig.sectionBgColor,
                                        ),
                                        child: Column(
                                          mainAxisAlignment: MainAxisAlignment.end,
                                          children: <Widget>[
                                            TabBar(
                                              indicator: MD2Indicator(
                                                  indicatorHeight: ScreenUtil.getInstance().getWidth(6),
                                                  indicatorWidth: indicatorWidth,
                                                  indicatorColor: appThemeState.themeConfig.embellishColorA,
                                                  indicatorSize: MD2IndicatorSize.tiny //3 different modes tiny-normal-full
                                                  ),
                                              tabs: [
                                                Container(
                                                  height: ScreenUtil.getInstance().getWidth(114),
                                                  alignment: Alignment.center,
                                                  child: Text(
                                                    '常诵佛经',
                                                    textScaleFactor: 1.0,
                                                    style: getTabTextStyle(),
                                                  ),
                                                ),
                                                Container(
                                                  height: ScreenUtil.getInstance().getWidth(114),
                                                  alignment: Alignment.center,
                                                  child: Text(
                                                    '乾隆大藏经',
                                                    textScaleFactor: 1.0,
                                                    style: getTabTextStyle(),
                                                  ),
                                                ),
                                                Container(
                                                  height: ScreenUtil.getInstance().getWidth(114),
                                                  alignment: Alignment.center,
                                                  child: Text(
                                                    '般若文海',
                                                    textScaleFactor: 1.0,
                                                    style: getTabTextStyle(),
                                                  ),
                                                ),
                                              ],
                                              controller: tabController,
                                              indicatorSize: TabBarIndicatorSize.label,
                                              labelColor: appThemeState.themeConfig.embellishColorA,
                                              unselectedLabelColor: appThemeState.themeConfig.mainTextColor,
                                              labelStyle: TextStyle(fontWeight: FontWeight.bold),
                                              unselectedLabelStyle:
                                                  TextStyle(color: appThemeState.themeConfig.mainTextColor, fontSize: ScreenUtil.getInstance().getWidth(40), fontWeight: FontWeight.normal),
                                            )
                                          ],
                                        ),
                                      ));
                                }),
                          ),
                        ))
                  ];
                },
                body: Container(
                  child: BlocBuilder(
                    bloc: _buddhistBloc,
                    builder: (context, state) {
                      return state is LoadBuddhistState
                          ? TabBarView(controller: tabController, children: [
                              CommonBuddhist(
                                from: 'common',
                              ),
                              QldzjCatalogList(
                                from: 'qldzj',
                                source: '1',
                                taskType: '1',
                              ),
                              BrwhTopCatalogList(
                                from: 'brwh',
                                source: '2',
                                taskType: '8',
                              ),
                            ])
                          : Center(
                              child: SpinKitThreeBounce(
                                color: GlobalColorConfig.buttonColorA,
                                size: 30.0,
                              ),
                            );
                    },
                  ),
                )),
            onNotification: (notification) {
              if (notification.metrics.axisDirection != AxisDirection.down) {
                _movePercent = notification.metrics.pixels / notification.metrics.viewportDimension;
                _movePercent = _movePercent % 1;
                _tabBloc.dispatch(_movePercent);
              }
              if (notification.metrics.pixels < 0 || notification.metrics.axisDirection != AxisDirection.down) {
                return;
              }
              int currentPosition = (notification.metrics.pixels / 10).floor() * 10;
              if (currentPosition > lastPosition) {
                hideBloc.dispatch(0.0);
                lastPosition = currentPosition.toDouble();
              } else if (currentPosition < lastPosition) {
                hideBloc.dispatch(1.0);
              }
              lastPosition = currentPosition.toDouble();
            },
          ),
        );
      },
    );
  }

  TextStyle getTabTextStyle() {
    return TextStyle(fontFamily: 'songti', fontSize: GlobalConfigUtil.getTabFontSize());
  }
}

class SearchBar extends StatelessWidget with PreferredSizeWidget {
  final TabController tabController;
  final TabBloc tabBloc;
  SearchBar({this.tabController, this.tabBloc});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder(
        bloc: tabBloc,
        builder: (BuildContext context, indicatorWidth) {
          return Container(
            height: ScreenUtil.getInstance().getWidth(180),
            // padding: EdgeInsets.all(10),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                TabBar(
                  indicator: MD2Indicator(
                      indicatorHeight: 5,
                      indicatorWidth: indicatorWidth,
                      indicatorColor: GlobalColorConfig.mainBackgroundColor,
                      indicatorSize: MD2IndicatorSize.tiny //3 different modes tiny-normal-full
                      ),
                  tabs: [
                    Container(
                      height: ScreenUtil.getInstance().getWidth(104),
                      alignment: Alignment.center,
                      child: Text(
                        '常诵佛经',
                        style: TextStyle(fontFamily: FontFamilyName.CHINESESTYLE),
                      ),
                    ),
                    Container(
                      height: ScreenUtil.getInstance().getWidth(104),
                      alignment: Alignment.center,
                      child: Text('乾隆大藏经', style: TextStyle(fontFamily: FontFamilyName.CHINESESTYLE)),
                    ),
                    Container(
                      height: ScreenUtil.getInstance().getWidth(104),
                      alignment: Alignment.center,
                      child: Text('般若文海', style: TextStyle(fontFamily: FontFamilyName.CHINESESTYLE)),
                    ),
                  ],
                  controller: tabController,
                  indicatorSize: TabBarIndicatorSize.label,
                  labelColor: GlobalColorConfig.mainTextColor,
                  labelStyle: TextStyle(fontSize: ScreenUtil.getInstance().getWidth(40), fontWeight: FontWeight.bold, fontFamily: FontFamilyName.CHINESESTYLE),
                  unselectedLabelStyle: TextStyle(fontSize: ScreenUtil.getInstance().getWidth(40), fontWeight: FontWeight.bold, fontFamily: FontFamilyName.CHINESESTYLE),
                )
              ],
            ),
          );
        });
  }

  @override
  Size get preferredSize => Size.fromHeight(ScreenUtil.getInstance().getWidth(180));
}
