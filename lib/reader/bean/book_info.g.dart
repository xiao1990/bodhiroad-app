// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'book_info.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

BookInfoBean _$BookInfoBeanFromJson(Map<String, dynamic> json) {
  return BookInfoBean(
      catelogList: (json['catelogList'] as List)
          ?.map((e) => e == null
              ? null
              : CatalogBean.fromJson(e as Map<String, dynamic>))
          ?.toList(),
      pageList: (json['pageList'] as List)
          ?.map((e) =>
              e == null ? null : PageBean.fromJson(e as Map<String, dynamic>))
          ?.toList(),
      bookMarkList: (json['bookMarkList'] as List)
          ?.map((e) => e == null
              ? null
              : BookmarkBean.fromJson(e as Map<String, dynamic>))
          ?.toList(),
      author: json['author'] as String,
      title: json['title'] as String,
      fileUrl: json['fileUrl'] as String,
      id: json['id'] as String);
}

Map<String, dynamic> _$BookInfoBeanToJson(BookInfoBean instance) =>
    <String, dynamic>{
      'catelogList': instance.catelogList,
      'pageList': instance.pageList,
      'bookMarkList': instance.bookMarkList,
      'title': instance.title,
      'author': instance.author,
      'fileUrl': instance.fileUrl,
      'id': instance.id
    };
