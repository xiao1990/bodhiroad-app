import 'dart:ui';

import 'package:bodhiroad/config/GlobalColorCOnfig.dart';
import 'package:bodhiroad/theme/bloc.dart';
import 'package:flustars/flustars.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class BodhiRoadConfirmDialog extends StatefulWidget {
  final String title;
  final Text description;
  final onConfirm;
  final String confirmText;
  final bool justClose;
  final String cancelText;
  const BodhiRoadConfirmDialog({this.title, this.description, this.onConfirm, this.cancelText, this.justClose = false, this.confirmText});
  @override
  _BodhiRoadConfirmDialogState createState() => _BodhiRoadConfirmDialogState();
}

class _BodhiRoadConfirmDialogState extends State<BodhiRoadConfirmDialog> {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder(
        bloc: AppThemeBloc(),
        builder: (context, ThemeChangeState appThemeState) {
          return Scaffold(
              backgroundColor: Colors.transparent,
              body: Container(
                child: Center(
                  child: Container(
                      width: ScreenUtil.getInstance().getWidth(840),
                      height: ScreenUtil.getInstance().getWidth(480),
                      decoration: BoxDecoration(
                          color: appThemeState.themeConfig.sectionBgColor,
                          borderRadius: BorderRadius.circular(12),
                          boxShadow: [BoxShadow(color: appThemeState.themeConfig.shadowColor, blurRadius: 30)],
                          image: DecorationImage(fit: BoxFit.fitWidth, alignment: Alignment.bottomCenter, image: AssetImage('assets/texture/ss11.png'))),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Container(
                            height: ScreenUtil.getInstance().getWidth(120),
                            alignment: Alignment.center,
                            child: Text(widget.title ?? '提示信息',
                                textScaleFactor: 1.0, style: TextStyle(fontSize: ScreenUtil.getInstance().getSp(42), color: appThemeState.themeConfig.mainTextColor, fontWeight: FontWeight.bold)),
                            decoration: BoxDecoration(border: Border(bottom: BorderSide(color: appThemeState.themeConfig.lightBorderColor, width: 0.5))),
                          ),
                          Container(
                            height: ScreenUtil.getInstance().getWidth(210),
                            alignment: Alignment.center,
                            child: widget.description,
                          ),
                          Container(
                              height: ScreenUtil.getInstance().getWidth(150),
                              decoration: BoxDecoration(border: Border(top: BorderSide(color: appThemeState.themeConfig.lightBorderColor, width: 0.5))),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Expanded(
                                    child: ButtonTheme(
                                      minWidth: ScreenUtil.getInstance().getWidth(420),
                                      height: ScreenUtil.getInstance().getWidth(150),
                                      child: FlatButton(
                                        child: Text(
                                          widget.cancelText,
                                          textScaleFactor: 1.0,
                                          style: TextStyle(fontSize: ScreenUtil.getInstance().getSp(42), color: appThemeState.themeConfig.mainTextColor, fontWeight: FontWeight.bold),
                                        ),
                                        onPressed: () {
                                          Navigator.pop(context);
                                        },
                                      ),
                                    ),
                                  ),
                                  Offstage(
                                    offstage: widget.justClose,
                                    child: Container(
                                      height: ScreenUtil.getInstance().getWidth(150),
                                      width: 0.5,
                                      color: appThemeState.themeConfig.lightBorderColor,
                                    ),
                                  ),
                                  Offstage(
                                    offstage: widget.justClose,
                                    child: ButtonTheme(
                                      height: ScreenUtil.getInstance().getWidth(150),
                                      minWidth: ScreenUtil.getInstance().getWidth(420),
                                      buttonColor: GlobalColorConfig.mainRedColor,
                                      child: FlatButton(
                                        child: Text(
                                          widget.confirmText,
                                          textScaleFactor: 1.0,
                                          style: TextStyle(fontSize: ScreenUtil.getInstance().getSp(42), color: appThemeState.themeConfig.buttonActiveTextColor, fontWeight: FontWeight.bold),
                                        ),
                                        onPressed: () {
                                          Navigator.pop(context);
                                          widget.onConfirm();
                                        },
                                      ),
                                    ),
                                  )
                                ],
                              ))
                        ],
                      )),
                ),
              ));
        });
  }
}
