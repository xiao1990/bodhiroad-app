// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'book_singlemark.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

BookSingleMarkBean _$BookSingleMarkBeanFromJson(Map<String, dynamic> json) {
  return BookSingleMarkBean(
      nodeIndex: json['nodeIndex'] as int,
      charIndex: json['charIndex'] as int,
      position: json['position']);
}

Map<String, dynamic> _$BookSingleMarkBeanToJson(BookSingleMarkBean instance) =>
    <String, dynamic>{
      'nodeIndex': instance.nodeIndex,
      'charIndex': instance.charIndex,
      'position': instance.position
    };
