import 'package:bodhiroad/task/read/bean/task_catalog.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class TaskEvent extends Equatable {
  TaskEvent([props = const []]) : super(props);
}

class TaskInitEvent extends TaskEvent {
  @override
  String toString() => 'TaskInitEvent';
}

class GetUserTaskEvent extends TaskEvent {
  @override
  String toString() => 'GetUserTaskEvent';
}

class GetTaskGatalogEvent extends TaskEvent {
  @override
  String toString() => 'TaskFrequencyEvent';
}

class AddTaskEvent extends TaskEvent {
  final int taskId;
  final int taskCatalogIndex;
  final int itemIndex;
  final List<TaskCatalog> list;
  AddTaskEvent({this.taskId, this.list, this.itemIndex, this.taskCatalogIndex})
      : super([taskId, list, taskCatalogIndex, itemIndex]);
  @override
  String toString() => 'AddTaskEvent';
}

class DeleteTaskEvent extends TaskEvent {
  final int taskId;
  DeleteTaskEvent({this.taskId}) : super([taskId]);
  @override
  String toString() => 'DeleteTaskEvent';
}

class ReportTaskEvent extends TaskEvent {
  final int taskId;
  final int count;
  final int taskType;
  final int itemIndex;
  ReportTaskEvent({this.taskId, this.count, this.taskType, this.itemIndex})
      : super([taskId, count, taskType]);
  @override
  String toString() => 'ReportTaskEvent';
}

class AddTaskRecordEvent extends TaskEvent {
  @override
  String toString() => 'AddTaskRecordEvent';
}

class UpdateTaskRecordEvent extends TaskEvent {
  @override
  String toString() => 'AddTaskRecordEvent';
}

class ReportTaskFinishEvent extends TaskEvent {
  @override
  String toString() => 'ReportTaskFinishEvent';
}

class DeleteTaskSuccessEvent extends TaskEvent {
  @override
  String toString() => 'DeleteTaskSuccessEvent';
}

class UserTaskEvent extends TaskEvent {
  final bool forceUpdate;
  UserTaskEvent({this.forceUpdate}) : super([forceUpdate]);
  @override
  String toString() => 'UserTaskEvent';
}
