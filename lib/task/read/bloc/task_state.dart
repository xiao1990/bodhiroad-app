import 'package:bodhiroad/index/bean/taskStatisticSimple.dart';
import 'package:bodhiroad/task/read/bean/task.dart';
import 'package:bodhiroad/task/read/bean/task_catalog.dart';
import 'package:bodhiroad/task/read/bean/task_classify.dart';
import 'package:meta/meta.dart';

@immutable
abstract class TaskState {
  TaskState([props = const []]);
}

class TaskInitState extends TaskState {
  @override
  String toString() => 'TaskInitState';
}

class GetUserTaskState extends TaskState {
  final List<Task> list;
  GetUserTaskState({this.list});
  @override
  String toString() => 'GetUserTaskState';
}

class GetTaskGatalogState extends TaskState {
  final List<TaskCatalog> list;
  final int count;
  final int tsp;
  GetTaskGatalogState({this.list, this.count, this.tsp});
  @override
  String toString() => 'GetTaskGatalogState';
}

class TaskGatalogShowState extends TaskState {
  final List<TaskCatalog> list;
  final int count;
  TaskGatalogShowState({this.list, this.count});
  @override
  String toString() => 'TaskGatalogShowState';
}

class AddTaskState extends TaskState {
  final int taskId;
  final List<TaskCatalog> list;
  AddTaskState({this.taskId, this.list}) : super([taskId, list]);
  @override
  String toString() => 'AddTaskState';
}

class UserTaskState extends TaskState {
  final List<TaskClassify> list;
  final List<TaskStatisticSimpleBean> total;
  final List<TaskStatisticSimpleBean> today;
  UserTaskState({this.list, this.today, this.total}) : super([list]);
  @override
  String toString() => 'UserTaskState';
}

class UserTaskQueryingState extends TaskState {
  UserTaskQueryingState() : super();
  @override
  String toString() => 'UserTaskQueryingState';
}

class DeleteTaskState extends TaskState {
  final int taskId;
  DeleteTaskState({this.taskId}) : super([taskId]);
  @override
  String toString() => 'DeleteTaskState';
}

class ReportTaskState extends TaskState {
  final int taskId;
  final int count;
  ReportTaskState({this.taskId, this.count}) : super([taskId, count]);
  @override
  String toString() => 'ReportTaskState';
}

class ReportTaskSuccessState extends TaskState {
  final int tsp;
  ReportTaskSuccessState({this.tsp}) : super([tsp]);
  @override
  String toString() => 'ReportTaskSuccessState';
}

class DeleteTaskSuccessState extends TaskState {
  @override
  String toString() => 'DeleteTaskSuccessState';
}
