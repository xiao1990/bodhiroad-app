import 'package:bodhiroad/config/CommonConfig.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:esys_flutter_share/esys_flutter_share.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class SharePage extends StatefulWidget {
  @override
  _SharePageState createState() => _SharePageState();
}

class _SharePageState extends State<SharePage> {
  int initialIndex = 1;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBody: true,
      appBar: AppBar(
        title: Text('分享'),
        elevation: 0,
        backgroundColor: Colors.white,
      ),
      body: LayoutBuilder(
        builder: (context, constraint) {
          return Container(
            height: constraint.maxHeight,
            width: constraint.maxWidth,
            color: Colors.white,
//            decoration: BoxDecoration(image: DecorationImage(image: AssetImage('assets/texture/bgt2.jpg'), fit: BoxFit.cover)),
            alignment: Alignment.topCenter,
            child: Column(
              children: <Widget>[
                Container(
                  height: 60,
                  alignment: Alignment.center,
                  child: Text('选择任意一张卡片分享'),
                ),
                CarouselSlider(
                  items: List.generate(CommonConfig.shareImages.length, (index) {
                    return Stack(
                      children: <Widget>[
                        Container(
                          width: 320,
                          height: constraint.maxHeight - 180,
                          alignment: Alignment.center,
                          child: Container(
//                        color: Colors.orange,
                            decoration: BoxDecoration(
                              color: Colors.white,
                              image: DecorationImage(image: AssetImage('assets/texture/item_bg.jpg'), fit: BoxFit.fitWidth, alignment: Alignment.bottomCenter),
                              borderRadius: BorderRadius.circular(15),
                              boxShadow: [BoxShadow(color: Colors.black12, blurRadius: 15)],
                            ),
                            width: 320,
                            height: constraint.maxHeight - 210,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: <Widget>[Image.asset(CommonConfig.shareImages[index])],
                            ),
                          ),
                        ),
                        Positioned(
                          child: Container(
                            width: 28,
                            height: 28,
                            decoration: BoxDecoration(color: Color(0xFFF2F2F2), borderRadius: BorderRadius.circular(14)),
                            alignment: Alignment.center,
                            child: Text(
                              (index + 1).toString(),
                              style: TextStyle(color: Color(0xFFA70400)),
                            ),
                          ),
                          left: 20,
                          top: 30,
                        )
                      ],
                    );
                  }).toList(),
                  height: constraint.maxHeight - 180,
                  aspectRatio: 0.68,
                  viewportFraction: 0.8,
                  initialPage: initialIndex,
                  enableInfiniteScroll: false,
                  reverse: false,
                  autoPlay: false,
                  autoPlayInterval: Duration(seconds: 3),
                  autoPlayAnimationDuration: Duration(milliseconds: 800),
                  autoPlayCurve: Curves.fastOutSlowIn,
                  pauseAutoPlayOnTouch: Duration(seconds: 10),
                  enlargeCenterPage: true,
                  onPageChanged: (index) {
                    initialIndex = index;
                    print(initialIndex);
                  },
                  scrollDirection: Axis.horizontal,
                ),
                Container(
                    height: 120,
                    alignment: Alignment.center,
                    child: GestureDetector(
                      onTap: () async {
                        ByteData image = await rootBundle.load(CommonConfig.shareImages[initialIndex]);
                        await Share.file('分享到', 'esys.png', image.buffer.asUint8List(), 'image/png', text: '菩提路');
                      },
                      child: Container(
                        width: 320,
                        height: 48,
                        decoration: BoxDecoration(color: Color(0xFFA70400), boxShadow: [BoxShadow(color: Colors.black26, blurRadius: 15)], borderRadius: BorderRadius.circular(8)),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Icon(
                              Icons.share,
                              color: Colors.white,
                              size: 14,
                            ),
                            Text(
                              '分享',
                              style: TextStyle(color: Colors.white),
                            )
                          ],
                        ),
                      ),
                    )),
              ],
            ),
          );
        },
      ),
    );
  }
}
