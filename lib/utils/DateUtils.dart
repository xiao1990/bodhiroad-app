import 'package:lunar_calendar_converter/lunar_solar_converter.dart';

class DateUtils {
  static List<String> lunarMonthList = ['正', '二', '三', '四', '五', '六', '七', '八', '九', '十', '冬', '腊'];
  static List<String> lunarDayList = [
    '初一',
    '初二',
    '初三',
    '初四',
    '初五',
    '初六',
    '初七',
    '初八',
    '初九',
    '初十',
    '十一',
    '十二',
    '十三',
    '十四',
    '十五',
    '十六',
    '十七',
    '十八',
    '十九',
    '二十',
    '廿一',
    '廿二',
    '廿三',
    '廿四',
    '廿五',
    '廿六',
    '廿七',
    '廿八',
    '廿九',
    '三十'
  ];
  static Lunar getLunar(DateTime day) {
    Solar solar = Solar(solarYear: day.year, solarMonth: day.month, solarDay: day.day);
    Lunar lunar = LunarSolarConverter.solarToLunar(solar);
    return lunar;
  }

  static String getKey(day) {
    Lunar lunar = getLunar(day);
    return lunar.lunarMonth.toString() + '-' + lunar.lunarDay.toString();
  }

  static String getMonthAndDay(DateTime day) {
    Solar solar = Solar(solarYear: day.year, solarMonth: day.month, solarDay: day.day);
    Lunar lunar = LunarSolarConverter.solarToLunar(solar);
    return lunar.toString().substring(lunar.toString().length - 4);
//    return lunarMonthList[lunar.lunarMonth-1]+'月'+ lunarMonthList[lunar.lunarDay];
  }

  static String calculateTime(int second) {
    int hour = (second / 3600).floor();
    int minute = ((second % 3600) / 60).floor();
    int sec = (second % 3600) % 60;
    String lastmin = minute < 10 ? '0${minute % 60}' : '${minute % 60}';
    String lastsecond = sec < 10 ? '0${sec % 60}' : '${sec % 60}';
    String hours = hour < 10 ? '0' + hour.toString() : hour.toString();
    return hours + ':' + lastmin + ':' + lastsecond;
  }
}
