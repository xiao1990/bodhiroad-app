import 'package:after_layout/after_layout.dart';
import 'package:bodhiroad/components/extended_text/lib/extended_text.dart';
import 'package:bodhiroad/components/extended_text_library/lib/src/special_text_span.dart';
import 'package:bodhiroad/components/transformerPageview/transformer_page_view.dart';
import 'package:bodhiroad/config/ReaderThemeConfig.dart';
import 'package:bodhiroad/reader/bean/book_page.dart';
import 'package:bodhiroad/reader/bean/bookmark.dart';
import 'package:bodhiroad/reader/bean/line_char.dart';
import 'package:bodhiroad/reader/bean/reader_theme.dart';
import 'package:bodhiroad/reader/bloc.dart';
import 'package:bodhiroad/reader/bloc/progress_bloc.dart';
import 'package:bodhiroad/reader/pages/VerticalScrollPage.dart';
import 'package:bodhiroad/reader/pages/text_canvas_page.dart';
import 'package:bodhiroad/reader/toolbar_bloc.dart';
import 'package:bodhiroad/utils/GlobalConfigUtil.dart';
import 'package:flustars/flustars.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:global_configuration/global_configuration.dart';
import 'package:zh_char_converter/zh_char_converter.dart';

import 'PageTitle.dart';
import 'select_controller.dart';

/// 水平翻页翻页
class HorizontalScrollPage extends StatefulWidget {
  final List<PageBean> pageList;
  final onPageChanged;
  final BuddhistReaderBlocBloc bloc;
  final ProgressBloc progressBloc;
  final VoidCallback openBottomMenu;
  final openMarkedMenu;
  final String title;
  final handleMyCut;
  final handleMyCopy;
  final handleMyPaste;
  final handleMySelectAll;
  final handleLike;
  final handleSearch;
  final handleDrawUnderline;
  final handleShare;
  final handleMakeBookmark;
  final handleRecovery;
  HorizontalScrollPage({
    this.pageList,
    this.onPageChanged,
    this.bloc,
    this.progressBloc,
    this.title,
    this.openBottomMenu,
    this.openMarkedMenu,
    this.handleMyCopy,
    this.handleMySelectAll,
    this.handleMyCut,
    this.handleMyPaste,
    this.handleLike,
    this.handleSearch,
    this.handleDrawUnderline,
    this.handleShare,
    this.handleMakeBookmark,
    this.handleRecovery,
  });

  @override
  _HorizontalScrollPageState createState() => _HorizontalScrollPageState();
}

class _HorizontalScrollPageState extends State<HorizontalScrollPage> with AfterLayoutMixin<HorizontalScrollPage> {
  bool reversed = false;
  int pagesCount = 0;
  int currentPageIndex = 0;
  int initPage = 0;
  IndexController _controller;
  @override
  void initState() {
    _controller = IndexController();
    initPage = SpUtil.getInt(GlobalConfigUtil.PREFIX_PAGE + GlobalConfiguration().getInt('source').toString() + GlobalConfiguration().getInt('id').toString());
    if (initPage == initPage) {
      widget.bloc.dispatch(ReaderUnReachBottomEvent());
      if (GlobalConfiguration().getInt('textLayOutDirection') == 0) {
        if (widget.pageList.length - 1 == initPage) {
          initPage = 0;
          GlobalConfigUtil.saveReaderProgress(initPage.toDouble());
        }
      }else{
        if (initPage==0) {
          initPage = widget.pageList.length - 1;
          GlobalConfigUtil.saveReaderProgress(initPage.toDouble());
        }
      }
      widget.progressBloc.dispatch(initPage.toDouble());
    }

    super.initState();
  }

  @override
  void afterFirstLayout(BuildContext context) {}

  ToolbarBloc toolbarBloc = ToolbarBloc();
  @override
  Widget build(BuildContext context) {
    reversed = GlobalConfiguration().getInt('textLayOutDirection') == 1;
    ReaderTheme _readerTheme = ReaderThemeConfig.instance.list[GlobalConfiguration().getInt('themeIndex')];
    // 最后一页 && 水平翻页，默认回到首页
    int lastInitPage = SpUtil.getInt(GlobalConfigUtil.PREFIX_PAGE + GlobalConfiguration().getInt('source').toString() + GlobalConfiguration().getInt('id').toString());
    /// 初次进入

    return BlocListener(
      bloc: widget.bloc,
      listener: (context, state) {
        if (state is ReaderRestartState) {
          // 回到第一页
          if (GlobalConfiguration().getInt('textLayOutDirection') == 1) {
            _controller.move(widget.pageList.length - 1, animation: false);
          } else {
            _controller.move(0, animation: false);
          }
        } else if (state is ChapterJumpState) {
          if (GlobalConfiguration().getInt('flipType') != 2) {
            Future.delayed(Duration(milliseconds: 100), () {
              _controller.move(state.index == -1 ? widget.pageList.length - 1 : state.index, animation: false);
              GlobalConfigUtil.saveReaderProgress(state.index == -1 ? 0.0 : state.index.toDouble());
              if (GlobalConfiguration().getInt('textLayOutDirection') == 1) {
                widget.progressBloc.dispatch(state.index == -1 ? 0 : widget.pageList.length - 1 - state.index.toDouble());
              } else {
                widget.progressBloc.dispatch(state.index.toDouble());
              }
            });
          }
        }
      },
      child: Container(
        height: ScreenUtil.getInstance().screenHeight,
        width: GlobalConfiguration().getDouble('viewWidth') + ScreenUtil.getInstance().getWidth(36),
        child: TransformerPageView(
            loop: false,
            transformer: ReaderThemeConfig.transformers[GlobalConfigUtil.getInt('flipType')],
            index: lastInitPage,
            controller: _controller,
            itemCount: widget.pageList.length,
            scrollDirection: Axis.horizontal,
            onPageChanged: (index) {
              widget.onPageChanged(index);
            },
            itemBuilder: (BuildContext context, int index) {
              return Stack(
                children: <Widget>[
                  Container(
                      padding: EdgeInsets.symmetric(horizontal: ScreenUtil.getInstance().getWidth(18), vertical: ScreenUtil.getInstance().getWidth(18)),
                      decoration:
                          BoxDecoration(color: _readerTheme.backgroundColor, boxShadow: GlobalConfiguration().getInt('flipType') == 1 ? [BoxShadow(color: Colors.black54, blurRadius: 20)] : null),
                      child: Column(
                        children: <Widget>[
                          PageTitle(
                            title: widget.title,
                            index: GlobalConfiguration().getInt('textLayOutDirection') == 1 ? widget.pageList.length - index : index + 1,
                            count: widget.pageList.length,
                          ),
                          Expanded(
                              child: Container(
                                  width: GlobalConfigUtil.getDouble('viewWidth'),
                                  padding: EdgeInsets.only(left: widget.pageList[index].padLeft),
                                  child: GlobalConfigUtil.getInt('taskType') == 4
                                      ? InkWell(
                                          onTap: () {
                                            widget.openBottomMenu();
                                          },
                                          child: CanvasText(
                                            text: widget.pageList[index].list,
                                            viewHeight: GlobalConfigUtil.getDouble('viewHeight'),
                                          ),
                                        )
                                      : GlobalConfiguration().getInt('textLayOutDirection') == 1
                                          ? Row(
                                              mainAxisAlignment: MainAxisAlignment.end,
                                              children: getChild(widget.pageList[index].charList),
                                            )
                                          : ReaderBody(
                                              lineCharList: widget.pageList[index].charList,
                                              openMarkedMenu: (Rect rect, int id, String content) {
                                                widget.openMarkedMenu(rect, id, content);
                                              },
                                              handleMakeBookmark: (BookMark value) {
                                                widget.handleMakeBookmark(value.title);
                                              },
                                              handleDrawUnderline: (BookMark value) {
                                                widget.handleDrawUnderline(value);
                                              },
                                              handleShare: (String value) {
                                                widget.handleShare(value);
                                              },
                                              handleSearch: (String value) {
                                                widget.handleSearch(value);
                                              },
                                              openBottomMenu: () {
                                                widget.openBottomMenu();
                                              },
                                              handleRecovery: (String value) {
                                                widget.handleRecovery(value);
                                              },
                                              toolBarBloc: toolbarBloc,
                                            ))),
                        ],
                      )),
                ],
              );
            }),
      ),
    );
  }

//
  String processChars(String chars) {
    return chars.replaceAll('!', '！').replaceAll('?', '？');
  }

  List<Widget> getChild(List<LineChar> list) {
    ReaderTheme _readerTheme = ReaderThemeConfig.instance.list[GlobalConfiguration().getInt('themeIndex')];
    list = list.reversed.toList();
    List<Widget> extList = [];
    double lastPointY = 0;
    double lastPointX = 0;
    list.forEach((LineChar e) {
      List<TextSpan> children = [];
      String keyPrefix = ReaderThemeConfig.fontSizeList[GlobalConfiguration().getInt('fontSize')].toString() + '-';
      double columnWidth = ReaderThemeConfig.textSizeMap[keyPrefix + e.tag].width;
      children.add(
        TextSpan(
            children: e.lineGroup
                .map((char) => char.isMarked && char.maskType == 2
                    ? SpecialTextSpan(
                        text: processChars(GlobalConfiguration().getBool('simplifiedChinese') ? char.chars : zhTW(char.chars)) + (char == e.lineGroup.last ? '\n' : ''),
                        actualText: char.chars,
                        recognizer: TapGestureRecognizer()
                          ..onTapUp = (d) {
                            lastPointY = d.globalPosition.dy;
                            lastPointX = d.globalPosition.dx;
                          }
                          ..onTap = () {
                            if (GlobalConfiguration().getBool('specialTapped') != null) {
                              GlobalConfiguration().updateValue('specialTapped', true);
                            } else {
                              GlobalConfiguration().addValue('specialTapped', true);
                            }
                            print(DateUtil.getNowDateMs());
                            double left;
                            double top;
                            left = MediaQuery.of(context).size.width * 0.05 + (lastPointX / MediaQuery.of(context).size.width) * MediaQuery.of(context).size.width * 0.1;
                            if (lastPointY < MediaQuery.of(context).size.width * 0.4) {
                              top = lastPointY + MediaQuery.of(context).size.width * 0.2;
                            } else {
                              top = lastPointY - 30;
                            }
                            Rect rect = Rect.fromLTWH(left, top, MediaQuery.of(context).size.width * 0.8, MediaQuery.of(context).size.width * 0.2);
                            widget.openMarkedMenu(rect, char.id, char.actualChars);
                            print('special text tapped');
                          },
                        style: TextStyle(
                            fontSize: e.fontSize,
                            backgroundColor: char.isMarked && char.maskType == 2 ? _readerTheme.markColor : Colors.transparent,
                            height: 1.2,
                            color: char.isTitle ? _readerTheme.titleColor : _readerTheme.fontColor,
                            letterSpacing: 0,
                            fontWeight: e.tag == 'h1' || e.tag == 'h2' || char.bold ? FontWeight.bold : FontWeight.normal))
                    : TextSpan(
                        text: processChars(GlobalConfiguration().getBool('simplifiedChinese') ? char.chars : zhTW(char.chars)) + (e.lineGroup.last == char ? '\n' : ''),
                        style: TextStyle(
                            fontSize: e.fontSize,
                            backgroundColor: char.isMarked && char.maskType == 1 ? Colors.green : Colors.transparent,
                            height: 1.2,
                            letterSpacing: 0,
                            color: char.isTitle ? _readerTheme.titleColor : _readerTheme.fontColor,
                            fontWeight: e.tag == 'h1' || e.tag == 'h2' || char.bold ? FontWeight.bold : FontWeight.normal)))
                .toList()),
      );
      ExtendedText ext = getExtendRich(children, e.fullTextIndex);
      Widget container = Container(
        width: columnWidth * 2,
//        padding: EdgeInsets.symmetric(horizontal: columnWidth / 4),
        height: GlobalConfiguration().getDouble('viewHeight'),
        alignment: Alignment.topCenter,
        decoration: BoxDecoration(border: Border(right: BorderSide(color: _readerTheme.fontColor.withOpacity(0.6), width: 0.5))),
        child: Column(
          children: <Widget>[
            Container(
              width: columnWidth,
              height: e.xOffset,
//              color: Colors.teal,
            ),
            Expanded(
              child: ext,
            )
          ],
        ),
      );

      extList.add(container);
    });
    return extList;
  }

  ExtendedText getExtendRich(List<TextSpan> list, int fullTextIndex) {
    MyExtendedMaterialTextSelectionControls cts = MyExtendedMaterialTextSelectionControls(
        toolbarBloc: toolbarBloc,
        fullTextIndex: fullTextIndex,
        handleMakeBookMark: (BookMark value) {
          widget.handleMakeBookmark(value.title);
        },
        handleDrawUnderline: (BookMark value) {
          widget.handleDrawUnderline(value);
        },
        handleMyCopy: (value) {
          widget.handleMyCopy(value);
        },
        handleMySelectAll: (value) {
          widget.handleMySelectAll(value);
        },
        handleMyCut: (value) {
          widget.handleMyCut(value);
        },
        handleMyPaste: (value) {
          widget.handleMyPaste(value);
        },
        handleSearch: (value) {
          widget.handleSearch(value);
        },
        handleShare: (value) {
          widget.handleShare(value);
        },
        handleRecovery: (value) {
          widget.handleRecovery(value);
        });
    ExtendedText extendedText = ExtendedText.rich(
      TextSpan(children: list),
      textAlign: TextAlign.center,
      selectionColor: Color(0xFFC8EBFA),
      softWrap: true,
      onTap: () {
        if (GlobalConfiguration().getBool('specialTapped') != null && GlobalConfiguration().getBool('specialTapped')) {
          GlobalConfiguration().updateValue('specialTapped', false);
        } else {
          if (GlobalConfiguration().getBool('tapped') != null && GlobalConfiguration().getBool('tapped')) {
            toolbarBloc.dispatch(true);
            GlobalConfiguration().updateValue('tapped', false);
          } else {
            widget.openBottomMenu();
          }
        }
      },
      selectionEnabled: true,
      textSelectionControls: cts,
    );
    return extendedText;
  }

  void hideToolbar() {}
}
