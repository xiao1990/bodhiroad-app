import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:bodhiroad/merits/bloc/merits_bloc.dart';
import 'package:bodhiroad/merits/bloc/merits_event.dart';
import 'package:bodhiroad/task/read/bloc/task_bloc.dart';
import 'package:bodhiroad/task/read/bloc/task_event.dart';
import 'package:bodhiroad/usercenter/bean/user.dart';
import 'package:bodhiroad/usercenter/bloc/user/bloc.dart';
import 'package:bodhiroad/utils/HttpUtils.dart';
import 'package:global_configuration/global_configuration.dart';

import 'login_event.dart';
import 'login_state.dart';

class LoginBloc extends Bloc<LoginEvent, LoginState> {
  static final LoginBloc _loginBlocSingleton = new LoginBloc._internal();

  factory LoginBloc() {
    return _loginBlocSingleton;
  }

  LoginBloc._internal();

  LoginState get initialState => new LoginState(statusCode: 0);

  TaskBloc taskBloc = TaskBloc();
  MeritsBloc meritsBloc = MeritsBloc();
  UserBloc userBloc = UserBloc();

  @override
  Stream<LoginState> mapEventToState(
    LoginEvent event,
  ) async* {
    try {
      if (event is RegisteringEvent) {
        yield LoginState(statusCode: 1);
      } else if (event is RegisterSuccessEvent) {
        yield LoginState(user: event.user, statusCode: 2);
      } else if (event is RegisterErrorEvent) {
        yield LoginState(statusCode: 3);
      } else if (event is LoggingInEvent) {
        yield LoginState(statusCode: 4);
      } else if (event is LoggedOnEvent) {
        yield LoginState(statusCode: 5);
        HttpUtils.forceCreateInstance();

        taskBloc.dispatch(UserTaskEvent(forceUpdate: true));
      } else if (event is LoginErrorEvent) {
        yield LoginState(statusCode: 6);
      } else if (event is LoginOutEvent) {
        Map map = User().toJson();
        GlobalConfiguration().loadFromMap(map);
        HttpUtils.forceCreateInstance();
        taskBloc.dispatch(UserTaskEvent(forceUpdate: false));
        meritsBloc.dispatch(MeritsUpdateEvent(forceUpdate: true));
        yield LoginState(statusCode: 0);
      } else if (event is ExitEvent) {
        print('exit');
      }
    } catch (_, stackTrace) {
      print('$_ $stackTrace');
      yield currentState;
    }
  }
}
