import 'dart:io';

import 'package:bodhiroad/components/bodhiroad_share_task.dart';
import 'package:bodhiroad/components/timeline/timeline.dart';
import 'package:bodhiroad/components/timeline/timeline_model.dart';
import 'package:bodhiroad/config/CommonConfig.dart';
import 'package:bodhiroad/config/GlobalColorCOnfig.dart';
import 'package:bodhiroad/theme/bloc.dart';
import 'package:bodhiroad/usercenter/bean/task_statistic.dart';
import 'package:bodhiroad/usercenter/bean/task_statistic_day.dart';
import 'package:bodhiroad/usercenter/bloc/task_statistic/statistic_bloc.dart';
import 'package:bodhiroad/usercenter/bloc/task_statistic/statistic_event.dart';
import 'package:bodhiroad/usercenter/bloc/task_statistic/statistic_state.dart';
import 'package:bodhiroad/usercenter/bloc/task_statistic/task_scroll_bloc.dart';
import 'package:bodhiroad/utils/GlobalConfigUtil.dart';
import 'package:flustars/flustars.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:screenshot/screenshot.dart';

class TaskStatisticsPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: StatisticsContent(),
    );
  }
}

class StatisticsContent extends StatefulWidget {
  @override
  _StatisticsContentState createState() => _StatisticsContentState();
}

class _StatisticsContentState extends State<StatisticsContent> {
  ScrollController _scrollController;
  TaskScrollBloc _taskScrollBloc = TaskScrollBloc();
  StatisticBloc _statisticBloc = StatisticBloc();
  List<TaskStatistic> list = [];
  AppThemeBloc appThemeBloc = AppThemeBloc();
  ScreenshotController screenshotController = ScreenshotController();
  File _imageFile;
  @override
  void initState() {
    _scrollController = ScrollController(initialScrollOffset: 0);
    _scrollController.addListener(() {
      _taskScrollBloc.dispatch(_scrollController.position.pixels);
    });
    _statisticBloc.dispatch(StatisticLoadDataEvent());
    super.initState();
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  showUpdateDialog(BuildContext context, TaskStatistic model) {
    showDialog(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context) {
          return StatefulBuilder(
            builder: (BuildContext context, setState) {
              return BodhiRoadShareTaskDialog(
                model: model,
              );
            },
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder(
      bloc: appThemeBloc,
      builder: (context, ThemeChangeState appThemeState) {
        return NestedScrollView(
          controller: _scrollController,
          headerSliverBuilder: (context, boxIsScrolled) {
            return [
              BlocBuilder(
                bloc: _taskScrollBloc,
                builder: (context, state) {
                  return SliverAppBar(
                    backgroundColor: appThemeState.themeConfig.sectionBgColor,
                    title: Text(
                      "我的菩提路",
                      textScaleFactor: 1.0,
                      style: TextStyle(color: appThemeState.themeConfig.mainTextColor, fontSize: GlobalConfigUtil.getTitleFontSize()),
                    ),
                    centerTitle: false,
                    pinned: true,
                    floating: true,
                    forceElevated: boxIsScrolled,
//                    bottom: PreferredSize(
//                      preferredSize: Size.fromHeight(90),
//                      child: BlocBuilder(
//                          bloc: _statisticBloc,
//                          condition: (prev, curt) {
//                            if (curt is StatisticLoadDataByCataState) {
//                              return true;
//                            } else {
//                              return false;
//                            }
//                          },
//                          builder: (context, state) {
//                            return state is StatisticLoadDataByCataState ? TotalCountChart(appThemeBloc: appThemeBloc, list: state.taskStatistics, maxDays: state.maxDays) : Container();
//                          }),
//                    ),
                  );
                },
              )
            ];
          },
          body: Container(
            decoration: BoxDecoration(color: appThemeState.themeConfig.mainBgColor, image: DecorationImage(fit: BoxFit.cover, image: AssetImage('assets/texture/bgt2.jpg'))),
            child: BlocBuilder(
              bloc: _statisticBloc,
              condition: (prev, curt) {
                if (curt is StatisticLoadDataState) {
                  list = curt.taskStatistics;
                  return true;
                } else {
                  return false;
                }
              },
              builder: (context, state) {
                return list.length > 0
                    ? getTimeLine(list, appThemeState)
                    : Center(
                        child: Text(
                          '暂无数据！',
                          textScaleFactor: 1.0,
                        ),
                      );
              },
            ),
          ),
        );
      },
    );
  }

  Widget getTimeLine(List<TaskStatistic> list, ThemeChangeState appThemeState) {
    List<TimelineModel> items = List.generate(list.length, (index) {
      return TimelineModel(
          Container(
            decoration: BoxDecoration(
              image: DecorationImage(image: AssetImage('assets/texture/item_bg.jpg'), alignment: Alignment.bottomCenter),
              boxShadow: [BoxShadow(color: appThemeState.themeConfig.shadowColor, blurRadius: 8)],
              borderRadius: BorderRadius.all(Radius.circular(8)),
              color: appThemeState.themeConfig.sectionBgColor,
            ),
            margin: EdgeInsets.only(top: 20),

            child: Column(
              mainAxisSize: MainAxisSize.max,
              children: <Widget>[
                Container(
                  height: 48,
                  padding: EdgeInsets.symmetric(horizontal: 15),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.vertical(top: Radius.circular(8)),
//                      color: appThemeState.themeConfig.titleBarBgColor,
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text(
                        list[index].dateString,
                        textScaleFactor: 1.0,
                        style: TextStyle(fontWeight: FontWeight.bold, color: Colors.black54, fontSize: GlobalConfigUtil.getTabMainBodyFontSize()),
                      ),
                      Material(
                        color: Colors.transparent,
                        child: Ink(
                          child: InkWell(
                            onTap: () async {
                              showUpdateDialog(context, list[index]);
                            },
                            child: Icon(MdiIcons.shareVariant, size: ScreenUtil.getInstance().getSp(45), color: Colors.black54),
                          ),
                        ),
                      )
                    ],
                  ),
                ),
                Divider(
                  color: appThemeState.themeConfig.lightBorderColor,
                  height: 0.5,
                )
              ]..addAll(list[index]
                  .list
                  .map((unit) => Container(
                        height: ScreenUtil.getInstance().getWidth(140),
                        alignment: Alignment.centerLeft,
                        padding: EdgeInsets.symmetric(horizontal: ScreenUtil.getInstance().getWidth(30)),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Text(
                              unit.taskName ?? 'null',
                              textScaleFactor: 1.0,
                              style: TextStyle(fontSize: GlobalConfigUtil.getTabMainBodyFontSize()),
                            ),
                            Text(
                              (unit.taskType == 3 ? (unit.taskCount / 60).round().toString() : unit.taskCount.toString()) + CommonConfig.taskCountUnit[unit.taskType] ?? 'null',
                              textScaleFactor: 1.0,
                              style: TextStyle(fontSize: GlobalConfigUtil.getTabMainBodyFontSize(), color: appThemeState.themeConfig.embellishColorA),
                            ),
                          ],
                        ),
                        decoration: BoxDecoration(
                          border: Border(bottom: unit == list[index].list.last ? BorderSide.none : BorderSide(color: appThemeState.themeConfig.mainBorderColor, width: 0.2)),
//                              image: DecorationImage(
//                                  fit: BoxFit.cover,
//                                  image: AssetImage('assets/item-back2.jpg'))
                        ),
                      ))
                  .toList()),
            ),
            // ),
          ),
          isFirst: index == 0,
          isLast: index == list.length - 1,
          position: TimelineItemPosition.random,
          iconBackground: Color(0xFFF2F2F2),
          icon: Icon(
            Icons.keyboard_arrow_down,
            size: 10,
            color: appThemeState.themeConfig.embellishColorA,
          ));
    }).toList();
    return Timeline(
      children: items,
      position: TimelinePosition.Left,
      lineColor: appThemeState.themeConfig.subTitleTextColor,
      lineWidth: 0.5,
      physics: BouncingScrollPhysics(),
    );
  }
}

class TotalCountChart extends StatelessWidget {
  final List<TaskStatisticDays> list;
  final int maxDays;
  final AppThemeBloc appThemeBloc;
  int currentIndex = -1;
  TotalCountChart({this.list, this.maxDays = 1, this.appThemeBloc});
  int selectedIndex = -1;
  @override
  Widget build(BuildContext context) {
    TaskScrollBloc bloc = TaskScrollBloc();
    StatisticBloc _statisticBloc = StatisticBloc();
    return BlocBuilder(
      bloc: appThemeBloc,
      builder: (context, ThemeChangeState appThemeBloc) {
        return BlocBuilder(
            bloc: bloc,
            builder: (context, state) {
              return Card(
                  elevation: 0,
                  margin: EdgeInsets.all(0),
                  color: appThemeBloc.themeConfig.sectionBgColor,
                  child: Container(
                      height: 226 - state,
                      decoration: BoxDecoration(borderRadius: BorderRadius.circular(8)),
                      child: Column(
                        children: <Widget>[
                          Expanded(
                            child: BlocBuilder(
                              bloc: _statisticBloc,
                              condition: (prev, curt) {
                                if (curt is StatisticChangeCataState) {
                                  selectedIndex = curt.taskType;
                                  return true;
                                } else {
                                  return false;
                                }
                              },
                              builder: (context, selState) {
                                return Row(
                                  children: List.generate(list.length, (index) {
                                    return Container(
                                        height: 226 - state,
                                        width: MediaQuery.of(context).size.width / 5,
                                        child: Material(
                                          color: Colors.transparent,
                                          child: Ink(
                                            color: selectedIndex == list[index].taskType ? GlobalColorConfig.mainforegroundColor.withOpacity(0.2) : Colors.transparent,
                                            child: InkWell(
                                              onTap: () {
                                                if (list[index].taskType == selectedIndex) {
                                                  _statisticBloc.dispatch(StatisticChangeCataEvent(taskType: -1));
                                                } else {
                                                  _statisticBloc.dispatch(StatisticChangeCataEvent(taskType: list[index].taskType));
                                                }
                                              },
                                              child: Column(
                                                mainAxisAlignment: MainAxisAlignment.end,
                                                children: <Widget>[
                                                  Text(
                                                    list[index].totalDays.toString(),
                                                    textScaleFactor: 1.0,
                                                    style: TextStyle(color: GlobalColorConfig.mainforegroundColor),
                                                  ),
                                                  Container(
                                                    decoration: BoxDecoration(
                                                      borderRadius: BorderRadius.circular(10),
                                                      color: appThemeBloc.themeConfig.embellishColorA,
                                                    ),
                                                    width: 8,
                                                    height: maxDays > 0 ? (170 - state) * list[index].totalDays / maxDays : 0,
                                                  ),
                                                  Container(
                                                    alignment: Alignment.center,
                                                    child: Text(list[index].catalogName, textScaleFactor: 1.0, style: TextStyle(color: GlobalColorConfig.mainforegroundColor)),
                                                    height: 36,
                                                  )
                                                ],
                                              ),
                                            ),
                                          ),
                                        ));
                                  }).toList(),
                                );
                              },
                            ),
                          ),
                        ],
                      )));
            });
      },
    );
  }
}
