import 'dart:math' as math;
import 'dart:ui';

import 'package:bodhiroad/api/api.dart';
import 'package:bodhiroad/task/zen/bean/music.dart';
import 'package:bodhiroad/task/zen/bloc/music_bloc.dart';
import 'package:bodhiroad/task/zen/bloc/music_event.dart';
import 'package:bodhiroad/task/zen/bloc/music_state.dart';
import 'package:bodhiroad/utils/GlobalConfigUtil.dart';
import 'package:bodhiroad/utils/ToastUtils.dart';
import 'package:bodhiroad/utils/screen_utils.dart';
import 'package:flustars/flustars.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:global_configuration/global_configuration.dart';
import 'package:volume_watcher/volume_watcher.dart';

class ZenMusic extends StatefulWidget {
  final VoidCallback toggleCard;
  ZenMusic({this.toggleCard});
  @override
  _ZenMusicState createState() => _ZenMusicState();
}

class _ZenMusicState extends State<ZenMusic> with TickerProviderStateMixin {
  MusicBloc _musicBloc = MusicBloc();
  List<Music> musicList = [];
  Animation animation;
  Animation rotationAnimation;
  AnimationController _animationController;
  AnimationController _rotationController;
  Music currentMusic;
  int playingState = 0;
  double dragStartPosition = 0.0;
  double initVolume = 0.0;
  double userInitVolume = 1.0;
  double systemInitVolume = 0.0;
  double maxVolume = 0.0;
  double currentDragValue = 0;
  double ratio = 0.0;
  double volumeHeight = 0.0;
  double brightHeight = 0.0;
  double initVolumeHeight = 0.0;
  double brightness = 0.0;
  double initBrightness = 0.0;
  double brightnessOpacity = 0.0;
  double volumeOpacity = 0.0;
  Future<void> initPlatformState() async {
    initVolume = (await VolumeWatcher.getCurrentVolume).toDouble();
    brightness = await ScreenUtils.getBrightness();
    initBrightness = brightness;

    /// 系统初始音量,页面退出时需要还原
    systemInitVolume = initVolume;
    maxVolume = (await VolumeWatcher.getMaxVolume).toDouble();
    volumeHeight = 300 * systemInitVolume / maxVolume;
    brightHeight = 300 * brightness;
    initVolumeHeight = volumeHeight;
    print('系统初始亮度:' + brightness.toString());
    print('系统初始音量:' + initVolume.toString());
    print('系统最大音量:' + maxVolume.toString());
    if (!mounted) return;
  }

  getMusicList() async {
    musicList = SpUtil.getObjList('musicList', (value) => Music.fromJson(value), defValue: []);
    if (musicList.length == 0) {
      Map map = await Api.getZenMusicList();
      if (map['code'] == 200) {
        List dlist = map['data'];
        musicList = dlist.map((item) => Music.fromJson(item)).toList();

        SpUtil.putObjectList('musicList', musicList);
      }
    }
    if (currentMusic == null) {
      currentMusic = musicList.first;
    }
    setState(() {});
  }

  playMusic() {
    _musicBloc.dispatch(MusicPlayEvent(currentMusicId: currentMusic.id, musicUrl: currentMusic.musicUrl, state: 1 - playingState));
    SpUtil.putObject('currentMusic', currentMusic);
    setState(() {});
  }

  @override
  void dispose() {
    ScreenUtils.setBrightness(initBrightness);
//    _musicBloc.dispatch(MusicStopEvent());
    _animationController.dispose();
    _rotationController.dispose();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    initPlatformState();
    _animationController = AnimationController(value: 0.0, duration: Duration(milliseconds: 100), vsync: this);
    _rotationController = AnimationController(value: 0.0, duration: Duration(milliseconds: 12000), vsync: this);
    animation = Tween(begin: 1.0, end: 0.0).animate(_animationController);
    rotationAnimation = Tween(begin: -1.0, end: 0.0).animate(_rotationController);
    _animationController.addListener(() {
      setState(() {});
    });
    _rotationController.addListener(() {
      if (rotationAnimation.isCompleted) {
        _rotationController.reset();
        _rotationController.reverse(from: 1.0);
      }
    });
    currentMusic = SpUtil.getObj('currentMusic', (value) => Music.fromJson(value));

    getMusicList();
    GlobalConfigUtil.setObject('currentMusic', currentMusic);
  }

  showMusicList() {
    showModalBottomSheet(
        backgroundColor: Colors.transparent,
        context: context,
        builder: (BuildContext context) {
          return StatefulBuilder(builder: (context, state) {
            return Container(
              height: ScreenUtil.getInstance().screenHeight * 0.4,
              decoration: BoxDecoration(color: Colors.white, borderRadius: BorderRadius.vertical(top: Radius.circular(15))),
              child: Column(
                children: <Widget>[
                  VolumeWatcher(
                    onVolumeChangeListener: (num volume) {
                      print(volume);
                    },
                  ),
                  Container(
                      height: ScreenUtil.getInstance().getWidth(120),
                      alignment: Alignment.centerLeft,
                      padding: EdgeInsets.symmetric(horizontal: ScreenUtil.getInstance().getWidth(40)),
                      decoration: BoxDecoration(border: Border(bottom: BorderSide(color: Colors.black12, width: 0.5))),
                      child: Row(
                        children: <Widget>[
                          Icon(
                            Icons.queue_music,
                            size: 20,
                            color: Colors.black38,
                          ),
                          Container(
                            width: 8,
                          ),
                          Text('选择背景音乐'),
                        ],
                      )),
                  Expanded(
                      child: ListView.builder(
                    itemCount: musicList.length,
                    itemExtent: ScreenUtil.getInstance().getWidth(120),
                    itemBuilder: (context, index) {
                      return Material(
                          color: Colors.transparent,
                          borderRadius: BorderRadius.circular(15),
                          child: Ink(
                            child: InkWell(
                              onTap: () {
                                state(() {
                                  currentMusic = musicList[index];
                                  GlobalConfigUtil.setObject('currentMusic', currentMusic);
                                  _musicBloc.dispatch(MusicPlayEvent(currentMusicId: currentMusic.id, musicUrl: currentMusic.musicUrl, state: 1 - playingState));
                                  SpUtil.putObject('currentMusic', currentMusic);
                                  SpUtil.putInt('currentMusicIndex', index);
                                });
                                setState(() {});
                              },
                              child: Container(
                                decoration: BoxDecoration(border: Border(bottom: BorderSide(color: Color(0xFFF2F2F2), width: 0.5))),
                                padding: EdgeInsets.symmetric(horizontal: 15),
                                alignment: Alignment.centerLeft,
                                child: Row(
                                  children: <Widget>[
                                    Expanded(
                                      child: Text(
                                        musicList[index].musicTitle,
                                        style: TextStyle(
                                            fontSize: ScreenUtil.getInstance().getSp(36), color: musicList[index].id == currentMusic.id && playingState == 1 ? Color(0xFFA70400) : Colors.black54),
                                      ),
                                    ),
                                    Offstage(
                                      offstage: musicList[index].id != currentMusic.id,
                                      child: Container(
                                        width: ScreenUtil.getInstance().getWidth(120),
                                        child: Icon(
                                          Icons.album,
                                          size: ScreenUtil.getInstance().getSp(48),
                                          color: Color(0xFFA70400),
                                        ),
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            ),
                          ));
                    },
                  ))
                ],
              ),
            );
          });
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Stack(
      children: <Widget>[
        FadeInImage(
          placeholder: AssetImage(''),
          image: AssetImage('assets/' + currentMusic.musicCover),
          width: ScreenUtil.getInstance().screenWidth,
          height: ScreenUtil.getInstance().screenHeight,
          fit: BoxFit.cover,
        ),
        Container(
          height: ScreenUtil.getInstance().screenHeight - ScreenUtil.getInstance().getWidth(320),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              GestureDetector(
                  onVerticalDragUpdate: (DragUpdateDetails details) async {
                    ratio = (details.localPosition.dy - dragStartPosition) / (ScreenUtil.getInstance().screenHeight - ScreenUtil.getInstance().getWidth(320));
                    print(details.localPosition.dy - dragStartPosition);
                    print(ScreenUtil.getInstance().screenHeight - ScreenUtil.getInstance().getWidth(320));

                    // 减小亮度
                    if (ratio > 0) {
                      currentDragValue = math.max(0, brightness - ratio);
                      // 增大亮度
                    } else {
                      currentDragValue = math.min(1.0, brightness - ratio);
                      print('brightness - ratio=' + (brightness - ratio).toString());
                    }
                    print('distance=' + (details.localPosition.dy - dragStartPosition).toString() + '--ratio:' + ratio.toString() + '--brightness=' + brightness.toString());
                    ScreenUtils.setBrightness(currentDragValue);
                    setState(() {
                      brightHeight = math.min(300, 300 * currentDragValue);
                    });
                  },
                  onVerticalDragEnd: (DragEndDetails details) {
                    brightness = currentDragValue;
                    Future.delayed(Duration(milliseconds: 300), () {
                      setState(() {
                        brightnessOpacity = 0.0;
                      });
                    });
                  },
                  onVerticalDragStart: (DragStartDetails details) async {
                    dragStartPosition = details.localPosition.dy;
                    setState(() {
                      brightnessOpacity = 1.0;
                    });
                  },
                  child: Container(
                    width: ScreenUtil.getInstance().screenWidth / 3,
                    height: ScreenUtil.getInstance().screenHeight - ScreenUtil.getInstance().getWidth(320),
                    color: Colors.transparent,
                  )),
              Container(
                height: ScreenUtil.getInstance().getWidth(900),
                width: ScreenUtil.getInstance().screenWidth / 3,
                child: Container(
                  width: ScreenUtil.getInstance().screenWidth / 6,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      ClipRRect(
                        borderRadius: BorderRadius.vertical(top: Radius.circular(24)),
                        child: Container(
                          height: 360,
                          child: Stack(
                            alignment: Alignment.bottomCenter,
                            children: <Widget>[
                              AnimatedOpacity(
                                opacity: brightnessOpacity,
                                duration: Duration(milliseconds: 100),
                                child: Container(
                                  height: ScreenUtil.getInstance().getWidth(900),
                                  width: ScreenUtil.getInstance().screenWidth / 6,
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.vertical(top: Radius.circular(24)),
                                  ),
                                  alignment: Alignment.bottomCenter,
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.end,
                                    children: <Widget>[
                                      Expanded(
                                          child: Container(
                                        color: Colors.black.withOpacity(0.5),
                                      )),
                                      ClipRRect(
                                          borderRadius: BorderRadius.vertical(top: Radius.circular(0)),
                                          child: BackdropFilter(
                                            filter: ImageFilter.blur(sigmaY: 4, sigmaX: 4),
                                            child: Column(
                                              children: <Widget>[
                                                Container(
                                                  width: ScreenUtil.getInstance().screenWidth / 6,
                                                  height: brightHeight,
                                                  child: Container(
                                                    color: Colors.white.withOpacity(0.3),
                                                  ),
                                                ),
                                                Container(
                                                  height: ScreenUtil.getInstance().getWidth(150),
                                                  width: ScreenUtil.getInstance().screenWidth / 6,
                                                  decoration: BoxDecoration(
                                                    borderRadius: BorderRadius.vertical(bottom: Radius.circular(24)),
                                                    color: Colors.white.withOpacity(0.3),
                                                  ),
                                                  child: Icon(
                                                    Icons.brightness_5,
                                                    color: Colors.black54,
                                                  ),
                                                )
                                              ],
                                            ),
                                          )),
                                    ],
                                  ),
                                ),
                              ),
                              AnimatedOpacity(
                                opacity: volumeOpacity,
                                duration: Duration(milliseconds: 100),
                                child: Container(
                                  height: ScreenUtil.getInstance().getWidth(900),
                                  width: ScreenUtil.getInstance().screenWidth / 6,
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.vertical(top: Radius.circular(24)),
                                  ),
                                  alignment: Alignment.bottomCenter,
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.end,
                                    children: <Widget>[
                                      Expanded(
                                          child: Container(
                                        color: Colors.black.withOpacity(0.5),
                                      )),
                                      ClipRRect(
                                          borderRadius: BorderRadius.vertical(top: Radius.circular(0)),
                                          child: BackdropFilter(
                                            filter: ImageFilter.blur(sigmaY: 4, sigmaX: 4),
                                            child: Column(
                                              children: <Widget>[
                                                Container(
                                                  width: ScreenUtil.getInstance().screenWidth / 6,
                                                  height: volumeHeight,
                                                  child: Container(
                                                    color: Colors.white.withOpacity(0.3),
                                                  ),
                                                ),
                                                Container(
                                                  height: ScreenUtil.getInstance().getWidth(150),
                                                  width: ScreenUtil.getInstance().screenWidth / 6,
                                                  decoration: BoxDecoration(
                                                    borderRadius: BorderRadius.vertical(bottom: Radius.circular(24)),
                                                    color: Colors.white.withOpacity(0.3),
                                                  ),
                                                  child: Icon(
                                                    Icons.volume_up,
                                                    color: Colors.black54,
                                                  ),
                                                )
                                              ],
                                            ),
                                          )),
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              GestureDetector(
                  onVerticalDragUpdate: (DragUpdateDetails details) async {
                    ratio = (details.localPosition.dy - dragStartPosition) / (ScreenUtil.getInstance().screenHeight - ScreenUtil.getInstance().getWidth(320));
                    // 减小音量
                    if (ratio > 0) {
                      currentDragValue = math.max(0, initVolume - maxVolume * ratio);
                      // 增大音量
                    } else {
                      currentDragValue = math.min(maxVolume, initVolume - maxVolume * ratio);
                    }
                    VolumeWatcher.setVolume(currentDragValue);
                    setState(() {
                      volumeHeight = math.min(300, 300 * currentDragValue / maxVolume);
                      print(volumeHeight);
                    });
                  },
                  onVerticalDragEnd: (DragEndDetails details) {
                    initVolume = currentDragValue;
                    initVolumeHeight = volumeHeight;
                    Future.delayed(Duration(milliseconds: 300), () {
                      setState(() {
                        volumeOpacity = 0.0;
                      });
                    });
                  },
                  onVerticalDragStart: (DragStartDetails details) async {
                    dragStartPosition = details.localPosition.dy;
                    setState(() {
                      volumeOpacity = 1.0;
                    });
                  },
                  child: Container(
                    width: ScreenUtil.getInstance().screenWidth / 3,
                    height: ScreenUtil.getInstance().screenHeight - ScreenUtil.getInstance().getWidth(320),
                    color: Colors.transparent,
                  )),
            ],
          ),
        ),
        GestureDetector(
            onTap: () {
              if (animation.value < 1) {
                widget.toggleCard();
              }
            },
            child: Container(
                alignment: Alignment.bottomLeft,
                child: Container(
                    height: ScreenUtil.getInstance().getWidth(320),
                    alignment: Alignment.centerLeft,
                    padding: EdgeInsets.symmetric(horizontal: ScreenUtil.getInstance().getWidth(40)),
                    child: AnimatedBuilder(
                        animation: animation,
                        builder: (context, child) {
                          return child;
                        },
                        child: BlocBuilder(
                            bloc: _musicBloc,
                            condition: (prevState, curtState) {
                              if (curtState is MusicLoadingState) {
                                // 正在加载
                                playingState = 1;
                              } else if (curtState is MusicLoadedState) {
                                // 加载完成
                                playingState = 2;
                              } else if (curtState is MusicPlayingState) {
                                _rotationController.repeat();
                                // 正在播放
                                playingState = 3;
                              } else if (curtState is MusicPauseState) {
                                _rotationController.stop(canceled: false);
                                // 暂停播放
                                playingState = 4;
                              } else if (curtState is MusicFinishState) {
                                _rotationController.stop(canceled: true);
                                // 播放完成
                                playingState = 5;
                              } else if (curtState is MusicErrorState) {
                                // 播放错误
                                playingState = 0;
                                ToastUtils.fail('加载失败!');
                                _rotationController.reset();
                              }
                              return true;
                            },
                            builder: (context, musicState) {
                              return Container(
                                  child: ClipRRect(
                                borderRadius: BorderRadius.horizontal(right: Radius.circular(ScreenUtil.getInstance().getWidth(80))),
                                child: BackdropFilter(
                                  filter: ImageFilter.blur(sigmaY: animation.value * 6, sigmaX: animation.value * 6),
                                  child: Row(
                                    children: <Widget>[
                                      GestureDetector(
                                        onTap: () {
//                                          _rotationController.forward();
//                                          _rotationController.repeat();
//                                          _rotationController.
                                          if (animation.value > 0) {
                                            _animationController.forward();
                                          } else {
                                            _animationController.reverse();
                                          }
                                        },
                                        child: Container(
                                          width: ScreenUtil.getInstance().getWidth(160),
                                          height: ScreenUtil.getInstance().getWidth(160),
                                          decoration: BoxDecoration(
                                            color: Colors.white.withOpacity(1 - animation.value / 2),
                                            borderRadius: BorderRadius.horizontal(
                                                left: Radius.circular(ScreenUtil.getInstance().getWidth(80)), right: Radius.circular(ScreenUtil.getInstance().getWidth(80 * (1 - animation.value)))),
                                          ),
                                          alignment: Alignment.center,
                                          child: playingState == 1
                                              ? SpinKitDoubleBounce(
                                                  color: Colors.white,
                                                  size: ScreenUtil.getInstance().getWidth(80),
                                                )
                                              : RotationTransition(
                                                  turns: _rotationController,
                                                  child: ClipOval(
                                                    child: Container(
                                                      width: ScreenUtil.getInstance().getWidth(140),
                                                      height: ScreenUtil.getInstance().getWidth(140),
                                                      child: Image.asset(
                                                        'assets/${currentMusic.musicCover}',
                                                        fit: BoxFit.fitWidth,
                                                      ),
                                                    ),
                                                  ),
                                                ),
                                        ),
                                      ),
                                      Opacity(
                                        opacity: animation.value,
                                        child: Container(
                                          height: ScreenUtil.getInstance().getWidth(160),
                                          width: animation.value * (ScreenUtil.getInstance().screenWidth - ScreenUtil.getInstance().getWidth(240)),
                                          decoration: BoxDecoration(
                                            color: Colors.white.withOpacity(0.5),
                                            borderRadius: BorderRadius.horizontal(right: Radius.circular(ScreenUtil.getInstance().getWidth(80))),
                                          ),
                                          child: Row(
                                            mainAxisAlignment: MainAxisAlignment.end,
                                            mainAxisSize: MainAxisSize.max,
                                            children: <Widget>[
                                              Expanded(
                                                child: Offstage(
                                                    offstage: animation.value < 0.1,
                                                    child: Container(
                                                      alignment: Alignment.centerLeft,
                                                      width: (ScreenUtil.getInstance().screenWidth - ScreenUtil.getInstance().getWidth(240)) / 5,
                                                      child: Text(
                                                        currentMusic.musicTitle ?? '还没有选择音乐',
                                                        maxLines: 2,
                                                        overflow: TextOverflow.ellipsis,
                                                      ),
                                                    )),
                                              ),
                                              Offstage(
                                                  offstage: animation.value < 0.2,
                                                  child: GestureDetector(
                                                    onTap: () {
                                                      print(SpUtil.getInt('currentMusicIndex'));
                                                      if (SpUtil.getInt('currentMusicIndex') == 0) {
                                                        ToastUtils.fail('已经是第一首了!');
                                                      } else {
                                                        currentMusic = musicList[SpUtil.getInt('currentMusicIndex') - 1];
                                                        SpUtil.putInt('currentMusicIndex', SpUtil.getInt('currentMusicIndex') - 1);
                                                        playMusic();
                                                        GlobalConfigUtil.setObject('currentMusic', currentMusic);
                                                      }
                                                    },
                                                    child: Container(
                                                      alignment: Alignment.center,
                                                      width: (ScreenUtil.getInstance().screenWidth - ScreenUtil.getInstance().getWidth(240)) / 5,
                                                      child: ImageIcon(
                                                        AssetImage('assets/icons/prev.png'),
                                                        size: ScreenUtil.getInstance().getSp(48),
                                                      ),
                                                    ),
                                                  )),
                                              Offstage(
                                                  offstage: animation.value < 0.4,
                                                  child: GestureDetector(
                                                      onTap: () {
                                                        if (playingState < 4 && playingState != 0) {
                                                          _musicBloc.dispatch(MusicPauseEvent());
                                                        } else if (playingState == 4) {
                                                          _musicBloc.dispatch(MusicResumeEvent());
                                                        } else {
                                                          playMusic();
                                                        }
                                                      },
                                                      child: Container(
                                                        alignment: Alignment.center,
                                                        width: (ScreenUtil.getInstance().screenWidth - ScreenUtil.getInstance().getWidth(240)) / 5,
                                                        child: ImageIcon(
                                                          AssetImage('assets/icons/${playingState > 3 || playingState == 0 ? 'play' : 'pause'}.png'),
                                                          color: Colors.black38,
                                                          size: ScreenUtil.getInstance().getSp(90),
                                                        ),
                                                      ))),
                                              Offstage(
                                                  offstage: animation.value < 0.6,
                                                  child: GestureDetector(
                                                    onTap: () {
                                                      print(SpUtil.getInt('currentMusicIndex'));
                                                      if (SpUtil.getInt('currentMusicIndex') == musicList.length - 1) {
                                                        ToastUtils.fail('已经是最后一首了!');
                                                      } else {
                                                        currentMusic = musicList[SpUtil.getInt('currentMusicIndex') + 1];
                                                        SpUtil.putInt('currentMusicIndex', SpUtil.getInt('currentMusicIndex') + 1);
                                                        GlobalConfigUtil.setObject('currentMusic', currentMusic);
                                                        playMusic();
                                                      }
                                                    },
                                                    child: Container(
                                                      alignment: Alignment.center,
                                                      width: (ScreenUtil.getInstance().screenWidth - ScreenUtil.getInstance().getWidth(240)) / 5,
                                                      child: ImageIcon(
                                                        AssetImage('assets/icons/next.png'),
                                                        size: ScreenUtil.getInstance().getSp(48),
                                                      ),
                                                    ),
                                                  )),
                                              Offstage(
                                                  offstage: animation.value < 0.8,
                                                  child: GestureDetector(
                                                    onTap: () {
                                                      showMusicList();
                                                    },
                                                    child: Container(
                                                      alignment: Alignment.center,
                                                      width: (ScreenUtil.getInstance().screenWidth - ScreenUtil.getInstance().getWidth(240)) / 5,
                                                      child: ImageIcon(
                                                        AssetImage('assets/icons/list.png'),
                                                        size: ScreenUtil.getInstance().getSp(48),
                                                      ),
                                                    ),
                                                  ))
                                            ],
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ));
                            })))))
      ],
    ));
  }
}
