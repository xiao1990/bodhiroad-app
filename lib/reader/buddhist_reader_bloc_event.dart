import 'package:bodhiroad/reader/bean/bookmark.dart';

abstract class BuddhistReaderBlocEvent {
  BuddhistReaderBlocEvent();
}

/// 重载阅读器事件
class InitReaderBlocEvent extends BuddhistReaderBlocEvent {
  InitReaderBlocEvent();
}

/// 重载阅读器事件
class ReloadReaderEvent extends BuddhistReaderBlocEvent {
  ReloadReaderEvent();
}

/// 简单重载阅读器事件
class SimpleReloadReaderEvent extends BuddhistReaderBlocEvent {
  final int flag;
  SimpleReloadReaderEvent({this.flag}) : super();
}

/// 简单重载阅读器重载完成事件
class SimpleReloadFinishReaderEvent extends BuddhistReaderBlocEvent {
  SimpleReloadFinishReaderEvent() : super();
}

/// 切换主题事件
class ChangeThemeEvent extends BuddhistReaderBlocEvent {
  ChangeThemeEvent();
}

/// 章节跳转事件
class ChapterJumpEvent extends BuddhistReaderBlocEvent {
  final int index;
  final double position;
  ChapterJumpEvent({this.index, this.position});
}

/// 阅读进度
class ReadProgressEvent extends BuddhistReaderBlocEvent {
  final double progress;
  ReadProgressEvent({this.progress});
  @override
  bool get reload => true;
}

// 阅读完成
class ReaderFinishEvent extends BuddhistReaderBlocEvent {
  @override
  String toString() => 'ReaderFinishEvent';
}

/// 到达最后一页
class ReaderReachBottomEvent extends BuddhistReaderBlocEvent {
  @override
  String toString() => 'ReaderReachBottomEvent';
}

/// 未到达最后一页
class ReaderUnReachBottomEvent extends BuddhistReaderBlocEvent {
  @override
  String toString() => 'ReaderUnReachBottomEvent';
}

/// 重新开始阅读
class ReaderRestartEvent extends BuddhistReaderBlocEvent {
  @override
  String toString() => 'ReaderRestartEvent';
}

/// 收藏事件
class GetFavoriteEvent extends BuddhistReaderBlocEvent {
  GetFavoriteEvent();
  @override
  String toString() => 'GetFavoriteEvent';
}

/// 删除收藏事件
class DeleteFavoriteEvent extends BuddhistReaderBlocEvent {
  DeleteFavoriteEvent();
  @override
  String toString() => 'DeleteFavoriteEvent';
}

/// 收藏事件
class AddFavoriteEvent extends BuddhistReaderBlocEvent {
  final int status;
  AddFavoriteEvent({this.status});
  @override
  String toString() => 'AddFavoriteEvent';
}

/// 收藏事件
class AddBookMarkEvent extends BuddhistReaderBlocEvent {
  final int startIndex;
  final int endIndex;
  final int maskType;
  final BookMark bookMark;
  AddBookMarkEvent({this.startIndex, this.endIndex, this.maskType, this.bookMark});
  @override
  String toString() => 'AddBookMarkEvent';
}

/// 打开底部菜单事件
class OpenBottomMenuEvent extends BuddhistReaderBlocEvent {
  OpenBottomMenuEvent();
  @override
  String toString() => 'OpenBottomMenuEvent';
}

/// 打开底部菜单事件
class OpenBottomMenuSuccessEvent extends BuddhistReaderBlocEvent {
  OpenBottomMenuSuccessEvent();
  @override
  String toString() => 'OpenBottomMenuSuccessEvent';
}

/// 打开底部菜单事件
class BottomMenuSlidingEvent extends BuddhistReaderBlocEvent {
  final double progress;
  BottomMenuSlidingEvent(this.progress);
  @override
  String toString() => 'BottomMenuSlidingEvent';
}

/// 打开底部菜单事件
class RemoveUnderLineEvent extends BuddhistReaderBlocEvent {
  final int id;
  RemoveUnderLineEvent(this.id);
  @override
  String toString() => 'RemoveUnderLineEvent';
}
