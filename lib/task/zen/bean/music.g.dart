// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'music.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Music _$MusicFromJson(Map<String, dynamic> json) {
  return Music(
      id: json['id'] as int,
      musicTitle: json['musicTitle'] as String,
      musicCover: json['musicCover'] as String,
      musicUrl: json['musicUrl'] as String,
      musicDuration: json['musicDuration'] as int);
}

Map<String, dynamic> _$MusicToJson(Music instance) => <String, dynamic>{
      'id': instance.id,
      'musicTitle': instance.musicTitle,
      'musicCover': instance.musicCover,
      'musicUrl': instance.musicUrl,
      'musicDuration': instance.musicDuration
    };
