import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:bodhiroad/api/api.dart';
import 'package:bodhiroad/index/bean/taskStatisticSimple.dart';
import 'package:bodhiroad/task/read/bean/task.dart';
import 'package:bodhiroad/task/read/bean/task_catalog.dart';
import 'package:bodhiroad/task/read/bean/task_classify.dart';
import 'package:bodhiroad/task/read/bean/task_record.dart';
import 'package:bodhiroad/utils/GlobalConfigUtil.dart';
import 'package:bodhiroad/utils/ToastUtils.dart';
import 'package:flustars/flustars.dart';
import 'package:global_configuration/global_configuration.dart';

import 'task_event.dart';
import 'task_state.dart';

class TaskBloc extends Bloc<TaskEvent, TaskState> {
  static final TaskBloc _taskBlocSingleton = new TaskBloc._internal();

  factory TaskBloc() {
    return _taskBlocSingleton;
  }

  TaskBloc._internal();

  TaskState get initialState => TaskInitState();

  @override
  void onTransition(Transition<TaskEvent, TaskState> transition) {
    super.onTransition(transition);
  }

  int currentItemIndex = -1;

  @override
  Stream<TaskState> mapEventToState(
    TaskEvent event,
  ) async* {
    try {
//      初始化
      if (event is TaskInitEvent) {
        yield TaskInitState();
//      所有功课列表
      } else if (event is GetTaskGatalogEvent) {
        var rest = await Api.getAllTask();
        if (rest['code'] == 200) {
          List<dynamic> dlist = rest['data'];
          List<TaskCatalog> taskList = dlist.map((tk) => TaskCatalog.fromJson(tk)).toList();
          yield GetTaskGatalogState(list: taskList);
        } else {
          yield GetTaskGatalogState(list: []);
        }
//        添加功课
      } else if (event is AddTaskEvent) {
        event.list[event.taskCatalogIndex].list[event.itemIndex].state = 1;
        yield GetTaskGatalogState(list: event.list, tsp: DateTime.now().millisecondsSinceEpoch);
        var res = await Api.addTask(data: {"taskId": event.taskId});
        if (res['code'] == 200) {
          SpUtil.remove('userTaskList');
          this.dispatch(UserTaskEvent(forceUpdate: true));
        }
//        删除功课
      } else if (event is DeleteTaskEvent) {
        var res = await Api.deleteTask(data: {"userTaskId": event.taskId});
        if (res['code'] == 200) {
          SpUtil.remove('userTaskList');
          _taskBlocSingleton.dispatch(DeleteTaskSuccessEvent());
        }
//      报数
      } else if (event is ReportTaskEvent) {
        currentItemIndex = event.itemIndex;
        reportTask(event.taskId, event.count, event.taskType);
//        报数完成
      } else if (event is ReportTaskFinishEvent) {
        yield ReportTaskSuccessState(tsp: DateTime.now().millisecondsSinceEpoch);
        SpUtil.remove("userTaskList");
        this.dispatch(UserTaskEvent(forceUpdate: true));
//        删除完成
      } else if (event is DeleteTaskSuccessEvent) {
        yield DeleteTaskSuccessState();
        this.dispatch(UserTaskEvent(forceUpdate: true));

//        获取用户功课
      } else if (event is UserTaskEvent) {
        print('开始获取功课列表');
        yield UserTaskQueryingState();
        List<TaskClassify> taskList = [];
        List<TaskStatisticSimpleBean> total = [];
        List<TaskStatisticSimpleBean> today = [];
        print(DateTime.now().millisecondsSinceEpoch);
        if (!event.forceUpdate) {
          taskList = SpUtil.getObjList('userTaskList', (item) => TaskClassify.fromJson(item));
          total = SpUtil.getObjList('userTaskList_total', (item) => TaskStatisticSimpleBean.fromJson(item));
          today = SpUtil.getObjList('userTaskList_today', (item) => TaskStatisticSimpleBean.fromJson(item));
        }
        if (taskList == null || taskList.length == 0) {
          var rest = await Api.getUserTask();
          if (rest != null && rest['code'] == 200) {
            List<dynamic> dlist1 = rest['data']['userTask'];
            List<dynamic> dlist2 = rest['data']['taskStatisticTotal'];
            List<dynamic> dlist3 = rest['data']['taskStatisticToday'];
            taskList = dlist1.map((item) => TaskClassify.fromJson(item)).toList();
            total = dlist2.map((item) => TaskStatisticSimpleBean.fromJson(item)).toList();
            today = dlist3.map((item) => TaskStatisticSimpleBean.fromJson(item)).toList();
            SpUtil.putObjectList('userTaskList', taskList);
            SpUtil.putObjectList('userTaskList_total', total);
            SpUtil.putObjectList('userTaskList_today', today);
            print('获取功课列表成功');
          }
        }
        yield UserTaskState(list: taskList, today: today, total: total);
      } else if (event is AddTaskRecordEvent) {
        saveRecord(0, 0);
      } else if (event is UpdateTaskRecordEvent) {
        saveRecord(1, 1);
      }
    } catch (_, stackTrace) {
      print('$_ $stackTrace');
      yield currentState;
    }
  }

  /// 报数
  void reportTask(int taskId, int taskCount, int taskType) async {
    if (SpUtil.getString('token') == null || SpUtil.getString('token').length == 0) {
      ToastUtils.fail('请先登录！');
      return;
    }
    TaskRecord taskRecord = TaskRecord(id: 0, taskId: taskId, taskCount: taskCount, taskType: taskType, state: 1, source: 0);
    var res = await Api.taskReport(data: taskRecord.toJson());
    if (res['code'] == 200) {
      _taskBlocSingleton.dispatch(ReportTaskFinishEvent());
    }
  }

  /// 正常功课记录【operate 0 开始 1 完成】
  void saveRecord(int operate, int state) async {
    if (SpUtil.getString('token') != null && SpUtil.getString('token').length > 0) {
      TaskRecord taskRecord = TaskRecord(
          id: GlobalConfiguration().getInt("currentTaskRecordId"),
          taskId: GlobalConfiguration().getInt("id"),
          taskCount: 1,
          taskType: GlobalConfiguration().getInt("taskType"),
          state: state,
          source: GlobalConfiguration().getInt("source"));
      if (operate == 0) {
        var res = await Api.taskRecordAdd(data: taskRecord.toJson());
        if (res['code'] == 200) {
          if (GlobalConfiguration().getInt('currentTaskRecordId') == null) {
            GlobalConfiguration().addValue('currentTaskRecordId', res['data'] as int);
          } else {
            GlobalConfigUtil.updateValue('currentTaskRecordId', res['data'] as int);
          }
          if (GlobalConfiguration().getInt("source") == 0) {
//        sortUserTask(GlobalConfiguration().getInt("taskId"));
          }
        }
      } else {
        // TODO
        Map map = await Api.taskRecordUpdate(data: taskRecord.toJson());
        if (map['code'] == 200) {
          ToastUtils.success('本次功课已完成！');
          _taskBlocSingleton.dispatch(UserTaskEvent(forceUpdate: true));
        }
        if (taskRecord.state == 1) {
          GlobalConfigUtil.updateValue("currentTaskRecordId", 0);
        }
      }
    }
  }

  /// 排序调整
  void sortUserTask(int taskId) async {
    List<Task> taskList = [];
    int index = -1;
    taskList = SpUtil.getObjList('userTaskList', (item) => Task.fromJson(item));
    taskList.forEach((item) {
      if (item.taskId == taskId) {
        index = taskList.indexOf(item);
        return;
      }
    });
    taskList.insert(0, taskList[index]);
    taskList.removeAt(index + 1);
    bool rest = await SpUtil.putObjectList('userTaskList', taskList);
    if (rest) {
//    taskBloc.dispatch(UserTaskEvent(forceUpdate: false));
    }
  }
}
