import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:bodhiroad/utils/GlobalConfigUtil.dart';

class LightAjustBloc extends Bloc<double, double> {
  static final LightAjustBloc _lightAjustBlocSingleton = new LightAjustBloc._internal();
  factory LightAjustBloc() {
    return _lightAjustBlocSingleton;
  }
  LightAjustBloc._internal();

  double get initialState => GlobalConfigUtil.getDouble('brightness');

  @override
  Stream<double> mapEventToState(
    double event,
  ) async* {
    try {
      yield event;
    } catch (_, stackTrace) {
      print('$_ $stackTrace');
      yield currentState;
    }
  }
}
