import 'package:bodhiroad/usercenter/bean/user.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class UserState extends Equatable {
  UserState([List props = const []]) : super(props);
}

class InitialUserState extends UserState {
  final User user;
  final int tsp;
  InitialUserState({this.user, this.tsp}) : super([user, tsp]);
}

class UserUpdateState extends UserState {
  // final User user;
  // final int tsp;
  // UserUpdateState({this.user, this.tsp}) : super([user, tsp]);
}
