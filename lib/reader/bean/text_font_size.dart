import 'package:bodhiroad/reader/bean/text_layout_size.dart';
import 'package:json_annotation/json_annotation.dart';

part 'text_font_size.g.dart';

@JsonSerializable()
class TextFontSize {
  String tagName;
  TextLayoutSize size;
  TextFontSize({this.tagName, this.size});
  factory TextFontSize.fromJson(Map<String, dynamic> json) {
    return _$TextFontSizeFromJson(json);
  }
  Map<String, dynamic> toJson() => _$TextFontSizeToJson(this);
}
