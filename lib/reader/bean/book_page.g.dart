// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'book_page.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PageBean _$PageBeanFromJson(Map<String, dynamic> json) {
  return PageBean(
      uuid: json['uuid'] as String,
      pageIndex: json['pageIndex'] as int,
      chapterIndex: json['chapterIndex'] as int,
      list: (json['list'] as List)
          ?.map((e) =>
              e == null ? null : CharBean.fromJson(e as Map<String, dynamic>))
          ?.toList(),
      charList: (json['charList'] as List)
          ?.map((e) =>
              e == null ? null : LineChar.fromJson(e as Map<String, dynamic>))
          ?.toList(),
      firstCatalog: json['firstCatalog'] == null
          ? null
          : CatalogBean.fromJson(json['firstCatalog'] as Map<String, dynamic>),
      lastCatalog: json['lastCatalog'] == null
          ? null
          : CatalogBean.fromJson(json['lastCatalog'] as Map<String, dynamic>),
      padLeft: (json['padLeft'] as num)?.toDouble(),
      bookMarkList: (json['bookMarkList'] as List)
          ?.map((e) => e == null
              ? null
              : BookmarkBean.fromJson(e as Map<String, dynamic>))
          ?.toList(),
      bookMarkPageList: (json['bookMarkPageList'] as List)
          ?.map((e) => e == null
              ? null
              : BookSingleMarkBean.fromJson(e as Map<String, dynamic>))
          ?.toList(),
      status: json['status'],
      pageTitle: json['pageTitle'] as String,
      fullTextIndex: json['fullTextIndex'] as int,
      nodeIndex: json['nodeIndex'] as int,
      marked: json['marked'] as int,
      pageHeight: (json['pageHeight'] as num)?.toDouble());
}

Map<String, dynamic> _$PageBeanToJson(PageBean instance) => <String, dynamic>{
      'uuid': instance.uuid,
      'pageIndex': instance.pageIndex,
      'nodeIndex': instance.nodeIndex,
      'chapterIndex': instance.chapterIndex,
      'list': instance.list,
      'charList': instance.charList,
      'firstCatalog': instance.firstCatalog,
      'lastCatalog': instance.lastCatalog,
      'status': instance.status,
      'pageTitle': instance.pageTitle,
      'padLeft': instance.padLeft,
      'bookMarkList': instance.bookMarkList,
      'bookMarkPageList': instance.bookMarkPageList,
      'marked': instance.marked,
      'pageHeight': instance.pageHeight,
      'fullTextIndex': instance.fullTextIndex
    };
