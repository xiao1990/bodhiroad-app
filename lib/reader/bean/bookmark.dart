import 'package:json_annotation/json_annotation.dart';
part 'bookmark.g.dart';

/// 书签类
@JsonSerializable()
class BookMark {
  /// 书签id
  int id;

  /// 所在文章id
  int articleId;

  /// 类型《功课，大藏经，文海，常诵佛经》
  int type;

  /// 标记类型
  int markType;

  /// 书签标题
  String title;

  /// 书签开始结束位置在全文中索引
  int startIndex;

  /// 书签开始结束位置在全文中索引
  int actualStartIndex;

  /// 书签结束位置在全文中索引
  int endIndex;
  int actualEndIndex;

  /// 书签所在页面
  int pageIndex;

  /// 所在段落索引
  int paragraphIndex;

  /// 所在章节索引
  int chapterIndex;

  /// 是否为书签
  bool isMarked;

  /// 书签位置
  double yOffset;

  /// 当被分到多个段落时判断是否为第一个分段
  bool isFirst;

  BookMark(
      {this.id,
      this.articleId,
      this.type,
      this.title,
      this.actualStartIndex,
      this.actualEndIndex,
      this.startIndex,
      this.endIndex,
      this.pageIndex,
      this.chapterIndex,
      this.paragraphIndex,
      this.yOffset,
      this.isFirst,
      this.markType = 1,
      this.isMarked});

  factory BookMark.fromJson(Map<String, dynamic> json) {
    return _$BookMarkFromJson(json);
  }
  Map<String, dynamic> toJson() => _$BookMarkToJson(this);
}
