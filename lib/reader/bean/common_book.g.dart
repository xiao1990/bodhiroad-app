// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'common_book.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CommonBook _$CommonBookFromJson(Map<String, dynamic> json) {
  return CommonBook(
      id: json['id'] as String,
      list: (json['list'] as List)
          ?.map((e) =>
              e == null ? null : Buddhist.fromJson(e as Map<String, dynamic>))
          ?.toList(),
      title: json['title'] as String);
}

Map<String, dynamic> _$CommonBookToJson(CommonBook instance) =>
    <String, dynamic>{
      'id': instance.id,
      'title': instance.title,
      'list': instance.list
    };
