import 'dart:async';

import 'package:bodhiroad/api/api.dart';
import 'package:bodhiroad/config/CommonConfig.dart';
import 'package:bodhiroad/reader/bean/buddhist.dart';
import 'package:bodhiroad/theme/bloc.dart';
import 'package:bodhiroad/utils/parameter_convert_cn.dart';
import 'package:flustars/flustars.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

import 'book_list_page.dart';
import 'book_view_page.dart';

class BuddhistArticleList extends StatefulWidget {
  final String catelogId;
  final String from;
  final String source;
  final String taskType;
  final int version;
  final String catalogName;

  BuddhistArticleList({this.catelogId, this.from, this.source, this.taskType, this.version, this.catalogName});

  @override
  _BuddhistArticleListState createState() => _BuddhistArticleListState();
}

class _BuddhistArticleListState extends State<BuddhistArticleList> {
  Future f;

  @override
  void initState() {
    f = getBuddhist();
    super.initState();
  }

  Future<List<Buddhist>> getBuddhist() async {
    List<Buddhist> list2;
    List<dynamic> oriList = SpUtil.getObjectList(widget.catelogId);
    if (oriList != null && oriList.length > 0) {
      list2 = oriList.map((item) => Buddhist.fromJson(item)).toList();
    } else {
      dynamic map = await Api.getBuddhistByCatalogId(data: {'catalogId': widget.catelogId, 'classifyId': widget.source});
      List<dynamic> list = map['data'];
      list2 = list.map((item) => Buddhist.fromJson(item)).toList();
      saveBuddhist(widget.catelogId, list2);
    }
    return list2;
  }

  saveBuddhist(String key, List<Buddhist> objList) async {
    SpUtil.putObjectList(CommonConfig.BUDDHIST_ARTICLE_LIST + key, objList);
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder(
      bloc: AppThemeBloc(),
      builder: (context, ThemeChangeState appThemeState) {
        return Scaffold(
          appBar: AppBar(
            elevation: 0,
            leading: InkWell(
              child: Icon(Icons.arrow_back, color: appThemeState.themeConfig.mainTextColor),
              onTap: () {
                Navigator.of(context).pop();
              },
            ),
            title: Text(
              ParameterConvertUtils.parseParameterCN(widget.catalogName),
              textScaleFactor: 1.0,
              style: TextStyle(color: appThemeState.themeConfig.mainTextColor, fontWeight: FontWeight.bold),
            ),
            backgroundColor: appThemeState.themeConfig.sectionBgColor,
          ),
          body: Container(
            decoration: BoxDecoration(color: Colors.red, image: DecorationImage(fit: BoxFit.cover, image: AssetImage('assets/texture/bgt2.jpg'))),
            child: FutureBuilder<List<Buddhist>>(
                future: f,
                builder: (BuildContext context, AsyncSnapshot<List<Buddhist>> snapshot) {
                  switch (snapshot.connectionState) {
                    case ConnectionState.none:
                      return Text('');
                    case ConnectionState.active:
                      return Text('');
                    case ConnectionState.waiting:
                      return SpinKitThreeBounce(
                        color: Colors.blueGrey,
                        size: 20.0,
                      );
                    case ConnectionState.done:
                      if (snapshot.hasError) {
                        return Text('Error: ${snapshot.error}');
                      }
                      return BookList(
                        list: snapshot.data,
                        from: widget.from,
                        centerClick: (Buddhist item) {
                          String bookName = ParameterConvertUtils.convertCN(item.title);
                          Navigator.of(context).push(MaterialPageRoute(builder: (BuildContext context) {
                            return BookView(
                              fileUrl: item.url,
                              source: int.parse(widget.source),
                              id: item.id,
                              taskType: 0,
                              bookName: bookName,
                              version: item.version,
                            );
                          }));
//                Routes.router.navigateTo(context, 'bookView?fileUrl=${item.url}&source=${widget.source}&id=${item.id}&taskId=0&taskType=${widget.taskType}&bookName=${bookName}');
                        },
                      );
                    default:
                      return Text('');
                  }
                }),
          ),
        );
      },
    );
  }
}
