import 'package:screen/screen.dart';

class ScreenUtils {
  static Future<double> getBrightness() async {
    return await Screen.brightness;
  }

  static setBrightness(double brightness) {
    Screen.setBrightness(brightness);
  }

  static void keepOn() {
    Screen.keepOn(true);
  }

  static Future<bool> getIsKeepOn() async {
    return await Screen.isKeptOn;
  }
}
