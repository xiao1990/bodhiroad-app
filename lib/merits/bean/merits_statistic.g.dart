// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'merits_statistic.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

MeritsStatistic _$MeritsStatisticFromJson(Map<String, dynamic> json) {
  return MeritsStatistic(
      meritsType: json['meritsType'] as int,
      list: (json['list'] as List)
          ?.map((e) => e == null
              ? null
              : MeritsStatisticDay.fromJson(e as Map<String, dynamic>))
          ?.toList());
}

Map<String, dynamic> _$MeritsStatisticToJson(MeritsStatistic instance) =>
    <String, dynamic>{'meritsType': instance.meritsType, 'list': instance.list};
