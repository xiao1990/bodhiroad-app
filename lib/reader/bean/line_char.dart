import 'package:bodhiroad/reader/bean/char.dart';
import 'package:json_annotation/json_annotation.dart';
part 'line_char.g.dart';

/// 书签类
/// 文本行
///  阅读器布局计算以行为单位
@JsonSerializable()
class LineChar {
  /// 行标签《h1,h2,h3...》
  String tag;

  /// 行字符串组
  ///   一行当中可能包含普通字符串和书签字符串
  List<Char> lineGroup = [];

  /// 行起始偏移量
  double xOffset = 0.0;

  /// 行字体大小
  double fontSize;

  /// 行垂直偏移量
  double yOffset = 0.0;

  /// 每一行起始位置在全文中的索引
  int fullTextIndex = 0;
  LineChar({this.tag, this.xOffset, this.yOffset, this.fontSize, this.lineGroup, this.fullTextIndex});

  factory LineChar.fromJson(Map<String, dynamic> json) {
    return _$LineCharFromJson(json);
  }
  Map<String, dynamic> toJson() => _$LineCharToJson(this);
}
