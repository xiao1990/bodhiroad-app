// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'task_statistic_day.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

TaskStatisticDays _$TaskStatisticDaysFromJson(Map<String, dynamic> json) {
  return TaskStatisticDays(
      taskType: json['taskType'] as int,
      totalDays: json['totalDays'] as int,
      catalogName: json['catalogName'] as String);
}

Map<String, dynamic> _$TaskStatisticDaysToJson(TaskStatisticDays instance) =>
    <String, dynamic>{
      'taskType': instance.taskType,
      'totalDays': instance.totalDays,
      'catalogName': instance.catalogName
    };
