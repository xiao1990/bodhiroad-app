// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'task_statistic.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

TaskStatistic _$TaskStatisticFromJson(Map<String, dynamic> json) {
  return TaskStatistic(
      dateString: json['dateString'] as String,
      list: (json['list'] as List)
          ?.map((e) => e == null
              ? null
              : TaskStatisticUnit.fromJson(e as Map<String, dynamic>))
          ?.toList());
}

Map<String, dynamic> _$TaskStatisticToJson(TaskStatistic instance) =>
    <String, dynamic>{'dateString': instance.dateString, 'list': instance.list};
