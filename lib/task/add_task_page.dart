import 'dart:convert';
import 'dart:ui';

import 'package:bodhiroad/components/content_preview_page.dart';
import 'package:bodhiroad/config/CommonConfig.dart';
import 'package:bodhiroad/config/GlobalColorCOnfig.dart';
import 'package:bodhiroad/config/ThemeConfig.dart';
import 'package:bodhiroad/reader/bean/common_book.dart';
import 'package:bodhiroad/task/task_list_page.dart';
import 'package:bodhiroad/theme/bloc.dart';
import 'package:bodhiroad/utils/GlobalConfigUtil.dart';
import 'package:bodhiroad/utils/ToastUtils.dart';
import 'package:flustars/flustars.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

import 'read/bean/task.dart';
import 'read/bean/task_catalog.dart';
import 'read/bloc/task_bloc.dart';
import 'read/bloc/task_event.dart';
import 'read/bloc/task_state.dart';

class AddTask extends StatefulWidget {
  @override
  _AddTaskState createState() => _AddTaskState();
}

class _AddTaskState extends State<AddTask> with SingleTickerProviderStateMixin {
  List<CommonBook> jinglist = [];
  List<TaskCatalog> taskList = [];
  TabController _tabController;
  TaskBloc _taskBloc;
  AppThemeBloc appThemeBloc = AppThemeBloc();
  @override
  void initState() {
    _taskBloc = TaskBloc();
    _tabController = TabController(initialIndex: 0, vsync: this, length: 5);
    _taskBloc.dispatch(GetTaskGatalogEvent());
    super.initState();
  }

  addTask(int taskId, int catalogIndex, int itemIndex, int state, List<TaskCatalog> list, BuildContext context, ThemeConfig themeConfig) {
    if (state == 1) {
      ToastUtils.fail('已添加过该功课!');
      return;
    } else if (SpUtil.getString('token') == '') {
      ToastUtils.fail('请先登录!');
    } else {
      _taskBloc.dispatch(AddTaskEvent(list: list, taskCatalogIndex: catalogIndex, taskId: taskId, itemIndex: itemIndex));
      ToastUtils.success('添加成功!');
      setState(() {});
    }
    // _taskBloc.dispatch(AddTaskEvent())
//     DioUtils().post('/task/addUserTask', (res) {
//       print(res);
//       // _tabController.index
//       int currentItemIndex = taskList[_tabController.index].list.indexOf(task);
//       task.id = int.parse(res.toString());
//       task.state = 1;
//       taskList[_tabController.index].list[currentItemIndex] = task;
//       Scaffold.of(context).showSnackBar(SnackBar(
//         content: Text('功课添加成功'),
//         backgroundColor: GlobalColorConfig.mainBackgroundColor,
//       ));
// //      List<dynamic> dlist = res;
// //      taskList = dlist.map((tk) => TaskCatalog.fromJson(tk)).toList();
//       setState(() {});
// //      SpUtil.putString('taskList', jsonEncode(res));
//     }, params: jsonDecode(jsonEncode(task)), errorCallBack: () {});
  }

  navigationTo(Task task) {}

  List<TaskCatalog> clist = [];

  @override
  void dispose() {
    _tabController.dispose();
    // _taskBloc.dispose();
    // TODO: implement dispose
    super.dispose();
  }

  showUpdateDialog(BuildContext context, Task task) {
    showDialog(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context) {
          return StatefulBuilder(
            builder: (BuildContext context, setState) {
              return ContentPreviewPage(
                content: task.description,
                title: task.taskName,
              );
            },
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder(
      bloc: appThemeBloc,
      builder: (context, ThemeChangeState appThemeState) {
        return Scaffold(
            backgroundColor: appThemeState.themeConfig.mainBgColor,
            appBar: AppBar(
              backgroundColor: appThemeState.themeConfig.sectionBgColor,
              title: Text(
                '全部功课',
                textScaleFactor: 1.0,
                style: TextStyle(color: appThemeState.themeConfig.mainTextColor, fontSize: GlobalConfigUtil.getTitleFontSize()),
              ),
              elevation: 4,
              leading: IconButton(
                icon: Icon(Icons.close),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
              bottom: SearchCondition(_tabController, taskList),
            ),
            body: Container(
              decoration: BoxDecoration(color: Colors.transparent, image: DecorationImage(fit: BoxFit.cover, image: AssetImage('assets/texture/bgt2.jpg'))),
              child: BlocBuilder(
                bloc: _taskBloc,
                condition: (previousState, currentState) {
                  if (currentState is GetTaskGatalogState) {
                    return true;
                  } else {
                    return false;
                    // clist = [];
                  }
                  // return true;
                },
                builder: (BuildContext context, taskState) {
                  return TabBarView(
                      controller: _tabController,
                      children: taskState is GetTaskGatalogState
                          ? taskState.list
                              .map((commonTask) => TaskList(
                                    appThemeBloc: appThemeBloc,
                                    list: commonTask.list,
                                    from: 'addTask',
                                    centerClick: (Task task) {
                                      showUpdateDialog(context, task);
                                    },
                                    rightClick: (int taskId, int itemIndex, int state) {
                                      addTask(taskId, taskState.list.indexOf(commonTask), itemIndex, state, taskState.list, context, appThemeState.themeConfig);
                                    },
                                    rightAction: true,
                                  ))
                              .toList()
                          : List.generate(5, (index) {
                              return Center(
                                child: SpinKitThreeBounce(
                                  color: GlobalColorConfig.buttonColorA,
                                  size: 30.0,
                                ),
                              );
                            }).toList());
                },
              ),
            ));
      },
    );
  }
}

class SearchCondition extends StatelessWidget with PreferredSizeWidget {
  final TabController tabController;
  final List<TaskCatalog> list;

  SearchCondition(this.tabController, this.list);

  Future<List<TaskCatalog>> loadCatalog() async {
    String str = await rootBundle.loadString('assets/task_catalog.json');
    List list = jsonDecode(str);
    return list.map((item) => TaskCatalog.fromJson(item)).toList();
  }

  @override
  Widget build(BuildContext context) {
    return TabBar(
      controller: tabController,
      indicatorSize: TabBarIndicatorSize.label,
      indicatorColor: Color(0xFFA70400),
      labelColor: Color(0xFFA70400),
      labelStyle: TextStyle(fontWeight: FontWeight.bold),
      unselectedLabelColor: GlobalColorConfig.mainTextColor,
      unselectedLabelStyle: TextStyle(fontWeight: FontWeight.normal),
      isScrollable: false,
      labelPadding: EdgeInsets.all(0),
      indicatorPadding: EdgeInsets.all(0),
      tabs: CommonConfig.taskCatalogList
          .map((item) => Center(
                child: Padding(
                  padding: EdgeInsets.symmetric(vertical: 15, horizontal: 4),
                  child: Text(
                    item,
                    textScaleFactor: 1.0,
                    style: TextStyle(fontFamily: 'songti'),
                  ),
                ),
              ))
          .toList(),
    );
  }

  @override
  Size get preferredSize => Size.fromHeight(ScreenUtil.getInstance().getWidth(136));
}
