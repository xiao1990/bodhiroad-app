import 'dart:convert';
import 'dart:math' as math;

import 'package:bodhiroad/common/FontFamilyName.dart';
import 'package:bodhiroad/config/CommonConfig.dart';
import 'package:bodhiroad/index/bloc/headslide_bloc.dart';
import 'package:bodhiroad/reader/bean/buddhist.dart';
import 'package:bodhiroad/reader/bean/buddhist_catalog.dart';
import 'package:bodhiroad/reader/pages/book_view_page.dart';
import 'package:bodhiroad/utils/GlobalConfigUtil.dart';
import 'package:bodhiroad/utils/parameter_convert_cn.dart';
import 'package:flustars/flustars.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:global_configuration/global_configuration.dart';
import 'package:extended_nested_scroll_view/extended_nested_scroll_view.dart' as extended;

class QldzjCoverIndex extends StatefulWidget {
  final String from;
  final BuddhistCatalogBean article;
  final String source;
  final String taskType;
  final int index;
  const QldzjCoverIndex({this.from, this.article, this.taskType, this.source, this.index});
  @override
  _QldzjCoverIndexState createState() => _QldzjCoverIndexState();
}

class _QldzjCoverIndexState extends State<QldzjCoverIndex> {
  Future f;
  int isFavorite = SpUtil.getInt('favorite_' + GlobalConfiguration().getInt('id').toString() + GlobalConfiguration().getInt('source').toString());
  List<Buddhist> list2 = [];
  HeadSlideBloc _headSlideBloc;
  ScrollController _scrollController;
  @override
  void initState() {
    f = getBuddhist();
    _headSlideBloc = HeadSlideBloc();
    _headSlideBloc.dispatch(0);
    _scrollController = ScrollController();
    _scrollController.addListener(() {
      if (_scrollController.offset > 72) {
        _headSlideBloc.dispatch(
            (_scrollController.offset - 72) / (ScreenUtil.getInstance().screenWidth * 0.5 - kToolbarHeight - 72 - ScreenUtil.getInstance().getWidth(GlobalConfigUtil.getDouble('statusBarHeight'))));
      } else {
        _headSlideBloc.dispatch(0);
      }
    });
    super.initState();
  }

  Future<List<Buddhist>> getBuddhist() async {
    List<dynamic> oriList = SpUtil.getObjectList(CommonConfig.QLDZJ_CATALOG + widget.article.id);
    if (oriList != null && oriList.length > 0) {
      list2 = oriList.map((item) => Buddhist.fromJson(item)).toList();
    } else {
      String content = await rootBundle.loadString('assets/qldzj/' + widget.article.id + ".json");
      List contentList = json.decode(content);
      list2 = contentList.map((item) => Buddhist.fromJson(item)).toList();
      SpUtil.putObjectList(CommonConfig.QLDZJ_CATALOG + widget.article.id, list2);
    }
    return list2;
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  List<Widget> getVerticalText(String text) {
    if (text.indexOf('（') > 0) {
      text = text.substring(0, text.indexOf('（'));
    }
    text = text.replaceAll("《", '').replaceAll("》", '');
    text = text.substring(0, math.min(14, text.length));

    int cols = (text.length / 7).ceil();
    List<Widget> list = [];
    for (int i = 0; i < cols; i++) {
      String text2 = text.substring(i * 7, math.min(i * 7 + 7 - 1, text.length - 1) + 1).replaceAll('', '\n');
      list.add(Container(
        width: ScreenUtil.getInstance().getWidth(23),
        height: ScreenUtil.getInstance().screenHeight * 0.5,
        alignment: Alignment.topCenter,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
              width: ScreenUtil.getInstance().getWidth(23),
//              decoration: BoxDecoration(border: Border(left: BorderSide(color: Colors.black26, width: 0.5))),
              alignment: i == 0 ? Alignment.topCenter : Alignment.topRight,
              child: Text(
                text2,
                style: TextStyle(
                    decoration: TextDecoration.none,
                    color: Colors.black87,
                    fontSize: ScreenUtil.getInstance().getSp(16),
                    shadows: [Shadow(color: Colors.black26, blurRadius: 6)],
                    fontWeight: FontWeight.normal,
                    fontFamily: FontFamilyName.SONGTI),
              ),
            )
          ],
        ),
      ));
    }
    return list.reversed.toList();
  }

  toRead(int index) {
    String bookName = ParameterConvertUtils.convertCN(list2[index].title);
    Navigator.of(context).push(MaterialPageRoute(builder: (BuildContext context) {
      return BookView(
        fileUrl: list2[index].url,
        source: int.parse(widget.source),
        id: list2[index].id,
        taskType: int.parse(widget.taskType),
        bookName: bookName,
        version: list2[index].version,
      );
    }));
  }

  @override
  Widget build(BuildContext context) {
//    final AppThemeBloc appThemeBloc = BlocProvider.of<AppThemeBloc>(context);
    return Scaffold(
        backgroundColor: Colors.white,
        body: extended.NestedScrollView(
            controller: _scrollController,
            pinnedHeaderSliverHeightBuilder: () {
              return kToolbarHeight + ScreenUtil.getInstance().getWidth(GlobalConfigUtil.getDouble('statusBarHeight'));
            },
            headerSliverBuilder: (context, boxIsScrolled) {
              return [
                BlocBuilder(
                    bloc: _headSlideBloc,
                    builder: (context, state) {
                      return SliverAppBar(
                        titleSpacing: 0,
                        bottom: PreferredSize(
                            child: Opacity(
                              opacity: math.min(1.0, state),
                              child: Container(
                                width: ScreenUtil.getInstance().screenWidth,
                                color: Colors.white,
                                height: kToolbarHeight,
                                child: Row(
                                  children: <Widget>[
                                    IconButton(
                                      icon: Icon(
                                        Icons.arrow_back,
                                        color: Colors.black87,
                                      ),
                                      onPressed: () {
                                        Navigator.pop(context);
                                      },
                                    ),
                                    Text(
                                      widget.article.title,
                                      style: TextStyle(
                                        fontSize: GlobalConfigUtil.getTitleFontSize(),
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            ),
                            preferredSize: Size.fromHeight(kToolbarHeight)),
                        automaticallyImplyLeading: false,
                        pinned: true,
                        floating: true,
                        forceElevated: false,
                        elevation: 0,
                        expandedHeight: ScreenUtil.getInstance().screenWidth * 0.5,
                        flexibleSpace: Container(
                          height: ScreenUtil.getInstance().screenWidth * 0.5 + GlobalConfigUtil.getDouble('statusBarHeight'),
                          decoration: BoxDecoration(
                            color: Colors.white,
                          ),
                          alignment: Alignment.center,
                          child: Opacity(
                            opacity: 1 - math.min(1.0, state),
                            child: Stack(
                              fit: StackFit.expand,
                              children: <Widget>[
                                Image.asset(
                                  'assets/texture/banner3.jpg',
                                  fit: BoxFit.fitWidth,
                                  alignment: Alignment.bottomCenter,
                                ),
                                Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    Text(
                                      widget.article.title,
                                      style: TextStyle(
                                          fontSize: GlobalConfigUtil.getTitleFontSize() * 1.5,
                                          letterSpacing: 10,
                                          color: Color(0xFF064560),
                                          fontWeight: FontWeight.bold,
                                          shadows: [Shadow(color: Colors.black12, blurRadius: 8)]),
                                    ),
                                    Text(
                                      widget.article.index,
                                      style: TextStyle(
                                          fontSize: GlobalConfigUtil.getSubTitleFontSize(),
                                          letterSpacing: 4,
                                          color: Color(0xFF064560),
                                          fontWeight: FontWeight.bold,
                                          shadows: [Shadow(color: Colors.black12, blurRadius: 8)]),
                                    )
                                  ],
                                )
                              ],
                            ),
                          ),
                        ),
                      );
                    })
              ];
            },
            body: FutureBuilder<List<Buddhist>>(
                future: f,
                builder: (context, AsyncSnapshot<List<Buddhist>> snapShot) {
                  return snapShot.data != null && snapShot.data.length > 0
                      ? Container(
                          decoration: BoxDecoration(image: DecorationImage(image: AssetImage('assets/texture/bgt2.png'), fit: BoxFit.cover)),
                          child: ListView.separated(
                              itemCount: snapShot.data == null ? 0 : snapShot.data.length + 1,
                              padding: EdgeInsets.zero,
                              physics: BouncingScrollPhysics(),
                              separatorBuilder: (context, index) {
                                return Container(
                                  height: 0.5,
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: List.generate(32, (index) {
                                      return Container(
                                        width: 4,
                                        height: 0.5,
                                        color: Colors.black12,
                                      );
                                    }).toList(),
                                  ),
                                );
                              },
                              itemBuilder: (BuildContext context, index) {
                                return index == 0
                                    ? Container(
                                        height: ScreenUtil.getInstance().getWidth(150),
                                        color: Colors.white,
                                        alignment: Alignment.center,
                                        child: Stack(
                                          alignment: Alignment.center,
                                          children: <Widget>[
                                            Image.asset(
                                              'assets/texture/bk3.png',
                                              color: Color(0xFFc0bbb2),
                                              width: ScreenUtil.getInstance().screenWidth - ScreenUtil.getInstance().getWidth(80),
                                            ),
                                            Text(
                                              '目录',
                                              style: TextStyle(fontSize: GlobalConfigUtil.getSubTitleFontSize()),
                                            )
                                          ],
                                        ),
                                      )
                                    : Material(
                                        color: Colors.transparent,
                                        child: Ink(
                                          child: InkWell(
                                              onTap: () {
                                                toRead(index - 1);
                                              },
                                              child: Hero(
                                                  tag: snapShot.data[index - 1].id,
                                                  child: Container(
                                                    padding: EdgeInsets.symmetric(horizontal: ScreenUtil.getInstance().getWidth(40)),
                                                    height: ScreenUtil.getInstance().getWidth(310),
                                                    margin: EdgeInsets.symmetric(vertical: ScreenUtil.getInstance().getWidth(15)),
                                                    child: Row(
                                                      children: <Widget>[
                                                        Container(
                                                          width: ScreenUtil.getInstance().getWidth(213),
                                                          height: ScreenUtil.getInstance().getWidth(300),
                                                          decoration: BoxDecoration(
                                                              border: Border.all(color: Color(0xFFdbd8d9), width: 0.5),
                                                              boxShadow: [BoxShadow(color: Colors.black12, blurRadius: 8)],
                                                              image: DecorationImage(
                                                                  fit: BoxFit.cover,
                                                                  image: AssetImage(
                                                                    'assets/texture/cover8.jpg',
                                                                  ))),
//                                    color: Colors.deepOrangeAccent,
//                                                          padding: EdgeInsets.only(right: 10),
                                                          child: Container(
                                                            width: ScreenUtil.getInstance().getWidth(46),
                                                            child: Row(
                                                              mainAxisAlignment: MainAxisAlignment.center,
                                                              children: getVerticalText(snapShot.data[index - 1].title),
                                                            ),
                                                          ),
                                                        ),
                                                        Expanded(
                                                            child: Container(
                                                          padding: EdgeInsets.only(
                                                              left: ScreenUtil.getInstance().getWidth(30), top: ScreenUtil.getInstance().getWidth(30), bottom: ScreenUtil.getInstance().getWidth(30)),
                                                          child: Column(
                                                            mainAxisAlignment: MainAxisAlignment.start,
                                                            crossAxisAlignment: CrossAxisAlignment.start,
                                                            children: <Widget>[
                                                              Container(
                                                                height: ScreenUtil.getInstance().getWidth(60),
                                                                child: Text(
                                                                  snapShot.data[index - 1].sectionIndex,
                                                                  softWrap: true,
                                                                  overflow: TextOverflow.ellipsis,
                                                                  maxLines: 3,
                                                                  style: TextStyle(
                                                                      fontSize: GlobalConfigUtil.getTabMainBodyFontSize(),
                                                                      color: Colors.black54,
                                                                      decoration: TextDecoration.none,
                                                                      fontWeight: FontWeight.normal,
                                                                      fontFamily: FontFamilyName.SONGTI),
                                                                ),
                                                              ),
                                                              Container(
                                                                child: Text(
                                                                  snapShot.data[index - 1].title,
                                                                  maxLines: 2,
                                                                  overflow: TextOverflow.ellipsis,
                                                                  style: TextStyle(
                                                                      fontSize: GlobalConfigUtil.getSubTitleFontSize(),
                                                                      fontWeight: FontWeight.bold,
                                                                      decoration: TextDecoration.none,
                                                                      color: Colors.black87,
                                                                      fontFamily: FontFamilyName.SONGTI),
                                                                ),
                                                              ),
                                                              Container(
                                                                height: ScreenUtil.getInstance().getWidth(90),
                                                                alignment: Alignment.centerLeft,
                                                                child: Text(
                                                                  (snapShot.data[index - 1].author.length == 0 ? '失' : snapShot.data[index - 1].author) +
                                                                      '【' +
                                                                      snapShot.data[index - 1].juanCount +
                                                                      '】',
                                                                  maxLines: 1,
                                                                  overflow: TextOverflow.ellipsis,
                                                                  style: TextStyle(
                                                                      fontSize: GlobalConfigUtil.getTabMainBodyFontSize(),
                                                                      decoration: TextDecoration.none,
                                                                      fontWeight: FontWeight.normal,
                                                                      color: Colors.black87,
                                                                      fontFamily: FontFamilyName.SONGTI),
                                                                ),
                                                              ),
                                                            ],
                                                          ),
                                                        ))
                                                      ],
                                                    ),
                                                  ))),
                                        ),
                                      );
                              }))
                      : Center(
                          child: SpinKitCircle(
                            size: 20,
                            color: Colors.black54,
                          ),
                        );
                })));
//      },
//    );
  }
}
