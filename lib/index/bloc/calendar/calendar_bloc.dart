import 'dart:async';
import 'package:bloc/bloc.dart';

import 'calendar_event.dart';
import 'calendar_state.dart';

class CalendarBloc extends Bloc<CalendarEvent, CalendarState> {
  static final CalendarBloc _calendarBlocSingleton =
      new CalendarBloc._internal();
  factory CalendarBloc() {
    return _calendarBlocSingleton;
  }
  CalendarBloc._internal();

  CalendarState get initialState => new UnCalendarState();

  @override
  Stream<CalendarState> mapEventToState(
    CalendarEvent event,
  ) async* {
    try {
      yield await event.applyAsync(currentState: currentState, bloc: this);
    } catch (_, stackTrace) {
      print('$_ $stackTrace');
      yield currentState;
    }
  }
}
