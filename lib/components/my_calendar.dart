import 'dart:convert';

import 'package:after_layout/after_layout.dart';
import 'package:bodhiroad/common/calendar_day.dart';
import 'package:bodhiroad/config/GlobalColorCOnfig.dart';
import 'package:bodhiroad/common/select_date_bloc.dart';
import 'package:bodhiroad/index/bloc/calendar/calendar_bloc.dart';
import 'package:bodhiroad/index/bloc/calendar/calendar_event.dart';
import 'package:bodhiroad/index/bloc/calendar/calendar_state.dart';
import 'package:bodhiroad/theme/bloc.dart';
import 'package:bodhiroad/common/update_title_bloc.dart';
import 'package:flustars/flustars.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class MyCalendar extends StatefulWidget {
  final double calendarHeight;
  final BoxDecoration decoration;
  final Color activeBackground;
  final Color activeMainTextColor;
  final Color activeSubTextColor;
  final double fontSize;
  final onTap;

//  static const Color defaultActiveBackground = Color(0xFFC3B59A);
//  static const Color defaultActiveMainTextColor = Color(0xFF2A1C0E);
//  static const Color defaultActiveSubTextColor = Color(0xFF2F2412);
  static const Color defaultActiveBackground = Color(0xFFC3B59A);
  static const Color defaultActiveMainTextColor =
      GlobalColorConfig.mainforegroundColor;
  static const Color defaultActiveSubTextColor =
      GlobalColorConfig.mainforegroundColor;
  static const double defaultCalendarHeight = 90;
  static const BoxDecoration defaultDecoration = BoxDecoration();
//  static const weekDayList = ['一','二','三','四','五','六','日'];

  MyCalendar(
      {Key key,
      this.activeBackground = defaultActiveBackground,
      this.activeMainTextColor = defaultActiveMainTextColor,
      this.activeSubTextColor = defaultActiveSubTextColor,
      this.calendarHeight = defaultCalendarHeight,
      this.decoration = defaultDecoration,
      this.fontSize = 16,
      this.onTap})
      : super(key: key);

  @override
  _MyCalendarState createState() => _MyCalendarState();
}

class _MyCalendarState extends State<MyCalendar> with AfterLayoutMixin<MyCalendar> {
  PageController _pageController = PageController(initialPage: 0);
  int weeks = 0;
  int startIndex = 0;
  DateTime firstDay = DateTime(2019, 6, 3);
  SelectDateBloc selectDateBloc = SelectDateBloc();
  UpdateTitleBloc updateTitleBloc = UpdateTitleBloc();
  List<CalendarDay> list2 = [];
  List<String> weekDayList = ['一', '二', '三', '四', '五', '六', '日'];
  CalendarBloc calendarBloc = CalendarBloc();
  int todayInpage = 0;
  CalendarDay todayC;
  void _incrementCounter() async {
    DateTime today = DateTime.now();
    print(today.toString().split(" ")[0]);

    String day = await rootBundle.loadString('assets/2019.json');
    List<dynamic> list = jsonDecode(day);

    int currentDayIndex = 0;

    list2 = list.map((json) {
      if (today.toString().split(" ")[0] == json['gregorianDateTime']) {
        json['isToday'] = true;
        json['active'] = true;
        currentDayIndex = list.indexOf(json);
        todayC = CalendarDay.fromJson(json);
        updateTitleBloc.dispatch(todayC);
      } else {
        json['isToday'] = false;
        json['active'] = false;
      }
      return CalendarDay.fromJson(json);
    }).toList();
    firstDay = DateTime(
        int.parse(list2[0].yt), int.parse(list2[0].m), int.parse(list2[0].d));
    weeks = ((list2.length + firstDay.weekday - 1) / 7).ceil();
    todayInpage = ((currentDayIndex + firstDay.weekday - 1) / 7).floor();
    _pageController.jumpToPage(todayInpage);
    setState(() {});
  }

  @override
  void initState() {
    super.initState();
    
  }
  @override
  void afterFirstLayout(BuildContext context) {
    _incrementCounter();
    print('afterFirstLayout');
  }

  @override
  void dispose() {
    _pageController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder(
      bloc: BlocProvider.of<AppThemeBloc>(context),
      builder: (context, ThemeChangeState themeState) {
        return BlocListener(
            bloc: calendarBloc,
            listener: (context, sta) {
              if (sta is InCalendarState) {
                updateTitleBloc.dispatch(todayC);
                _pageController.animateToPage(todayInpage,
                    duration: Duration(milliseconds: 300),
                    curve: Curves.easeInOutSine);
                calendarBloc.dispatch(InitCalendarEvent());
              }
            },
            child: Container(
              height: ScreenUtil.getInstance().getWidth(180),
              padding: EdgeInsets.symmetric(horizontal: 15),
              alignment: Alignment.center,
              child: PageView.builder(
                itemCount: weeks.toInt(),
                controller: _pageController,
                itemBuilder: (BuildContext context, index) {
                  List<CalendarDay> list = [];
                  List<CalendarDay> prev = [];
                  if (index == 0) {
                    list = list2.sublist(
                        0, DateTime.daysPerWeek - firstDay.weekday + 1);
                    prev = List.generate(firstDay.weekday - 1, (ix) {
                      return CalendarDay();
                    }).toList();
                    list.insertAll(0, prev);
                    print(prev.length);
                  } else {
                    int startIndex = DateTime.daysPerWeek -
                        firstDay.weekday +
                        1 +
                        (index - 1) * DateTime.daysPerWeek;
                    list = list2.sublist(startIndex, startIndex + 7);
                  }

                  return Container(
                      height: ScreenUtil.getInstance().getWidth(180),
                      child: Column(
                        children: <Widget>[
                          Row(
                            mainAxisAlignment: index == 0
                                ? MainAxisAlignment.end
                                : MainAxisAlignment.start,
                            children: list
                                .map((item) => InkWell(
                                      child: BlocBuilder(
                                        bloc: selectDateBloc,
                                        builder: (BuildContext context, map) {
                                          return Stack(
                                            children: <Widget>[
                                              Container(
                                                height: ScreenUtil.getInstance()
                                                    .getWidth(180),
                                                padding: EdgeInsets.all(3),
//                                        decoration: BoxDecoration(
//                                          border: item.isToday != null &&
//                                                  item.isToday
//                                              ? Border.all(
//                                                  color: GlobalColorConfig
//                                                      .buttonColorA,
//                                                  width: 0.5)
//                                              : Border.fromBorderSide(
//                                                  BorderSide.none),
//                                        ),
                                                width: (MediaQuery.of(context)
                                                            .size
                                                            .width -
                                                        30) /
                                                    7,
                                                child: Container(
//                                          decoration: BoxDecoration(
//                                            color: item.isToday != null &&
//                                                    item.isToday
//                                                ? GlobalColorConfig.mainRedColor
//                                                    .withOpacity(0.4)
//                                                : map['weekIndex'] == index &&
//                                                        map['dayIndex'] ==
//                                                            list.indexOf(item)
//                                                    ? widget.activeBackground
//                                                    : Colors.transparent,
//                                              border:
//                                                  item.isToday != null &&
//                                                          item.isToday
//                                                      ? Border.all(
//                                                          color:
//                                                              GlobalColorConfig
//                                                                  .buttonColorA,
//                                                          width: 1.0)
//                                                      : Border.fromBorderSide(
//                                                          BorderSide.none)
//                                          ),
                                                  child: Column(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .spaceAround,
                                                    children: <Widget>[
                                                      Text(weekDayList[list.indexOf(item)],
                                                          textScaleFactor: 1.0,
                                                          style: TextStyle(
                                                              fontSize: 12,
                                                              fontWeight: item.isToday !=
                                                                          null &&
                                                                      item
                                                                          .isToday
                                                                  ? FontWeight
                                                                      .bold
                                                                  : FontWeight
                                                                      .normal,
                                                              color: item.isToday !=
                                                                          null &&
                                                                      item
                                                                          .isToday
                                                                  ? GlobalColorConfig
                                                                      .mainRedColor
                                                                  : map['weekIndex'] ==
                                                                              index &&
                                                                          map['dayIndex'] ==
                                                                              list.indexOf(
                                                                                  item)
                                                                      ? themeState
                                                                          .themeConfig
                                                                          .embellishColorB
                                                                      : GlobalColorConfig
                                                                          .mainforegroundColor)),
                                                      Text(item.d,
                                                          textScaleFactor: 1.0,
                                                          style: TextStyle(
                                                              fontWeight: item.isToday !=
                                                                          null &&
                                                                      item
                                                                          .isToday
                                                                  ? FontWeight
                                                                      .bold
                                                                  : FontWeight
                                                                      .normal,
                                                              fontSize: widget
                                                                  .fontSize,
                                                              color: item.isToday !=
                                                                          null &&
                                                                      item
                                                                          .isToday
                                                                  ? GlobalColorConfig
                                                                      .mainRedColor
                                                                  : map['weekIndex'] == index &&
                                                                          map['dayIndex'] ==
                                                                              list.indexOf(
                                                                                  item)
                                                                      ? themeState
                                                                          .themeConfig
                                                                          .embellishColorB
                                                                      : GlobalColorConfig.mainforegroundColor)),
                                                      Text(
                                                        item.lDay,
                                                        textScaleFactor: 1.0,
                                                        style: TextStyle(
                                                            fontWeight: item.isToday !=
                                                                        null &&
                                                                    item.isToday
                                                                ? FontWeight
                                                                    .bold
                                                                : FontWeight
                                                                    .normal,
                                                            fontSize: 10,
                                                            color: item.isToday !=
                                                                        null &&
                                                                    item.isToday
                                                                ? GlobalColorConfig
                                                                    .mainRedColor
                                                                : map['weekIndex'] ==
                                                                            index &&
                                                                        map['dayIndex'] ==
                                                                            list.indexOf(
                                                                                item)
                                                                    ? themeState
                                                                        .themeConfig
                                                                        .embellishColorB
                                                                    : GlobalColorConfig
                                                                        .mainforegroundColor),
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                              ),
                                              Positioned(
                                                right: 6,
                                                top: 3,
                                                child: ClipRRect(
                                                  borderRadius:
                                                      BorderRadius.circular(10),
                                                  child: Container(
                                                    height: 6,
                                                    width: 6,
                                                    color: item.foposa != null
                                                        ? GlobalColorConfig
                                                            .mainRedColor
                                                        : Colors.transparent,
                                                  ),
                                                ),
                                              )
                                            ],
                                          );
                                        },
                                      ),
                                      onTap: () {
                                        list[list.indexOf(item)].active = true;
                                        widget.onTap(item);
                                        Map map = Map();
                                        map['weekIndex'] = index;
                                        map['dayIndex'] = list.indexOf(item);
                                        selectDateBloc.dispatch(map);
                                        // setState(() {});
                                      },
                                    ))
                                .toList(),
                          ),
                        ],
                      ));
                },
              ),
            ));
      },
    );
  }
}
