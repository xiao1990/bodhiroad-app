import 'dart:ui';

import 'package:bodhiroad/config/ReaderThemeConfig.dart';
import 'package:bodhiroad/reader/bean/book_catalog.dart';
import 'package:bodhiroad/reader/bean/book_page.dart';
import 'package:bodhiroad/reader/bean/bookmark.dart';
import 'package:bodhiroad/reader/bean/reader_theme.dart';
import 'package:bodhiroad/reader/bloc.dart';
import 'package:bodhiroad/utils/GlobalConfigUtil.dart';
import 'package:bodhiroad/utils/Route.dart';
import 'package:flustars/flustars.dart';
import 'package:flutter/material.dart';
import 'package:global_configuration/global_configuration.dart';

class ReaderDrawer extends StatefulWidget {
  final List<CatalogBean> titleList;
  final List<BookMark> bookMarkList;
  final List<BookMark> underLineList;
  final List<PageBean> pageList;
  final String bookName;
  final BuddhistReaderBlocBloc bloc;
  ReaderDrawer({this.titleList, this.bookMarkList, this.underLineList, this.pageList, this.bookName, this.bloc});
  @override
  _ReaderDrawerState createState() => _ReaderDrawerState();
}

class _ReaderDrawerState extends State<ReaderDrawer> with SingleTickerProviderStateMixin {
  TabController _tabController;
  Map<int, List<BookMark>> map = Map();
  @override
  void initState() {
    _tabController = TabController(length: 2, vsync: this, initialIndex: GlobalConfigUtil.getInt('drawerType') ?? 0);
    super.initState();
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  getChildren(ReaderTheme theme) {
    List<Widget> children = [];
//    List.ge
    map.forEach((int chapterIndex, List<BookMark> list) {
      Widget c = Container(
          child: Column(
              children: [
        Container(
          height: 50,
          alignment: Alignment.centerLeft,
          padding: EdgeInsets.symmetric(horizontal: 15),
          child: Text(
            widget.titleList[chapterIndex].title,
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: ScreenUtil.getInstance().getSp(42), color: theme.fontColor),
          ),
        )
      ]..addAll(List.generate(
                  list.length,
                  (index) => Container(
                        child: Material(
                          color: Colors.transparent,
                          child: Ink(
                            width: ScreenUtil.getInstance().screenWidth * 0.8,
                            child: InkWell(
                              highlightColor: theme.backgroundColor.withOpacity(0.6),
                              child: Row(
                                children: <Widget>[
                                  Container(
                                    width: 36,
                                    height: 48,
                                    alignment: Alignment.centerRight,
                                    child: Icon(
                                      Icons.bookmark_border,
                                      size: 18,
                                      color: theme.fontColor,
                                    ),
                                  ),
                                  Expanded(
                                    child: Container(
                                      height: 72,
                                      alignment: Alignment.centerLeft,
                                      padding: EdgeInsets.only(right: 4, left: 15),
                                      decoration: BoxDecoration(border: Border(bottom: BorderSide(color: theme.fontColor.withOpacity(0.1), width: 0.6))),
                                      child: Text(
                                        list[index].title,
                                        maxLines: 3,
                                        overflow: TextOverflow.ellipsis,
                                        textScaleFactor: 1.0,
                                        style: TextStyle(fontSize: ScreenUtil.getInstance().getSp(36), color: theme.fontColor, height: 1.5),
                                      ),
                                    ),
                                  )
                                ],
                              ),
                              onTap: () {
                                if (GlobalConfiguration().getInt('flipType') == 2) {
                                  widget.bloc.dispatch(ChapterJumpEvent(position: list[index].yOffset));
                                } else {
                                  if (GlobalConfiguration().getInt('textLayOutDirection') == 1) {
                                    widget.bloc.dispatch(ChapterJumpEvent(index: widget.pageList.length - list[index].pageIndex - 1));
                                  } else {
                                    widget.bloc.dispatch(ChapterJumpEvent(index: list[index].pageIndex));
                                  }
                                }
                                Navigator.of(context).pop();
                              },
                            ),
                          ),
                        ),
                      )))));
      children.add(c);
    });
    return children;
  }

  @override
  Widget build(BuildContext context) {
    widget.bookMarkList.sort((a, b) => a.chapterIndex >= b.chapterIndex && a.actualStartIndex > b.actualStartIndex ? 1 : -1);
    widget.bookMarkList.forEach((e) {
      if (map[e.chapterIndex] == null) {
        map[e.chapterIndex] = [];
      }
      map[e.chapterIndex].add(e);
    });
    ReaderTheme theme = ReaderThemeConfig.instance.list[GlobalConfiguration().getInt('themeIndex')];
    return Container(
        width: ScreenUtil.getInstance().screenWidth * 0.8,
        decoration: BoxDecoration(borderRadius: BorderRadius.circular(20)),
        child: ClipRect(
            child: BackdropFilter(
          filter: new ImageFilter.blur(sigmaX: 4, sigmaY: 4),
          child: Container(
              width: ScreenUtil.getInstance().screenWidth * 0.8,
              color: theme.catalogBackgroundColor,
              child: Column(
                children: <Widget>[
                  Container(
                    height: 40,
                    alignment: Alignment.center,
                    child: Text(
                      widget.bookName,
                      textScaleFactor: 1.0,
                      style: TextStyle(fontSize: 20, color: theme.fontColor),
                    ),
                  ),
                  Container(
                    height: 40,
                    child: TabBar(
                      controller: _tabController,
                      indicatorSize: TabBarIndicatorSize.label,
                      indicatorColor: theme.catalogFontColor,
                      labelColor: theme.catalogFontColor,
                      unselectedLabelColor: theme.catalogFontColor,
                      labelStyle: TextStyle(fontWeight: FontWeight.bold, fontFamily: 'songti'),
                      unselectedLabelStyle: TextStyle(fontWeight: FontWeight.normal, fontFamily: 'songti'),
                      tabs: <Widget>[
                        Text(
                          '目录',
                          textScaleFactor: 1.0,
                          style: TextStyle(color: theme.fontColor),
                        ),
                        Text('标记', textScaleFactor: 1.0),
                      ],
                    ),
                  ),
                  Expanded(
                    flex: 1,
                    child: TabBarView(
                      controller: _tabController,
                      children: <Widget>[
                        ListView.builder(
                          itemCount: widget.titleList.length,
                          physics: BouncingScrollPhysics(),
                          itemBuilder: (BuildContext context, int index) {
                            return Ink(
                              child: Container(
                                width: ScreenUtil.getInstance().screenWidth * 0.8,
                                child: InkWell(
                                  child: Row(
                                    children: <Widget>[
                                      Container(
                                        width: widget.titleList[index].level == 1 ? 18 : 36,
                                        alignment: widget.titleList[index].level == 1 ? Alignment.centerLeft : Alignment.centerRight,
                                        height: 48,
                                        padding: EdgeInsets.symmetric(horizontal: 5),
                                        child: Icon(
                                          Icons.fiber_manual_record,
                                          color: theme.catalogFontColor,
                                          size: widget.titleList[index].level == 1 ? 10 : 8,
                                        ),
                                        decoration: BoxDecoration(border: Border(bottom: BorderSide(color: theme.catalogFontColor.withOpacity(0.1), width: 0.6))),
                                      ),
                                      Expanded(
                                        child: Container(
                                          height: 48,
                                          alignment: Alignment.centerLeft,
                                          padding: EdgeInsets.only(right: 8),
                                          child: Text(
                                            widget.titleList[index].level == 1 ? widget.titleList[index].title : widget.titleList[index].title,
                                            textScaleFactor: 1.0,
                                            style: TextStyle(fontSize: widget.titleList[index].level == 1 ? 16 : 14, color: theme.catalogFontColor),
                                          ),
                                          decoration: BoxDecoration(
                                              // color: Colors.red,
                                              border: Border(bottom: BorderSide(color: theme.catalogFontColor.withOpacity(0.1), width: 0.6))),
                                        ),
                                      )
                                    ],
                                  ),
                                  onTap: () {
                                    if (GlobalConfiguration().getInt('flipType') == 2) {
                                      widget.bloc.dispatch(ChapterJumpEvent(position: widget.titleList[index].yOffset, index: 0));
                                    } else {
                                      if (GlobalConfiguration().getInt('textLayOutDirection') == 1) {
                                        widget.bloc.dispatch(ChapterJumpEvent(index: widget.titleList[index].pageIndex));
                                      } else {
                                        widget.bloc.dispatch(ChapterJumpEvent(index: widget.titleList[index].pageIndex));
                                      }
                                    }
                                    Navigator.of(context).pop();
                                  },
                                ),
                              ),
                            );
                          },
                        ),
                        ListView(physics: BouncingScrollPhysics(), children: getChildren(theme)),
                      ],
                    ),
                  )
                ],
              )),
        )));
  }
}
