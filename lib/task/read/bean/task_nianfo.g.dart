// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'task_nianfo.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

TaskNianfo _$TaskNianfoFromJson(Map<String, dynamic> json) {
  return TaskNianfo(
      taskId: json['taskId'] as int,
      taskName: json['taskName'] as String,
      count: json['count'] as int,
      current: json['current'] as bool,
      frequency: (json['frequency'] as num)?.toDouble(),
      followedId: json['followedId'] as int);
}

Map<String, dynamic> _$TaskNianfoToJson(TaskNianfo instance) =>
    <String, dynamic>{
      'taskId': instance.taskId,
      'taskName': instance.taskName,
      'count': instance.count,
      'current': instance.current,
      'frequency': instance.frequency,
      'followedId': instance.followedId
    };
