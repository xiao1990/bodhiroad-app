// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'task_classify.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

TaskClassify _$TaskClassifyFromJson(Map<String, dynamic> json) {
  return TaskClassify(
      taskTypeId: json['taskTypeId'] as int,
      taskTypeName: json['taskTypeName'] as String,
      list: (json['list'] as List)
          ?.map((e) =>
              e == null ? null : Task.fromJson(e as Map<String, dynamic>))
          ?.toList());
}

Map<String, dynamic> _$TaskClassifyToJson(TaskClassify instance) =>
    <String, dynamic>{
      'taskTypeId': instance.taskTypeId,
      'taskTypeName': instance.taskTypeName,
      'list': instance.list
    };
