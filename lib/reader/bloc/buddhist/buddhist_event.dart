import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class BuddhistEvent extends Equatable {
  BuddhistEvent([List props = const []]) : super(props);
}

class InitBuddhistEvent extends BuddhistEvent {
  @override
  String toString() => 'InitBuddhistEvent';
}

class LoadBuddhistEvent extends BuddhistEvent {
  final bool forceUpdate;
  LoadBuddhistEvent({this.forceUpdate = false}) : super([forceUpdate]);

  @override
  String toString() => 'LoadBuddhistEvent';
}
