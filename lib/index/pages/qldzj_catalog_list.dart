import 'dart:convert';
import 'dart:math' as math;

import 'package:bodhiroad/common/FontFamilyName.dart';
import 'package:bodhiroad/components/extended_text/lib/src/extended_text.dart';
import 'package:bodhiroad/config/CommonConfig.dart';
import 'package:bodhiroad/reader/bean/buddhist_catalog.dart';
import 'package:bodhiroad/utils/GlobalConfigUtil.dart';
import 'package:flustars/flustars.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

import 'qldzj_cover_index.dart';

class QldzjCatalogList extends StatefulWidget {
  final String from;
  final String source;
  final String taskType;
  const QldzjCatalogList({this.taskType, this.source, this.from});
  @override
  _QldzjCatalogListState createState() => _QldzjCatalogListState();
}

class _QldzjCatalogListState extends State<QldzjCatalogList> {
  Future f;

  @override
  void initState() {
    f = getBuddhist();
    super.initState();
  }

  Future<List<BuddhistCatalogBean>> getBuddhist() async {
    List<BuddhistCatalogBean> list2;
    List<dynamic> oriList = SpUtil.getObjectList(CommonConfig.QLDZJ_CATALOG);
    oriList = [];
    if (oriList != null && oriList.length > 0) {
      list2 = oriList.map((item) => BuddhistCatalogBean.fromJson(item)).toList();
    } else {
      List qldzjDynamic = jsonDecode(await rootBundle.loadString('assets/qldzj/zongmulu.json'));
      list2 = qldzjDynamic.map((item) => BuddhistCatalogBean.fromJson(item)).toList();
      SpUtil.putObjectList(CommonConfig.QLDZJ_CATALOG, list2);
    }
    return list2;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        child: FutureBuilder<List<BuddhistCatalogBean>>(
            future: f,
            builder: (context, AsyncSnapshot<List<BuddhistCatalogBean>> snapShot) {
              return snapShot.data != null && snapShot.data.length > 0
                  ? Container(
                      child: ListView.separated(
                          itemCount: snapShot.data == null ? 0 : snapShot.data.length,
                          physics: BouncingScrollPhysics(),
                          padding: EdgeInsets.all(0),
                          separatorBuilder: (context, index) {
                            return Container(
                              height: 0.5,
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: List.generate(32, (index) {
                                  return Container(
                                    width: 4,
                                    height: 0.5,
                                    color: Colors.black12,
                                  );
                                }).toList(),
                              ),
                            );
                          },
                          itemBuilder: (BuildContext context, index) {
                            return Material(
                              color: Colors.transparent,
                              child: Ink(
                                child: InkWell(
                                    onTap: () {
                                      Navigator.of(context).push(MaterialPageRoute(builder: (BuildContext context) {
                                        return QldzjCoverIndex(
                                          from: widget.from,
                                          source: widget.source,
                                          taskType: widget.taskType,
                                          article: snapShot.data[index],
                                          index: index,
                                        );
                                      }));
                                    },
                                    child: Hero(
                                        tag: snapShot.data[index].id,
                                        child: Container(
                                          padding: EdgeInsets.symmetric(horizontal: ScreenUtil.getInstance().getWidth(40)),
                                          height: ScreenUtil.getInstance().getWidth(310.0 + (index == snapShot.data.length - 1 ? 160 : 0)),
                                          margin: EdgeInsets.symmetric(vertical: ScreenUtil.getInstance().getWidth(15)),
                                          child: Row(
                                            children: <Widget>[
                                              Container(
                                                width: ScreenUtil.getInstance().getWidth(213),
                                                height: ScreenUtil.getInstance().getWidth(300),
                                                decoration: BoxDecoration(
                                                    color: Colors.white,
                                                    border: Border.all(color: Color(0xFFBDBABB), width: 0),
                                                    boxShadow: [BoxShadow(color: Colors.black12, blurRadius: 6)],
                                                    image: DecorationImage(
                                                        fit: BoxFit.cover,
                                                        image: AssetImage(
                                                          'assets/qldzj/${index}.jpg',
                                                        ))),
//                                    color: Colors.deepOrangeAccent,
                                                padding: EdgeInsets.only(right: ScreenUtil.getInstance().getWidth(25)),
                                              ),
                                              Expanded(
                                                  child: Container(
                                                height: ScreenUtil.getInstance().getWidth(300),
                                                padding: EdgeInsets.only(
                                                    left: ScreenUtil.getInstance().getWidth(30),
                                                    top: ScreenUtil.getInstance().getWidth(30),
                                                    bottom: ScreenUtil.getInstance().getWidth(30)),
                                                child: Column(
                                                  mainAxisAlignment: MainAxisAlignment.start,
                                                  crossAxisAlignment: CrossAxisAlignment.start,
                                                  children: <Widget>[
                                                    Container(
                                                      height: ScreenUtil.getInstance().getWidth(75),
                                                      child: ExtendedText.rich(
                                                        TextSpan(children: [
                                                          TextSpan(
                                                            text: snapShot.data[index].title,
                                                            style: TextStyle(
                                                                fontSize: GlobalConfigUtil.getSubTitleFontSize(),
                                                                fontWeight: FontWeight.bold,
                                                                decoration: TextDecoration.none,
                                                                color: Colors.black87,
                                                                fontFamily: FontFamilyName.SONGTI),
                                                          ),
                                                          TextSpan(
                                                            text: '（' + snapShot.data[index].index + '）',
                                                            style: TextStyle(
                                                                fontSize: GlobalConfigUtil.getTabMainBodyFontSize(),
                                                                fontWeight: FontWeight.bold,
                                                                decoration: TextDecoration.none,
                                                                color: Colors.black54,
                                                                fontFamily: FontFamilyName.SONGTI),
                                                          )
                                                        ]),
                                                        overflow: TextOverflow.ellipsis,
                                                      ),
                                                    ),
                                                  ]..addAll(List.generate(snapShot.data[index].list != null ? snapShot.data[index].list.length : 0, (idx) {
                                                      return Expanded(
                                                          child: Container(
                                                        alignment: Alignment.centerLeft,
                                                        child: Text(
                                                          '·' + snapShot.data[index].list[idx].title,
                                                          maxLines: 1,
                                                          overflow: TextOverflow.ellipsis,
                                                          style: TextStyle(
                                                              fontSize: GlobalConfigUtil.getTabMainBodyFontSize(),
                                                              decoration: TextDecoration.none,
                                                              fontWeight: FontWeight.normal,
                                                              color: Colors.black54,
                                                              fontFamily: FontFamilyName.SONGTI),
                                                        ),
                                                      ));
                                                    })),
                                                ),
                                              ))
                                            ],
                                          ),
                                        ))),
                              ),
                            );
                          }))
                  : Center(
                      child: SpinKitCircle(
                        size: ScreenUtil.getInstance().getWidth(50),
                        color: Colors.black54,
                      ),
                    );
            }));
//      },
//    );
  }
}
