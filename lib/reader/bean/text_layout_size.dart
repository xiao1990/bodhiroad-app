import 'package:json_annotation/json_annotation.dart';

part 'text_layout_size.g.dart';

@JsonSerializable()
class TextLayoutSize {
  double heightHorizontal;
  double heightVertical;
  double width;
  double fontSize;
  double fontHeight;
  TextLayoutSize({this.width, this.heightHorizontal, this.heightVertical, this.fontSize, this.fontHeight});
  factory TextLayoutSize.fromJson(Map<String, dynamic> json) {
    return _$TextLayoutSizeFromJson(json);
  }
  Map<String, dynamic> toJson() => _$TextLayoutSizeToJson(this);
}
