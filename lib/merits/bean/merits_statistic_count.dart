import 'package:json_annotation/json_annotation.dart';

import 'merits_statistic_count_unit.dart';

part 'merits_statistic_count.g.dart';

@JsonSerializable()
class MeritsStatisticCount {
  int meritsType;
  List<MeritsStatisticCountUnit> list;

  MeritsStatisticCount({
    this.meritsType,
    this.list,
  });
  factory MeritsStatisticCount.fromJson(Map<String, dynamic> json) {
    return _$MeritsStatisticCountFromJson(json);
  }
  Map<String, dynamic> toJson() => _$MeritsStatisticCountToJson(this);
}
