import 'dart:async';
import 'package:bloc/bloc.dart';

class TabBloc extends Bloc<double, double> {
  static final TabBloc _themeBlocSingleton = new TabBloc._internal();
  factory TabBloc() {
    return _themeBlocSingleton;
  }
  TabBloc._internal();

  double get initialState => 30;

  @override
  Stream<double> mapEventToState(
    double event,
  ) async* {
    try {
      if (event < 0.5) {
        yield event = 30 + 114 * event;
      } else {
        yield event = 87 - 114 * (event - 0.5);
      }
    } catch (_, stackTrace) {
      print('$_ $stackTrace');
      yield currentState;
    }
  }
}
