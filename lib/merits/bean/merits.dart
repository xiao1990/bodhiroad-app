import 'package:bodhiroad/usercenter/bean/user_merits.dart';
import 'package:json_annotation/json_annotation.dart';

part 'merits.g.dart';

@JsonSerializable()
class Merits {
  int id;
  String title;
  int catalogId;
  String catalogName;
  String lastUpdate;
  int isGood;
  List<Merits> list;
  List<UserMerits> userMerits;

  Merits(
      {this.id,
      this.title,
      this.catalogId,
      this.catalogName,
      this.lastUpdate,
      this.isGood,
      this.list,
      this.userMerits});
  factory Merits.fromJson(Map<String, dynamic> json) {
    return _$MeritsFromJson(json);
  }
  Map<String, dynamic> toJson() => _$MeritsToJson(this);
}
