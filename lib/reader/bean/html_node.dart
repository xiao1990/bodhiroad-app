import 'package:json_annotation/json_annotation.dart';
part 'html_node.g.dart';

@JsonSerializable()
class HtmlNodeBean {
  final String type;
  final String text;
  HtmlNodeBean({this.type, this.text});
  factory HtmlNodeBean.fromJson(Map<String, dynamic> json) {
    return _$HtmlNodeBeanFromJson(json);
  }
  Map<String, dynamic> toJson() => _$HtmlNodeBeanToJson(this);
}
