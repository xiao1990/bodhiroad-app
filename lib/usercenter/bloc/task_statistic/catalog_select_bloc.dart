import 'dart:async';
import 'package:bloc/bloc.dart';

class CatalogSelectBloc extends Bloc<int, int> {
  static final CatalogSelectBloc _catalogSelectBlocSingleton =
      new CatalogSelectBloc._internal();
  factory CatalogSelectBloc() {
    return _catalogSelectBlocSingleton;
  }
  CatalogSelectBloc._internal();

  int get initialState => -1;

  @override
  Stream<int> mapEventToState(
    int event,
  ) async* {
    try {
      yield event;
    } catch (_, stackTrace) {
      print('$_ $stackTrace');
      yield currentState;
    }
  }
}
