// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'read_config.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ReadConfig _$ReadConfigFromJson(Map<String, dynamic> json) {
  return ReadConfig(
      fontFamily: json['fontFamily'] as String,
      fontSize: json['fontSize'] as int,
      textLayOutDirection: json['textLayOutDirection'] as int,
      simplifiedChinese: json['simplifiedChinese'] as bool,
      themeIndex: json['themeIndex'] as int,
      fontHeight: (json['fontHeight'] as num)?.toDouble(),
      screenDirection: json['screenDirection'] as int,
      fileUrl: json['fileUrl'] as String,
      id: json['id'] as int,
      source: json['source'] as int,
      taskType: json['taskType'] as int,
      version: json['version'] as int,
      viewHeight: (json['viewHeight'] as num)?.toDouble(),
      viewWidth: (json['viewWidth'] as num)?.toDouble(),
      statusBarHeight: (json['statusBarHeight'] as num)?.toDouble(),
      currentPageIndex: json['currentPageIndex'] as int,
      flipType: json['flipType'] as int,
      scrollOffset: (json['scrollOffset'] as num)?.toDouble(),
      readFinished: json['readFinished'] as bool,
      sliderStart: json['sliderStart'] as bool,
      currentTaskRecordId: json['currentTaskRecordId'] as int,
      brightness: (json['brightness'] as num)?.toDouble());
}

Map<String, dynamic> _$ReadConfigToJson(ReadConfig instance) =>
    <String, dynamic>{
      'id': instance.id,
      'fileUrl': instance.fileUrl,
      'viewWidth': instance.viewWidth,
      'viewHeight': instance.viewHeight,
      'statusBarHeight': instance.statusBarHeight,
      'fontFamily': instance.fontFamily,
      'source': instance.source,
      'taskType': instance.taskType,
      'version': instance.version,
      'fontSize': instance.fontSize,
      'textLayOutDirection': instance.textLayOutDirection,
      'flipType': instance.flipType,
      'simplifiedChinese': instance.simplifiedChinese,
      'themeIndex': instance.themeIndex,
      'brightness': instance.brightness,
      'fontHeight': instance.fontHeight,
      'currentPageIndex': instance.currentPageIndex,
      'screenDirection': instance.screenDirection,
      'readFinished': instance.readFinished,
      'scrollOffset': instance.scrollOffset,
      'sliderStart': instance.sliderStart,
      'currentTaskRecordId': instance.currentTaskRecordId
    };
