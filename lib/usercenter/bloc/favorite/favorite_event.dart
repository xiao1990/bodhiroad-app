import 'package:equatable/equatable.dart';

abstract class FavoriteEvent extends Equatable {
  FavoriteEvent([props = const []]) : super([]);
}

class FavoriteInitEvent extends FavoriteEvent {
  FavoriteInitEvent() : super();
}

class FavoriteLoadEvent extends FavoriteEvent {
  FavoriteLoadEvent() : super();
}
