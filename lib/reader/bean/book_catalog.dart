import 'package:json_annotation/json_annotation.dart';

import 'book_page.dart';

part 'book_catalog.g.dart';

@JsonSerializable()
class CatalogBean {
  String title;
  String subTitle;
  int pageIndex;
  int level;
  int nodeIndex;
  PageBean indexedPage;
  double yOffset;
  String tag;
  int fullTextIndex;

  CatalogBean({this.title, this.subTitle, this.pageIndex, this.level, this.nodeIndex, this.indexedPage, this.yOffset, this.fullTextIndex, this.tag});

  factory CatalogBean.fromJson(Map<String, dynamic> json) {
    return _$CatalogBeanFromJson(json);
  }
  Map<String, dynamic> toJson() => _$CatalogBeanToJson(this);
}
