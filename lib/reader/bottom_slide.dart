import 'package:bloc/bloc.dart';

class BottomSlideBloc extends Bloc<double, double> {
  static final BottomSlideBloc _readerBlocSingleton = new BottomSlideBloc._internal();
  factory BottomSlideBloc() {
    return _readerBlocSingleton;
  }
  BottomSlideBloc._internal();
  @override
  double get initialState => 0;

  @override
  Stream<double> mapEventToState(
    double event,
  ) async* {
    yield event;
  }
}
