import 'package:bodhiroad/utils/pinyin_utils.dart';

class TextConfig {
  static Map _rareTextIndex;
  static Map _rareText;
  static Map _rareTextPinyinRela;
  static TextConfig textConfig;
  static TextConfig get instance => _getInstance();
  factory TextConfig() => _getInstance();
  TextConfig._interal() {
    _rareTextPinyinRela = Map();
    _rareTextIndex = Map();
    _rareText = Map();
    _rareTextIndex['𤙖'] = '1';
    _rareTextPinyinRela['1'] = PinYinUtils.convertZh2Pinyin('新');
    _rareText['1'] = '𤙖';
    _rareTextIndex['𠾆'] = '2';
    _rareTextPinyinRela['2'] = PinYinUtils.convertZh2Pinyin('利');
    _rareText['2'] = '𠾆';

    // _rareTextIndex['隸'] = '3';
    // _rareTextPinyinRela['3'] = PinYinUtils.convertZh2Pinyin('利');
    // _rareText['3'] = '隸';
  }
  static TextConfig _getInstance() {
    if (textConfig == null) {
      textConfig = TextConfig._interal();
    }
    return textConfig;
  }

  Map get rareTextIndex => _rareTextIndex;
  Map get rareTextPinyinRela => _rareTextPinyinRela;
  Map get rareText => _rareText;
}
