import 'dart:convert';

class ParameterConvertUtils {
  /// 转码
  static String convertCN(String str) {
    return jsonEncode(Utf8Encoder().convert(str));
  }

  /// 解码
  static String parseParameterCN(String str) {
    var list = List<int>();
    jsonDecode(str).forEach(list.add);
    return Utf8Decoder().convert(list);
  }
}
