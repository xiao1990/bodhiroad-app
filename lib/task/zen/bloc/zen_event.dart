import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class ZenEvent extends Equatable {
  ZenEvent([props = const []]) : super(props);
}

/// 初始化
class ZenInitEvent extends ZenEvent {
  final int position;
  final String countDownTextStr;
  ZenInitEvent({this.position, this.countDownTextStr})
      : super([position, countDownTextStr]);
  @override
  String toString() => 'ZenInitEvent';
}

/// 改变禅修时间
class ZenChangeTimeEvent extends ZenEvent {
  final int position;
  final String countDownTextStr;
  ZenChangeTimeEvent({this.position, this.countDownTextStr})
      : super([position, countDownTextStr]);
  @override
  String toString() => 'ZenChangeTimeEvent';
}

/// 禅修开始
class ZenStartEvent extends ZenEvent {
  final int totalTime;
  final int originalTotalTime;
  ZenStartEvent({this.totalTime, this.originalTotalTime})
      : super([totalTime, originalTotalTime]);
  @override
  String toString() => 'ZenStartEvent';
}

/// 禅修停止
class ZenStopEvent extends ZenEvent {
  @override
  String toString() => 'ZenStopEvent';
}

/// 倒计时事件
class ZenUnderwayEvent extends ZenEvent {
  final int position;
  final String countDownTextStr;
  ZenUnderwayEvent({this.position, this.countDownTextStr})
      : super([position, countDownTextStr]);
  @override
  String toString() => 'ZenUnderwayEvent';
}

class ZenContinueEvent extends ZenEvent {
  final int totalTime;
  ZenContinueEvent({this.totalTime}) : super([totalTime]);
  @override
  String toString() => 'ZenContinueEvent';
}

class ZenFinishEvent extends ZenEvent {
  final int position;
  final String countDownTextStr;
  ZenFinishEvent({this.position, this.countDownTextStr})
      : super([position, countDownTextStr]);
  @override
  String toString() => 'ZenFinishEvent';
}
