import 'package:bodhiroad/components/h1.dart';
import 'package:bodhiroad/config/GlobalColorCOnfig.dart';
import 'package:bodhiroad/reader/bean/buddhist.dart';
import 'package:bodhiroad/theme/bloc.dart';
import 'package:flustars/flustars.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class BookList extends StatelessWidget {
  const BookList({Key key, @required this.list, @required this.from, this.rightAction = false, this.centerClick, this.rightClick}) : super(key: key);

  final List<Buddhist> list;
  final String from;
  final centerClick;
  final rightClick;
  final bool rightAction;

  @override
  Widget build(BuildContext context) {
    return BlocBuilder(
      bloc: AppThemeBloc(),
      builder: (context, ThemeChangeState appThemeState) {
        return Padding(
            padding: EdgeInsets.only(top: 0),
            child: ListView.builder(
              itemCount: list.length,
              padding: EdgeInsets.all(0),
              physics: BouncingScrollPhysics(),
              itemBuilder: (BuildContext context, index) {
                return Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(
                      width: ScreenUtil.getInstance().screenWidth - 30,
                      margin: EdgeInsets.symmetric(vertical: 8),
                      decoration: BoxDecoration(boxShadow: [BoxShadow(color: appThemeState.themeConfig.shadowColor, blurRadius: 12)]),
                      child: Material(
                        color: Colors.white,
                        elevation: 0,
                        borderRadius: BorderRadius.circular(8),
                        child: Ink(
                            width: MediaQuery.of(context).size.width - 30,
                            height: ScreenUtil.getInstance().getWidth(200),
                            padding: EdgeInsets.only(left: 20),
                            decoration: BoxDecoration(borderRadius: BorderRadius.circular(8), border: Border.all(color: GlobalColorConfig.grayBackGroundColor, width: 0.5)),
                            child: ClipRRect(
                                borderRadius: BorderRadius.circular(8),
                                child: Row(
                                  children: <Widget>[
                                    Expanded(
                                      child: InkWell(
                                        splashColor: GlobalColorConfig.mainBorderColor.withOpacity(0.5),
                                        highlightColor: Colors.white.withOpacity(0.2),
                                        borderRadius: BorderRadius.circular(8),
                                        child: Row(
                                          children: <Widget>[
                                            Expanded(
                                              child: Column(
                                                crossAxisAlignment: CrossAxisAlignment.start,
                                                mainAxisAlignment: MainAxisAlignment.center,
                                                children: <Widget>[
                                                  TitleText(list[index].title, appThemeState.themeConfig.mainTextColor).h2,
                                                  TitleText(list[index].author, appThemeState.themeConfig.subTitleTextColor).h3,
                                                ],
                                              ),
                                            ),
                                            Container(
                                              alignment: Alignment.center,
                                              width: ScreenUtil.getInstance().getWidth(180),
                                              height: ScreenUtil.getInstance().getWidth(200),
                                              child: Icon(
                                                Icons.keyboard_arrow_right,
                                                color: appThemeState.themeConfig.subTitleTextColor,
                                              ),
                                            )
                                          ],
                                        ),
                                        onTap: () {
                                          centerClick(list[index]);
                                        },
                                      ),
                                    ),
                                    Offstage(
                                      offstage: !rightAction,
                                      child: InkWell(
                                        splashColor: GlobalColorConfig.mainBorderColor.withOpacity(0.5),
                                        highlightColor: Colors.white.withOpacity(0.2),
                                        borderRadius: BorderRadius.circular(8),
                                        child: Container(
                                          alignment: Alignment.center,
                                          width: ScreenUtil.getInstance().getWidth(180),
                                          height: ScreenUtil.getInstance().getWidth(200),
                                          child: Icon(Icons.add),
                                        ),
                                        onTap: () {
                                          rightClick(list[index]);
                                        },
                                      ),
                                    ),
                                  ],
                                ))),
                      ),
                    )
                  ],
                );
              },
            ));
      },
    );
  }
}
