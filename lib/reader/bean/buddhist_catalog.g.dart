// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'buddhist_catalog.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

BuddhistCatalogBean _$BuddhistCatalogBeanFromJson(Map<String, dynamic> json) {
  return BuddhistCatalogBean(
      id: json['id'] as String,
      title: json['title'] as String,
      index: json['index'] as String,
      juanCount: json['juanCount'] as String,
      list: (json['list'] as List)
          ?.map((e) =>
              e == null ? null : Buddhist.fromJson(e as Map<String, dynamic>))
          ?.toList());
}

Map<String, dynamic> _$BuddhistCatalogBeanToJson(
        BuddhistCatalogBean instance) =>
    <String, dynamic>{
      'id': instance.id,
      'title': instance.title,
      'index': instance.index,
      'juanCount': instance.juanCount,
      'list': instance.list
    };
