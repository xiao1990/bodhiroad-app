import 'package:json_annotation/json_annotation.dart';

part 'brwh_catalog.g.dart';

@JsonSerializable()
class BrwhCatalogBean {
  int id;
  int parentId;
  String title;
  String author;
  String url;
  String desc;
  String catalogId;
  int version;
  List<BrwhCatalogBean> subTitle = [];

  BrwhCatalogBean({this.id, this.author = '', this.title = '', this.url = '', this.catalogId, this.desc = '', this.parentId, this.subTitle, this.version = 0});

  factory BrwhCatalogBean.fromJson(Map<String, dynamic> json) {
    return _$BrwhCatalogBeanFromJson(json);
  }
  Map<String, dynamic> toJson() => _$BrwhCatalogBeanToJson(this);
}
