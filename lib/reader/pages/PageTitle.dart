import 'package:bodhiroad/config/ReaderThemeConfig.dart';
import 'package:bodhiroad/reader/bean/reader_theme.dart';
import 'package:flustars/flustars.dart';
import 'package:flutter/material.dart';
import 'package:global_configuration/global_configuration.dart';

/// 标题栏
class PageTitle extends StatelessWidget {
  final String title;
  final int index;
  final int count;
  const PageTitle({this.index, this.title, this.count});
  @override
  Widget build(BuildContext context) {
    ReaderTheme _readerTheme = ReaderThemeConfig.instance.list[GlobalConfiguration().getInt('themeIndex')];
    return Container(
        width: ScreenUtil.getInstance().screenWidth,
        height: GlobalConfiguration().getDouble('statusBarHeight'),
//                          decoration: BoxDecoration(border: Border(bottom: BorderSide(color: readerTheme.fontColor.withOpacity(0.7), width: 0.5))),
        padding: EdgeInsets.symmetric(horizontal: ScreenUtil.getInstance().getWidth(18)),
        alignment: Alignment.centerLeft,
        decoration: BoxDecoration(color: _readerTheme.backgroundColor, border: Border(bottom: BorderSide(width: 0.5, color: _readerTheme.fontColor.withOpacity(0.2)))),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Text(
              title,
              textScaleFactor: 1.0,
              style: TextStyle(color: _readerTheme.fontColor.withOpacity(0.6)),
            ),
            Row(
              children: <Widget>[
                Text(
                  index.toString() + ' | ' + count.toString(),
                  style: TextStyle(color: _readerTheme.fontColor.withOpacity(0.6)),
                  textScaleFactor: 1.0,
                ),
              ],
            )
          ],
        ));
  }
}
