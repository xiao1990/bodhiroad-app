// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'buddhist.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Buddhist _$BuddhistFromJson(Map<String, dynamic> json) {
  return Buddhist(
      id: json['id'] as int,
      title: json['title'] as String,
      juanCount: json['juanCount'] as String,
      sectionIndex: json['sectionIndex'] as String,
      url: json['url'] as String,
      catalogId: json['catalogId'] as String,
      catalogName: json['catalogName'] as String,
      classify: json['classify'] as int,
      version: json['version'] as int)
    ..author = json['author'] as String;
}

Map<String, dynamic> _$BuddhistToJson(Buddhist instance) => <String, dynamic>{
      'id': instance.id,
      'title': instance.title,
      'author': instance.author,
      'juanCount': instance.juanCount,
      'sectionIndex': instance.sectionIndex,
      'url': instance.url,
      'catalogId': instance.catalogId,
      'catalogName': instance.catalogName,
      'classify': instance.classify,
      'version': instance.version
    };
