import 'dart:async';
import 'dart:io';

import 'package:bloc/bloc.dart';
import 'package:bodhiroad/api/api.dart';
import 'package:bodhiroad/common/text_config.dart';
import 'package:bodhiroad/reader/bean/book_char.dart';
import 'package:bodhiroad/reader/bean/book_info.dart';
import 'package:bodhiroad/reader/bean/book_paragraph_unit.dart';
import 'package:bodhiroad/reader/bean/favorite.dart';
import 'package:bodhiroad/reader/bloc/buddhist/buddhist_bloc.dart';
import 'package:bodhiroad/reader/bloc/reader_event.dart';
import 'package:bodhiroad/reader/bloc/reader_state.dart';
import 'package:bodhiroad/task/read/bean/task.dart';
import 'package:bodhiroad/task/read/bean/task_record.dart';
import 'package:bodhiroad/task/read/bloc/task_bloc.dart';
import 'package:bodhiroad/task/read/bloc/task_event.dart';
import 'package:bodhiroad/utils/HttpUtils.dart';
import 'package:bodhiroad/utils/pinyin_utils.dart';
import 'package:flustars/flustars.dart';
import 'package:flutter/foundation.dart';
import 'package:global_configuration/global_configuration.dart';
import 'package:html/dom.dart';
import 'package:html/parser.dart';
import 'package:path_provider/path_provider.dart';

import 'buddhist/buddhist_event.dart';

class ReaderBloc extends Bloc<ReaderEvent, ReaderState> {
  static final ReaderBloc _readerBlocSingleton = new ReaderBloc._internal();
  factory ReaderBloc() {
    return _readerBlocSingleton;
  }
  ReaderBloc._internal();

  ReaderState get initialState => new ReaderInitState();
  TaskBloc taskBloc = TaskBloc();
  BuddhistBloc buddhistBloc = BuddhistBloc();
  // static List<BookParagraphUnitBean> unitList = [];
  static Map<String, List<BookParagraphUnitBean>> map = Map();
  @override
  Stream<ReaderState> mapEventToState(
    ReaderEvent event,
  ) async* {
    try {
      if (event is LoadReaderEvent) {
        yield ReaderParsingState(info: '检查更新');
        bool latest = true;
        // 检查经文是否有更新
        dynamic checkUpdate = await Api.buddhistCheckUpdate(data: {'articleId': event.readConfig.id, 'typeId': event.readConfig.source + 1, 'version': event.readConfig.version});
        if (checkUpdate['data'] > 0) {
          latest = false;
          if (event.readConfig.source == 3) {
            buddhistBloc.dispatch(LoadBuddhistEvent(forceUpdate: true));
          } else if (event.readConfig.source == 0) {
            taskBloc.dispatch(UserTaskEvent(forceUpdate: true));
          }
        }
        print('开始获取书籍信息****************************************');
        print(DateTime.now().millisecondsSinceEpoch);
        String htmlId = 'book_html_' + (event.readConfig.source + 1).toString() + '_' + event.readConfig.id.toString();
        // 读取本地存储HTML
        String bookHtml = SpUtil.getString(htmlId);
        event.bookHtml = bookHtml;
        LoadReaderEvent levent;
        print('开始解析HTML****************************************');
        if (bookHtml == null || bookHtml == '' || !latest) {
          yield ReaderParsingState(info: '正在下载经文···');
          print(DateTime.now().millisecondsSinceEpoch);
          Directory tempDir = await getTemporaryDirectory();
          String tempPath = tempDir.path;
          File file = File(tempPath + '/' + event.readConfig.fileUrl);
          if (!file.parent.existsSync()) {
            file.parent.createSync();
          }
          await HttpUtils.download('https://bodhiroad-file.cpolar.cn/${event.readConfig.fileUrl}', tempPath + '/' + event.readConfig.fileUrl);
          event.bookHtml = await file.readAsString();
          print(DateTime.now().millisecondsSinceEpoch);
          yield ReaderParsingState(info: '正在解析书籍信息');
          levent = event.readConfig.taskType == 4 ? readHtml(event) : await compute(readHtml, event);
          print(DateTime.now().millisecondsSinceEpoch);
          save2Local(htmlId, levent.bookHtml);

          print(DateTime.now().millisecondsSinceEpoch);
        } else {
          levent = readHtml(event);
        }
        yield ReaderParsingState(info: '解析书籍信息完成');
        print(DateTime.now().millisecondsSinceEpoch);
        print('获取书籍信息完成****************************************');
        print(DateTime.now().millisecondsSinceEpoch);
        yield ReaderLoadState(
          catalogList: levent.catelogList,
          pageList: levent.pageList,
          bookMarkList: levent.bookMarkList,
          unitList: levent.unitList,
        );
      } else if (event is ReaderInitEvent) {
        saveRecord(0, 0, event.source, event.taskType);
        yield ReaderInitState();
      } else if (event is ReaderFinishEvent) {
        saveRecord(1, 1, 1, 1);
        yield ReaderFinishState();
      } else if (event is ReaderReachBottomEvent) {
        yield ReaderReachBottomState();
      } else if (event is ReaderUnReachBottomEvent) {
        yield ReaderUnReachBottomState();
      } else if (event is ReaderFavoriteCheckEvent) {
        int isFavorite = 0;
        SpUtil.getInt('favorite_${event.buddhistId}');
        var res = await Api.validateFavorite(data: {'buddhistId': event.buddhistId});
        if (res['code'] == 200) {
          isFavorite = 1;
        }
        yield ReaderAddFavoriteSuccessState(isFavorite);
      } else if (event is ReaderAddFavoriteEvent) {
        List<Favorite> list = SpUtil.getObjList('favorite', (item) => Favorite.fromJson(item));
        if (list.length == 0) {
          list = <Favorite>[];
        }
        if (event.isFavorite == 0) {
          var it = list.iterator;
          while (it.moveNext()) {
            if (it.current != null && it.current.buddhistId == event.buddhistId) {
              list.remove(it.current);
              break;
            }
          }
          SpUtil.remove('favorite');
          Api.deleteFavorite(data: {"buddhistId": event.buddhistId});
        } else {
//          Favorite favorite = Favorite(buddhistId: event.buddhistId, fileUrl: event.fileUrl, source: event.source, taskType: event.taskType, bookName: event.bookName);
//          list.add(favorite);
//          Api.addFavorite(data: {"buddhistId": event.buddhistId});
        }
        SpUtil.putObjectList('favorite', list);
        SpUtil.putInt('favorite_${event.buddhistId}', event.isFavorite);
        yield ReaderAddFavoriteSuccessState(event.isFavorite);
//        var result = await HttpUtils.get(url, data: {"buddhistId": event.buddhistId});
      }
//      if (event.action == 'page') {
//        this.dispatch(
//            ReaderInitEvent(source: event.source, taskType: event.taskType));
//      }
    } catch (_, stackTrace) {
      print('$_ $stackTrace');
      yield currentState;
    }
  }

  /// 保存记录
  /// operate 0 新增 1修改
  saveRecord(int operate, int state, int source, int taskType) async {
    TaskRecord taskRecord = TaskRecord(
        id: GlobalConfiguration().getInt("currentTaskRecordId"), taskCount: 1, taskType: GlobalConfiguration().getInt("taskType"), state: state, source: GlobalConfiguration().getInt("source"));
    if (operate == 0) {
      var res = await Api.taskRecordAdd(data: taskRecord.toJson());
      if (res['code'] == 200) {
        GlobalConfiguration().loadFromMap({"currentTaskRecordId": res['data']});
        if (source == 0) {
          sortUserTask(GlobalConfiguration().getInt("taskId"));
        }
      }
    } else {
      await Api.taskRecordUpdate(data: taskRecord.toJson());
      if (taskRecord.state == 1) {
        GlobalConfiguration().setValue("currentTaskRecordId", 0);
      }
    }
  }

  sortUserTask(int taskId) async {
    List<Task> taskList = [];
    int index = -1;
    taskList = SpUtil.getObjList('userTaskList', (item) => Task.fromJson(item));
    taskList.forEach((item) {
      if (item.taskId == taskId) {
        index = taskList.indexOf(item);
        return;
      }
    });
    taskList.insert(0, taskList[index]);
    taskList.removeAt(index + 1);
    bool rest = await SpUtil.putObjectList('userTaskList', taskList);
    if (rest) {
      taskBloc.dispatch(UserTaskEvent(forceUpdate: false));
    }
  }

  static save2Local(String key, String value) async {
    SpUtil.putString(key, value);
  }

  /// 读取HTML文件内容
  static LoadReaderEvent readHtml(LoadReaderEvent event) {
    print('开始读取HTML文本内容并转化****************************************');
    print(DateTime.now().millisecondsSinceEpoch);
    BookInfoBean bookInfoBean = BookInfoBean(catelogList: [], pageList: [], id: event.readConfig.id.toString());

    if (map[event.readConfig.id] == null) {
      map.clear();
      List paragraphsListStr = SpUtil.getObjectList('unitList_' + (event.readConfig.source + 1).toString() + '_' + event.readConfig.id.toString());
      List<BookParagraphUnitBean> unitList = [];
      if (paragraphsListStr != null && paragraphsListStr.length > 0) {
        unitList = paragraphsListStr.map((f) => BookParagraphUnitBean.fromJson(f)).toList();
      }
      if (unitList.length == 0) {
        Document document = parse(event.bookHtml);
        print(DateTime.now().millisecondsSinceEpoch);

        if (event.readConfig.taskType == 4) {
          unitList = document
              .getElementsByTagName('body')[0]
              .children
              .map((e) => BookParagraphUnitBean(tag: e.localName, text: e.text.trim(), markIndexs: [], chars: e.localName == 'p' ? getChar(e.text.trim(), e.localName, event) : []))
              .toList();
        } else {
          unitList = document.getElementsByTagName('body')[0].children.map((e) => BookParagraphUnitBean(tag: e.localName, text: e.text.trim(), markIndexs: [])).toList();
        }

        SpUtil.putObjectList('unitList_' + (event.readConfig.source + 1).toString() + '_' + event.readConfig.id.toString(), unitList);
      }
      map[event.readConfig.id.toString()] = unitList;
    }

    print('开始计算段落分页****************************************');
    print(DateTime.now().millisecondsSinceEpoch);
//    if (event.readConfig.taskType == 4) {
//      bookInfoBean = PageUtils.zhou(
//        map[event.readConfig.id.toString()],
//        event.readConfig,
//        bookInfoBean,
//      );
//    } else if (event.readConfig.textLayOutDirection ==
//            TextLayOutDirection.VERTICAL &&
//        event.readConfig.pagingDirection == PagingDirection.HORIZONTAL) {
//      bookInfoBean = PageUtils.textVerticalPage(
//        map[event.readConfig.id.toString()],
//        event.readConfig,
//        bookInfoBean,
//      );
//    } else if (event.readConfig.textLayOutDirection ==
//            TextLayOutDirection.HORIZONTAL &&
//        event.readConfig.pagingDirection == PagingDirection.HORIZONTAL) {
//      print(event.readConfig);
//      bookInfoBean = PageUtils.textHorizontalPage(
//        map[event.readConfig.id.toString()],
//        event.readConfig,
//        bookInfoBean,
//      );
//    } else {
//      bookInfoBean = PageUtils.textVerticalScrollPage(
//        map[event.readConfig.id.toString()],
//        event.readConfig,
//        bookInfoBean,
//      );
//    }
    print('计算段落分页完成****************************************开始缓存');
    event.pageList = bookInfoBean.pageList;
    event.catelogList = bookInfoBean.catelogList;
    event.bookMarkList = bookInfoBean.bookMarkList;
    // event.unitList = map['unitList'];
    print(DateTime.now().millisecondsSinceEpoch);
    return event;
  }

  static List<CharBean> getChar(String text, String tag, LoadReaderEvent event) {
    text = text.replaceAll('\n', '');
    text = text.replaceAll(' ', '');
    List t = TextConfig.instance.rareTextIndex.keys.toList();
    t.forEach((key) {
      if (text.contains(key.toString())) {
        text = text.replaceAll(key.toString(), TextConfig.instance.rareTextIndex[key.toString()]);
      }
    });

    String pinyin = PinYinUtils.convertZh2Pinyin(text);
    List pinList = pinyin.split(' ');
    List<CharBean> charList = [];
    for (var i = 0; i < pinList.length; i++) {
      CharBean c = CharBean();
      c.char = text.substring(i, i + 1);
      c.tag = tag;
      c.pinyin = TextConfig.instance.rareTextPinyinRela[c.char] ?? pinList[i];
//      TextPainter textPainter1 =
//          TextPainter(text: TextSpan(text: c.pinyin, style: TextStyle(fontFamily: 'pingfang', fontSize: event.readConfig.textLayoutSize[tag].fontSize * 0.7)), textDirection: TextDirection.ltr);
//      textPainter1.layout();
//      c.pinyinWidth = textPainter1.size.width;
      charList.add(c);
    }
    return charList;
  }
}
