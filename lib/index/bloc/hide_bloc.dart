import 'dart:async';
import 'package:bloc/bloc.dart';

class HideBloc extends Bloc<double, double> {
  static final HideBloc _HideBlocSingleton = new HideBloc._internal();
  factory HideBloc() {
    return _HideBlocSingleton;
  }
  HideBloc._internal();

  double get initialState => 0.0;

  @override
  Stream<double> mapEventToState(
    double event,
  ) async* {
    try {
      yield event;
    } catch (_, stackTrace) {
      print('$_ $stackTrace');
      yield currentState;
    }
  }
}
