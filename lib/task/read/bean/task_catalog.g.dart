// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'task_catalog.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

TaskCatalog _$TaskCatalogFromJson(Map<String, dynamic> json) {
  return TaskCatalog(
      catalogName: json['catalogName'] as String,
      list: (json['list'] as List)
          ?.map((e) =>
              e == null ? null : Task.fromJson(e as Map<String, dynamic>))
          ?.toList());
}

Map<String, dynamic> _$TaskCatalogToJson(TaskCatalog instance) =>
    <String, dynamic>{
      'catalogName': instance.catalogName,
      'list': instance.list
    };
