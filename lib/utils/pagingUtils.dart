import 'dart:math' as math;

import 'package:bodhiroad/config/ReaderThemeConfig.dart';
import 'package:bodhiroad/reader/bean/book_catalog.dart';
import 'package:bodhiroad/reader/bean/book_page.dart';
import 'package:bodhiroad/reader/bean/book_paragraph_unit.dart';
import 'package:bodhiroad/reader/bean/bookmark.dart';
import 'package:bodhiroad/reader/bean/char.dart';
import 'package:bodhiroad/reader/bean/line_char.dart';
import 'package:bodhiroad/reader/bean/text_layout_size.dart';
import 'package:bodhiroad/reader/buddhist_reader_bloc_state.dart';
import 'package:bodhiroad/utils/GlobalConfigUtil.dart';

/// 分页计算工具类
class PagingUtils {
  /// 文字水平排版，向左滑动翻页
  static ReloadReaderState textHorizontalLayoutWithFlip2Left(List<BookParagraphUnitBean> paragraphList, ReloadReaderState book) {
    /// 不同字号对应的文字宽高尺寸
    Map<String, TextLayoutSize> map = book.map;

    /// 分页集合
    List<PageBean> pageList = [];

    /// 目录集合
    List<CatalogBean> catalogList = [];

    /// 书签集合
    List<BookMark> bookMarkList = [];

    /// 行字符串集合
    List<LineChar> lineList = [];

    /// 章节索引
    int chapterIndex = -1;

    /// 字体大小尺寸key前缀
    String keyPrefix = ReaderThemeConfig.fontSizeList[book.configuration.getInt('fontSize')].toString() + '-';

    /// 行垂直偏移量
    double yOffset = 0;
    double actualViewWidth = 0;
    int maxPCharCount = (book.configuration.get('viewWidth') ~/ map[keyPrefix + 'p'].width);
    actualViewWidth = maxPCharCount * map[keyPrefix + 'p'].width;
    double padLeft = (book.configuration.get('viewWidth') - actualViewWidth) / 2;
    List<BookMark> lst = book.bookMarkList2;

    /// 开始循环所有段落
    for (var k = 0; k < paragraphList.length; k++) {
//      print('-------------' + k.toString());
      BookParagraphUnitBean bookParagraphUnitBean = paragraphList[k];

      /// 本段字符串
      String charStr = bookParagraphUnitBean.text;
      if (charStr.length == 0) {
        continue;
      }
//      print('-------------' + charStr);

      /// 每行最多容纳字符数
      int maxCharCount = (actualViewWidth ~/ map[keyPrefix + bookParagraphUnitBean.tag].width);

      /// 段落整体起始偏移量
      double xOffset = (actualViewWidth - maxCharCount * map[keyPrefix + bookParagraphUnitBean.tag].width) / 2;

      /// 本段落行数
      int paragraphLineCount = bookParagraphUnitBean.tag == 'p' ? ((charStr.length + 2) / maxCharCount).ceil() : ((charStr.length) / maxCharCount).ceil();

      /// 一页最多容纳的行数
      int maxLinePerPage = book.configuration.get('viewHeight') ~/ map[keyPrefix + bookParagraphUnitBean.tag].heightHorizontal;

      /// 本段落开始截取位置
      int startIndex = 0;

      /// 本段落分页结束截取位置
      int endIndex = 0;

      /// 目录
      CatalogBean catalog;
      if (bookParagraphUnitBean.tag == 'h1' || bookParagraphUnitBean.tag == 'h2') {
        catalog = CatalogBean(title: charStr, level: bookParagraphUnitBean.tag == 'h1' ? 1 : 2, fullTextIndex: bookParagraphUnitBean.firstIndex);
        chapterIndex++;
      }

      /// 段落起始偏移量
      double lineStartOffset = 0;

      if (yOffset + map[keyPrefix + bookParagraphUnitBean.tag].heightHorizontal > book.configuration.getDouble('viewHeight')) {
        pageList.add(PageBean(charList: lineList, chapterIndex: chapterIndex, padLeft: padLeft));
        lineList = [];
        yOffset = 0;
        if (catalog != null) {
          catalog.pageIndex = pageList.length - 1;
          catalogList.add(catalog);
          catalog = null;
        }
      }

      /// 段落字符集合
      LineChar lineChar;

      /// 本段是否需要分页
      if (yOffset + paragraphLineCount * map[keyPrefix + bookParagraphUnitBean.tag].heightHorizontal > book.configuration.getDouble('viewHeight')) {
        /// 分页
        /// 计算本页最多还能容纳几行【本段一定需要分页】
        int reduceLineNum = (book.configuration.getDouble('viewHeight') - yOffset) ~/ map[keyPrefix + bookParagraphUnitBean.tag].heightHorizontal;
        endIndex = math.min(startIndex + maxCharCount * reduceLineNum - 1 - (bookParagraphUnitBean.tag == 'p' ? 2 : 0), charStr.length - 1);

        if (catalog != null) {
          catalog.pageIndex = pageList.length;
          catalogList.add(catalog);
          catalog = null;
        }

        /// 分页之前的行
        /// 如果是非 p 标签 段落居中
        if (bookParagraphUnitBean.tag != 'p') {
          lineStartOffset = (actualViewWidth - (endIndex - startIndex + 1) * map[keyPrefix + bookParagraphUnitBean.tag].width) / 2;
        } else {
          /// 首行缩进两个字符
          lineStartOffset = map[keyPrefix + bookParagraphUnitBean.tag].width * 2;
        }
        lineChar = LineChar(
            tag: bookParagraphUnitBean.tag,
            fullTextIndex: bookParagraphUnitBean.firstIndex,
            xOffset: lineStartOffset,
            yOffset: yOffset,
            lineGroup: [],
            fontSize: map[keyPrefix + bookParagraphUnitBean.tag].fontSize);

        List<BookMark> spanList = includeBookmark(bookParagraphUnitBean.firstIndex + startIndex, bookParagraphUnitBean.firstIndex + endIndex, lst);
        if (spanList.length > 0) {
          spanList.forEach((span) {
            lineChar.lineGroup.add(Char(
                id: span.isMarked && span.isFirst ? span.actualStartIndex : -1,
                chars: charStr.substring(span.startIndex - bookParagraphUnitBean.firstIndex, span.endIndex - bookParagraphUnitBean.firstIndex + 1),
                actualChars: span.isMarked ? span.title : '',
                isMarked: span.isMarked,
                bold: bookParagraphUnitBean.bold,
                isTitle: bookParagraphUnitBean.tag == 'h1' || bookParagraphUnitBean.tag == 'h2',
                maskType: span.markType));
            if (span.isMarked && span.isFirst) {
              span.chapterIndex = chapterIndex;
              span.pageIndex = pageList.length;
              bookMarkList.add(span);
            }
          });
        } else {
          lineChar.lineGroup.add(Char(
            chars: charStr.substring(startIndex, endIndex + 1),
            isMarked: false,
            bold: bookParagraphUnitBean.bold,
            isTitle: bookParagraphUnitBean.tag == 'h1' || bookParagraphUnitBean.tag == 'h2',
          ));
        }
        lineList.add(lineChar);
//        yOffset += paragraphLineCount * map[keyPrefix + bookParagraphUnitBean.tag].heightHorizontal;
        pageList.add(PageBean(charList: lineList, chapterIndex: chapterIndex, padLeft: padLeft));
        lineList = [];
        yOffset = 0;
        startIndex = endIndex + 1;

        /// 剩余段落分页
        String reduceChar = charStr.substring(endIndex);
        var reducePageNum = (reduceChar.length / (maxCharCount * maxLinePerPage)).ceil();
        for (int m = 0; m < reducePageNum; m++) {
          LineChar lineChar2 = LineChar(
              tag: bookParagraphUnitBean.tag,
              fullTextIndex: bookParagraphUnitBean.firstIndex + startIndex + m * maxCharCount,
              xOffset: 0,
              yOffset: yOffset,
              lineGroup: [],
              fontSize: map[keyPrefix + bookParagraphUnitBean.tag].fontSize);
          int pageStartIndex = bookParagraphUnitBean.firstIndex + endIndex + 1 + m * maxLinePerPage * maxCharCount;
          int pageEndIndex = math.min(bookParagraphUnitBean.firstIndex + startIndex + (m + 1) * (maxLinePerPage * maxCharCount) - 1, bookParagraphUnitBean.firstIndex + charStr.length - 1);

          List<BookMark> spanList = includeBookmark(pageStartIndex, pageEndIndex, lst);

          /// 如果有标记
          if (spanList.length > 0) {
            spanList.forEach((span) {
              lineChar2.lineGroup.add(Char(
                  id: span.isMarked && span.isFirst ? span.actualStartIndex : -1,
                  chars: charStr.substring(span.startIndex - bookParagraphUnitBean.firstIndex, span.endIndex - bookParagraphUnitBean.firstIndex + 1),
                  isMarked: span.isMarked,
                  bold: bookParagraphUnitBean.bold,
                  isTitle: bookParagraphUnitBean.tag == 'h1' || bookParagraphUnitBean.tag == 'h2',
                  actualChars: span.isMarked ? span.title : '',
                  maskType: span.markType));

              if (span.isMarked && span.isFirst) {
                span.chapterIndex = chapterIndex;
                span.pageIndex = pageList.length;
//                  span.yOffset = yOffset + pLineIndex * map[keyPrefix + bookParagraphUnitBean.tag].heightHorizontal;
                bookMarkList.add(span);
              }
            });
          } else {
            /// 如果无标记
            lineChar2.lineGroup.add(Char(
                chars: charStr.substring(pageStartIndex - bookParagraphUnitBean.firstIndex, pageEndIndex - bookParagraphUnitBean.firstIndex + 1),
                isTitle: bookParagraphUnitBean.tag == 'h1' || bookParagraphUnitBean.tag == 'h2',
                bold: bookParagraphUnitBean.bold,
                isMarked: false));
          }
          lineList.add(lineChar2);
          yOffset += (((pageEndIndex - pageStartIndex + 1) / maxCharCount).ceil()) * map[keyPrefix + bookParagraphUnitBean.tag].heightHorizontal;
          if (m != reducePageNum - 1 || ((pageEndIndex - pageStartIndex + 1) / maxCharCount).ceil() >= maxLinePerPage) {
            pageList.add(PageBean(charList: lineList, chapterIndex: chapterIndex, padLeft: padLeft));
            lineList = [];
            yOffset = 0;
          }

          /// 到此剩余段落分页完成，继续下一个段落循环
//          break;
        }
      } else {
        if (catalog != null) {
          catalog.pageIndex = pageList.length;
          catalogList.add(catalog);
          catalog = null;
        }

        /// 无需分页情况
        /// 需要居中的多行段落，需要拆分按行计算
        if (paragraphLineCount > 1 && bookParagraphUnitBean.tag != 'p') {
          for (int pLineIndex = 0; pLineIndex < paragraphLineCount; pLineIndex++) {
            endIndex = math.min(startIndex + (pLineIndex + 1) * maxCharCount - 1, charStr.length - 1);
            String dividedParagraphText = charStr.substring(startIndex, endIndex + 1);
            lineStartOffset = (actualViewWidth - dividedParagraphText.length * map[keyPrefix + bookParagraphUnitBean.tag].width) / 2;
            lineChar = LineChar(
                tag: bookParagraphUnitBean.tag,
                fullTextIndex: bookParagraphUnitBean.firstIndex + pLineIndex * maxCharCount,
                xOffset: lineStartOffset,
                yOffset: yOffset,
                lineGroup: [],
                fontSize: map[keyPrefix + bookParagraphUnitBean.tag].fontSize);

            /// 获取本段标记集合
            List<BookMark> spanList = includeBookmark(bookParagraphUnitBean.firstIndex + startIndex, bookParagraphUnitBean.firstIndex + dividedParagraphText.length - 1, lst);
            if (spanList.length > 0) {
              spanList.forEach((span) {
                lineChar.lineGroup.add(Char(
                    id: span.isMarked && span.isFirst ? span.actualStartIndex : -1,
                    chars: dividedParagraphText,
                    actualChars: span.isMarked ? span.title : '',
                    isMarked: span.isMarked,
                    bold: bookParagraphUnitBean.bold,
                    isTitle: bookParagraphUnitBean.tag == 'h1' || bookParagraphUnitBean.tag == 'h2',
                    maskType: span.markType));
                if (span.isMarked && span.isFirst) {
                  span.chapterIndex = chapterIndex;
                  span.pageIndex = pageList.length;
                  bookMarkList.add(span);
                }
              });
            } else {
              lineChar.lineGroup
                  .add(Char(chars: dividedParagraphText, bold: bookParagraphUnitBean.bold, isTitle: bookParagraphUnitBean.tag == 'h1' || bookParagraphUnitBean.tag == 'h2', isMarked: false));
            }
            lineList.add(lineChar);
            yOffset += map[keyPrefix + bookParagraphUnitBean.tag].heightHorizontal;
            startIndex = endIndex + 1;
          }
        } else {
          /// 多行的p标签或者单行的非p标签
          if (bookParagraphUnitBean.tag == 'p') {
            lineStartOffset = map[keyPrefix + bookParagraphUnitBean.tag].width * 2;
          } else {
            lineStartOffset = (actualViewWidth - charStr.length * map[keyPrefix + bookParagraphUnitBean.tag].width) / 2;
          }
          lineChar = LineChar(
              tag: bookParagraphUnitBean.tag,
              fullTextIndex: bookParagraphUnitBean.firstIndex,
              xOffset: lineStartOffset,
              yOffset: yOffset,
              lineGroup: [],
              fontSize: map[keyPrefix + bookParagraphUnitBean.tag].fontSize);
          List<BookMark> spanList = includeBookmark(bookParagraphUnitBean.firstIndex, bookParagraphUnitBean.firstIndex + charStr.length - 1, lst);
          if (spanList.length > 0) {
            spanList.forEach((span) {
              lineChar.lineGroup.add(Char(
                  id: span.isMarked && span.markType == 2 ? span.actualStartIndex : -1,
                  chars: charStr.substring(span.startIndex - bookParagraphUnitBean.firstIndex, span.endIndex - bookParagraphUnitBean.firstIndex + 1),
                  actualChars: span.isMarked ? span.title : '',
                  isTitle: bookParagraphUnitBean.tag == 'h1' || bookParagraphUnitBean.tag == 'h2',
                  bold: bookParagraphUnitBean.bold,
                  isMarked: span.isMarked,
                  maskType: span.markType));
              if (span.isMarked && span.isFirst) {
                span.chapterIndex = chapterIndex;
                span.pageIndex = pageList.length;
                bookMarkList.add(span);
              }
            });
          } else {
            lineChar.lineGroup.add(Char(
              chars: charStr,
              isTitle: bookParagraphUnitBean.tag == 'h1' || bookParagraphUnitBean.tag == 'h2',
              bold: bookParagraphUnitBean.bold,
            ));
          }
          lineList.add(lineChar);
          yOffset += paragraphLineCount * map[keyPrefix + bookParagraphUnitBean.tag].heightHorizontal;
        }
      }
    }
    if (lineList.length > 0) {
      pageList.add(PageBean(charList: lineList, chapterIndex: chapterIndex, padLeft: padLeft));
      lineList = [];
      yOffset = 0;
    }
    book.pageList = pageList;
    book.catalogList = catalogList;
    book.bookMarkList2 = bookMarkList;
    return book;
  }

  /// 文字直排，向右滑动翻页
  static ReloadReaderState textVerticalLayoutWithFlip2Left(List<BookParagraphUnitBean> paragraphList, ReloadReaderState book) {
    /// 不同字号对应的文字宽高尺寸
    Map<String, TextLayoutSize> map = book.map;

    /// 分页集合
    List<PageBean> pageList = [];

    /// 目录集合
    List<CatalogBean> catalogList = [];

    /// 书签集合
    List<BookMark> bookMarkList = [];

    /// 行字符串集合
    List<LineChar> lineList = [];

    /// 章节索引
    int chapterIndex = -1;

    /// 字体大小尺寸key前缀
    String keyPrefix = ReaderThemeConfig.fontSizeList[book.configuration.getInt('fontSize')].toString() + '-';

    /// 全文字符索引
    int charIndex = 0;

    /// 页面第一个字符在全文中的索引
    int pageFirstCharIndex = 0;

    /// 每一列水平偏移量
    double xOffset = 0;
    List<BookMark> lst = book.bookMarkList2;

    /// 开始循环所有段落
    for (var k = 0; k < paragraphList.length; k++) {
      BookParagraphUnitBean bookParagraphUnitBean = paragraphList[k];

      /// 本段字符串
      String charStr = bookParagraphUnitBean.text;

      /// 每列最多容纳字符数
      int maxCharCount = (book.configuration.get('viewHeight') ~/ map[keyPrefix + bookParagraphUnitBean.tag].heightVertical);

      /// 段落起始偏移量
      double yOffset = (book.configuration.get('viewHeight') - maxCharCount * map[keyPrefix + bookParagraphUnitBean.tag].heightVertical) / 2;

      /// 本段落列数
      int paragraphLineCount = bookParagraphUnitBean.tag == 'p' ? ((charStr.length + 2) / maxCharCount).ceil() : ((charStr.length) / maxCharCount).ceil();

      /// 本段落分行开始截取位置
      int startIndex = 0;

      /// 本段落分行结束截取位置
      int endIndex = 0;

      /// 目录
      CatalogBean catalog;
      if (bookParagraphUnitBean.tag == 'h1' || bookParagraphUnitBean.tag == 'h2') {
        catalog = CatalogBean(title: charStr, level: bookParagraphUnitBean.tag == 'h1' ? 1 : 2, fullTextIndex: bookParagraphUnitBean.firstIndex);
        chapterIndex++;
      }

      /// 遍历行
      for (var pColumnIndex = 0; pColumnIndex < paragraphLineCount; pColumnIndex++) {
        /// 是否是最后一列
        bool lastLine = k == paragraphList.length - 1 && pColumnIndex == paragraphLineCount - 1;

        /// 行缩进
        double columnOffset = 0;

        if (bookParagraphUnitBean.tag == 'p') {
          if (pColumnIndex == 0) {
            endIndex = math.min(charStr.length - 1, maxCharCount - 2 - 1);
          } else {
            endIndex = math.min(startIndex + maxCharCount - 1, charStr.length - 1);
          }
        } else {
          endIndex = math.min(startIndex + maxCharCount - 1, charStr.length - 1);
        }

        /// 段落首列缩进
        if (pColumnIndex == 0 && bookParagraphUnitBean.tag == 'p') {
          columnOffset += yOffset + map[keyPrefix + bookParagraphUnitBean.tag].heightVertical * 2;
        } else if (bookParagraphUnitBean.tag != 'p') {
          columnOffset += map[keyPrefix + bookParagraphUnitBean.tag].heightVertical;
        } else {
          columnOffset += yOffset;
        }

        /// 每列开始一个新的LineChar
        LineChar lineChar = LineChar(
            lineGroup: [],
            fontSize: map[keyPrefix + bookParagraphUnitBean.tag].fontSize,
            tag: bookParagraphUnitBean.tag,
            xOffset: columnOffset,
            fullTextIndex: bookParagraphUnitBean.firstIndex + startIndex);

        /// 判断新增一列是否需要分页
        if (book.configuration.get('viewWidth') < xOffset + map[keyPrefix + bookParagraphUnitBean.tag].width * 2) {
          pageList.insert(0, PageBean(charList: lineList, fullTextIndex: pageFirstCharIndex, chapterIndex: chapterIndex));
          pageFirstCharIndex = charIndex;

          /// 清空每页行集合
          lineList = [];
          xOffset = 0;
        }
        if (catalog != null) {
          catalog.pageIndex = pageList.length;
          catalogList.add(catalog);
          catalog = null;
        }
        List<BookMark> spanList = includeBookmark(bookParagraphUnitBean.firstIndex + startIndex, bookParagraphUnitBean.firstIndex + endIndex, lst);
        if (spanList.length > 0) {
          spanList.forEach((span) {
            lineChar.lineGroup.add(Char(
                id: span.isMarked && span.isFirst ? span.actualStartIndex : -1,
                chars: charStr.substring(span.startIndex - bookParagraphUnitBean.firstIndex, span.endIndex - bookParagraphUnitBean.firstIndex + 1),
                actualChars: span.isMarked ? span.title : '',
                isTitle: bookParagraphUnitBean.tag == 'h1' || bookParagraphUnitBean.tag == 'h2',
                isMarked: span.isMarked,
                bold: bookParagraphUnitBean.bold,
                maskType: span.markType));
            if (span.isMarked && span.isFirst) {
              span.pageIndex = pageList.length;
              span.chapterIndex = chapterIndex;
              bookMarkList.add(span);
            }
          });
        } else {
          lineChar.lineGroup.add(Char(chars: charStr.substring(startIndex, endIndex + 1), isTitle: bookParagraphUnitBean.tag == 'h1' || bookParagraphUnitBean.tag == 'h2', isMarked: false));
        }

        /// 行结束index+
        charIndex += (endIndex - startIndex + 1);
        startIndex = endIndex + 1;
        lineList.add(lineChar);
        if (lastLine) {
          pageList.insert(0, PageBean(charList: lineList, fullTextIndex: pageFirstCharIndex, chapterIndex: chapterIndex));
          pageFirstCharIndex = charIndex;

          /// 清空每页行集合
          lineList = [];
          yOffset = 0;
        }
        xOffset += map[keyPrefix + bookParagraphUnitBean.tag].width * 2;
      }
    }
    List<PageBean> nList = [];
    List<CatalogBean> cList = [];

    CatalogBean first = null;
    CatalogBean last = null;

    List<PageBean> newList = pageList.reversed.toList();
    for (int i = 0; i < pageList.length; i++) {
      /// 从头开始分页
      PageBean pageBean = pageList[i];

      /// 页面中所有列
      List<LineChar> columnCharList = pageBean.charList;
      columnCharList.reversed.forEach((columnChar) {
        /// 判断标题，加入目录列表
        if (columnChar.tag == 'h1' || columnChar.tag == 'h2') {
          CatalogBean temp = CatalogBean(tag: columnChar.tag, title: columnChar.lineGroup[0].chars, pageIndex: i);
//          if (first == null) {
//            first = temp;
//          } else {
//            last = temp;
//          }
          cList.insert(0, temp);
        }
      });
//      pageBean.chapterIndex = cList.length;

//      if (first == null) {
//        pageBean.firstCatalog = cList.first;
//        pageBean.lastCatalog = cList.first;
//      } else if (last == null) {
//        pageBean.firstCatalog = first;
//        pageBean.lastCatalog = first;
//      } else {
//        pageBean.firstCatalog = first;
//        pageBean.lastCatalog = last;
//      }
//      first = null;
//      last = null;
//      nList.add(pageBean);
    }
    book.pageList = pageList;
    book.catalogList = cList;
    book.bookMarkList2 = bookMarkList;
    return book;
  }

  /// 文字水平排版，上下滑动翻页
  static ReloadReaderState textHorizontalLayoutWithVerticalScroll2(List<BookParagraphUnitBean> paragraphList, ReloadReaderState book) {
    /// 不同字号对应的文字宽高尺寸
    Map<String, TextLayoutSize> map = book.map;

    /// 分页集合
    List<PageBean> pageList = [];

    /// 目录集合
    List<CatalogBean> catalogList = [];

    /// 书签集合
    List<BookMark> bookMarkList = [];

    /// 行字符串集合
    List<LineChar> lineList = [];

    /// 章节索引
    int chapterIndex = -1;

    /// 字体大小尺寸key前缀
    String keyPrefix = ReaderThemeConfig.fontSizeList[book.configuration.getInt('fontSize')].toString() + '-';

    /// 全文字符索引
    int charIndex = 0;

    /// 页面第一个字符在全文中的索引
    int pageFirstCharIndex = 0;

    /// 行垂直偏移量
    double yOffset = 0;

    /// 全文垂直偏移量
    double fullTextOffset = 0.0;
    double actualViewWidth = 0;
    int maxPCharCount = (book.configuration.get('viewWidth') ~/ map[keyPrefix + 'p'].width);
    actualViewWidth = maxPCharCount * map[keyPrefix + 'p'].width;
    double padLeft = (book.configuration.get('viewWidth') - actualViewWidth) / 2;

    List<BookMark> lst = book.bookMarkList2;
//    lst = [];

    /// 开始循环所有段落
    for (var k = 0; k < paragraphList.length; k++) {
      BookParagraphUnitBean bookParagraphUnitBean = paragraphList[k];
      if (bookParagraphUnitBean.tag == 'img') {
        continue;
      }

      /// 本段字符串
      String charStr = bookParagraphUnitBean.text;

      /// 每行最多容纳字符数
      int maxCharCount = (actualViewWidth ~/ map[keyPrefix + bookParagraphUnitBean.tag].width);

      /// 段落起始偏移量
      double xOffset = (actualViewWidth - maxCharCount * map[keyPrefix + bookParagraphUnitBean.tag].width) / 2;

      /// 本段落行数
      int paragraphLineCount = bookParagraphUnitBean.tag == 'p' ? ((charStr.length + 2) / maxCharCount).ceil() : ((charStr.length) / maxCharCount).ceil();

      /// 本段落分行开始截取位置
      int startIndex = 0;

      /// 本段落分行结束截取位置
      int endIndex = 0;

//      charIndex+=charStr.length;
      /// 目录
      CatalogBean catalog;
      if (bookParagraphUnitBean.tag == 'h1' || bookParagraphUnitBean.tag == 'h2') {
        catalog = CatalogBean(title: charStr, level: bookParagraphUnitBean.tag == 'h1' ? 1 : 2, fullTextIndex: bookParagraphUnitBean.firstIndex);

        chapterIndex++;
        if (lineList.length > 0) {
          pageList.add(PageBean(charList: lineList, chapterIndex: chapterIndex, padLeft: padLeft));
          lineList = [];
        }
        catalog.yOffset = yOffset;
        catalogList.add(catalog);
      }
      double lineXoff = 0.0;

      if (paragraphLineCount > 1 && bookParagraphUnitBean.tag != 'p') {
        for (int pLineIndex = 0; pLineIndex < paragraphLineCount; pLineIndex++) {
          endIndex = math.min(maxCharCount + startIndex - 1, charStr.length - 1);
          lineXoff = (actualViewWidth - (endIndex - startIndex + 1) * map[keyPrefix + bookParagraphUnitBean.tag].width) / 2;

          LineChar lineChar = LineChar(
              tag: bookParagraphUnitBean.tag,
              fullTextIndex: bookParagraphUnitBean.firstIndex,
              xOffset: lineXoff,
              yOffset: fullTextOffset,
              lineGroup: [],
              fontSize: map[keyPrefix + bookParagraphUnitBean.tag].fontSize);
          List<BookMark> spanList = includeBookmark(bookParagraphUnitBean.firstIndex + startIndex, bookParagraphUnitBean.firstIndex + endIndex, lst);

          if (spanList.length > 0) {
            spanList.forEach((span) {
              lineChar.lineGroup.add(Char(
                  id: span.isMarked && span.isFirst ? span.actualStartIndex : -1,
                  chars: charStr.substring(span.startIndex - bookParagraphUnitBean.firstIndex, span.endIndex - bookParagraphUnitBean.firstIndex + 1),
                  actualChars: span.isMarked ? span.title : '',
                  isTitle: bookParagraphUnitBean.tag == 'h1' || bookParagraphUnitBean.tag == 'h2',
                  isMarked: span.isMarked,
                  bold: bookParagraphUnitBean.bold,
                  maskType: span.markType));
              if (span.isMarked && span.isFirst) {
                span.yOffset = yOffset + pLineIndex * map[keyPrefix + bookParagraphUnitBean.tag].heightHorizontal;
                bookMarkList.add(span);
              }
            });
          } else {
            lineChar.lineGroup.add(Char(
                chars: charStr.substring(startIndex, endIndex + 1),
                bold: bookParagraphUnitBean.bold,
                isTitle: bookParagraphUnitBean.tag == 'h1' || bookParagraphUnitBean.tag == 'h2',
                isMarked: false));
          }
          lineList.add(lineChar);
          yOffset += map[keyPrefix + bookParagraphUnitBean.tag].heightHorizontal;
          fullTextOffset += paragraphLineCount * map[keyPrefix + bookParagraphUnitBean.tag].heightHorizontal;
          startIndex = endIndex + 1;
        }
      } else {
        if (bookParagraphUnitBean.tag == 'p') {
          lineXoff = map[keyPrefix + bookParagraphUnitBean.tag].width * 2;
        } else {
          lineXoff = (actualViewWidth - charStr.length * map[keyPrefix + bookParagraphUnitBean.tag].width) / 2;
        }
        LineChar lineChar = LineChar(
            tag: bookParagraphUnitBean.tag,
            fullTextIndex: bookParagraphUnitBean.firstIndex,
            xOffset: lineXoff,
            yOffset: fullTextOffset,
            lineGroup: [],
            fontSize: map[keyPrefix + bookParagraphUnitBean.tag].fontSize);
        List<BookMark> spanList = includeBookmark(bookParagraphUnitBean.firstIndex, bookParagraphUnitBean.firstIndex + charStr.length - 1, lst);
        if (spanList.length > 0) {
          spanList.forEach((span) {
            lineChar.lineGroup.add(Char(
                id: span.isMarked && span.markType == 2 ? span.actualStartIndex : -1,
                chars: charStr.substring(span.startIndex - bookParagraphUnitBean.firstIndex, span.endIndex - bookParagraphUnitBean.firstIndex + 1),
                actualChars: span.isMarked ? span.title : '',
                isTitle: bookParagraphUnitBean.tag == 'h1' || bookParagraphUnitBean.tag == 'h2',
                bold: bookParagraphUnitBean.bold,
                isMarked: span.isMarked,
                maskType: span.markType));
            if (span.isMarked && span.isFirst) {
              int markedLineNun = bookParagraphUnitBean.tag == 'p' ? (span.startIndex - bookParagraphUnitBean.firstIndex + 2) ~/ maxCharCount : 0;
              span.yOffset = yOffset + markedLineNun * map[keyPrefix + bookParagraphUnitBean.tag].heightHorizontal;
              bookMarkList.add(span);
            }
          });
        } else {
          lineChar.lineGroup.add(Char(
            chars: charStr,
            bold: bookParagraphUnitBean.bold,
            isTitle: bookParagraphUnitBean.tag == 'h1' || bookParagraphUnitBean.tag == 'h2',
          ));
        }
        lineList.add(lineChar);
        yOffset += paragraphLineCount * map[keyPrefix + bookParagraphUnitBean.tag].heightHorizontal;
        fullTextOffset += paragraphLineCount * map[keyPrefix + bookParagraphUnitBean.tag].heightHorizontal;
      }

      if (k == paragraphList.length - 1 && lineList.length > 0) {
        pageList.add(PageBean(charList: lineList, chapterIndex: chapterIndex, padLeft: padLeft));
      }
    }

    book.pageList = pageList;
    book.catalogList = catalogList;
    book.bookMarkList2 = bookMarkList;
    return book;
  }

  /// 返回段落Textspan【包含普通文本区间和书签文本区间】

  /// 返回该段包含的书签
  /// startIndex段落起始位置在全文的索引
  /// endIndex段落起始位置在全文的索引
  static List<BookMark> includeBookmark(int startIndex, int endIndex, List<BookMark> list) {
    List<BookMark> included = [];
    for (int i = 0; i < list.length; i++) {
      BookMark mark = list[i];

      if (!(mark.actualStartIndex > endIndex || mark.actualEndIndex - 1 < startIndex)) {
        mark.startIndex = mark.actualStartIndex < startIndex ? startIndex : mark.actualStartIndex;
        mark.endIndex = mark.actualEndIndex - 1 > endIndex ? endIndex : mark.actualEndIndex - 1;
        mark.isMarked = true;

        /// 只有这种情况下会落在多个段落
        if (mark.actualStartIndex >= startIndex && mark.actualStartIndex - 1 <= endIndex) {
          mark.isFirst = true;
        } else {
          mark.isFirst = false;
        }
        if (included.length > 0) {
          if (mark.endIndex <= included.last.endIndex && mark.startIndex >= included.last.startIndex) {
            continue;
          } else if (mark.startIndex <= included.last.endIndex) {
            included.last.endIndex = mark.endIndex;
            continue;
          }
        }
        included.add(mark);
      }
    }

    if (included == null || included.length == 0) {
      return [];
    }
    List<BookMark> newInclude2 = [];
    for (int i = 0; i < included.length; i++) {
      BookMark bean = included[i];
      // 加前一个
      if (i == 0 && bean.startIndex != startIndex) {
        newInclude2.add(BookMark(startIndex: startIndex, endIndex: bean.startIndex - 1, isMarked: false));
      } else if (i > 0) {
        if (newInclude2.last.isMarked) {
          newInclude2.add(BookMark(startIndex: included[i - 1].endIndex, endIndex: bean.startIndex, isMarked: false));
        }
      }
      // 加自身
      newInclude2.add(bean);
      // 加后一个
      if (i != included.length - 1) {
        newInclude2.add(BookMark(startIndex: bean.endIndex + 1, endIndex: included[i + 1].startIndex - 1, isMarked: false));
      } else {
        if (bean.endIndex != endIndex) {
          newInclude2.add(BookMark(startIndex: bean.endIndex + 1, endIndex: endIndex, isMarked: false));
        }
      }
    }
    return newInclude2;
  }
}
