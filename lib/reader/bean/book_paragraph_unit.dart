import 'package:bodhiroad/reader/bean/book_char.dart';
import 'package:json_annotation/json_annotation.dart';
part 'book_paragraph_unit.g.dart';

@JsonSerializable()
class BookParagraphUnitBean {
  String text;
  String textTraditional;
  int type;
  String tag;
  int marked = 0;
  int firstIndex = 0;
  int endIndex = 0;
  bool bold = false;
  String image = "";
  bool isImage = false;
  bool colored = false;
  List<int> markIndexs = [];
  List<CharBean> chars = [];

  BookParagraphUnitBean(
      {this.text,
      this.type,
      this.tag,
      this.textTraditional,
      this.marked,
      this.chars,
      this.firstIndex,
      this.endIndex,
      this.markIndexs,
      this.bold,
      this.colored,
      this.image,
      this.isImage});

  factory BookParagraphUnitBean.fromJson(Map<String, dynamic> json) {
    return _$BookParagraphUnitBeanFromJson(json);
  }
  Map<String, dynamic> toJson() => _$BookParagraphUnitBeanToJson(this);
}
