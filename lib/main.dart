import 'dart:io';
import 'dart:ui';

import 'package:bloc/bloc.dart';
import 'package:bodhiroad/components/bodhiroad_dialog_confirm.dart';
import 'package:bodhiroad/reader/bean/read_config.dart';
import 'package:bodhiroad/reader/bean/text_layout_size.dart';
import 'package:bodhiroad/simple_bloc_delegate.dart';
import 'package:bodhiroad/task/zen/bloc/music_bloc.dart';
import 'package:bodhiroad/task/zen/bloc/music_event.dart';
import 'package:bodhiroad/task/zen/bloc/music_state.dart';
import 'package:bodhiroad/theme/theme_bloc.dart';
import 'package:bodhiroad/utils/DatabaseUtils.dart';
import 'package:bodhiroad/utils/DateUtils.dart';
import 'package:bodhiroad/utils/Route.dart';
import 'package:fluro/fluro.dart';
import 'package:flustars/flustars.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';

import 'package:global_configuration/global_configuration.dart';
import 'package:oktoast/oktoast.dart';
import 'package:path_provider/path_provider.dart';
import 'package:rxdart/rxdart.dart';
import 'MyLocalizationsDelegates.dart';
import 'components/extended_text/lib/extended_text.dart';
import 'config/GlobalColorCOnfig.dart';
import 'config/ReaderThemeConfig.dart';
import 'index/pages/index.dart';
import 'usercenter/bean/user.dart';

final FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin = FlutterLocalNotificationsPlugin();

// Streams are created so that app can respond to notification-related events since the plugin is initialised in the `main` function
final BehaviorSubject<ReceivedNotification> didReceiveLocalNotificationSubject = BehaviorSubject<ReceivedNotification>();

final BehaviorSubject<String> selectNotificationSubject = BehaviorSubject<String>();

class ReceivedNotification {
  final int id;
  final String title;
  final String body;
  final String payload;

  ReceivedNotification({@required this.id, @required this.title, @required this.body, @required this.payload});
}

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  BlocSupervisor.delegate = SimpleBlocDelegate();
  Directory directory = await getApplicationDocumentsDirectory();
  Directory dr = Directory('${directory.path}/headPortrait');
  if (!dr.existsSync()) {
    dr.create(recursive: true);
  }
  // 初始化路由配置
  Routes.configureRoutes(Router());
  // 初始化SpUtil
  await SpUtil.getInstance();

  ReaderThemeConfig.initialFontSizeList();

  runApp(MyApp());
  var initializationSettingsAndroid = AndroidInitializationSettings('app_icon');
  var initializationSettingsIOS = IOSInitializationSettings(onDidReceiveLocalNotification: (int id, String title, String body, String payload) async {
    didReceiveLocalNotificationSubject.add(ReceivedNotification(id: id, title: title, body: body, payload: payload));
  });
  var initializationSettings = InitializationSettings(initializationSettingsAndroid, initializationSettingsIOS);
  await flutterLocalNotificationsPlugin.initialize(initializationSettings, onSelectNotification: (String payload) async {
    if (payload != null) {
      debugPrint('notification payload: ' + payload);
    }
    selectNotificationSubject.add(payload);
  });

  /// 沉浸式状态栏
  if (Platform.isAndroid) {
//      SystemChrome.setEnabledSystemUIOverlays([SystemUiOverlay.bottom]);
    SystemUiOverlayStyle systemUiOverlayStyle = SystemUiOverlayStyle(statusBarColor: Colors.transparent);
    SystemChrome.setSystemUIOverlayStyle(systemUiOverlayStyle);
  }
  DatabaseUtils.initArticleTables();
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  static const Map<TargetPlatform, PageTransitionsBuilder> _defaultBuilders = <TargetPlatform, PageTransitionsBuilder>{
    TargetPlatform.android: CupertinoPageTransitionsBuilder(),
    TargetPlatform.iOS: CupertinoPageTransitionsBuilder(),
  };

  void initFontSize() {}
  Future<void> _cancelNotification() async {
    await flutterLocalNotificationsPlugin.cancel(1);
  }

  Future<void> _showOngoingNotification({String musicTitle, String time, String prefix}) async {
//    var androidPlatformChannelSpecifics =
//        AndroidNotificationDetails('your channel id', 'your channel name', 'your channel description', importance: Importance.Max, priority: Priority.High, ticker: 'ticker');
//    var iOSPlatformChannelSpecifics = IOSNotificationDetails();
//    var platformChannelSpecifics = NotificationDetails(androidPlatformChannelSpecifics, iOSPlatformChannelSpecifics);
//    await flutterLocalNotificationsPlugin.show(0, 'plain title', 'plain body', platformChannelSpecifics, payload: 'item x');

    var messages = List<Message>();
    var lunchBot = Person(name: prefix, key: 'bot', bot: true);
    messages.add(Message('《' + musicTitle + '》' + time, DateTime.now().add(Duration(minutes: 10)), lunchBot));
    var messagingStyle = MessagingStyleInformation(lunchBot, groupConversation: true, conversationTitle: '禅修', htmlFormatContent: true, htmlFormatTitle: true, messages: messages);

    var androidPlatformChannelSpecifics = AndroidNotificationDetails('your channel id', 'your channel name', 'your channel description',
        ticker: '开始播放《' + musicTitle + '》',
        style: AndroidNotificationStyle.Messaging,
        styleInformation: messagingStyle,
        importance: Importance.Max,
        priority: Priority.High,
        ongoing: true,
        onlyAlertOnce: true,
        channelAction: AndroidNotificationChannelAction.Update,
        autoCancel: false);
    var iOSPlatformChannelSpecifics = IOSNotificationDetails();
    var platformChannelSpecifics = NotificationDetails(androidPlatformChannelSpecifics, iOSPlatformChannelSpecifics);
    await flutterLocalNotificationsPlugin.show(1, '禅修', '正在播放···' + musicTitle, platformChannelSpecifics);
  }

  List<Widget> getChildren() {
    List<Widget> list = [];
    ReaderThemeConfig.textSizeMap.forEach((key, textLayoutSize) {
      list.add(TextSizeCalculate(
        fontSizeKey: key,
        fontSize: textLayoutSize.fontSize,
        height: ReaderThemeConfig.fontHeightList[key.substring(key.indexOf('-') + 1)],
      ));
    });
    ReaderThemeConfig.textSizeMap.forEach((key, textLayoutSize) {
      list.add(TextSizeCalculate(
        fontSizeKey: key,
        fontSize: textLayoutSize.fontSize,
      ));
    });
    list.add(AppIndexPage());
    return list;
  }

  MusicBloc musicBloc = MusicBloc();

  @override
  void initState() {
    setDesignWHD(1080, 2160);
    if (SpUtil.getDouble('statusBarHeight') == 0) {
      SpUtil.putDouble('statusBarHeight', ScreenUtil.getInstance().statusBarHeight);
    }
    Map map = SpUtil.getObject('readerGlobalConfig');
    if (map != null) {
      GlobalConfiguration().loadFromMap(map);
    } else {
      Map userMap = SpUtil.getObject('user');
      if (userMap == null) {
        userMap = User().toJson();
      }
      // 加载用户信息
      GlobalConfiguration().loadFromMap(userMap);
      // 加载阅读器配置信息
      GlobalConfiguration().loadFromMap(ReadConfig().toJson());
    }
    GlobalConfiguration().updateValue('statusBarHeight', SpUtil.getDouble('statusBarHeight'));
    GlobalConfiguration().updateValue('viewWidth', ScreenUtil.getInstance().screenWidth - ScreenUtil.getInstance().getWidth(36));
    GlobalConfiguration().updateValue('viewHeight', ScreenUtil.getInstance().screenHeight - SpUtil.getDouble('statusBarHeight') - ScreenUtil.getInstance().getWidth(36));
    if (ReaderThemeConfig.textSizeMap.length == 0) {
      ReaderThemeConfig.fontSizeList.forEach((fontsize) {
        ReaderThemeConfig.fontScaleList.forEach((tag, scale) {
          ReaderThemeConfig.textSizeMap[fontsize.toString() + '-' + tag] = TextLayoutSize(fontSize: (fontsize * scale).floorToDouble());
          ReaderThemeConfig.verticalTextSizeMap[fontsize.toString() + '-' + tag] = TextLayoutSize(fontSize: (fontsize * scale).floorToDouble());
        });
      });
    }
    super.initState();

    selectNotificationSubject.stream.listen((String payload) async {
      musicBloc.dispatch(MusicStopConfirmEvent());
      print('222222');
//      await Navigator.push(
//        context,
//        MaterialPageRoute(builder: (context) => SecondScreen(payload)),
//      );
    });
  }

  showUpdateDialog(BuildContext context, String description, String title, bool justClose) {
    showDialog(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context) {
          return StatefulBuilder(
            builder: (BuildContext context, setState) {
              return BodhiRoadConfirmDialog(
                title: title,
                confirmText: '立即停止',
                cancelText: justClose ? '关 闭' : '继续听',
                justClose: justClose,
                description: Text(description.replaceAll('-', '\n'), textScaleFactor: 1.0, style: TextStyle(fontSize: 14, color: GlobalColorConfig.mainTextColor)),
                onConfirm: () {
                  musicBloc.dispatch(MusicStopEvent());
                  _cancelNotification();
//                  Navigator.pop(context);
                },
              );
            },
          );
        });
  }

  @override
  void dispose() {
    didReceiveLocalNotificationSubject.close();
    selectNotificationSubject.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return OKToast(
        backgroundColor: Colors.white,
        child: MaterialApp(
          color: Colors.white,
          theme: ThemeData(
              pageTransitionsTheme: PageTransitionsTheme(builders: _defaultBuilders),
              primaryIconTheme: IconThemeData(color: GlobalColorConfig.mainTextColor),
              primaryColorLight: Colors.white,
              primaryColor: Color(0xFFF2F2F2),
              fontFamily: 'songti'),
          home: BlocListener(
              bloc: musicBloc,
              listener: (context, state) {
                dynamic map = GlobalConfiguration().get('currentMusic');
                if (state is MusicLoadedState) {
//            _showOngoingNotification(musicTitle: map.musicTitle.toString(), time: '');
                } else if (state is MusicPauseState) {
                  _showOngoingNotification(musicTitle: map.musicTitle.toString(), time: '', prefix: "已暂停");
//            _cancelNotification();
                } else if (state is MusicPlayingState) {
                  _showOngoingNotification(musicTitle: map.musicTitle.toString(), time: DateUtils.calculateTime(state.position.toInt()), prefix: "正在播放");
                } else if (state is MusicStopConfirmState) {
                  showUpdateDialog(context, "是否停止背景音乐？", "提示信息", false);
                }
              },
              child: BlocProvider<AppThemeBloc>(
                  builder: (BuildContext context) => AppThemeBloc(),
                  child: Stack(
                    children: getChildren(),
                  ))),
          debugShowCheckedModeBanner: false,
          showPerformanceOverlay: false,
          locale: Locale('zh', 'cn'),
          localizationsDelegates: [
            MyLocalizationsDelegates(),
          ],
          supportedLocales: [Locale('zh', 'cn')],
        ));
  }
}

class TextSizeCalculate extends StatefulWidget {
  final String fontSizeKey;
  final double fontSize;
  final double height;
  const TextSizeCalculate({this.fontSizeKey, this.fontSize, this.height});
  @override
  _TextSizeCalculateState createState() => _TextSizeCalculateState();
}

class _TextSizeCalculateState extends State<TextSizeCalculate> {
  WidgetUtil widgetUtil = new WidgetUtil();
  @override
  Widget build(BuildContext context) {
    widgetUtil.asyncPrepare(context, true, (Rect rect) {
      if (widget.height == null) {
        ReaderThemeConfig.textSizeMap[widget.fontSizeKey].heightVertical = rect.height / 10;
        print(widget.fontSize.toString() + '-------------' + rect.height.toString());
        print(rect.height.toString());
      } else {
        ReaderThemeConfig.textSizeMap[widget.fontSizeKey].width = rect.width / 10;
        ReaderThemeConfig.textSizeMap[widget.fontSizeKey].heightHorizontal = rect.height / 10;
      }
    });
    return ExtendedText.rich(
      TextSpan(
        children: [
          TextSpan(
              text: widget.height != null
                  ? '\u200B中中中中中中中中中中\n\u200B中中中中中中中中中中\n\u200B中中中中中中中中中中\n\u200B中中中中中中中中中中\n\u200B中中中中中中中中中中\n\u200B中中中中中中中中中中\n\u200B中中中中中中中中中中\n\u200B中中中中中中中中中中\n\u200B中中中中中中中中中中\n\u200B中中中中中中中中中中'
                  : '中中中中中中中中中中\n中中中中中中中中中中\n中中中中中中中中中中\n中中中中中中中中中中\n中中中中中中中中中中\n中中中中中中中中中中\n中中中中中中中中中中\n中中中中中中中中中中\n中中中中中中中中中中\n中中中中中中中中中中',
              style: TextStyle(
                  fontSize: widget.fontSize, fontWeight: FontWeight.normal, letterSpacing: 0, height: widget.height ?? 1.2, fontFamily: 'songti', color: Colors.red, decoration: TextDecoration.none))
        ],
      ),
    );
  }
}
