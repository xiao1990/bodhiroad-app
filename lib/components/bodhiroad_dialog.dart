import 'dart:ui';

import 'package:bodhiroad/config/GlobalColorCOnfig.dart';
import 'package:bodhiroad/theme/theme_bloc.dart';
import 'package:bodhiroad/theme/theme_state.dart';
import 'package:flustars/flustars.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';

class BodhiRoadDialog extends StatefulWidget {
  final Text title;
  final Text description;
  final Text unit;
  final onConfirm;
  BodhiRoadDialog({this.title, this.description, this.onConfirm, this.unit});
  @override
  _BodhiRoadDialogState createState() => _BodhiRoadDialogState();
}

class _BodhiRoadDialogState extends State<BodhiRoadDialog> {
  TextEditingController _textEditingController = TextEditingController();
  final GlobalKey<FormBuilderState> _fbKey = GlobalKey<FormBuilderState>();
  @override
  Widget build(BuildContext context) {
    return BlocBuilder(
      bloc: AppThemeBloc(),
      builder: (context, ThemeChangeState appThemeState) {
        return Scaffold(
            backgroundColor: Colors.transparent,
            body: Container(
              child: Center(
                child: Container(
                    width: ScreenUtil.getInstance().getWidth(840),
                    height: ScreenUtil.getInstance().getWidth(600),
                    decoration: BoxDecoration(
                        color: appThemeState.themeConfig.sectionBgColor,
                        borderRadius: BorderRadius.circular(12),
                        boxShadow: [
                          BoxShadow(
                              color: appThemeState.themeConfig.shadowColor,
                              blurRadius: 30)
                        ],
                        image: DecorationImage(
                            fit: BoxFit.fitWidth,
                            alignment: Alignment.bottomCenter,
                            image: AssetImage('assets/texture/ss10.png'))),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Container(
                          height: ScreenUtil.getInstance().getWidth(120),
                          child: widget.title,
                          alignment: Alignment.center,
                          decoration: BoxDecoration(
                              border: Border(
                                  bottom: BorderSide(
                                      color: appThemeState
                                          .themeConfig.lightBorderColor,
                                      width: 0.5))),
                        ),
                        Container(
                          height: ScreenUtil.getInstance().getWidth(120),
                          alignment: Alignment.center,
                          child: widget.description,
                        ),
                        Container(
                            height: ScreenUtil.getInstance().getWidth(210),
                            padding: EdgeInsets.symmetric(
                                vertical: 0, horizontal: 20),
                            child: FormBuilder(
                                key: _fbKey,
                                child: FormBuilderTextField(
                                  keyboardType: TextInputType.number,
                                  controller: _textEditingController,
                                  attribute: 'taskCount',
                                  cursorColor:
                                      appThemeState.themeConfig.mainTextColor,
                                  validators: [
                                    FormBuilderValidators.required(
                                        errorText: '请输入报数数目'),
                                    FormBuilderValidators.numeric(
                                        errorText: '请输入数字类型'),
                                    (val) {
                                      if (int.parse(val) < 0) {
                                        return "必须是正整数";
                                      }
                                    },
                                  ],
                                  decoration: InputDecoration(
                                    contentPadding:
                                        EdgeInsets.symmetric(horizontal: 6),
                                    filled: true,
                                    // hintText: '请输入报数数目,只能是正整数',
                                    alignLabelWithHint: false,
                                    hasFloatingPlaceholder: false,
                                    labelText: '请输入报数数目,只能是正整数',
                                    suffix: widget.unit,
                                    prefixIcon: Icon(
                                      Icons.edit,
                                      color: Colors.black54,
                                    ),
                                    isDense: true,
                                    fillColor: appThemeState
                                        .themeConfig.mainBgColor
                                        .withOpacity(0.4),
                                    hoverColor: Colors.white,
                                    enabledBorder: UnderlineInputBorder(
                                        borderSide: BorderSide.none,
                                        borderRadius: BorderRadius.circular(6)),
                                    focusedBorder: OutlineInputBorder(
                                        borderSide: BorderSide.none,
                                        borderRadius: BorderRadius.circular(6)),
                                  ),
                                ))),
                        Container(
                            height: ScreenUtil.getInstance().getWidth(150),
                            decoration: BoxDecoration(
                                border: Border(
                                    top: BorderSide(
                                        color: appThemeState
                                            .themeConfig.lightBorderColor,
                                        width: 0.5))),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Expanded(
                                  child: ButtonTheme(
                                    minWidth: 90,
                                    height:
                                        ScreenUtil.getInstance().getWidth(150),
                                    buttonColor: Color(0xFFF2F2F2),
                                    child: FlatButton(
                                      child: Text(
                                        '取消',
                                        textScaleFactor: 1.0,
                                        style: TextStyle(
                                            fontSize: ScreenUtil.getInstance()
                                                .getSp(42),
                                            color: appThemeState
                                                .themeConfig.mainTextColor,
                                            fontWeight: FontWeight.bold),
                                      ),
                                      onPressed: () {
                                        Navigator.pop(context);
                                      },
                                    ),
                                  ),
                                ),
                                Container(
                                  height:
                                      ScreenUtil.getInstance().getWidth(150),
                                  width: 0.5,
                                  color: appThemeState
                                      .themeConfig.lightBorderColor,
                                ),
                                Expanded(
                                  child: ButtonTheme(
                                    minWidth: 90,
                                    height:
                                        ScreenUtil.getInstance().getWidth(150),
                                    buttonColor: GlobalColorConfig.mainRedColor,
                                    child: FlatButton(
                                      child: Text(
                                        '报数',
                                        textScaleFactor: 1.0,
                                        style: TextStyle(
                                            fontSize: ScreenUtil.getInstance()
                                                .getSp(42),
                                            color: appThemeState.themeConfig
                                                .buttonActiveTextColor,
                                            fontWeight: FontWeight.bold),
                                      ),
                                      onPressed: () {
                                        _fbKey.currentState.save();
                                        print(_fbKey.currentState.value);
                                        if (_fbKey.currentState.validate()) {
                                          widget.onConfirm(
                                              _textEditingController.text);
                                        }
                                      },
                                    ),
                                  ),
                                )
                              ],
                            ))
                      ],
                    )),
              ),
            ));
      },
    );
  }
}
