abstract class SukhavatiSetEvent {
  SukhavatiSetEvent([props = const []]) : super();
}

class SukhavatiInitEvent extends SukhavatiSetEvent {
  final int count;
  SukhavatiInitEvent({this.count}) : super([count]);
  @override
  String toString() => 'SukhavatiInitEvent';
}

class SukhavatiCountEvent extends SukhavatiSetEvent {
  int count;
  SukhavatiCountEvent({this.count}) : super([count]);
  @override
  String toString() => 'SukhavatiCountEvent';
}

class SukhavatiFrequencyEvent extends SukhavatiSetEvent {
  final double frequency;
  SukhavatiFrequencyEvent({this.frequency}) : super([frequency]);
  @override
  String toString() => 'SukhavatiFrequencyEvent';
}

class SukhavatiFrequencyPreviewEvent extends SukhavatiSetEvent {
  final double frequency;
  SukhavatiFrequencyPreviewEvent({this.frequency}) : super([frequency]);
  @override
  String toString() => 'SukhavatiFrequencyEvent';
}

class SukhavatiFrequencyPreviewStopEvent extends SukhavatiSetEvent {
  SukhavatiFrequencyPreviewStopEvent() : super();
  @override
  String toString() => 'SukhavatiFrequencyEvent';
}

class SukhavatiFrequencyPreviewFinishEvent extends SukhavatiSetEvent {
  SukhavatiFrequencyPreviewFinishEvent() : super();
  @override
  String toString() => 'SukhavatiFrequencyEvent';
}

class SukhavatiVolumnEvent extends SukhavatiSetEvent {
  final double volume;
  SukhavatiVolumnEvent({this.volume = 0.2}) : super([volume]);
  @override
  String toString() => 'SukhavatiVolumnEvent';
}

class SukhavatiStartEvent extends SukhavatiSetEvent {
  final int interval;
  final int count;
  final int taskId;
  SukhavatiStartEvent({this.interval = 500, this.count = 10, this.taskId}) : super([interval, count, taskId]);
  @override
  String toString() => 'SukhavatiStartEvent';
}

class SukhavatiContinueEvent extends SukhavatiSetEvent {
  final int interval;
  final int count;
  final int taskId;
  SukhavatiContinueEvent({this.interval = 500, this.count = 10, this.taskId}) : super([interval, count, taskId]);
  @override
  String toString() => 'SukhavatiContinueEvent';
}

class SukhavatiPauseEvent extends SukhavatiSetEvent {
  final int reduceCount;
  final int taskId;
  SukhavatiPauseEvent({this.reduceCount, this.taskId}) : super([reduceCount, taskId]);
  @override
  String toString() => 'SukhavatiPauseEvent';
}

class SukhavatiStopEvent extends SukhavatiSetEvent {
  final int reduceCount;
  final int taskId;
  SukhavatiStopEvent({this.reduceCount, this.taskId}) : super([reduceCount, taskId]);
  @override
  String toString() => 'SukhavatiStopEvent';
}

class SukhavatiUnderwayEvent extends SukhavatiSetEvent {
  final int count;
  SukhavatiUnderwayEvent({this.count}) : super([count]);
  @override
  String toString() => 'SukhavatiUnderwayEvent';
}

class SukhavatiFinishEvent extends SukhavatiSetEvent {
  final int taskId;
  SukhavatiFinishEvent({this.taskId}) : super([taskId]);
  @override
  String toString() => 'SukhavatiFinishEvent';
}

class SukhavatiGoingEvent extends SukhavatiSetEvent {
  @override
  String toString() => 'SukhavatiGoingEvent';
}
