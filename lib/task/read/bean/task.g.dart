// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'task.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Task _$TaskFromJson(Map<String, dynamic> json) {
  return Task(
      id: json['id'] as int,
      userId: json['userId'] as int,
      taskId: json['taskId'] as int,
      taskName: json['taskName'] as String,
      taskTypeId: json['taskTypeId'] as int,
      taskTypeName: json['taskTypeName'] as String,
      taskContentUrl: json['taskContentUrl'] as String,
      originalId: json['originalId'] as String,
      description: json['description'] as String,
      author: json['author'] as String,
      count: json['count'] as int,
      totalCount: json['totalCount'] as int,
      state: json['state'] as int,
      times: json['times'] as int,
      version: json['version'] as int);
}

Map<String, dynamic> _$TaskToJson(Task instance) => <String, dynamic>{
      'id': instance.id,
      'userId': instance.userId,
      'taskId': instance.taskId,
      'taskName': instance.taskName,
      'taskTypeId': instance.taskTypeId,
      'taskTypeName': instance.taskTypeName,
      'taskContentUrl': instance.taskContentUrl,
      'state': instance.state,
      'originalId': instance.originalId,
      'description': instance.description,
      'author': instance.author,
      'count': instance.count,
      'totalCount': instance.totalCount,
      'times': instance.times,
      'version': instance.version
    };
