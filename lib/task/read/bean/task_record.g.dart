// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'task_record.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

TaskRecord _$TaskRecordFromJson(Map<String, dynamic> json) {
  return TaskRecord(
      id: json['id'] as int,
      taskId: json['taskId'] as int,
      taskCount: json['taskCount'] as int,
      taskType: json['taskType'] as int,
      state: json['state'] as int,
      source: json['source'] as int);
}

Map<String, dynamic> _$TaskRecordToJson(TaskRecord instance) =>
    <String, dynamic>{
      'id': instance.id,
      'taskId': instance.taskId,
      'taskCount': instance.taskCount,
      'taskType': instance.taskType,
      'state': instance.state,
      'source': instance.source
    };
