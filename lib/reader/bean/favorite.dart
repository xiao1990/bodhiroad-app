import 'package:json_annotation/json_annotation.dart';
part 'favorite.g.dart';

@JsonSerializable()
class Favorite {
  int id;
  String buddhistId;
  String title;
  String url;
  String author;
  int type;

  Favorite({this.id, this.buddhistId, this.title, this.url, this.author, this.type});
  factory Favorite.fromJson(Map<String, dynamic> json) {
    return _$FavoriteFromJson(json);
  }
  Map<String, dynamic> toJson() => _$FavoriteToJson(this);
}
